# $Id$
# 
# Dependency management for projects using OpenVR
# 
# Copyright (c) 2021 Emergent Space Technologies, Inc.
#
# Downloads and manages OpenVR
#
# This approach uses FetchContent instead of ExternalProject because the latter
# does all work at build-time, which is incompatible with FIND_PACKAGE when
# later searching for OSG, OpenFrames, etc. In contrast, FetchContent downloads
# at configure-time.
# Build & install steps are also done immediately so that libraries will be
# found by FIND_PACKAGE during search.
#
# Original Author: Ravi Mathur
#
# DO NOT MODIFY THIS FILE UNLESS YOU KNOW WHAT YOU ARE DOING!

###############################################################################
# OpenVR (currently only on Windows)
###############################################################################

SET(OpenVR_SRC "${DEPENDENCY_PATH}/OpenVR-git")
SET(OpenVR_ROOT_DIR "${OpenVR_SRC}" CACHE PATH "Path to OpenVR SDK directory (e.g. the Git clone)")

IF(WIN32)
  # Set OpenVR default values
  SET(OpenVR_GITTAG "5aa6c5f0")
  SET(OPENFRAMESINTERFACE_OVERRIDE_OPENVR_GITTAG "" CACHE STRING "(Optional) Override the default OpenVR Git tag. Leave empty to use the default.")
  IF(OPENFRAMESINTERFACE_OVERRIDE_OPENVR_GITTAG)
    SET(OpenVR_GITTAG ${OPENFRAMESINTERFACE_OVERRIDE_OPENVR_GITTAG})
  ENDIF()

  # Download OpenVR (comes prebuilt)
  MESSAGE(STATUS "Downloading OpenVR [${OpenVR_GITTAG}]...")
  FetchContent_Populate(openvr
    GIT_REPOSITORY https://github.com/ValveSoftware/openvr.git
    GIT_TAG ${OpenVR_GITTAG}
    SOURCE_DIR ${OpenVR_SRC}
    BINARY_DIR ${DEPENDENCY_BUILDPATH}/openvr
    SUBBUILD_DIR ${DEPENDENCY_BUILDPATH}/openvr-subbuild
    QUIET
  )
ENDIF()

###############################################################################
# Initialize CMake variables
###############################################################################

FIND_PACKAGE(OpenVR QUIET)

###############################################################################
# Install libraries
###############################################################################

IF(OpenVR_FOUND)
  IF(WIN32)
    SET(OpenVR_LIB64_DIR "${DEPEND_LIB_DIR}/win64")
  ELSEIF(APPLE)
    SET(OpenVR_LIB64_DIR "${DEPEND_LIB_DIR}/osx32") # OpenVR OSX binaries are 64-bit
  ELSE()
    SET(OpenVR_LIB64_DIR "${DEPEND_LIB_DIR}/linux64")
  ENDIF()

  INSTALL(DIRECTORY "${OpenVR_ROOT_DIR}/${OpenVR_LIB64_DIR}/"
    DESTINATION ${DEPEND_INSTALL_DIR}
    PATTERN "*.pdb" EXCLUDE
  )
  IF(OPENFRAMESINTERFACE_INSTALL_PREFIX)
    INSTALL(DIRECTORY "${OpenVR_ROOT_DIR}/${OpenVR_LIB64_DIR}/"
      DESTINATION "${OPENFRAMESINTERFACE_INSTALL_PREFIX}/${DEPEND_INSTALL_DIR}"
      PATTERN "*.pdb" EXCLUDE
    )
  ENDIF()
ENDIF()
