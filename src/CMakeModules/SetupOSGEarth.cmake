# $Id$
# 
# Dependency management for projects using osgEarth
# 
# Copyright (c) 2021 Emergent Space Technologies, Inc.
#
# Downloads and manages osgEarth
#
# This approach uses FetchContent instead of ExternalProject because the latter
# does all work at build-time, which is incompatible with FIND_PACKAGE when
# later searching for OSG, OpenFrames, etc. In contrast, FetchContent downloads
# at configure-time.
# Build & install steps are also done immediately so that libraries will be
# found by FIND_PACKAGE during search.
#
# Original Author: Ravi Mathur
#
# DO NOT MODIFY THIS FILE UNLESS YOU KNOW WHAT YOU ARE DOING!

###############################################################################
# Download and setup osgEarth
###############################################################################

# OFI doesn't yet support osgEarth on OSX
IF(APPLE)
  return()
ENDIF()

# Windows requires OSG 3rd-party libs
# Linux requires dependencies to be installed in system
IF(WIN32 AND NOT OSG_3RDPARTY_DIR)
  return()
ENDIF()

# Make osgEarth support optional
OPTION(OPENFRAMESINTERFACE_USE_OSGEARTH "Enable osgEarth support" ON)
IF(NOT OPENFRAMESINTERFACE_USE_OSGEARTH)
  return()
ENDIF()

SET(OSGEARTH_SRC "${DEPENDENCY_PATH}/osgEarth-git")
SET(OSGEARTH_DIR "${OSGEARTH_SRC}/installed" CACHE PATH "Path to osgEarth install directory")

# Set osgEarth build defaults
SET(OSGEARTH_GITTAG "f7a922ba")
SET(OPENFRAMESINTERFACE_OVERRIDE_OSGEARTH_GITTAG "" CACHE STRING "(Optional) Override the default osgEarth Git tag. Leave empty to use the default.")
IF(OPENFRAMESINTERFACE_OVERRIDE_OSGEARTH_GITTAG)
  SET(OSGEARTH_GITTAG ${OPENFRAMESINTERFACE_OVERRIDE_OSGEARTH_GITTAG})
ENDIF()

# OS-specific build options
IF(WIN32)
  SET(OSGEARTH_CONFIG_OPTIONS
    -DTHIRD_PARTY_DIR:PATH=${OSG_3RDPARTY_DIR}
    -DWIN32_USE_MP:BOOL=ON
  )
ELSEIF(APPLE)
  SET(OSGEARTH_CONFIG_OPTIONS
    -DCMAKE_MACOSX_RPATH:BOOL=ON
  )
ELSE() # Linux
  SET(OSGEARTH_CONFIG_OPTIONS
  )
ENDIF()

MESSAGE(STATUS "Setting up osgEarth [${OSGEARTH_GITTAG}], this could take a while...")

# Get repo commit before downloading
GetGitCommit(${OSGEARTH_SRC} COMMIT_BEFORE)

# Download osgEarth
FetchContent_Populate(osgearth
  GIT_REPOSITORY https://github.com/gwaldron/osgearth.git
  GIT_TAG ${OSGEARTH_GITTAG}
  SOURCE_DIR ${OSGEARTH_SRC}
  BINARY_DIR ${DEPENDENCY_BUILDPATH}/oe
  SUBBUILD_DIR ${DEPENDENCY_BUILDPATH}/oe-subbuild
  QUIET
)

# Get repo commit after downloading
GetGitCommit(${OSGEARTH_SRC} COMMIT_AFTER)

# Update osgEarth if repo changed or it hasn't been built yet
IF(NOT (COMMIT_BEFORE STREQUAL COMMIT_AFTER)
   OR NOT EXISTS ${OSGEARTH_DIR})

  # osgEarth searches for SilverLining in default install locations, so we force it to
  # use our FindSilverLining module instead
  FILE(COPY "${CMAKE_CURRENT_SOURCE_DIR}/CMakeModules/FindSilverLining.cmake" 
    DESTINATION "${OSGEARTH_SRC}/CMakeModules"
  )

  # Disable SilverLining and Triton environment variables so osgEarth does not build with them
  SET(SILVERLINING_PATH_ORIG $ENV{SILVERLINING_PATH})
  SET(TRITON_PATH_ORIG $ENV{TRITON_PATH})
  UNSET(ENV{SILVERLINING_PATH})
  UNSET(ENV{TRITON_PATH})
    
  # Configure osgEarth
  EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND}
      ${osgearth_SOURCE_DIR}
      ${COMMON_BUILD_OPTIONS}
      ${OSGEARTH_CONFIG_OPTIONS}
      -DBUILD_APPLICATIONS:BOOL=ON
      -DBUILD_OSGEARTH_EXAMPLES:BOOL=OFF
      -DBUILD_TESTS:BOOL=OFF
      -DCMAKE_PREFIX_PATH:PATH=${OSG_DIR} # Force osgEarth to find our OSG even if another one
      -DOSG_DIR:PATH=${OSG_DIR}           # exists in the path environment variable. Needed
                                          # because osgEarth uses a custom OSG search method.
      -DCMAKE_INSTALL_PREFIX:PATH=${OSGEARTH_DIR}
    WORKING_DIRECTORY ${osgearth_BINARY_DIR}
    OUTPUT_QUIET ERROR_QUIET
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/osgEarth_configure.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/osgEarth_configure.log
    RESULT_VARIABLE OSGEARTH_CONFIGURE_RESULT
  )

  # Restore SilverLining and Triton environment variables so other dependencies can use them
  SET(ENV{SILVERLINING_PATH} ${SILVERLINING_PATH_ORIG})
  SET(ENV{TRITON_PATH} ${TRITON_PATH_ORIG})
  UNSET(SILVERLINING_PATH_ORIG)
  UNSET(TRITON_PATH_ORIG)

  IF(NOT (OSGEARTH_CONFIGURE_RESULT EQUAL 0))
    MESSAGE(FATAL_ERROR "osgEarth could not be configured. See details in log file ${DEPENDENCY_LOGPATH}/osgEarth_configure.log")
  ENDIF()

  # Build and install osgEarth
  EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND} --build . --target install --config Release ${UNIX_PARALLEL_OPTION}
    WORKING_DIRECTORY ${osgearth_BINARY_DIR}
    OUTPUT_QUIET ERROR_QUIET
    OUTPUT_FILE ${DEPENDENCY_LOGPATH}/osgEarth_build.log
    ERROR_FILE ${DEPENDENCY_LOGPATH}/osgEarth_build.log
    RESULT_VARIABLE OSGEARTH_BUILD_RESULT
  )

  IF(NOT (OSGEARTH_BUILD_RESULT EQUAL 0))
    FILE(REMOVE_RECURSE "${OSGEARTH_DIR}")
    MESSAGE(FATAL_ERROR "osgEarth did not build. See details in log file ${DEPENDENCY_LOGPATH}/osgEarth_build.log")
  ENDIF()

ENDIF()
###############################################################################
# Initialize CMake variables
###############################################################################

FIND_PACKAGE(OsgEarth)

###############################################################################
# Install libraries
###############################################################################

GetLibDirPath("${OSGEARTH_DIR}/${DEPEND_LIB_DIR}" OSGEARTH_LIB_DIR)
INSTALL(DIRECTORY ${OSGEARTH_LIB_DIR}/
  DESTINATION ${DEPEND_INSTALL_DIR}
  PATTERN "osgearth_*" EXCLUDE   # We will install osgEarth applications ourselves
)
IF(OPENFRAMESINTERFACE_INSTALL_PREFIX)
  INSTALL(DIRECTORY ${OSGEARTH_LIB_DIR}/
    DESTINATION "${OPENFRAMESINTERFACE_INSTALL_PREFIX}/${DEPEND_INSTALL_DIR}"
    PATTERN "osgearth_*" EXCLUDE
  )
ENDIF()

# Install osgEarth applications
SET(OSGEARTH_APP_PATTERNS PATTERN "osgearth_conv*" PATTERN "osgearth_viewer*")
INSTALL(DIRECTORY ${OSGEARTH_DIR}/bin/
  DESTINATION bin
  USE_SOURCE_PERMISSIONS
  FILES_MATCHING ${OSGEARTH_APP_PATTERNS}
)
IF(OPENFRAMESINTERFACE_INSTALL_PREFIX)
  INSTALL(DIRECTORY ${OSGEARTH_DIR}/bin/
    DESTINATION "${OPENFRAMESINTERFACE_INSTALL_PREFIX}/bin"
    USE_SOURCE_PERMISSIONS
    FILES_MATCHING ${OSGEARTH_APP_PATTERNS}
  )
ENDIF()

# Install additional OSG 3rd-party libraries (only applies to Windows)
IF(WIN32)
  SET(OSGEARTH_3RDPARTY_LIBS
    "${OSG_3RDPARTY_DIR}/bin/gdal201.dll"     # Used by osgEarth
    "${OSG_3RDPARTY_DIR}/bin/libcurl.dll"     # Used by GDAL
    "${OSG_3RDPARTY_DIR}/bin/libeay32.dll"    # Used by ssleay
    "${OSG_3RDPARTY_DIR}/bin/cares.dll"       # Used by curl
    "${OSG_3RDPARTY_DIR}/bin/ssleay32.dll"    # Used by curl
    "${OSG_3RDPARTY_DIR}/bin/tiff.dll"        # Used by osgEarth
    "${OSG_3RDPARTY_DIR}/bin/zlib.dll"        # Used by curl
    "${OFI_DATA_DIR}/bin/proj.dll"            # Used by GDAL
  )
  
  INSTALL(FILES
    ${OSGEARTH_3RDPARTY_LIBS}
    DESTINATION ${DEPEND_INSTALL_DIR}
  )
  IF(OPENFRAMESINTERFACE_INSTALL_PREFIX)
    INSTALL(FILES
      ${OSGEARTH_3RDPARTY_LIBS}
      DESTINATION "${OPENFRAMESINTERFACE_INSTALL_PREFIX}/${DEPEND_INSTALL_DIR}"
    )
  ENDIF()
ENDIF()
