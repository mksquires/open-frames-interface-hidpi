//$Id$
//------------------------------------------------------------------------------
//                                  OFSpaceObject
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  @class OFSpaceObject
 *  Conveniently wraps all OpenFrames objects that implement a single model for
 *  a GMAT spacecraft object.
 */
//------------------------------------------------------------------------------

#ifndef OFSPACEOBJECT_H
#define OFSPACEOBJECT_H

#include "OpenFramesInterface_defs.hpp"
#include "SpacePointOptions.hpp"
#include "RgbColorFloat.hpp"
#include "TrajectoryListener.hpp"

#include <string>
#include <osg/ref_ptr>
#include <osg/Referenced>

// Forward declaration of OpenFrames classes
namespace OpenFrames
{
class Trajectory;
class DrawableTrajectory;
class CurveArtist;
class SegmentArtist;
class MarkerArtist;
class TrajectoryFollower;
class ReferenceFrame;
class LatLonGrid;
class RadialPlane;
}
class OFSegment;
class SpacePointOptions;
class RgbColorFloat;


class OpenFramesInterface_API OFSpaceObject : public TrajectoryListener
{
public:
   OFSpaceObject(const SpacePointOptions &options, const std::string &frameName, bool isSpacecraft,
                 Integer numberOfSolverTrajectories);
   virtual ~OFSpaceObject();

   void ResetState();
   virtual void ResetTrajectories();
   bool AttemptReconfigure(const SpacePointOptions &options, const std::string &frameName, bool isSpacecraft,
                           Integer numberOfSolverTrajectories);
   void Configure();
   void AddPrimaryToParentFrame(OpenFrames::ReferenceFrame *parent);

   const std::string &GetSpacecraftName() const { return mOptions.GetName(); }
   virtual bool IsSpacecraft() const { return mIsSpacecraft; }
   virtual void UpdateColor();
   virtual void AddSegment(const std::string &propName, OpenFrames::Trajectory *trajectory);
   virtual void RemoveSegment(OpenFrames::Trajectory *trajectory);
   virtual bool HasSegment(OpenFrames::Trajectory *trajectory);
   virtual void AddSolution(OpenFrames::Trajectory *trajectory);
   void SetSegmentColor(RgbColorFloat &color);
   void SetDefaultSegmentColor();
   void SetOrbitColor(RgbColorFloat &color);
   void SetTargetColor(RgbColorFloat &color);
   virtual Integer FinalizeSolverTrajectory();
   virtual void RemoveLastSolverTrajectory(Integer numTrajToRemove);
   virtual void RemoveAllSolverTrajectories();
   OpenFrames::ReferenceFrame *FrameOnWholeTrajectory() const;
   OpenFrames::ReferenceFrame *WholeTrajectory() const;
   OpenFrames::ReferenceFrame *TrajectorySegment(const std::string &propName) const;

private:
   enum BodyReferenceFrameType
   {
      OF_REFERENCE_FRAME,
      OF_SPHERE,
      OF_MODEL,
   };

   /// The unique name of this space point
   SpacePointOptions mOptions;
   /// Indicates that the color should be changed
   bool mSetNewColor;
   /// The new color is an override (true) or default (false)
   bool mNewColorOverride;
   /// THe new color to set
   RgbColorFloat mNewColor;
   /// The set of frames and trajectories that represent this spacecraft
   std::vector<osg::ref_ptr<OFSegment>> mSegments;
   /// Drawable trajectory for the spacecraft window
   osg::ref_ptr<OpenFrames::DrawableTrajectory> mDrawTraj;
   /// Reference frame that represents the spacecraft view
   osg::ref_ptr<OpenFrames::ReferenceFrame> mViewRefFrame;
   /// XY Plane in the objects relative frame
   osg::ref_ptr<OpenFrames::RadialPlane> mXYPlane;
   /// An optional grid around the object
   osg::ref_ptr<OpenFrames::LatLonGrid> mLatLonGrid;
   /// Drawable trajectory to hold the spacecraft center marker
   osg::ref_ptr<OpenFrames::DrawableTrajectory> mDrawCenter;
   /// Marker artist to draw the segment start and end
   osg::ref_ptr<OpenFrames::MarkerArtist> mCenterMarker;
   /// Trajectory follower that represents the spacecraft view
   osg::ref_ptr<OpenFrames::TrajectoryFollower> mFollowView;
   /// Collection of solver trajectories
   std::vector<osg::ref_ptr<OpenFrames::Trajectory>> mSolverTrajs;
   /// Collection of artists for the solver trajectories
   std::vector<osg::ref_ptr<OpenFrames::CurveArtist>> mSolverArtists;
   /// Radius of the spacecraft model
   double mRadius;
   /// Filename of the model file used for this object
   std::string mModelFilename;
   /// The number of solver trajectories that this object should keep
   Integer mNumberOfSolverTrajectories;
   /// True if this object is a spacecraft, false otherwise (i.e. celestial body or ground station)
   bool mIsSpacecraft;

   /// Remove old parents and put on new parents
   void ChangeChildFrameOnParents(OpenFrames::ReferenceFrame *frame);

   /// A collection of the views that this objects primaries have been added to
   std::vector<osg::ref_ptr<OpenFrames::ReferenceFrame>> mPrimaryParents;

   /// Reserve the copy constructor
   OFSpaceObject(const OFSpaceObject &source) = delete;
   /// Reserve the assignment operator
   OFSpaceObject& operator=(const OFSpaceObject &rhs) = delete;
};

#endif
