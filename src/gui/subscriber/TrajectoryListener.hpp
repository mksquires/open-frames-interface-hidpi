//$Id$
//------------------------------------------------------------------------------
//                                  TrajectoryListener
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  @class TrajectoryListener
 *  Conveniently wraps all OpenFrames objects that implement a single model for
 *  a GMAT spacecraft object.
 */
//------------------------------------------------------------------------------

#ifndef TRAJECTORYLISTENER_H
#define TRAJECTORYLISTENER_H

#include "OpenFramesInterface_defs.hpp"

#include <string>
#include <osg/ref_ptr>
#include <osg/Referenced>

// Forward declaration of OpenFrames classes
namespace OpenFrames
{
class Trajectory;
}


class OpenFramesInterface_API TrajectoryListener : public osg::Referenced
{
public:
   TrajectoryListener();
   virtual ~TrajectoryListener();

   /// Reserve the copy constructor
   TrajectoryListener(const TrajectoryListener &source) = delete;
   /// Reserve the assignment operator
   TrajectoryListener& operator=(const TrajectoryListener &rhs) = delete;

   virtual void ResetTrajectories() = 0;
   virtual bool IsNameSameAs(const std::string &name) const { return mObjectName == name; };
   virtual bool IsFrameSameAs(const std::string &frameName) const { return mFrameName == frameName; };
   virtual bool IsNameSameAs(const TrajectoryListener &listener) const { return mObjectName == listener.mObjectName; };
   virtual bool IsFrameSameAs(const TrajectoryListener &listener) const { return mFrameName == listener.mFrameName; };
   virtual const std::string &GetObjectName() const { return mObjectName; }
   virtual const std::string &GetFrameName() const { return mFrameName; }
   virtual bool IsSpacecraft() const = 0;
   virtual void UpdateColor() = 0;
   virtual void AddSegment(const std::string &propName, OpenFrames::Trajectory *trajectory) = 0;
   virtual void RemoveSegment(OpenFrames::Trajectory *trajectory) = 0;
   virtual void AddSolution(OpenFrames::Trajectory *trajectory) = 0;
   virtual bool HasSegment(OpenFrames::Trajectory *trajectory) = 0;
   virtual Integer FinalizeSolverTrajectory() = 0;
   virtual void RemoveLastSolverTrajectory(Integer numTrajToRemove) = 0;
   virtual void RemoveAllSolverTrajectories() = 0;

protected:
   virtual void SetObjectName(const std::string &name) { mObjectName = name; }
   virtual void SetFrameName(const std::string &name) { mFrameName = name; }

private:
   /// The name of the object
   std::string mObjectName;
   /// The name of the frame that the trajectories are in
   std::string mFrameName;
};

#endif
