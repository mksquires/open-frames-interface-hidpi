//$Id$
//------------------------------------------------------------------------------
//                                  OFSpacecraftFrame
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  @class OFSpacecraftFrame
 *  Conveniently wraps all OpenFrames objects that implement a single Gmat space
 *  object
 */
//------------------------------------------------------------------------------

#ifndef OFSEGMENT_H
#define OFSEGMENT_H

#include "OpenFramesInterface_defs.hpp"
#include "RgbColorFloat.hpp"

#include <string>
#include <osg/ref_ptr>
#include <osg/Referenced>

// Forward declaration of OpenFrames classes
namespace OpenFrames
{
class Trajectory;
class DrawableTrajectory;
class CurveArtist;
class SegmentArtist;
class MarkerArtist;
class TrajectoryFollower;
class ReferenceFrame;
}
class SpacePointOptions;


class OpenFramesInterface_API OFSegment : public osg::Referenced
{
public:
   OFSegment(const std::string &propName, const SpacePointOptions &options,
             OpenFrames::DrawableTrajectory *drawableTrajectory, double radius,
             OpenFrames::Trajectory *trajectory);
   virtual ~OFSegment();

   void Configure(const SpacePointOptions &options);
   void SetOrbitColor(RgbColorFloat &color, bool overrideColor);

   const std::string &GetPropagatorName() const { return mPropagatorName; }
   OpenFrames::Trajectory &Trajectory() { return *mTrajectory; }
   OpenFrames::ReferenceFrame &Frame() { return *mFrame; }
   OpenFrames::TrajectoryFollower &Follower() { return *mTrajectoryFollower; }
   OpenFrames::CurveArtist *CurveArtist() { return mCurveArtist; }

private:
   /// The name of the propagator that generated this data
   std::string mPropagatorName;
   /// The drawable trajectory
   osg::ref_ptr<OpenFrames::DrawableTrajectory> mDrawableTraj;
   /// Trajectory data
   osg::ref_ptr<OpenFrames::Trajectory> mTrajectory;
   /// CurveArtist for the trajectory, which by default uses x/y/z positions from the trajectory for plotting
   osg::ref_ptr<OpenFrames::CurveArtist> mCurveArtist;
   /// SegmentArtist for velocity vectors
   osg::ref_ptr<OpenFrames::SegmentArtist> mSegmentArtist;
   /// Marker artist to draw the segment start and end
   osg::ref_ptr<OpenFrames::MarkerArtist> mEndMarker;
   /// Body frame of this spacepoint
   osg::ref_ptr<OpenFrames::ReferenceFrame> mFrame;
   /// Tell model to follow trajectory (by default in LOOP mode)
   osg::ref_ptr<OpenFrames::TrajectoryFollower> mTrajectoryFollower;

   /// Set to true if this color is an override of the default color
   bool mOverrideColor;

   /// Reserve the copy constructor
   OFSegment(const OFSegment &source) = delete;
   /// Reserve the assignment operator
   OFSegment& operator=(const OFSegment &rhs) = delete;
};

#endif
