//$Id$
//------------------------------------------------------------------------------
//                                  OFStarsDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
//------------------------------------------------------------------------------
/**
 * Dialog to configure OpenFramesInterface stars options
 */
//------------------------------------------------------------------------------

#include "OFStarsDialog.hpp"
#include "OpenFramesInterface.hpp"

#include "MessageInterface.hpp"
#include "gmatwxdefs.hpp"

//------------------------------
// event tables for wxWindows
//------------------------------
BEGIN_EVENT_TABLE(OFStarsDialog, wxDialog)
   EVT_BUTTON(ID_BUTTON_OK, OFStarsDialog::OnOK)
   EVT_BUTTON(ID_BUTTON_CANCEL, OFStarsDialog::OnCancel)
   EVT_BUTTON(ID_BUTTON_HELP, OFStarsDialog::OnHelp)
   EVT_CLOSE(OFStarsDialog::OnSystemClose)

   EVT_TEXT(ID_TEXTCTRL, OFStarsDialog::OnTextChange)
   EVT_RADIOBUTTON(ID_RADIOBUTTON, OFStarsDialog::OnRadioChange)
END_EVENT_TABLE()


const wxString OFStarsDialog::DIALOG_TITLE = "Star Options";


//------------------------------------------------------------------------------
// OFStarsDialog(wxWindow *parent, const wxString &subscriberName)
//------------------------------------------------------------------------------
/**
 * Constructs OFStarsDialog object.
 *
 * @param <forObject> The object to configure star options for
 * @param <parent> input parent.
 *
 * @note Creates the OFStarsDialog GUI
 */
//------------------------------------------------------------------------------
OFStarsDialog::OFStarsDialog(OpenFramesInterface *forObject, wxWindow *parent) :
    wxDialog(parent, -1, DIALOG_TITLE, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxMAXIMIZE_BOX),
    mOFInterface(forObject),
    mHasCustomDataChanged(false),
    mHasPresetDataChanged(false),
    mDataChanged(false),
    mCanClose(true)
{
   InitializeData();
   Create();
   Show();

   // shortcut keys
   wxAcceleratorEntry entries[2];
   entries[0].Set(wxACCEL_NORMAL, WXK_F1, ID_BUTTON_HELP);
   entries[1].Set(wxACCEL_CTRL, static_cast<int>('W'), ID_BUTTON_CANCEL);
   wxAcceleratorTable accel(2, entries);
   this->SetAcceleratorTable(accel);
}


//------------------------------------------------------------------------------
// ~OFStarsDialog()
//------------------------------------------------------------------------------
/**
 * Destructor
 */
OFStarsDialog::~OFStarsDialog()
{
}


//------------------------------------------------------------------------------
// void InitializeData()
//------------------------------------------------------------------------------
/**
 * Initializes data that is not wxWidgets
 */
void OFStarsDialog::InitializeData()
{
   mHasCustomDataChanged = false;
   mHasPresetDataChanged = false;
   mDataChanged = false;
   mCanClose = true;
}


//------------------------------------------------------------------------------
// void Create()
//------------------------------------------------------------------------------
/**
 * Creates wxWidgets and adds them to the panel
 */
void OFStarsDialog::Create()
{
   int bsize = 2; // border size

   // create axis array
   wxArrayString emptyList;

   //-----------------------------------------------------------------
   // Main sizers and buttons
   //-----------------------------------------------------------------
   mPanelSizer = new wxBoxSizer(wxVERTICAL);
   mMiddleSizer = (wxSizer*)(new wxStaticBoxSizer(wxVERTICAL, this));
   mBottomSizer = new wxStaticBoxSizer(wxVERTICAL, this );
   wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

   // create bottom buttons
   mOkButton = new wxButton(this, ID_BUTTON_OK, "OK", wxDefaultPosition, wxDefaultSize, 0);
   mOkButton->SetToolTip("Apply Changes and Close");
   mCancelButton = new wxButton(this, ID_BUTTON_CANCEL, "Cancel", wxDefaultPosition, wxDefaultSize, 0);
   mCancelButton->SetToolTip("Close Without Applying Changes");
   mHelpButton = new wxButton(this, ID_BUTTON_HELP, GUI_ACCEL_KEY"Help", wxDefaultPosition, wxDefaultSize, 0);
   mHelpButton->SetToolTip("OpenFramesInterface Help (F1)");
   mOkButton->SetDefault();

   buttonSizer->Add(mOkButton, 0, wxALIGN_CENTER | wxALL, bsize);
   buttonSizer->Add(mCancelButton, 0, wxALIGN_CENTER | wxALL, bsize);
   buttonSizer->Add(0, 1, wxALIGN_RIGHT);
   buttonSizer->Add(mHelpButton, 0, wxALIGN_RIGHT | wxALL, bsize);

   mBottomSizer->Add(buttonSizer, 0, wxGROW | wxALL, bsize);

   //-----------------------------------------------------------------
   // Data collect and update frequency
   //-----------------------------------------------------------------
   mPresetMonitorRadio = new wxRadioButton(this, ID_RADIOBUTTON,
      OpenFramesInterface::STAR_SETTING_STRINGS[OpenFramesInterface::STARS_MONITOR], wxDefaultPosition, wxDefaultSize,
      wxRB_GROUP);
   mPresetVRRadio = new wxRadioButton(this, ID_RADIOBUTTON,
      OpenFramesInterface::STAR_SETTING_STRINGS[OpenFramesInterface::STARS_VR], wxDefaultPosition, wxDefaultSize, 0U);
   mPresetCustomRadio = new wxRadioButton(this, ID_RADIOBUTTON,
      OpenFramesInterface::STAR_SETTING_STRINGS[OpenFramesInterface::STARS_CUSTOM], wxDefaultPosition, wxDefaultSize,
      0U);
   wxStaticText *starCountStaticText = new wxStaticText(this, -1, wxT("Number of stars to plot "), wxDefaultPosition, wxSize(-1,-1), 0U);
   mStarCountTextCtrl = new wxSpinCtrl(this, ID_TEXTCTRL, wxT("7000"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT);
   mStarCountTextCtrl->SetRange(0, 10000000);
   wxStaticText *minStarMagStaticText = new wxStaticText(this, -1, wxT("Minimum star magnitude in catalog "), wxDefaultPosition, wxSize(-1,-1), 0U);
   mMinStarMagTextCtrl = new wxSpinCtrlDouble(this, ID_TEXTCTRL, wxT("6.0"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT);
   mMinStarMagTextCtrl->SetRange(-2.0, 20.0);
   mMinStarMagTextCtrl->SetDigits(4U);
   mMinStarMagTextCtrl->SetIncrement(0.5);
   wxStaticText *maxStarMagStaticText = new wxStaticText(this, -1, wxT("Maximum star magnitude in catalog "), wxDefaultPosition, wxSize(-1,-1), 0U);
   mMaxStarMagTextCtrl = new wxSpinCtrlDouble(this, ID_TEXTCTRL, wxT("6.0"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT);
   mMaxStarMagTextCtrl->SetRange(-2.0, 20.0);
   mMaxStarMagTextCtrl->SetDigits(4U);
   mMaxStarMagTextCtrl->SetIncrement(0.5);
   wxStaticText *minStarPixelsStaticText = new wxStaticText(this, -1, wxT("Minimum star size on plot (pixels) "), wxDefaultPosition, wxSize(-1,-1), 0U);
   mMinStarPixelsTextCtrl = new wxSpinCtrlDouble(this, ID_TEXTCTRL, wxT("1.0"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT);
   mMinStarPixelsTextCtrl->SetRange(0.0, 100.0);
   mMinStarPixelsTextCtrl->SetDigits(4U);
   mMinStarPixelsTextCtrl->SetIncrement(1.0);
   wxStaticText *maxStarPixelsStaticText = new wxStaticText(this, -1, wxT("Maximum star size on plot (pixels) "), wxDefaultPosition, wxSize(-1,-1), 0U);
   mMaxStarPixelsTextCtrl = new wxSpinCtrlDouble(this, ID_TEXTCTRL, wxT("10.0"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT);
   mMaxStarPixelsTextCtrl->SetRange(0.0, 100.0);
   mMaxStarPixelsTextCtrl->SetDigits(4U);
   mMaxStarPixelsTextCtrl->SetIncrement(1.0);
   wxStaticText *minStarDimRatioStaticText = new wxStaticText(this, -1, wxT("Minimum star dim ratio "), wxDefaultPosition, wxSize(-1,-1), 0U);
   mMinStarDimRatioTextCtrl = new wxSpinCtrlDouble(this, ID_TEXTCTRL, wxT("0.5"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxALIGN_RIGHT);
   mMinStarDimRatioTextCtrl->SetRange(0.0, 1.0);
   mMinStarDimRatioTextCtrl->SetDigits(4U);
   mMinStarDimRatioTextCtrl->SetIncrement(0.1);

   wxBoxSizer *presetSizer = new wxBoxSizer(wxHORIZONTAL);
   presetSizer->Add(mPresetMonitorRadio, 0, wxALIGN_CENTER|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   presetSizer->Add(mPresetVRRadio, 0, wxALIGN_CENTER|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   presetSizer->Add(mPresetCustomRadio, 0, wxALIGN_CENTER|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   wxFlexGridSizer *plotOptionSizer = new wxFlexGridSizer(2, 0, 0);
   plotOptionSizer->Add(starCountStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(mStarCountTextCtrl, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(minStarMagStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(mMinStarMagTextCtrl, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(maxStarMagStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(mMaxStarMagTextCtrl, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(minStarPixelsStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(mMinStarPixelsTextCtrl, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(maxStarPixelsStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(mMaxStarPixelsTextCtrl, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(minStarDimRatioStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   plotOptionSizer->Add(mMinStarDimRatioTextCtrl, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);

   wxStaticBoxSizer *plotOptionStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Star Options");
   plotOptionStaticSizer->Add(presetSizer, 0, wxALIGN_CENTER|wxALL, bsize);
   plotOptionStaticSizer->Add(plotOptionSizer, 0, wxALIGN_LEFT|wxALL, bsize);

   //-----------------------------------------------------------------
   // Add to page sizer
   //-----------------------------------------------------------------

   wxFlexGridSizer *pageSizer1 = new wxFlexGridSizer(3, 2, 0, 0);
   pageSizer1->Add(plotOptionStaticSizer, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);

   //-----------------------------------------------------------------
   // Add to middle sizer
   //-----------------------------------------------------------------
   wxBoxSizer *pageSizer = new wxBoxSizer(wxVERTICAL);
   pageSizer->Add(pageSizer1, 0, wxALIGN_CENTRE|wxALL, bsize);

   mMiddleSizer->Add(pageSizer, 0, wxALIGN_CENTRE|wxALL, bsize);
   Centre();
}


//------------------------------------------------------------------------------
// void Show()
//------------------------------------------------------------------------------
/**
 * Shows the panel.
 */
//------------------------------------------------------------------------------
void OFStarsDialog::Show()
{
   mPanelSizer->Add(mMiddleSizer, 1, wxGROW | wxALL, 1);
   mPanelSizer->Add(mBottomSizer, 0, wxGROW | wxALL, 1);

   SetAutoLayout(true); // tell the enclosing window to adjust to the size of the sizer
   SetSizer(mPanelSizer); //use the sizer for layout
   mPanelSizer->SetSizeHints(this); //set size hints to honour minimum size

   LoadData();

   // call Layout() to force layout of the children anew
   mPanelSizer->Layout();
   Centre();
}


//------------------------------------------------------------------------------
// void LoadData()
//------------------------------------------------------------------------------
/**
 * Loads data from mOFInterface to this instance's widgets
 */
void OFStarsDialog::LoadData()
{
   try
   {
      // Load data from the core engine
      std::string preset = mOFInterface->GetStringParameter("StarSettings");
      if (preset.size() > 0)
         if (preset == OpenFramesInterface::STAR_SETTING_STRINGS[OpenFramesInterface::STARS_MONITOR])
            mPresetMonitorRadio->SetValue(true);
         else if (preset == OpenFramesInterface::STAR_SETTING_STRINGS[OpenFramesInterface::STARS_VR])
            mPresetVRRadio->SetValue(true);
         else
            mPresetCustomRadio->SetValue(true);
      else
         mPresetMonitorRadio->SetValue(true);

      mCustom.SetStarCatalog(mOFInterface->GetStringParameter("StarCatalog"));
      mCustom.SetMinStarMag(mOFInterface->GetRealParameter("MinStarMag"));
      mCustom.SetMaxStarMag(mOFInterface->GetRealParameter("MaxStarMag"));
      mCustom.SetStarCount(mOFInterface->GetIntegerParameter("StarCount"));
      mCustom.SetMinStarPixels(mOFInterface->GetRealParameter("MinStarPixels"));
      mCustom.SetMaxStarPixels(mOFInterface->GetRealParameter("MaxStarPixels"));
      mCustom.SetMinStarDimRatio(mOFInterface->GetRealParameter("MinStarDimRatio"));
      mSavedStarOptions = mCustom;

      LoadStarOptions();
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }

   EnableUpdate(false);
}


//------------------------------------------------------------------------------
// void LoadStarOptions()
//------------------------------------------------------------------------------
/**
 * Loads star options for the specified preset
 */
void OFStarsDialog::LoadStarOptions()
{
   try
   {
      // Load data from the core engine
      StarOptions opt;

      // Get star options based on selected radio
      if (mPresetVRRadio->GetValue())
         opt = StarOptions::VIRTUAL_REALITY;
      else if (mPresetCustomRadio->GetValue())
         opt = mCustom;
      else
         opt = StarOptions::MONITOR;

      // Populate the spin controls
      mStarCountTextCtrl->SetValue(opt.GetStarCount());
      mMinStarMagTextCtrl->SetValue(opt.GetMinStarMag());
      mMaxStarMagTextCtrl->SetValue(opt.GetMaxStarMag());
      mMinStarPixelsTextCtrl->SetValue(opt.GetMinStarPixels());
      mMaxStarPixelsTextCtrl->SetValue(opt.GetMaxStarPixels());
      mMinStarDimRatioTextCtrl->SetValue(opt.GetMinStarDimRatio());
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }
}


//------------------------------------------------------------------------------
// void SaveData()
//------------------------------------------------------------------------------
/**
 * Applies changes buffered by the widgets to the mOFInterface
 */
void OFStarsDialog::SaveData()
{
   // Assume custom star options will be valid
   mCanClose = true;
   
   // save values to base, base code will do the range checking and throw exceptions
   try
   {
      // save custom star options
      if (mHasCustomDataChanged && mPresetCustomRadio->GetValue())
      {
         SaveCustomStarOptions(mCustom);
         mHasCustomDataChanged = false;
      }

      if (mHasPresetDataChanged)
      {
         if (mPresetMonitorRadio->GetValue())
            mOFInterface->SetStringParameter("StarSettings", mPresetMonitorRadio->GetLabel().ToStdString());
         else if (mPresetVRRadio->GetValue())
            mOFInterface->SetStringParameter("StarSettings", mPresetVRRadio->GetLabel().ToStdString());
         else
            mOFInterface->SetStringParameter("StarSettings", mPresetCustomRadio->GetLabel().ToStdString());
         
         mHasPresetDataChanged = false;
      }

      EnableUpdate(false);
   }
   catch (BaseException &e)
   {
      // Invalid star options: show error message, reload previous settings, keep dialog open
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
      try {
         SaveCustomStarOptions(mSavedStarOptions);
      } catch (BaseException &e2) { // This should never happen!
         MessageInterface::PopupMessage(Gmat::ERROR_, e2.GetFullMessage().c_str());
      }
      mCanClose = false;
   }
}


//------------------------------------------------------------------------------
// void SaveCustomStarOptions()
//------------------------------------------------------------------------------
/**
 * Save the provided star options into the mOFInterface. Must be called in a try-catch block to catch Gmat::BaseException
 */
void OFStarsDialog::SaveCustomStarOptions(const StarOptions& options)
{
   mOFInterface->SetIntegerParameter("StarCount", options.GetStarCount());
   mOFInterface->SetRealParameter("MinStarMag", options.GetMinStarMag());
   mOFInterface->SetRealParameter("MaxStarMag", options.GetMaxStarMag());
   mOFInterface->SetRealParameter("MinStarPixels", options.GetMinStarPixels());
   mOFInterface->SetRealParameter("MaxStarPixels", options.GetMaxStarPixels());
   mOFInterface->SetRealParameter("MinStarDimRatio", options.GetMinStarDimRatio());
}

//------------------------------------------------------------------------------
// void OnTextChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a spin controller changes
 *
 * @param <event> Details of the event
 */
void OFStarsDialog::OnTextChange(wxCommandEvent& event)
{
   wxObject *obj = event.GetEventObject();

   mHasCustomDataChanged = true;
   if (obj == mStarCountTextCtrl)
      mCustom.SetStarCount(mStarCountTextCtrl->GetValue());
   else if (obj == mMinStarMagTextCtrl)
      mCustom.SetMinStarMag(mMinStarMagTextCtrl->GetValue());
   else if (obj == mMaxStarMagTextCtrl)
      mCustom.SetMaxStarMag(mMaxStarMagTextCtrl->GetValue());
   else if (obj == mMinStarPixelsTextCtrl)
      mCustom.SetMinStarPixels(mMinStarPixelsTextCtrl->GetValue());
   else if (obj == mMaxStarPixelsTextCtrl)
      mCustom.SetMaxStarPixels(mMaxStarPixelsTextCtrl->GetValue());
   else if (obj == mMinStarDimRatioTextCtrl)
      mCustom.SetMinStarDimRatio(mMinStarDimRatioTextCtrl->GetValue());

   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnTextChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a spin controller changes
 *
 * @param <event> Details of the event
 */
void OFStarsDialog::OnRadioChange(wxCommandEvent& event)
{
   wxObject *obj = event.GetEventObject();

   mHasPresetDataChanged = true;
   EnableUpdate(true);
   LoadStarOptions();
}


//------------------------------------------------------------------------------
// void OnOk()
//------------------------------------------------------------------------------
/**
 * Saves the data and closes the page
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFStarsDialog::OnOK(wxCommandEvent &event)
{
   if (mDataChanged)
   {
      SaveData();
      if (mCanClose)
         EndModal(wxID_OK);
   }
   else
   {
      if (mCanClose)
         EndModal(wxID_CANCEL);
   }

}


//------------------------------------------------------------------------------
// void OnCancel()
//------------------------------------------------------------------------------
/**
 * Close page without prompting about changes
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFStarsDialog::OnCancel(wxCommandEvent &event)
{
   EndModal(wxID_CANCEL);
}

//------------------------------------------------------------------------------
// void OnHelp()
//------------------------------------------------------------------------------
/**
 * Callback that shows help
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFStarsDialog::OnHelp(wxCommandEvent &event)
{
   wxLaunchDefaultBrowser("https://gitlab.com/EmergentSpaceTechnologies/OpenFramesInterface/wikis/docs/user/current/Star-Options");
}

//------------------------------------------------------------------------------
// void OnSystemClose(wxCloseEvent& even)
//------------------------------------------------------------------------------
/**
 * Close page after prompting about changes.
 *
 * @param <event> Details of the event
 */
//------------------------------------------------------------------------------
void OFStarsDialog::OnSystemClose(wxCloseEvent& event)
{
   // Prompt user if changes were made
   if (mDataChanged)
   {
      if (wxMessageBox(_T("There were changes made to \"" + GetTitle() + "\" panel"
         " which will be lost on Close. \nDo you want to close anyway?"),
         _T("Please Confirm Close"),
         wxICON_QUESTION | wxYES_NO) != wxYES)
      {
         event.Veto();
         return;
      }
   }

   EndModal(wxID_CANCEL);
}


//------------------------------------------------------------------------------
// virtual void EnableUpdate(bool enable = true)
//------------------------------------------------------------------------------
/**
 * Changes the mode of the dialog based on whether there are data changes to
 * apply
 *
 * @param <enable> If true, then application of data changes is enabled
 */
void OFStarsDialog::EnableUpdate(bool enable)
{
   mDataChanged = enable;
   if (mOkButton != nullptr)
      mOkButton->Enable(enable);

   // enable/disable custom inputs as necessary
   bool customEnabled = mPresetCustomRadio->GetValue();
   if (customEnabled != mStarCountTextCtrl->IsEnabled())
   {
      mStarCountTextCtrl->Enable(customEnabled);
      mMinStarMagTextCtrl->Enable(customEnabled);
      mMaxStarMagTextCtrl->Enable(customEnabled);
      mMinStarPixelsTextCtrl->Enable(customEnabled);
      mMaxStarPixelsTextCtrl->Enable(customEnabled);
      mMinStarDimRatioTextCtrl->Enable(customEnabled);
   }
}
