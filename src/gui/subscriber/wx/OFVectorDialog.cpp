//$Id$
//------------------------------------------------------------------------------
//                                  OFVectorDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Yasir Majeed Khan, Emergent Space Technologies, Inc.
// Created: September 24, 2018
//------------------------------------------------------------------------------
/**
* Dialog to configure OpenFramesVector objects
*/
//------------------------------------------------------------------------------

#include "OFVectorDialog.hpp"
#include "OpenFramesVector.hpp"

#include "gmatwxdefs.hpp"
#include "MessageInterface.hpp"
#include "Moderator.hpp"

#include "bitmaps/up.xpm"

#include <float.h>


//------------------------------
// event tables for wxWindows
//------------------------------
BEGIN_EVENT_TABLE(OFVectorDialog, wxDialog)
   EVT_BUTTON(ID_BUTTON_OK, OFVectorDialog::OnOK)
   EVT_BUTTON(ID_BUTTON_CANCEL, OFVectorDialog::OnCancel)
   EVT_BUTTON(ID_BUTTON_HELP, OFVectorDialog::OnHelp)
   EVT_CLOSE(OFVectorDialog::OnSystemClose)

   EVT_COMBOBOX(ID_COMBOBOX_VECTOR_TYPE, OFVectorDialog::OnComboBoxVectorTypeChange)
   EVT_COMBOBOX(ID_COMBOBOX_VECTOR_SOURCE, OFVectorDialog::OnComboBoxSourceObjectChange)
   EVT_COMBOBOX(ID_COMBOBOX_VECTOR_DESTINATION, OFVectorDialog::OnComboBoxDestinationObjectChange)
   EVT_COMBOBOX(ID_COMBOBOX_VECTOR_LENGTH, OFVectorDialog::OnComboBoxVectorLengthChange)
   EVT_TEXT(ID_TEXTCTRL, OFVectorDialog::OnTextChange)
   EVT_COLOURPICKER_CHANGED(ID_COLOR_CTRL, OFVectorDialog::OnColorPickerChange)
   EVT_TEXT(ID_SPINCTRL, OFVectorDialog::OnSpinChange)
END_EVENT_TABLE()


const wxString OFVectorDialog::DIALOG_TITLE = "Vector Definition";

//------------------------------------------------------------------------------
// OFVectorDialog(wxWindow *parent, const wxString &subscriberName)
//------------------------------------------------------------------------------
/**
* Constructs OFVectorDialog object.
*
* @param <forObject> The vector to change settings for
* @param <objects> List of possible objects to point the vector from
* @param <parent> input parent
* @param <enableUpdate> Enable apply mode immediately (for new vectors)
*
* @note Creates the OFVectorDialog GUI
*/
//------------------------------------------------------------------------------

OFVectorDialog::OFVectorDialog(OpenFramesVector *forObject, const wxArrayString &objects, wxWindow *parent, bool enableUpdate) :
wxDialog(parent, -1, DIALOG_TITLE, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE | wxMAXIMIZE_BOX),
mVector(forObject),
mObjects(objects),
mDataChanged(enableUpdate),
mCanClose(true)
{
   InitializeData();
   Create();
   Refresh();
   Show();
   EnableUpdate(enableUpdate);
   
   // shortcut keys
   wxAcceleratorEntry entries[2];
   entries[0].Set(wxACCEL_NORMAL, WXK_F1, ID_BUTTON_HELP);
   entries[1].Set(wxACCEL_CTRL, static_cast<int>('W'), ID_BUTTON_CANCEL);
   wxAcceleratorTable accel(2, entries);
   this->SetAcceleratorTable(accel);
}


//------------------------------------------------------------------------------
// ~OFVectorDialog()
//------------------------------------------------------------------------------
/**
* Destructor
*/
OFVectorDialog::~OFVectorDialog()
{
	// nothing to clean up here
}


//------------------------------------------------------------------------------
// void InitializeData()
//------------------------------------------------------------------------------
/**
* Initializes data that is not wxWidgets
*/
void OFVectorDialog::InitializeData()
{
	mDataChanged = false;
	mCanClose = true;
}


//------------------------------------------------------------------------------
// void Create()
//------------------------------------------------------------------------------
/**
* Creates wxWidgets and adds them to the panel
*/
void OFVectorDialog::Create()
{
	int bsize = 2; // border size

	// create axis array
	wxArrayString emptyList;

	//-----------------------------------------------------------------
	// Main sizers and OK/Cancel buttons
	//-----------------------------------------------------------------
	mPanelSizer = new wxBoxSizer(wxVERTICAL);
	mMiddleSizer = (wxSizer*)(new wxStaticBoxSizer(wxVERTICAL, this));
	mBottomSizer = new wxStaticBoxSizer(wxVERTICAL, this);
	wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

	// create bottom buttons
	mOkButton = new wxButton(this, ID_BUTTON_OK, "OK", wxDefaultPosition, wxDefaultSize, 0);
	mOkButton->SetToolTip("Apply Changes and Close");
	mCancelButton = new wxButton(this, ID_BUTTON_CANCEL, "Cancel", wxDefaultPosition, wxDefaultSize, 0);
	mCancelButton->SetToolTip("Close Without Applying Changes");
   mHelpButton = new wxButton(this, ID_BUTTON_HELP, GUI_ACCEL_KEY"Help", wxDefaultPosition, wxDefaultSize, 0);
   mHelpButton->SetToolTip("OpenFramesInterface Help (F1)");
   mOkButton->SetDefault();

	buttonSizer->Add(mOkButton, 0, wxALIGN_CENTER | wxALL, bsize);
	buttonSizer->Add(mCancelButton, 0, wxALIGN_CENTER | wxALL, bsize);
   buttonSizer->Add(0, 1, wxALIGN_RIGHT);
   buttonSizer->Add(mHelpButton, 0, wxALIGN_RIGHT | wxALL, bsize);

	mBottomSizer->Add(buttonSizer, 0, wxGROW | wxALL, bsize);

	//-----------------------------------------------------------------
	// Create the dialog
	//-----------------------------------------------------------------

  //Create Source Object Combo box drop-down
  wxStaticText *vectorSourceLabel = new wxStaticText(this, -1, wxT("Source Object"), wxDefaultPosition, wxSize(-1, -1), 0);
  mVectorSourceComboBox = new wxComboBox(this, ID_COMBOBOX_VECTOR_SOURCE, "Object List", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_DROPDOWN | wxCB_READONLY);
  mVectorSourceComboBox->SetToolTip("Select source object");
  wxFlexGridSizer *sourceObjectSizer = new wxFlexGridSizer(2, 0, 0);
  sourceObjectSizer->Add(vectorSourceLabel, 0, wxALIGN_LEFT| wxALIGN_CENTER_VERTICAL | wxALL, bsize);
  sourceObjectSizer->Add(mVectorSourceComboBox, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);

  //Create Vector Type Combo box drop-down
  wxStaticText *vectorTypeLabel = new wxStaticText(this, -1, wxT("Vector Type"), wxDefaultPosition, wxSize(-1, -1), 0);
  mVectorTypeComboBox = new wxComboBox(this, ID_COMBOBOX_VECTOR_TYPE, "Vector List", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_DROPDOWN| wxCB_READONLY);
  mVectorTypeComboBox->SetToolTip("Select vector type");
  wxFlexGridSizer *vectorTypeSizer = new wxFlexGridSizer(2, 0, 0);
  vectorTypeSizer->Add(vectorTypeLabel, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
  vectorTypeSizer->Add(mVectorTypeComboBox, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);

  //Create Destination Object Combo box drop-down
  mVectorDestinationLabel = new wxStaticText(this, -1, wxT("Destination Object"), wxDefaultPosition, wxSize(-1, -1), 0);
  mVectorDestinationComboBox = new wxComboBox(this, ID_COMBOBOX_VECTOR_DESTINATION, "Object List", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_DROPDOWN | wxCB_READONLY);
  mVectorDestinationComboBox->SetToolTip("Select destination object");
  mDestinationObjectSizer = new wxFlexGridSizer(2, 0, 0);
  mDestinationObjectSizer->Add(mVectorDestinationLabel, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
  mDestinationObjectSizer->Add(mVectorDestinationComboBox, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);

  //Create Body-fixed start point and direction vector inputs
  mBFStartPointLabel = new wxStaticText(this, STATIC_TEXT, wxT("Start Point [x y z]"), wxDefaultPosition, wxDefaultSize, 0);
  mBFStartPointLabel->SetToolTip("Body-Fixed vector starting point coordinates");
  mBFStartPointXSpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
  mBFStartPointXSpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
  mBFStartPointXSpinCtrl->SetDigits(8U);
  mBFStartPointYSpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
  mBFStartPointYSpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
  mBFStartPointYSpinCtrl->SetDigits(8U);
  mBFStartPointZSpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
  mBFStartPointZSpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
  mBFStartPointZSpinCtrl->SetDigits(8U);
  mBFDirectionLabel = new wxStaticText(this, STATIC_TEXT, wxT("Direction [x y z]"), wxDefaultPosition, wxDefaultSize, 0);
  mBFDirectionLabel->SetToolTip("Body-Fixed vector direction");
  mBFDirectionXSpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
  mBFDirectionXSpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
  mBFDirectionXSpinCtrl->SetDigits(8U);
  mBFDirectionYSpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
  mBFDirectionYSpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
  mBFDirectionYSpinCtrl->SetDigits(8U);
  mBFDirectionZSpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
  mBFDirectionZSpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
  mBFDirectionZSpinCtrl->SetDigits(8U);

  mBFSizer = new wxFlexGridSizer(2, 4, 0, 0);
  mBFSizer->Add(mBFStartPointLabel, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxGROW | wxALL, bsize);
  mBFSizer->Add(mBFStartPointXSpinCtrl, 0, wxALIGN_CENTRE | wxGROW | wxALL, bsize);
  mBFSizer->Add(mBFStartPointYSpinCtrl, 0, wxALIGN_CENTRE | wxGROW | wxALL, bsize);
  mBFSizer->Add(mBFStartPointZSpinCtrl, 0, wxALIGN_CENTRE | wxGROW | wxALL, bsize);
  mBFSizer->Add(mBFDirectionLabel, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxGROW | wxALL, bsize);
  mBFSizer->Add(mBFDirectionXSpinCtrl, 0, wxALIGN_CENTRE | wxGROW | wxALL, bsize);
  mBFSizer->Add(mBFDirectionYSpinCtrl, 0, wxALIGN_CENTRE | wxGROW | wxALL, bsize);
  mBFSizer->Add(mBFDirectionZSpinCtrl, 0, wxALIGN_CENTRE | wxGROW | wxALL, bsize);

  //Create a sizer to store the vector definition objects to the left in the window
  wxStaticBoxSizer *vectorSelectionStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Vector Selection");
  vectorSelectionStaticSizer->SetMinSize(600, 210);
  vectorSelectionStaticSizer->Add(sourceObjectSizer, 0, wxALIGN_LEFT | wxALL, bsize);
  vectorSelectionStaticSizer->Add(vectorTypeSizer, 0, wxALIGN_LEFT | wxALL, bsize);
  vectorSelectionStaticSizer->Add(mDestinationObjectSizer, 0, wxALIGN_LEFT | wxALL, bsize);
  vectorSelectionStaticSizer->Add(mBFSizer, 0, wxALIGN_LEFT | wxALL, bsize);

  //Create Options 
  //Vector Name
  wxStaticText *vectorNameText = new wxStaticText(this, -1, wxT("Name"), wxDefaultPosition, wxSize(-1, -1), 0);
  mVectorName = new wxTextCtrl(this, ID_TEXTCTRL, mVector->GetName(), wxDefaultPosition, wxDefaultSize, 0);
  mVectorName->SetToolTip("Vector Name");
  wxFlexGridSizer *vectorNameSizer = new wxFlexGridSizer(2, 0, 0);
  vectorNameSizer->Add(vectorNameText, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
  vectorNameSizer->Add(mVectorName, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
  
  //Vector Label
  wxStaticText *vectorLabel = new wxStaticText(this, -1, wxT("Label"), wxDefaultPosition, wxSize(-1, -1), 0);
  mVectorLabel = new wxTextCtrl(this, ID_TEXTCTRL, "NewVector1", wxDefaultPosition, wxDefaultSize, 0);
  mVectorLabel->SetToolTip("Vector Label");
  wxFlexGridSizer *vectorLabelSizer = new wxFlexGridSizer(2, 0, 0);
  vectorLabelSizer->Add(vectorLabel, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
  vectorLabelSizer->Add(mVectorLabel, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);

  //Vector Color 
  wxStaticText *vectorColorLabel = new wxStaticText(this, -1, wxT("Color"), wxDefaultPosition, wxSize(-1, -1), 0);
  mVectorColorCtrl = new wxColourPickerCtrl(this, ID_COLOR_CTRL, *wxRED, wxDefaultPosition, wxDefaultSize, 0);
  mVectorColorCtrl->SetToolTip("Select vector color");
  wxFlexGridSizer *vectorColorSizer = new wxFlexGridSizer(3, 0, 0);
  vectorColorSizer->Add(vectorColorLabel, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
  vectorColorSizer->Add(mVectorColorCtrl, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);

  //Vector Length
  wxStaticText *vectorLengthLabel = new wxStaticText(this, -1, wxT("Length"), wxDefaultPosition, wxSize(-1, -1), 0);
  mVectorLengthComboBox = new wxComboBox(this, ID_COMBOBOX_VECTOR_LENGTH, "Auto", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_DROPDOWN | wxCB_READONLY);
  mVectorLengthComboBox->SetToolTip("Select vector length type");
  //Vector length input box
  mVectorLengthCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "1.0", wxDefaultPosition, wxDefaultSize, 0);
  mVectorLengthCtrl->SetRange(-DBL_MAX, DBL_MAX);
  mVectorLengthCtrl->SetDigits(8U);
  mVectorLengthCtrl->Disable();//Disable intially. 
  wxFlexGridSizer *vectorLengthSizer = new wxFlexGridSizer(3, 0, 0);
  vectorLengthSizer->Add(vectorLengthLabel, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
  vectorLengthSizer->Add(mVectorLengthComboBox, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
  vectorLengthSizer->Add(mVectorLengthCtrl, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
  
  //Create an Options sizer 
  wxStaticBoxSizer *vectorOptionsSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Vector Options");
  vectorOptionsSizer->Add(vectorNameSizer, 0, wxALIGN_LEFT | wxALL, bsize);
  vectorOptionsSizer->Add(vectorLabelSizer, 0, wxALIGN_LEFT | wxALL, bsize);
  vectorOptionsSizer->Add(vectorColorSizer, 0, wxALIGN_LEFT | wxALL, bsize);
  vectorOptionsSizer->Add(vectorLengthSizer, 0, wxALIGN_LEFT | wxALL, bsize);
  

	//-----------------------------------------------------------------
	// Add to page sizer
	//-----------------------------------------------------------------

  wxFlexGridSizer *pageSizer1 = new wxFlexGridSizer(3, 2, 0, 0); 
  pageSizer1->Add(vectorSelectionStaticSizer, 0, wxALIGN_LEFT | wxGROW | wxALL, bsize);
  pageSizer1->Add(vectorOptionsSizer, 0, wxALIGN_LEFT | wxGROW | wxALL, bsize);


	//-----------------------------------------------------------------
	// Add to middle sizer
	//-----------------------------------------------------------------
	wxBoxSizer *pageSizer = new wxBoxSizer(wxVERTICAL);
	pageSizer->Add(pageSizer1, 0, wxALIGN_CENTRE | wxALL, bsize);
	mMiddleSizer->Add(pageSizer, 0, wxALIGN_CENTRE | wxALL, bsize);
}


//------------------------------------------------------------------------------
// void Refresh()
//------------------------------------------------------------------------------
/**
* Refreshes lists of objects within the panel.
*/
//------------------------------------------------------------------------------
void OFVectorDialog::Refresh()
{
  //Add strings to the drop-down Vector List combo box
  wxString vectorsList[3] = { "Body-Fixed","Relative Position", "Thrust Vector" };
  mVectorTypeComboBox->Append(vectorsList[0]);
  mVectorTypeComboBox->Append(vectorsList[1]);
  //mVectorTypeComboBox->Append(vectorsList[2]);

  //Add strings to the drop-down Vector Length combo box
  wxString vectorsLengthType[2] = { "Auto", "Manual" };
  mVectorLengthComboBox->Append(vectorsLengthType[0]);
  mVectorLengthComboBox->Append(vectorsLengthType[1]);


  //**************************************************************
  //Add selected objects to the Source and Destination combo boxes
  // Remember vector combo box selections
  wxString sourceVector = mVectorSourceComboBox->GetStringSelection();
  mVectorSourceComboBox->Clear();
  mVectorDestinationComboBox->Clear();
  
  // Spacecraft objects
  mVectorSourceComboBox->Set(mObjects);
  //mVectorSourceComboBox->Append(mObjects);
  mVectorDestinationComboBox->Append(mObjects);
  mVectorSourceComboBox->Select(0);

  //Reselect old selections for vector comboboxes or first item if old selection removed
 /* if (sourceVector.size() == 0)
   mVectorSourceComboBox->Select(wxNOT_FOUND);
  else if (mVectorSourceComboBox->FindString(sourceVector) == wxNOT_FOUND)
    mVectorSourceComboBox->Select(0);
  else
    mVectorSourceComboBox->SetValue(sourceVector);*/

}


//------------------------------------------------------------------------------
// void Show()
//------------------------------------------------------------------------------
/**
* Shows the panel.
*/
//------------------------------------------------------------------------------
void OFVectorDialog::Show()
{

	mPanelSizer->Add(mMiddleSizer, 1, wxGROW | wxALL, 1);
	mPanelSizer->Add(mBottomSizer, 0, wxGROW | wxALL, 1);

	SetAutoLayout(true); // tell the enclosing window to adjust to the size of the sizer
	SetSizer(mPanelSizer); //use the sizer for layout
	mPanelSizer->SetSizeHints(this); //set size hints to honour minimum size

  //********    HIDE/SHOW objects   ********
  //Hide the destination comboBox intially when the dialog is created. 
  //Show the comboBox when Relative Position is selected (done in OnComboBoxVectorTypeChange() )
  mDestinationObjectSizer->Hide(mVectorDestinationLabel);
  mDestinationObjectSizer->Hide(mVectorDestinationComboBox);

  //Show the destination comboBox when loading data from the OpenFramesVector and there is a 
  //destination objected stored, and hide Body-Fixed vector options
  if (mVector->GetStringParameter("VectorType") == "Relative Position")
  {
    mDestinationObjectSizer->Show(mVectorDestinationLabel);
    mDestinationObjectSizer->Show(mVectorDestinationComboBox);

    mBFSizer->Hide(mBFStartPointLabel);
    mBFSizer->Hide(mBFStartPointXSpinCtrl);
    mBFSizer->Hide(mBFStartPointYSpinCtrl);
    mBFSizer->Hide(mBFStartPointZSpinCtrl);
    mBFSizer->Hide(mBFDirectionLabel);
    mBFSizer->Hide(mBFDirectionXSpinCtrl);
    mBFSizer->Hide(mBFDirectionYSpinCtrl);
    mBFSizer->Hide(mBFDirectionZSpinCtrl);
  }
  else if (mVector->GetStringParameter("VectorType") == "Body-Fixed")
  {
    mDestinationObjectSizer->Hide(mVectorDestinationLabel);
    mDestinationObjectSizer->Hide(mVectorDestinationComboBox);

    mBFSizer->Show(mBFStartPointLabel);
    mBFSizer->Show(mBFStartPointXSpinCtrl);
    mBFSizer->Show(mBFStartPointYSpinCtrl);
    mBFSizer->Show(mBFStartPointZSpinCtrl);
    mBFSizer->Show(mBFDirectionLabel);
    mBFSizer->Show(mBFDirectionXSpinCtrl);
    mBFSizer->Show(mBFDirectionYSpinCtrl);
    mBFSizer->Show(mBFDirectionZSpinCtrl);
  }

  //Hide the Body-fixed start point and direction vectors intially when the dialog is created. 
  //Show the vectors when Body-Fixed is selected (done in OnComboBoxVectorTypeChange() )
 /* mBFSizer->Hide(mBFStartPointLabel);
  mBFSizer->Hide(mBFStartPointXSpinCtrl);
  mBFSizer->Hide(mBFStartPointYSpinCtrl);
  mBFSizer->Hide(mBFStartPointZSpinCtrl);
  mBFSizer->Hide(mBFDirectionLabel);
  mBFSizer->Hide(mBFDirectionXSpinCtrl);
  mBFSizer->Hide(mBFDirectionYSpinCtrl);
  mBFSizer->Hide(mBFDirectionZSpinCtrl);*/

  //Enable the Vector lengthctrl when loading data from the OpenFramesVector and the
  //VectorLengthType is Manual
  if (mVector->GetStringParameter("VectorLengthType") == "Manual")
  {
    mVectorLengthCtrl->Enable();
  }

  
 
	LoadData();
	EnableUpdate(false);

	// call Layout() to force layout of the children anew
	mPanelSizer->Layout();
	Centre();

}

//------------------------------------------------------------------------------
// void LoadData()
//------------------------------------------------------------------------------
/**
* Loads data from mOFInterface to this instance's widgets
*/
void OFVectorDialog::LoadData()
{
  try
  {
    // Load data from the core engine

    //load color
    //For color, convert from string to and int value, then to rgb then to wxcolor
    std::string colorStr = mVector->GetStringParameter("VectorColor");
    UnsignedInt colorInt = RgbColor::ToIntColor(colorStr);
    RgbColor rgbColor;
    rgbColor.Set(colorInt);
    wxColour wxColor;
    wxColor.Set(rgbColor.Red(), rgbColor.Green(), rgbColor.Blue());
    mVectorColorCtrl->SetColour(wxColor);

    //load source object
    wxString sourceObject = mVector->GetStringParameter("SourceObject");
    int index = mObjects.Index(sourceObject);
    if (index == wxNOT_FOUND)
    {
      mVectorSourceComboBox->Select(0);
    }
    else
    {
      mVectorSourceComboBox->Select(index);
    }

    //load vector type
    wxString vectorType = mVector->GetStringParameter("VectorType");
    mVectorTypeComboBox->SetValue(vectorType);

    //load destination object
    wxString destinationObject = mVector->GetStringParameter("DestinationObject");
    index = mObjects.Index(destinationObject);
    if (index == wxNOT_FOUND) 
    {
      if (mObjects.size() >= 2)
      {
        mVectorDestinationComboBox->Select(1);
      }
      else
      {
        mVectorDestinationComboBox->Select(0);
      }
      
    }
    else
    {
      mVectorDestinationComboBox->Select(index);
    }

    //load Body-Fixed vectors
    mBFStartPointXSpinCtrl->SetValue(mVector->GetRealParameter("BFStartPoint", 0));
    mBFStartPointYSpinCtrl->SetValue(mVector->GetRealParameter("BFStartPoint", 1));
    mBFStartPointZSpinCtrl->SetValue(mVector->GetRealParameter("BFStartPoint", 2));
    mBFDirectionXSpinCtrl->SetValue(mVector->GetRealParameter("BFDirection", 0));
    mBFDirectionYSpinCtrl->SetValue(mVector->GetRealParameter("BFDirection", 1));
    mBFDirectionZSpinCtrl->SetValue(mVector->GetRealParameter("BFDirection", 2));
    


    //load vector label
    wxString vectorLabel = mVector->GetStringParameter("VectorLabel");
    mVectorLabel->SetValue(vectorLabel);

    //load vector length combo box
    mVectorLengthComboBox->SetValue(mVector->GetStringParameter("VectorLengthType"));

    //load vector length
    mVectorLengthCtrl->SetValue(mVector->GetRealParameter("VectorLength"));

   

  }
  catch (BaseException &e)
  {
    MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
  }
    
  EnableUpdate(false);

}



//------------------------------------------------------------------------------
// void SaveData()
//------------------------------------------------------------------------------
/**
* Applies changes buffered by the widgets to the mOFInterface
*/
bool OFVectorDialog::SaveData()
{
	bool success = true;

	//-----------------------------------------------------------------
	// save values to base, base code should do the range checking
	//-----------------------------------------------------------------
	try
	{
		if (mDataChanged)
		{
      //Save Color: To save vector color, first convert to RGB, then to string
      wxColour wxcolor = mVectorColorCtrl->GetColour();
      RgbColor color = RgbColor(wxcolor.Red(), wxcolor.Green(), wxcolor.Blue());
      std::string colorStr = RgbColor::ToRgbString(color.GetIntColor());
     
      mVector->SetStringParameter("VectorColor", colorStr);
      mVector->SetStringParameter("SourceObject", mVectorSourceComboBox->GetValue().WX_TO_STD_STRING);
      mVector->SetStringParameter("VectorType", mVectorTypeComboBox->GetValue().WX_TO_STD_STRING);
      mVector->SetStringParameter("DestinationObject", mVectorDestinationComboBox->GetValue().WX_TO_STD_STRING);
      mVector->SetRealParameter("BFStartPoint", mBFStartPointXSpinCtrl->GetValue(), 0);
      mVector->SetRealParameter("BFStartPoint", mBFStartPointYSpinCtrl->GetValue(), 1);
      mVector->SetRealParameter("BFStartPoint", mBFStartPointZSpinCtrl->GetValue(), 2);
      mVector->SetRealParameter("BFDirection", mBFDirectionXSpinCtrl->GetValue(), 0);
      mVector->SetRealParameter("BFDirection", mBFDirectionYSpinCtrl->GetValue(), 1);
      mVector->SetRealParameter("BFDirection", mBFDirectionZSpinCtrl->GetValue(), 2);
      mVector->SetStringParameter("VectorLabel", mVectorLabel->GetValue().WX_TO_STD_STRING);
      mVector->SetStringParameter("VectorLengthType", mVectorLengthComboBox->GetValue().WX_TO_STD_STRING);
      mVector->SetRealParameter("VectorLength", mVectorLengthCtrl->GetValue() );

			Moderator::Instance()->ConfigurationChanged(mVector, true);
			EnableUpdate(false);
		}

    if (mVector->GetName() != mVectorName->GetValue().WX_TO_STD_STRING)
    {
    Moderator *theModerator = Moderator::Instance();
    success = theModerator->RenameObject(GmatType::GetTypeId("OpenFramesVector"), mVector->GetName(),
    mVectorName->GetValue().WX_TO_STD_STRING);
    }

	}
	catch (BaseException &e)
	{
		MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
		success = false;
	}

	return success;
}



//------------------------------------------------------------------------------
// void OnComboBoxSourceObjectChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when Source Vector combo box changes
*
* @param <event> Details of the event
*/
void OFVectorDialog::OnComboBoxSourceObjectChange(wxCommandEvent& event)
{
  //As of now, nothing happens except enabling updates
  EnableUpdate(true);
}

//------------------------------------------------------------------------------
// void OnComboBoxDestinationObjectChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when Source Vector combo box changes
*
* @param <event> Details of the event
*/
void OFVectorDialog::OnComboBoxDestinationObjectChange(wxCommandEvent& event)
{
  //As of now, nothing happens except enabling updates
  EnableUpdate(true);
}

//------------------------------------------------------------------------------
// void OnComboBoxVectorTypeChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when Vector Type combo box changes
*
* @param <event> Details of the event
*/
void OFVectorDialog::OnComboBoxVectorTypeChange(wxCommandEvent& event)
{
  wxString vectorTypeString = mVectorTypeComboBox->GetValue();

  if (!vectorTypeString.compare("Relative Position"))
  {
    mDestinationObjectSizer->Show(mVectorDestinationComboBox);
    mDestinationObjectSizer->Show(mVectorDestinationLabel);

    mBFSizer->Hide(mBFStartPointLabel);
    mBFSizer->Hide(mBFStartPointXSpinCtrl);
    mBFSizer->Hide(mBFStartPointYSpinCtrl);
    mBFSizer->Hide(mBFStartPointZSpinCtrl);
    mBFSizer->Hide(mBFDirectionLabel);
    mBFSizer->Hide(mBFDirectionXSpinCtrl);
    mBFSizer->Hide(mBFDirectionYSpinCtrl);
    mBFSizer->Hide(mBFDirectionZSpinCtrl);

  }
  else if (!vectorTypeString.compare("Body-Fixed"))
  {
    mDestinationObjectSizer->Hide(mVectorDestinationComboBox);
    mDestinationObjectSizer->Hide(mVectorDestinationLabel);

    mBFSizer->Show(mBFStartPointLabel);
    mBFSizer->Show(mBFStartPointXSpinCtrl);
    mBFSizer->Show(mBFStartPointYSpinCtrl);
    mBFSizer->Show(mBFStartPointZSpinCtrl);
    mBFSizer->Show(mBFDirectionLabel);
    mBFSizer->Show(mBFDirectionXSpinCtrl);
    mBFSizer->Show(mBFDirectionYSpinCtrl);
    mBFSizer->Show(mBFDirectionZSpinCtrl);
  }
  else
  {
    //Hide 
    mDestinationObjectSizer->Hide(mVectorDestinationComboBox);
    mDestinationObjectSizer->Hide(mVectorDestinationLabel);

    mBFSizer->Hide(mBFStartPointLabel);
    mBFSizer->Hide(mBFStartPointXSpinCtrl);
    mBFSizer->Hide(mBFStartPointYSpinCtrl);
    mBFSizer->Hide(mBFStartPointZSpinCtrl);
    mBFSizer->Hide(mBFDirectionLabel);
    mBFSizer->Hide(mBFDirectionXSpinCtrl);
    mBFSizer->Hide(mBFDirectionYSpinCtrl);
    mBFSizer->Hide(mBFDirectionZSpinCtrl);
  }

  EnableUpdate(true);

}


//------------------------------------------------------------------------------
// void OnComboBoxVectorLengthChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when Vector Length combo box changes
*
* @param <event> Details of the event
*/
void OFVectorDialog::OnComboBoxVectorLengthChange(wxCommandEvent& event)
{
  wxString vectorLengthString = mVectorLengthComboBox->GetValue();

  if (!vectorLengthString.compare("Manual"))
  {
    
    mVectorLengthCtrl->Enable();

  }
  else
  {
    mVectorLengthCtrl->Disable();

  }
 
 
  EnableUpdate(true);

}


//------------------------------------------------------------------------------
// void OnTextChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when a text box changes
*
* @param <event> Details of the event
*/
void OFVectorDialog::OnTextChange(wxCommandEvent& event)
{
	EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnOk()
//------------------------------------------------------------------------------
/**
* Saves the data and closes the page
*
* @param <event> Details of the event
*/
//------------------------------------------------------------------------------
void OFVectorDialog::OnOK(wxCommandEvent &event)
{
	if (mDataChanged)
	{
		bool success = SaveData();
		if (mCanClose && success)
			EndModal(wxID_OK);
	}
	else
	{
		if (mCanClose)
			EndModal(wxID_CANCEL);
	}

}


//------------------------------------------------------------------------------
// void OnCancel()
//------------------------------------------------------------------------------
/**
 * Close page without prompting about changes
*
* @param <event> Details of the event
*/
//------------------------------------------------------------------------------
void OFVectorDialog::OnCancel(wxCommandEvent &event)
{
	EndModal(wxID_CANCEL);
}

//------------------------------------------------------------------------------
// void OnHelp()
//------------------------------------------------------------------------------
/**
 * Callback that shows help
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFVectorDialog::OnHelp(wxCommandEvent &event)
{
   wxLaunchDefaultBrowser("https://gitlab.com/EmergentSpaceTechnologies/OpenFramesInterface/wikis/docs/user/current/Vector-Definition");
}

//------------------------------------------------------------------------------
// void OnSystemClose(wxCloseEvent& even)
//------------------------------------------------------------------------------
/**
 * Close page after prompting about changes.
*
* @param <event> Details of the event
*/
//------------------------------------------------------------------------------
void OFVectorDialog::OnSystemClose(wxCloseEvent& event)
{
   // Prompt user if changes were made
   if (mDataChanged)
   {
      if (wxMessageBox(_T("There were changes made to \"" + GetTitle() + "\" panel"
         " which will be lost on Close. \nDo you want to close anyway?"),
         _T("Please Confirm Close"),
         wxICON_QUESTION | wxYES_NO) != wxYES)
      {
         event.Veto();
         return;
      }
   }

	EndModal(wxID_CANCEL);
}


//------------------------------------------------------------------------------
// virtual void EnableUpdate(bool enable = true)
//------------------------------------------------------------------------------
/**
* Changes the mode of the dialog based on whether there are data changes to
* apply
*
* @param <enable> If true, then application of data changes is enabled
*/
void OFVectorDialog::EnableUpdate(bool enable)
{
	mDataChanged = enable;
	if (mOkButton != nullptr)
		mOkButton->Enable(enable);
}


//------------------------------------------------------------------------------
// void OnColorPickerChange(wxColourPickerEvent& event)
//------------------------------------------------------------------------------
void OFVectorDialog::OnColorPickerChange(wxColourPickerEvent& event)
{
  wxColour wxcolor = mVectorColorCtrl->GetColour();
  RgbColor color = RgbColor(wxcolor.Red(), wxcolor.Green(), wxcolor.Blue());
  std::string colorStr = RgbColor::ToRgbString(color.GetIntColor());
  mVector->SetStringParameter(mVector->GetParameterID("VectorColor"), colorStr);

  mDataChanged = true;
  EnableUpdate(true);
}

//------------------------------------------------------------------------------
// void OnSpinChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when a spin control changes
*
* @param <event> Details of the event
*/
void OFVectorDialog::OnSpinChange(wxCommandEvent& event)
{
  EnableUpdate(true);
}
