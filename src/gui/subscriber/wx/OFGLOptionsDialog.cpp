//$Id$
//------------------------------------------------------------------------------
//                                  OFGLOptionsDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
//------------------------------------------------------------------------------
/**
 * Dialog to configure advanced OpenFramesInterface stars options
 */
//------------------------------------------------------------------------------

#include "OFGLOptionsDialog.hpp"
#include "OpenFramesInterface.hpp"

#include "MessageInterface.hpp"


//------------------------------
// event tables for wxWindows
//------------------------------
BEGIN_EVENT_TABLE(OFGLOptionsDialog, wxDialog)
   EVT_BUTTON(ID_BUTTON_OK, OFGLOptionsDialog::OnOK)
   EVT_BUTTON(ID_BUTTON_CANCEL, OFGLOptionsDialog::OnCancel)
   EVT_BUTTON(ID_BUTTON_HELP, OFGLOptionsDialog::OnHelp)
   EVT_CLOSE(OFGLOptionsDialog::OnSystemClose)

   EVT_CHECKBOX(ID_CHECKBOX, OFGLOptionsDialog::OnCheckBoxChange)
   EVT_COMBOBOX(ID_COMBOBOX, OFGLOptionsDialog::OnComboBoxChange)
END_EVENT_TABLE()


const wxString OFGLOptionsDialog::DIALOG_TITLE = "Graphics Options";


//------------------------------------------------------------------------------
// OFGLOptionsDialog(wxWindow *parent, const wxString &subscriberName)
//------------------------------------------------------------------------------
/**
 * Constructs OFGLOptionsDialog object.
 *
 * @param <forObject> The object to configure star options for
 * @param <parent> input parent.
 *
 * @note Creates the OFGLOptionsDialog GUI
 */
//------------------------------------------------------------------------------
OFGLOptionsDialog::OFGLOptionsDialog(OpenFramesInterface *forObject, wxWindow *parent) :
    wxDialog(parent, -1, DIALOG_TITLE, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxMAXIMIZE_BOX),
    mOFInterface(forObject),
    mHasCustomDataChanged(false),
    mDataChanged(false),
    mCanClose(true)
{
   InitializeData();
   Create();
   Show();

   // shortcut keys
   wxAcceleratorEntry entries[2];
   entries[0].Set(wxACCEL_NORMAL, WXK_F1, ID_BUTTON_HELP);
   entries[1].Set(wxACCEL_CTRL, static_cast<int>('W'), ID_BUTTON_CANCEL);
   wxAcceleratorTable accel(2, entries);
   this->SetAcceleratorTable(accel);
}


//------------------------------------------------------------------------------
// ~OFGLOptionsDialog()
//------------------------------------------------------------------------------
/**
 * Destructor
 */
OFGLOptionsDialog::~OFGLOptionsDialog()
{
}


//------------------------------------------------------------------------------
// void InitializeData()
//------------------------------------------------------------------------------
/**
 * Initializes data that is not wxWidgets
 */
void OFGLOptionsDialog::InitializeData()
{
   mHasCustomDataChanged = false;
   mDataChanged = false;
   mCanClose = true;
}


//------------------------------------------------------------------------------
// void Create()
//------------------------------------------------------------------------------
/**
 * Creates wxWidgets and adds them to the panel
 */
void OFGLOptionsDialog::Create()
{
   int bsize = 2; // border size

   // create axis array
   wxArrayString emptyList;

   //-----------------------------------------------------------------
   // Main sizers and buttons
   //-----------------------------------------------------------------
   mPanelSizer = new wxBoxSizer(wxVERTICAL);
   mMiddleSizer = (wxSizer*)(new wxStaticBoxSizer(wxVERTICAL, this));
   mBottomSizer = new wxStaticBoxSizer(wxVERTICAL, this );
   wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

   // create bottom buttons
   mOkButton = new wxButton(this, ID_BUTTON_OK, "OK", wxDefaultPosition, wxDefaultSize, 0);
   mOkButton->SetToolTip("Apply Changes and Close");
   mCancelButton = new wxButton(this, ID_BUTTON_CANCEL, "Cancel", wxDefaultPosition, wxDefaultSize, 0);
   mCancelButton->SetToolTip("Close Without Applying Changes");
   mHelpButton = new wxButton(this, ID_BUTTON_HELP, GUI_ACCEL_KEY"Help", wxDefaultPosition, wxDefaultSize, 0);
   mHelpButton->SetToolTip("OpenFramesInterface Help (F1)");
   mOkButton->SetDefault();

   buttonSizer->Add(mOkButton, 0, wxALIGN_CENTER | wxALL, bsize);
   buttonSizer->Add(mCancelButton, 0, wxALIGN_CENTER | wxALL, bsize);
   buttonSizer->Add(0, 1, wxALIGN_RIGHT);
   buttonSizer->Add(mHelpButton, 0, wxALIGN_RIGHT | wxALL, bsize);

   mBottomSizer->Add(buttonSizer, 0, wxGROW | wxALL, bsize);

   //-----------------------------------------------------------------
   // Options controls
   //-----------------------------------------------------------------
   mShowVRCheckBox = new wxCheckBox(this, ID_CHECKBOX, wxT("Enable Virtual Reality Headset"), wxDefaultPosition, wxSize(-1, -1), bsize);

   wxStaticText *MSAASamplesText = new wxStaticText(this, ID_TEXT, wxT("Multisample Anti-aliasing"), wxDefaultPosition, wxDefaultSize, 0);
   wxArrayString options;
   options.Add("Disabled");
   const OpenGLOptions::MSAASampleSet& availableMSAASamples = mCustom.GetAvailableMSAASamples();
   for(int msaaSample : availableMSAASamples)
   {
      // Add each MSAA sample to the combobox
      options.Add(wxString::Format(wxT("%ix"), msaaSample));
   }
   wxString currMSAASample = wxString::Format(wxT("%ix"), mCustom.GetMSAASamples());
   mMSAASamplesComboBox = new wxComboBox(this, ID_COMBOBOX, currMSAASample, wxDefaultPosition, wxDefaultSize, options, wxCB_DROPDOWN | wxCB_READONLY);

   wxBoxSizer *fullSizer = new wxBoxSizer(wxVERTICAL);
   fullSizer->Add(mShowVRCheckBox, 0, wxALIGN_LEFT|wxALL, bsize);
   wxFlexGridSizer *gridSizer = new wxFlexGridSizer(2, 0, 0);
   gridSizer->Add(MSAASamplesText, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   gridSizer->Add(mMSAASamplesComboBox, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);

   //-----------------------------------------------------------------
   // Star map
   //-----------------------------------------------------------------
   wxStaticBoxSizer *plotOptionStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, DIALOG_TITLE);
   plotOptionStaticSizer->Add(fullSizer, 0, wxALIGN_LEFT|wxALL, bsize);
   plotOptionStaticSizer->Add(gridSizer, 0, wxALIGN_LEFT|wxALL, bsize);

   //-----------------------------------------------------------------
   // Add to page sizer
   //-----------------------------------------------------------------

   wxFlexGridSizer *pageSizer1 = new wxFlexGridSizer(3, 2, 0, 0);
   pageSizer1->Add(plotOptionStaticSizer, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);

   //-----------------------------------------------------------------
   // Add to middle sizer
   //-----------------------------------------------------------------
   wxBoxSizer *pageSizer = new wxBoxSizer(wxVERTICAL);
   pageSizer->Add(pageSizer1, 0, wxALIGN_CENTRE|wxALL, bsize);

   mMiddleSizer->Add(pageSizer, 0, wxALIGN_CENTRE|wxALL, bsize);
}


//------------------------------------------------------------------------------
// void Show()
//------------------------------------------------------------------------------
/**
 * Shows the panel.
 */
//------------------------------------------------------------------------------
void OFGLOptionsDialog::Show()
{
   mPanelSizer->Add(mMiddleSizer, 1, wxGROW | wxALL, 1);
   mPanelSizer->Add(mBottomSizer, 0, wxGROW | wxALL, 1);

   SetAutoLayout(true); // tell the enclosing window to adjust to the size of the sizer
   SetSizer(mPanelSizer); //use the sizer for layout
   mPanelSizer->SetSizeHints(this); //set size hints to honour minimum size

   LoadData();
   EnableUpdate(false);

   // call Layout() to force layout of the children anew
   mPanelSizer->Layout();
   Centre();
}


//------------------------------------------------------------------------------
// void LoadData()
//------------------------------------------------------------------------------
/**
 * Loads data from mOFInterface to this instance's widgets
 */
void OFGLOptionsDialog::LoadData()
{
   try
   {
      // Load data from the core engine
      wxString str;
      std::string preset = mOFInterface->GetStringParameter("StarSettings");

      mCustom = mOFInterface->GetGLOptions();

      // Set true/false options
      mShowVRCheckBox->SetValue(mCustom.GetEnableVR());

      // Set the MSAA selections
      if (mCustom.GetEnableMSAA())
      {
         str.Printf("%dx", mCustom.GetMSAASamples());
         mMSAASamplesComboBox->SetValue(str);
      }
      else
         mMSAASamplesComboBox->SetSelection(0);
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }

   EnableUpdate(false);
}


//------------------------------------------------------------------------------
// void SaveData()
//------------------------------------------------------------------------------
/**
 * Applies changes buffered by the widgets to the mOFInterface
 */
void OFGLOptionsDialog::SaveData()
{
   //-----------------------------------------------------------------
   // save values to base, base code should do the range checking
   //-----------------------------------------------------------------
   try
   {
      //--------------------------------------------------------------
      // save scale factors
      //--------------------------------------------------------------
      if (mHasCustomDataChanged)
      {
         mHasCustomDataChanged = false;

         mOFInterface->SetGLOptions(mCustom);
      }

      EnableUpdate(false);
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }
}


//------------------------------------------------------------------------------
// void OnComboBoxChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a combobox changes
 *
 * @param <event> Details of the event
 */
void OFGLOptionsDialog::OnComboBoxChange(wxCommandEvent& event)
{
   wxObject *obj = event.GetEventObject();

   if (mMSAASamplesComboBox->GetSelection() == 0)
   {
      mCustom.SetEnableMSAA(false);
      mCustom.SetMSAASamples(0);
      mHasCustomDataChanged = true;
      EnableUpdate(true);
   }
   else
   {
      Integer samples = std::stoi(mMSAASamplesComboBox->GetValue().WX_TO_STD_STRING);
      if (samples >= 0)
      {
         mCustom.SetEnableMSAA(true);
         mCustom.SetMSAASamples(samples);
         mHasCustomDataChanged = true;
         EnableUpdate(true);
      }
   }
}


//------------------------------------------------------------------------------
// void OnCheckBoxChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a checkbox changes
 *
 * @param <event> Details of the event
 */
void OFGLOptionsDialog::OnCheckBoxChange(wxCommandEvent& event)
{
   wxObject *obj = event.GetEventObject();

   if (event.GetEventObject() == mShowVRCheckBox)
      mCustom.SetEnableVR(mShowVRCheckBox->GetValue());

   mHasCustomDataChanged = true;
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnOk()
//------------------------------------------------------------------------------
/**
 * Saves the data and closes the page
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFGLOptionsDialog::OnOK(wxCommandEvent &event)
{
   if (mDataChanged)
   {
      SaveData();
      if (mCanClose)
         EndModal(wxID_OK);
   }
   else
   {
      if (mCanClose)
         EndModal(wxID_CANCEL);
   }

}


//------------------------------------------------------------------------------
// void OnCancel()
//------------------------------------------------------------------------------
/**
 * Close page without prompting about changes
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFGLOptionsDialog::OnCancel(wxCommandEvent &event)
{
   EndModal(wxID_CANCEL);
}

//------------------------------------------------------------------------------
// void OnHelp()
//------------------------------------------------------------------------------
/**
 * Callback that shows help
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFGLOptionsDialog::OnHelp(wxCommandEvent &event)
{
   wxLaunchDefaultBrowser("https://gitlab.com/EmergentSpaceTechnologies/OpenFramesInterface/wikis/docs/user/current/Graphics-Options");
}

//------------------------------------------------------------------------------
// void OnSystemClose(wxCloseEvent& even)
//------------------------------------------------------------------------------
/**
 * Close page after prompting about changes.
 *
 * @param <event> Details of the event
 */
//------------------------------------------------------------------------------
void OFGLOptionsDialog::OnSystemClose(wxCloseEvent& event)
{
   // Prompt user if changes were made
   if (mDataChanged)
   {
      if (wxMessageBox(_T("There were changes made to \"" + GetTitle() + "\" panel"
         " which will be lost on Close. \nDo you want to close anyway?"),
         _T("Please Confirm Close"),
         wxICON_QUESTION | wxYES_NO) != wxYES)
      {
         event.Veto();
         return;
      }
   }

   EndModal(wxID_CANCEL);
}


//------------------------------------------------------------------------------
// virtual void EnableUpdate(bool enable = true)
//------------------------------------------------------------------------------
/**
 * Changes the mode of the dialog based on whether there are data changes to
 * apply
 *
 * @param <enable> If true, then application of data changes is enabled
 */
void OFGLOptionsDialog::EnableUpdate(bool enable)
{
   mDataChanged = enable;
   if (mOkButton != nullptr)
      mOkButton->Enable(enable);
}
