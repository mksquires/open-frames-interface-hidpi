//$Id$
//------------------------------------------------------------------------------
//                                  OFSensorMaskDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under SBIR contract 80NSSC19C0044
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: January 10, 2020
//------------------------------------------------------------------------------
/**
* Dialog to configure OpenFramesSensorMask objects
*/
//------------------------------------------------------------------------------

#include "OFSensorMaskDialog.hpp"
#include "OpenFramesSensorMask.hpp"

#include "gmatwxdefs.hpp"
#include "MessageInterface.hpp"
#include "Moderator.hpp"

#include "bitmaps/up.xpm"

#include <float.h>


//------------------------------
// event tables for wxWindows
//------------------------------
BEGIN_EVENT_TABLE(OFSensorMaskDialog, wxDialog)
   EVT_BUTTON(ID_BUTTON_OK, OFSensorMaskDialog::OnOK)
   EVT_BUTTON(ID_BUTTON_CANCEL, OFSensorMaskDialog::OnCancel)
   EVT_BUTTON(ID_BUTTON_HELP, OFSensorMaskDialog::OnHelp)
   EVT_CLOSE(OFSensorMaskDialog::OnSystemClose)

   EVT_COMBOBOX(ID_COMBOBOX_SENSORMASK_SOURCE, OFSensorMaskDialog::OnComboBoxSourceObjectChange)
   EVT_COMBOBOX(ID_COMBOBOX_SENSORMASK_HARDWARE, OFSensorMaskDialog::OnComboBoxHardwareFOVChange)
   EVT_COMBOBOX(ID_COMBOBOX_SENSORMASK_LENGTH, OFSensorMaskDialog::OnComboBoxSensorMaskLengthChange)
   EVT_TEXT(ID_TEXTCTRL, OFSensorMaskDialog::OnTextChange)
   EVT_TEXT(ID_SPINCTRL, OFSensorMaskDialog::OnSpinChange)
END_EVENT_TABLE()


const wxString OFSensorMaskDialog::DIALOG_TITLE = "Mask Definition";
const wxUniChar OFSensorMaskDialog::HARDWARE_FOV_SEPARATOR = '-';

//------------------------------------------------------------------------------
// OFSensorMaskDialog(wxWindow *parent, const wxString &subscriberName)
//------------------------------------------------------------------------------
/**
* Constructs OFSensorMaskDialog object.
*
* @param <forObject> The mask to change settings for
* @param <objects> List of possible objects to point the mask from
* @param <parent> input parent
* @param <enableUpdate> Enable apply mode immediately (for new masks)
*
* @note Creates the OFSensorMaskDialog GUI
*/
//------------------------------------------------------------------------------

OFSensorMaskDialog::OFSensorMaskDialog(OpenFramesSensorMask *forObject, const wxArrayString &objects, wxWindow *parent, bool enableUpdate) :
wxDialog(parent, -1, DIALOG_TITLE, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE | wxMAXIMIZE_BOX),
mSensorMask(forObject),
mObjects(objects),
mDataChanged(enableUpdate),
mCanClose(true)
{
   FilterObjectsWithFOV();
   if(!mObjects.IsEmpty())
   {
      InitializeData();
      Create();
      Refresh();
      Show();
      EnableUpdate(enableUpdate);
   }
   
   // shortcut keys
   wxAcceleratorEntry entries[2];
   entries[0].Set(wxACCEL_NORMAL, WXK_F1, ID_BUTTON_HELP);
   entries[1].Set(wxACCEL_CTRL, static_cast<int>('W'), ID_BUTTON_CANCEL);
   wxAcceleratorTable accel(2, entries);
   this->SetAcceleratorTable(accel);
}


//------------------------------------------------------------------------------
// ~OFSensorMaskDialog()
//------------------------------------------------------------------------------
/**
* Destructor
*/
OFSensorMaskDialog::~OFSensorMaskDialog()
{
	// nothing to clean up here
}


//------------------------------------------------------------------------------
// void InitializeData()
//------------------------------------------------------------------------------
/**
* Initializes data that is not wxWidgets
*/
void OFSensorMaskDialog::InitializeData()
{
   mDataChanged = false;
   mCanClose = true;
}


//------------------------------------------------------------------------------
// void FilterObjectsWithFOV()
//------------------------------------------------------------------------------
/**
* Filter the mObjects list and only keep those objects that have a FieldOfView attached to them
*/
void OFSensorMaskDialog::FilterObjectsWithFOV()
{
   wxArrayString filteredObjects;
   Moderator *theModerator = Moderator::Instance();
   wxArrayString hwNames, fovNames, displayHWFOVNames;
   
   // Loop through all possible object names
   for(auto objNameItr = mObjects.begin(); objNameItr != mObjects.end(); ++objNameItr)
   {
      // Save object if it has attached FieldOfView
      GetHardwareWithFOV(*objNameItr, hwNames, fovNames, displayHWFOVNames);
      if(!fovNames.IsEmpty()) filteredObjects.Add(*objNameItr);
   }
   
   // Only allow objects with FOV to be selected by the user
   mObjects = filteredObjects;
}

//------------------------------------------------------------------------------
// void GetHardwareWithFOV(const wxString objName, wxArrayString& hwNames, wxArrayString& fovNames)
//------------------------------------------------------------------------------
/**
* For given GmatBase object, get list of each Hardware that has attached FieldOfView.
 Note that there is a 1:1 relationship between Hardware array and FieldOfView array, since it is assumed that each
 Hardware can only contain 1 FieldOfView object.
*/
void OFSensorMaskDialog::GetHardwareWithFOV(wxString objName, wxArrayString& hwNames, wxArrayString& fovNames, wxArrayString& displayHWFOVNames)
{
   hwNames.Empty();
   fovNames.Empty();
   
   // Get the configured GMAT object
   // Note configured objects aren't connected together, so we can't directly get their
   // components. So we have to get names of their components, then poll the Moderator
   // for the corresponding configured objects.
   Moderator *theModerator = Moderator::Instance();
   GmatBase *obj = theModerator->GetConfiguredObject(objName.ToStdString());
   if(obj == nullptr) return;

   // Get names of all Hardware attached to the object
   StringArray hwNameArray = obj->GetRefObjectNameArray(Gmat::HARDWARE);
   
   // Loop over name of each Hardware attached to the object
   for(auto hwNameItr = hwNameArray.begin(); hwNameItr != hwNameArray.end(); ++hwNameItr)
   {
      // Get the configured GMAT Hardware
      GmatBase *hwObj = theModerator->GetConfiguredObject(*hwNameItr);
      
      // Get the FieldOfView object from the configured Hardware
      std::string fovName = hwObj->GetRefObjectName(Gmat::FIELD_OF_VIEW);
      GmatBase *fovObj = theModerator->GetConfiguredObject(fovName);

      // Save Hardware if it has an FOV
      if(fovObj != nullptr)
      {
         hwNames.Add(*hwNameItr);
         fovNames.Add(fovName);
         displayHWFOVNames.Add(*hwNameItr + HARDWARE_FOV_SEPARATOR + fovName);
      }
   }
}

//------------------------------------------------------------------------------
// void Create()
//------------------------------------------------------------------------------
/**
* Creates wxWidgets and adds them to the panel
*/
void OFSensorMaskDialog::Create()
{
   int bsize = 2; // border size

   // create axis array
   wxArrayString emptyList;

   //-----------------------------------------------------------------
   // Main sizers and OK/Cancel buttons
   //-----------------------------------------------------------------
   mPanelSizer = new wxBoxSizer(wxVERTICAL);
   mMiddleSizer = new wxStaticBoxSizer(wxVERTICAL, this);
   mBottomSizer = new wxStaticBoxSizer(wxHORIZONTAL, this);

   // create bottom buttons
   mOkButton = new wxButton(this, ID_BUTTON_OK, "OK", wxDefaultPosition, wxDefaultSize, 0);
   mOkButton->SetToolTip("Apply Changes and Close");
   mCancelButton = new wxButton(this, ID_BUTTON_CANCEL, "Cancel", wxDefaultPosition, wxDefaultSize, 0);
   mCancelButton->SetToolTip("Close Without Applying Changes");
   mHelpButton = new wxButton(this, ID_BUTTON_HELP, GUI_ACCEL_KEY"Help", wxDefaultPosition, wxDefaultSize, 0);
   mHelpButton->SetToolTip("OpenFramesInterface Help (F1)");
   mOkButton->SetDefault();

   mBottomSizer->Add(mOkButton, 0, wxALIGN_LEFT | wxALL, bsize);
   mBottomSizer->Add(mCancelButton, 0, wxALIGN_LEFT | wxALL, bsize);
   mBottomSizer->Add(0, 1, wxALIGN_RIGHT);
   mBottomSizer->Add(mHelpButton, 0, wxALIGN_RIGHT | wxALL, bsize);

   //-----------------------------------------------------------------
   // Create the dialog
   //-----------------------------------------------------------------

   //-----------------------------------------------------------------
   // Mask Selection
   wxFlexGridSizer *maskSelectionFlexSizer = new wxFlexGridSizer(2, 0, 0);

   // Source Object combo box drop-down
   wxStaticText *maskSourceLabel = new wxStaticText(this, -1, wxT("Source Object"), wxDefaultPosition, wxDefaultSize, 0);
   mSensorMaskSource = new wxComboBox(this, ID_COMBOBOX_SENSORMASK_SOURCE, "Object List", wxDefaultPosition, wxDefaultSize, mObjects, wxCB_DROPDOWN | wxCB_READONLY);
   mSensorMaskSource->SetToolTip("Select source object");
   maskSelectionFlexSizer->Add(maskSourceLabel, 0, wxALIGN_RIGHT| wxALIGN_CENTER_VERTICAL | wxALL, bsize);
   maskSelectionFlexSizer->Add(mSensorMaskSource, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL | wxGROW, bsize);

   // Hardware-FOV combo box drop-down
   wxStaticText *maskHardwareFOVLabel = new wxStaticText(this, -1, wxT("Hardware-FOV"), wxDefaultPosition, wxDefaultSize, 0);
   mSensorMaskHardwareFOV = new wxComboBox(this, ID_COMBOBOX_SENSORMASK_HARDWARE, "Hardware-FOV List", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_DROPDOWN | wxCB_READONLY);
   mSensorMaskHardwareFOV->SetToolTip("Select Hardware-FOV pair");
   maskSelectionFlexSizer->Add(maskHardwareFOVLabel, 0, wxALIGN_RIGHT| wxALIGN_CENTER_VERTICAL | wxALL, bsize);
   maskSelectionFlexSizer->Add(mSensorMaskHardwareFOV, 0,
                               wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL | wxGROW, bsize);

   // Sizer to store the mask definition objects to the left in the window
   wxStaticBoxSizer *maskSelectionStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Mask Selection");
   maskSelectionStaticSizer->Add(maskSelectionFlexSizer, 0, wxALIGN_LEFT | wxALL | wxGROW, bsize);

   //-----------------------------------------------------------------
   // Mask Options
   wxFlexGridSizer *maskOptionsFlexSizer = new wxFlexGridSizer(3, 0, 0);

   // Mask Name
   wxStaticText *maskNameLabel = new wxStaticText(this, -1, wxT("Name"), wxDefaultPosition, wxDefaultSize, 0);
   mSensorMaskName = new wxTextCtrl(this, ID_TEXTCTRL, mSensorMask->GetName(), wxDefaultPosition, wxDefaultSize, 0);
   mSensorMaskName->SetToolTip("Mask Name");
   maskOptionsFlexSizer->Add(maskNameLabel, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
   maskOptionsFlexSizer->Add(mSensorMaskName, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
   maskOptionsFlexSizer->Add(0, 0);

   // Mask Label
   wxStaticText *maskLabelLabel = new wxStaticText(this, -1, wxT("Label"), wxDefaultPosition, wxDefaultSize, 0);
   mSensorMaskLabel = new wxTextCtrl(this, ID_TEXTCTRL, "New", wxDefaultPosition, wxDefaultSize, 0);
   mSensorMaskLabel->SetToolTip("Mask Label");
   maskOptionsFlexSizer->Add(maskLabelLabel, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
   maskOptionsFlexSizer->Add(mSensorMaskLabel, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL | wxGROW, bsize);
   maskOptionsFlexSizer->Add(0, 0);

   // Mask Length
   wxStaticText *maskLengthLabel = new wxStaticText(this, -1, wxT("Length"), wxDefaultPosition, wxDefaultSize, 0);
   mSensorMaskLengthType = new wxComboBox(this, ID_COMBOBOX_SENSORMASK_LENGTH, "Auto", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_DROPDOWN | wxCB_READONLY);
   mSensorMaskLengthType->SetToolTip("Select mask length type");
   mSensorMaskLengthValue = new wxSpinCtrlDouble(this, ID_SPINCTRL, "1.0", wxDefaultPosition, wxDefaultSize, 0);
   mSensorMaskLengthValue->SetRange(-DBL_MAX, DBL_MAX);
   mSensorMaskLengthValue->SetDigits(6U);
   mSensorMaskLengthValue->Disable();//Disable intially.
   maskOptionsFlexSizer->Add(maskLengthLabel, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
   maskOptionsFlexSizer->Add(mSensorMaskLengthType, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);
   maskOptionsFlexSizer->Add(mSensorMaskLengthValue, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, bsize);

   // Options sizer
   wxStaticBoxSizer *maskOptionsStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Mask Options");
   maskOptionsStaticSizer->Add(maskOptionsFlexSizer, 0, wxALIGN_LEFT | wxALL, bsize);


   //-----------------------------------------------------------------
   // Add to page sizer
   //-----------------------------------------------------------------

   wxFlexGridSizer *pageSizer = new wxFlexGridSizer(2, 0, 0);
   pageSizer->Add(maskSelectionStaticSizer, 0, wxALIGN_LEFT | wxGROW | wxALL, bsize);
   pageSizer->Add(maskOptionsStaticSizer, 0, wxALIGN_LEFT | wxGROW | wxALL, bsize);


   //-----------------------------------------------------------------
   // Add to middle sizer
   //-----------------------------------------------------------------
   mMiddleSizer->Add(pageSizer, 0, wxALIGN_CENTRE | wxALL, bsize);
}


//------------------------------------------------------------------------------
// void Refresh()
//------------------------------------------------------------------------------
/**
* Refreshes lists of objects within the panel.
*/
//------------------------------------------------------------------------------
void OFSensorMaskDialog::Refresh()
{
   //Add strings to the drop-down Mask Length combo box
   wxArrayString maskLengthTypes;
   for(int i = 0; i < OpenFramesSensorMask::NUM_MASK_LENGTH_TYPES; ++i)
   {
      wxString maskLengthType = mSensorMask->GetMaskLengthTypeString((OpenFramesSensorMask::MaskLengthType)i);
      maskLengthTypes.Add(maskLengthType);
   }
   mSensorMaskLengthType->Set(maskLengthTypes);

   //----------------------------------------------
   // Add available objects to the Source combo box
   mSensorMaskSource->Set(mObjects);
   mSensorMaskSource->SetSelection(0);
}


//------------------------------------------------------------------------------
// void Show()
//------------------------------------------------------------------------------
/**
* Shows the panel.
*/
//------------------------------------------------------------------------------
void OFSensorMaskDialog::Show()
{
   mPanelSizer->Add(mMiddleSizer, 1, wxGROW | wxALL, 1);
   mPanelSizer->Add(mBottomSizer, 0, wxGROW | wxALL, 1);

   SetAutoLayout(true); // tell the enclosing window to adjust to the size of the sizer
   SetSizer(mPanelSizer); //use the sizer for layout
   mPanelSizer->SetSizeHints(this); //set size hints to honour minimum size

   //Enable the Mask lengthctrl when loading data from the OpenFramesSensorMask and the
   //length type is Manual
   if (mSensorMask->GetSensorMaskAxisLengthType() == OpenFramesSensorMask::MANUAL)
   {
      mSensorMaskLengthValue->Enable();
   }

   LoadData();
   EnableUpdate(false);

   // call Layout() to force layout of the children anew
   mPanelSizer->Layout();
   Centre();
}

//------------------------------------------------------------------------------
// void LoadData()
//------------------------------------------------------------------------------
/**
* Loads data from mOFInterface to this instance's widgets
*/
void OFSensorMaskDialog::LoadData()
{
   try
   {
      // Load data from the core engine

      //----------------------------------------------
      // Load source object
      wxString sourceObject = mSensorMask->GetSensorMaskSource();
      int index = mObjects.Index(sourceObject);
      if (index == wxNOT_FOUND) mSensorMaskSource->SetSelection(0);
      else mSensorMaskSource->SetSelection(index);
      
      //----------------------------------------------
      // Add available Hardware-FPV pairs for the selected object
      
      // Get all Hardware and FOV for the selected object
      wxArrayString hwNames, fovNames, displayHWFOVNames;
      GetHardwareWithFOV(mSensorMaskSource->GetStringSelection(), hwNames, fovNames, displayHWFOVNames);
      
      // Display all Hardware-FOV pairs and and Select the appropriate one
      mSensorMaskHardwareFOV->Set(displayHWFOVNames);
      wxString hwObject = mSensorMask->GetSensorMaskHardware();
      index = hwNames.Index(hwObject);
      if(index == wxNOT_FOUND) mSensorMaskHardwareFOV->SetSelection(0);
      else mSensorMaskHardwareFOV->SetSelection(index);
      
      //----------------------------------------------
      //load mask label
      wxString maskLabel = mSensorMask->GetSensorMaskLabel();
      mSensorMaskLabel->SetValue(maskLabel);

      //----------------------------------------------
      // Load mask length type combo box
      mSensorMaskLengthType->SetSelection(mSensorMask->GetSensorMaskAxisLengthType());

      //----------------------------------------------
      // Load mask length
      mSensorMaskLengthValue->SetValue(mSensorMask->GetSensorMaskAxisLength());
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }
    
   EnableUpdate(false);
}



//------------------------------------------------------------------------------
// void SaveData()
//------------------------------------------------------------------------------
/**
* Applies changes buffered by the widgets to the mOFInterface
*/
bool OFSensorMaskDialog::SaveData()
{
   bool success = true;

   //-----------------------------------------------------------------
   // save values to base, base code should do the range checking
   //-----------------------------------------------------------------
   try
   {
      if (mDataChanged)
      {
         mSensorMask->SetSensorMaskSource(mSensorMaskSource->GetValue().WX_TO_STD_STRING);
         wxString currHardwareFOV = mSensorMaskHardwareFOV->GetStringSelection();
         wxString currHardware = currHardwareFOV.BeforeFirst(HARDWARE_FOV_SEPARATOR);
         mSensorMask->SetSensorMaskHardware(currHardware.WX_TO_STD_STRING);
         mSensorMask->SetSensorMaskLabel(mSensorMaskLabel->GetValue().WX_TO_STD_STRING);
         mSensorMask->SetSensorMaskAxisLengthType(mSensorMaskLengthType->GetValue().WX_TO_STD_STRING);
         mSensorMask->SetSensorMaskAxisLength(mSensorMaskLengthValue->GetValue());

         Moderator::Instance()->ConfigurationChanged(mSensorMask, true);
         EnableUpdate(false);
      }

      if (mSensorMask->GetName() != mSensorMaskName->GetValue().WX_TO_STD_STRING)
      {
         Moderator *theModerator = Moderator::Instance();
         success = theModerator->RenameObject(GmatType::GetTypeId("OpenFramesSensorMask"), mSensorMask->GetName(),
                                              mSensorMaskName->GetValue().WX_TO_STD_STRING);
      }
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
      success = false;
   }

   return success;
}


//------------------------------------------------------------------------------
// void OnComboBoxSourceObjectChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when Source Mask combo box changes
*
* @param <event> Details of the event
*/
void OFSensorMaskDialog::OnComboBoxSourceObjectChange(wxCommandEvent& event)
{
   // Display all Hardware-FOV pairs for the selected object
   wxArrayString hwNames, fovNames, displayHWFOVNames;
   wxString newMaskSource = mSensorMaskSource->GetStringSelection();
   GetHardwareWithFOV(newMaskSource, hwNames, fovNames, displayHWFOVNames);
   mSensorMaskHardwareFOV->Set(displayHWFOVNames);
   
   // If selected mask source equals source for OpenFramesSensorMask, then
   // select that source's saved Hardware-FOV pair
   wxString currMaskSource = mSensorMask->GetSensorMaskSource();
   if(newMaskSource == currMaskSource)
   {
      wxString hwObject = mSensorMask->GetSensorMaskHardware();
      int index = hwNames.Index(hwObject);
      if(index == wxNOT_FOUND) mSensorMaskHardwareFOV->SetSelection(0);
      else mSensorMaskHardwareFOV->SetSelection(index);
   }
   
   // Otherwise select the first available Hardware-FOV pair
   else mSensorMaskHardwareFOV->SetSelection(0);
   
   // Enable updates
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnComboBoxHardwareFOVChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when Hardware-FOV  combo box changes
*
* @param <event> Details of the event
*/
void OFSensorMaskDialog::OnComboBoxHardwareFOVChange(wxCommandEvent& event)
{
   // Enable updates
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnComboBoxSensorMaskLengthChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when Mask Length combo box changes
*
* @param <event> Details of the event
*/
void OFSensorMaskDialog::OnComboBoxSensorMaskLengthChange(wxCommandEvent& event)
{
   wxString maskLengthString = mSensorMaskLengthType->GetValue();

   if (!maskLengthString.compare("Manual")) mSensorMaskLengthValue->Enable();
   else mSensorMaskLengthValue->Disable();

   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnTextChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when a text box changes
*
* @param <event> Details of the event
*/
void OFSensorMaskDialog::OnTextChange(wxCommandEvent& event)
{
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnOk()
//------------------------------------------------------------------------------
/**
* Saves the data and closes the page
*
* @param <event> Details of the event
*/
//------------------------------------------------------------------------------
void OFSensorMaskDialog::OnOK(wxCommandEvent &event)
{
   if (mDataChanged)
   {
      bool success = SaveData();
      if (mCanClose && success) EndModal(wxID_OK);
   }
   else
   {
      if (mCanClose) EndModal(wxID_CANCEL);
   }
}


//------------------------------------------------------------------------------
// void OnCancel()
//------------------------------------------------------------------------------
/**
 * Close page without prompting about changes
*
* @param <event> Details of the event
*/
//------------------------------------------------------------------------------
void OFSensorMaskDialog::OnCancel(wxCommandEvent &event)
{
   EndModal(wxID_CANCEL);
}

//------------------------------------------------------------------------------
// void OnHelp()
//------------------------------------------------------------------------------
/**
 * Callback that shows help
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFSensorMaskDialog::OnHelp(wxCommandEvent &event)
{
   wxLaunchDefaultBrowser("https://gitlab.com/EmergentSpaceTechnologies/OpenFramesInterface/wikis/docs/user/current/SensorMask-Definition");
}

//------------------------------------------------------------------------------
// void OnSystemClose(wxCloseEvent& even)
//------------------------------------------------------------------------------
/**
 * Close page after prompting about changes.
*
* @param <event> Details of the event
*/
//------------------------------------------------------------------------------
void OFSensorMaskDialog::OnSystemClose(wxCloseEvent& event)
{
   // Prompt user if changes were made
   if (mDataChanged)
   {
      if (wxMessageBox(_T("There were changes made to \"" + GetTitle() + "\" panel"
         " which will be lost on Close. \nDo you want to close anyway?"),
         _T("Please Confirm Close"),
         wxICON_QUESTION | wxYES_NO) != wxYES)
      {
         event.Veto();
         return;
      }
   }

	EndModal(wxID_CANCEL);
}


//------------------------------------------------------------------------------
// virtual void EnableUpdate(bool enable = true)
//------------------------------------------------------------------------------
/**
* Changes the mode of the dialog based on whether there are data changes to
* apply
*
* @param <enable> If true, then application of data changes is enabled
*/
void OFSensorMaskDialog::EnableUpdate(bool enable)
{
   mDataChanged = enable;
   if (mOkButton != nullptr) mOkButton->Enable(enable);
}

//------------------------------------------------------------------------------
// void OnSpinChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when a spin control changes
*
* @param <event> Details of the event
*/
void OFSensorMaskDialog::OnSpinChange(wxCommandEvent& event)
{
   EnableUpdate(true);
}
