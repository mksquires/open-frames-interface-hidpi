//$Id$
//------------------------------------------------------------------------------
//                                  OFSensorMaskDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under SBIR contract 80NSSC19C0044
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: January 10, 2020
/**
*  @class OFSensorMaskDialog
*  Dialog to configure OpenFramesSensorMask objects
*/
//------------------------------------------------------------------------------

#ifndef OFSENSORMASKDIALOG_hpp
#define OFSENSORMASKDIALOG_hpp

#include "wx/wx.h"
#include "wx/spinctrl.h"

#include "GmatBase.hpp"

// Forward declaration of referenced types
class OpenFramesSensorMask;


class OFSensorMaskDialog : public wxDialog
{
public:
   /// Title for the dialog titlebar
   static const wxString DIALOG_TITLE;
   static const wxUniChar HARDWARE_FOV_SEPARATOR;

   OFSensorMaskDialog(OpenFramesSensorMask *forObject, const wxArrayString &objects, wxWindow *parent, bool enableUpdate);
   virtual ~OFSensorMaskDialog();

   bool isValid() const { return !mObjects.IsEmpty(); }

protected:
   /// Combo box to select the mask source object
   wxComboBox *mSensorMaskSource;
   /// Combo box to select the mask hardware
   wxComboBox *mSensorMaskHardwareFOV;

   /// OPTIONS
   /// Text box for renaming the mask
   wxTextCtrl *mSensorMaskName;
   /// Text box for renaming the mask label
   wxTextCtrl *mSensorMaskLabel;
   /// Combo box to select the mask length options
   wxComboBox *mSensorMaskLengthType;
   /// Sensor Mask Length input value (double)
   wxSpinCtrlDouble *mSensorMaskLengthValue;

   /// True when the dialog is in apply mode because there are data changes to
   /// be applied
   bool mDataChanged;
   /// The when the dialog is able to close (currently always true)
   bool mCanClose;

   /// The object for storing mask data
   OpenFramesSensorMask *mSensorMask;
   /// Possible source objects for the mask
   wxArrayString mObjects;
  
   /// A sizer to separate dialog buttons from the content page
   wxBoxSizer *mPanelSizer;
   /// A sizer for the content page
   wxStaticBoxSizer *mMiddleSizer;
   /// The top-level sizer for the dialog
   wxStaticBoxSizer *mBottomSizer;

   /// OK button, applies and closes
   wxButton *mOkButton;
   /// Cancel button, prompts to apply then closes
   wxButton *mCancelButton;
   /// Help button displays help
   wxButton *mHelpButton;

   // for initializing data
   void InitializeData();

   // Filter objecs that have a FieldOfView attached to them
   void FilterObjectsWithFOV();
   
   // Get hardware and associated FOV for given object
   static void GetHardwareWithFOV(wxString objName, wxArrayString& hwNames,
                                  wxArrayString& fovNames, wxArrayString& displayHWFOVNames);

   // methods based on GmatPanel
   virtual void Create();
   void Refresh();
   virtual void Show();
   void LoadData();
   bool SaveData();

   void OnOK(wxCommandEvent &event);
   void OnCancel(wxCommandEvent &event);
   void OnHelp(wxCommandEvent &event);
   void OnSystemClose(wxCloseEvent& event);

   void OnComboBoxSourceObjectChange(wxCommandEvent& event);
   void OnComboBoxHardwareFOVChange(wxCommandEvent& event);
   void OnComboBoxSensorMaskLengthChange(wxCommandEvent& event);
   void OnTextChange(wxCommandEvent& event);
   void EnableUpdate(bool enable);
   void OnSpinChange(wxCommandEvent& event);

   DECLARE_EVENT_TABLE();

   // IDs for the controls and the menu commands
   enum
   {
      ID_BUTTON_OK = 8000,
      ID_BUTTON_CANCEL,
      ID_BUTTON_HELP,
   };
   enum
   {
      ID_COMBOBOX = 15600,
      ID_COMBOBOX_SENSORMASK_SOURCE,
      ID_COMBOBOX_SENSORMASK_HARDWARE,
      ID_COMBOBOX_SENSORMASK_LENGTH,
      CHECKBOX,
      RADIOBUTTON,
      STATIC_TEXT,
      ID_TEXTCTRL,
      ID_TEXTCTRL_SENSORMASK_NAME,
      ID_SPINCTRL,
      COPY_UP_BUTTON,
   };
};

#endif
