//$Id$
//------------------------------------------------------------------------------
//                                  OFConfigurationPanel
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
//------------------------------------------------------------------------------
/**
 * Panel that is driven by the OpenFramesInterface object
 */
//------------------------------------------------------------------------------

#include "OFConfigurationPanel.hpp"
#include "OFStarsDialog.hpp"
#include "OFViewDialog.hpp"
#include "OFVectorDialog.hpp"
#include "OFSensorMaskDialog.hpp"
#include "OFGLOptionsDialog.hpp"
#include "OpenFramesInterface.hpp"
#include "OpenFramesView.hpp"
#include "OpenFramesVector.hpp"
#include "OpenFramesSensorMask.hpp"
#include "OFWindowProxyPanel.hpp"
#include "OFScene.hpp"

#include "SandboxException.hpp"
#include "MessageInterface.hpp"
#include "FileManager.hpp"
#include "GmatMdiChildFrame.hpp"
#include "Moderator.hpp"

#include <wx/aboutdlg.h>
#include <wx/textdlg.h>
#include <wx/utils.h>

#include "bitmaps/NewScript.xpm"
#include "bitmaps/up.xpm"
#include "bitmaps/down.xpm"
#include "bitmaps/Erase.xpm"

#include <functional>


//------------------------------
// event tables for wxWindows
//------------------------------
BEGIN_EVENT_TABLE(OFConfigurationPanel, wxPanel)
   EVT_BUTTON(ID_BUTTON_OK, OFConfigurationPanel::OnOK)
   EVT_BUTTON(ID_BUTTON_APPLY, OFConfigurationPanel::OnApply)
   EVT_BUTTON(ID_BUTTON_CANCEL, OFConfigurationPanel::OnCancel)
   EVT_BUTTON(ID_BUTTON_ABOUT, OFConfigurationPanel::OnAbout)
   EVT_BUTTON(ID_BUTTON_HELP, OFConfigurationPanel::OnHelp)
   EVT_BUTTON(ID_BUTTON_SCRIPT, OFConfigurationPanel::OnScript)

   EVT_BUTTON(ADD_SP_BUTTON, OFConfigurationPanel::OnAddSpacePoint)
   EVT_BUTTON(REMOVE_SP_BUTTON, OFConfigurationPanel::OnRemoveSpacePoint)
   EVT_BUTTON(CLEAR_SP_BUTTON, OFConfigurationPanel::OnClearSpacePoint)
   EVT_LISTBOX(ID_LISTBOX, OFConfigurationPanel::OnSelectAvailObject)
   EVT_LISTBOX(SC_SEL_LISTBOX, OFConfigurationPanel::OnSelectSpacecraft)
   EVT_LISTBOX(OBJ_SEL_LISTBOX, OFConfigurationPanel::OnSelectOtherObject)
   EVT_LISTBOX_DCLICK(ID_LISTBOX, OFConfigurationPanel::OnAddSpacePoint)
   EVT_LISTBOX_DCLICK(SC_SEL_LISTBOX, OFConfigurationPanel::OnRemoveSpacePoint)
   EVT_LISTBOX_DCLICK(OBJ_SEL_LISTBOX, OFConfigurationPanel::OnRemoveSpacePoint)
   EVT_BUTTON(NEW_VIEW_BUTTON, OFConfigurationPanel::OnNewView)
   EVT_BUTTON(NEW_VECTOR_BUTTON, OFConfigurationPanel::OnNewVector)
   EVT_BUTTON(NEW_MASK_BUTTON, OFConfigurationPanel::OnNewMask)
   EVT_BUTTON(ADD_VIEW_BUTTON, OFConfigurationPanel::OnAddView)
   EVT_BUTTON(ADD_VECTOR_BUTTON, OFConfigurationPanel::OnAddVector)
   EVT_BUTTON(ADD_MASK_BUTTON, OFConfigurationPanel::OnAddMask)
   EVT_BUTTON(EDIT_VIEW_BUTTON, OFConfigurationPanel::OnEditView)
   EVT_BUTTON(EDIT_VECTOR_BUTTON, OFConfigurationPanel::OnEditVector)
   EVT_BUTTON(EDIT_MASK_BUTTON, OFConfigurationPanel::OnEditMask)
   EVT_BUTTON(DELETE_VIEW_BUTTON, OFConfigurationPanel::OnDeleteView)
   EVT_BUTTON(DELETE_VECTOR_BUTTON, OFConfigurationPanel::OnDeleteVector)
   EVT_BUTTON(DELETE_MASK_BUTTON, OFConfigurationPanel::OnDeleteMask)
   EVT_BUTTON(VIEW_UP_BUTTON, OFConfigurationPanel::OnViewUp)
   EVT_BUTTON(VECTOR_UP_BUTTON, OFConfigurationPanel::OnVectorUp)
   EVT_BUTTON(MASK_UP_BUTTON, OFConfigurationPanel::OnMaskUp)
   EVT_BUTTON(VIEW_DOWN_BUTTON, OFConfigurationPanel::OnViewDown)
   EVT_BUTTON(VECTOR_DOWN_BUTTON, OFConfigurationPanel::OnVectorDown)
   EVT_BUTTON(MASK_DOWN_BUTTON, OFConfigurationPanel::OnMaskDown)
   EVT_LISTBOX(VIEW_SEL_LISTBOX, OFConfigurationPanel::OnSelectView)
   EVT_LISTBOX(VECTOR_SEL_LISTBOX, OFConfigurationPanel::OnSelectVector)
   EVT_LISTBOX(MASK_SEL_LISTBOX, OFConfigurationPanel::OnSelectMask)
   EVT_CHECKBOX(CHECKBOX, OFConfigurationPanel::OnCheckBoxChange)
   EVT_COMBOBOX(ID_COMBOBOX, OFConfigurationPanel::OnComboBoxChange)
   EVT_COMBOBOX_DROPDOWN(ID_COMBOBOX, OFConfigurationPanel::OnComboBoxDropdown)
   EVT_SPINCTRL(ID_SPINCTRL, OFConfigurationPanel::OnSpinChange)
   EVT_SPINCTRLDOUBLE(ID_SPINCTRL, OFConfigurationPanel::OnSpinChangeDouble)
   EVT_BUTTON(STAR_OPTIONS, OFConfigurationPanel::OnStarOptions)
   EVT_BUTTON(GL_OPTIONS, OFConfigurationPanel::OnGLOptions)
   EVT_MENU_RANGE(START_VIEW_MENU, END_VIEW_MENU, OFConfigurationPanel::OnExistingView)
   EVT_MENU_RANGE(START_VECTOR_MENU, END_VECTOR_MENU, OFConfigurationPanel::OnExistingVector)
   EVT_MENU_RANGE(START_MASK_MENU, END_MASK_MENU, OFConfigurationPanel::OnExistingMask)
END_EVENT_TABLE()


const wxBitmap OFConfigurationPanel::BITMAP_UP_ARROW(up_xpm);
const wxBitmap OFConfigurationPanel::BITMAP_DOWN_ARROW(down_xpm);
const wxBitmap OFConfigurationPanel::BITMAP_LEFT_ARROW(wxImage(up_xpm).Rotate90(false));
const wxBitmap OFConfigurationPanel::BITMAP_RIGHT_ARROW(wxImage(up_xpm).Rotate90(true));
const wxBitmap OFConfigurationPanel::BITMAP_REMOVE(Erase_xpm);


/// Client data specifying a spacecraft
Gmat::ObjectType OFConfigurationPanel::SPACECRAFT_CLIENT_DATA = Gmat::SPACECRAFT;
/// Client data specifying a ground station
Gmat::ObjectType OFConfigurationPanel::GROUND_STATION_CLIENT_DATA = Gmat::GROUND_STATION;
/// Client data specifying a celestial body
Gmat::ObjectType OFConfigurationPanel::CELESTIAL_BODY_CLIENT_DATA = Gmat::CELESTIAL_BODY;


//------------------------------------------------------------------------------
// OFConfigurationPanel(wxWindow *parent, const wxString &subscriberName)
//------------------------------------------------------------------------------
/**
 * Constructs OFConfigurationPanel object.
 *
 * @param <forObject> the object that this panel shall configure
 * @param <parent> input parent
 *
 * @note Creates the OFConfigurationPanel GUI
 */
//------------------------------------------------------------------------------
OFConfigurationPanel::OFConfigurationPanel(GmatBase *forObject, wxWindow *parent) :
    wxPanel(parent),
    mOFInterface(nullptr),
    mBufferInterface(nullptr),
    mHasRealDataChanged(false),
    mHasDrawingOptionChanged(false),
    mHasSpChanged(false),
    mHasIntegerDataChanged(false),
    mHasShowObjectChanged(false),
    mHasCoordSysChanged(false),
    mHasStarOptionChanged(false),
    mHasViewsChanged(false),
    mHasVectorsChanged(false),
    mHasMasksChanged(false),
    mHasBufferChanged(false),
    mHasTimeSyncChanged(false),
    mDataChanged(false),
    mCanClose(true)
{
   if (forObject->IsOfType("OpenFramesInterface"))
   {
      mOFInterface = static_cast<OpenFramesInterface *>(forObject);
   }

   if (mOFInterface != nullptr)
   {
      InitializeData();
      Create();
      Refresh();
      Show();
   }
   else
   {
      MessageInterface::PopupMessage
         (Gmat::WARNING_, "The object pointer has not been set\n");
   }

   // shortcut keys
   wxAcceleratorEntry entries[3];
   entries[0].Set(wxACCEL_NORMAL,  WXK_F1, ID_BUTTON_HELP);
   entries[1].Set(wxACCEL_NORMAL,  WXK_F7, ID_BUTTON_SCRIPT);
   entries[2].Set(wxACCEL_CTRL,  static_cast<int>('W'), ID_BUTTON_CANCEL);
   wxAcceleratorTable accel(3, entries);
   this->SetAcceleratorTable(accel);
}


//------------------------------------------------------------------------------
// ~OFConfigurationPanel()
//------------------------------------------------------------------------------
/**
 * Destructor
 */
OFConfigurationPanel::~OFConfigurationPanel()
{
   if (mBufferInterface != nullptr)
      delete mBufferInterface;
}


//------------------------------------------------------------------------------
// void RenameObject(const std::string &oldName, const std::string newName,
//                   const UnsignedInt ofType)
//------------------------------------------------------------------------------
void OFConfigurationPanel::RenameObject(const std::string &oldName,
                                        const std::string newName,
                                        const UnsignedInt ofType)
{
   // Check spacecraft
   if (ofType == Gmat::SPACECRAFT || ofType == Gmat::GROUND_STATION)
   {
      for (int ii = 0; ii < mExcludedScList.Count(); ii++)
      {
         if (mExcludedScList[ii].WX_TO_STD_STRING == oldName)
            mExcludedScList[ii] = newName;
      }
      for (int ii = 0; ii < mSpacecraftListBox->GetCount(); ii++)
      {
         if (mSpacecraftListBox->GetString(ii).WX_TO_STD_STRING == oldName)
            mSpacecraftListBox->SetString(ii, newName);
      }
      for (int ii =0; ii < mSelectedScListBox->GetCount(); ii++)
      {
         if (mSelectedScListBox->GetString(ii).WX_TO_STD_STRING == oldName)
            mSelectedScListBox->SetString(ii, newName);
      }
   }

   // Check celestial bodies
   if (ofType == Gmat::CELESTIAL_BODY)
   {
      for (int ii = 0; ii < mExcludedCelesPointList.Count(); ii++)
      {
         if (mExcludedCelesPointList[ii].WX_TO_STD_STRING == oldName)
            mExcludedCelesPointList[ii] = newName;
      }
      for (int ii = 0; ii < mCelesPointListBox->GetCount(); ii++)
      {
         if (mCelesPointListBox->GetString(ii).WX_TO_STD_STRING == oldName)
            mCelesPointListBox->SetString(ii, newName);
      }
      for (int ii =0; ii < mSelectedObjListBox->GetCount(); ii++)
      {
         if (mSelectedObjListBox->GetString(ii).WX_TO_STD_STRING == oldName)
            mSelectedObjListBox->SetString(ii, newName);
      }
   }

   // Check views (Does not seem to work for OpenFramesViews)
   if (ofType == GmatType::GetTypeId("OpenFramesView"))
   {
      for (int ii = 0; ii < mSelectedViewListBox->GetCount(); ii++)
      {
         if (mSelectedViewListBox->GetString(ii).WX_TO_STD_STRING == oldName)
            mSelectedViewListBox->SetString(ii, newName);
      }
   }

   // Check vectors 
   if (ofType == GmatType::GetTypeId("OpenFramesVector"))
   {
     for (int ii = 0; ii < mSelectedVectorListBox->GetCount(); ii++)
     {
       if (mSelectedVectorListBox->GetString(ii).WX_TO_STD_STRING == oldName)
         mSelectedVectorListBox->SetString(ii, newName);
     }
   }
   
   // Check sensor masks
   if (ofType == GmatType::GetTypeId("OpenFramesSensorMask"))
   {
     for (int ii = 0; ii < mSelectedMaskListBox->GetCount(); ii++)
     {
       if (mSelectedMaskListBox->GetString(ii).WX_TO_STD_STRING == oldName)
         mSelectedMaskListBox->SetString(ii, newName);
     }
   }

   // Check coordinate systems (Known not to work with Coordinate Systems)
   if (ofType == Gmat::COORDINATE_SYSTEM)
   {
      for (int ii = 0; ii < mCoordSysComboBox->GetCount(); ii++)
      {
         if (mCoordSysComboBox->GetString(ii).WX_TO_STD_STRING == oldName)
            mCoordSysComboBox->SetString(ii, newName);
      }
   }

   // Check possible parents for time synchronization
   if (ofType == Gmat::SUBSCRIBER)
   {
      for (int ii = 0; ii < mSyncParentCombobox->GetCount(); ii++)
      {
         if (mSyncParentCombobox->GetString(ii).WX_TO_STD_STRING == oldName)
            mSyncParentCombobox->SetString(ii, newName);
      }
   }
}


//------------------------------------------------------------------------------
// void UpdateObjectList(const UnsignedInt ofType)
//------------------------------------------------------------------------------
/**
 * Notifies a plugin widget that there has been a change in an object type
 *
 * @param ofType The object type that has changed (typically vis a new object)
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::UpdateObjectList(const UnsignedInt ofType)
{
   if (ofType == Gmat::CELESTIAL_BODY || ofType == Gmat::SPACECRAFT || ofType == Gmat::GROUND_STATION)
      Refresh();
   if (ofType == Gmat::COORDINATE_SYSTEM)
      Refresh();
   if (ofType == Gmat::SUBSCRIBER)
      RefreshPossibleParents();
}


//------------------------------------------------------------------------------
// void InitializeData()
//------------------------------------------------------------------------------
/**
 * Initializes data that is not wxWidgets
 */
void OFConfigurationPanel::InitializeData()
{
   mHasRealDataChanged = false;
   mHasDrawingOptionChanged = false;
   mHasSpChanged = false;
   mHasIntegerDataChanged = false;
   mHasShowObjectChanged = false;
   mHasCoordSysChanged = false;
   mDataChanged = false;
   mCanClose = true;
   mExcludedScList.empty();
   mExcludedCelesPointList.empty();
}


//------------------------------------------------------------------------------
// void Create()
//------------------------------------------------------------------------------
/**
 * Creates wxWidgets and adds them to the panel
 */
void OFConfigurationPanel::Create()
{
   // create axis array
   wxArrayString emptyList;

   //-----------------------------------------------------------------
   // Main sizers and buttons
   //-----------------------------------------------------------------
   mPanelSizer = new wxBoxSizer(wxVERTICAL);
   mMiddleSizer = (wxSizer*)(new wxStaticBoxSizer(wxVERTICAL, this));
   mBottomSizer = new wxStaticBoxSizer(wxVERTICAL, this );
   wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

   // create bottom buttons
   mScriptButton = new wxBitmapButton(this, ID_BUTTON_SCRIPT, wxBitmap(NewScript_xpm), wxDefaultPosition, wxDefaultSize, 4);
   mScriptButton->SetToolTip("Show Script (F7)");
   mOkButton = new wxButton(this, ID_BUTTON_OK, "OK", wxDefaultPosition, wxDefaultSize, 0);
   mOkButton->SetToolTip("Apply Changes and Close");
   mApplyButton = new wxButton(this, ID_BUTTON_APPLY, GUI_ACCEL_KEY"Apply", wxDefaultPosition, wxDefaultSize, 0);
   mApplyButton->SetToolTip("Apply Changes");
   mCancelButton = new wxButton(this, ID_BUTTON_CANCEL, "Cancel", wxDefaultPosition, wxDefaultSize, 0);
   mCancelButton->SetToolTip("Close Without Applying Changes");
   mAboutButton = new wxButton(this, ID_BUTTON_ABOUT, "About", wxDefaultPosition, wxDefaultSize, 0);
   mAboutButton->SetToolTip("About the OpenFramesInterface");
   mHelpButton = new wxButton(this, ID_BUTTON_HELP, GUI_ACCEL_KEY"Help", wxDefaultPosition, wxDefaultSize, 0);
   mHelpButton->SetToolTip("OpenFramesInterface Help (F1)");
   mApplyButton->SetDefault();

   buttonSizer->Add(mScriptButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   buttonSizer->AddSpacer(10);
   buttonSizer->Add(mOkButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   buttonSizer->Add(mApplyButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   buttonSizer->Add(mCancelButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   buttonSizer->Add(0, 1, wxALIGN_RIGHT);
   buttonSizer->Add(mAboutButton, 0, wxALIGN_RIGHT | wxALL, BORDER_SIZE);
   buttonSizer->Add(mHelpButton, 0, wxALIGN_RIGHT | wxALL, BORDER_SIZE);

   mBottomSizer->Add(buttonSizer, 0, wxGROW | wxALL, BORDER_SIZE);

   //-----------------------------------------------------------------
   // Data collect and update frequency
   //-----------------------------------------------------------------
   mEnableStarsCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Enable Stars"), wxDefaultPosition, wxDefaultSize, 0);
   mStarsButton = new wxButton(this, STAR_OPTIONS, OFStarsDialog::DIALOG_TITLE, wxDefaultPosition, wxDefaultSize, 4);

   //-----------------------------------------------------------------
   // Show plot
   //-----------------------------------------------------------------
   mShowPlotCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Show Plot"), wxDefaultPosition, wxSize(-1, -1), BORDER_SIZE);
   mShowToolbarCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Show Toolbar"), wxDefaultPosition, wxSize(-1, -1), BORDER_SIZE);

   //-----------------------------------------------------------------
   // Parent time synchronization
   //-----------------------------------------------------------------
   wxStaticText *timeSyncLabel = new wxStaticText(this, -1, wxT("Time Synchronization"), wxDefaultPosition, wxSize(-1,-1), 0);
   mSyncParentCombobox = new wxComboBox(this, ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxDefaultSize, emptyList, wxCB_READONLY);
   mSyncParentCombobox->AppendString("Clock Time");
   mSyncParentCombobox->SetSelection(0);

   //-----------------------------------------------------------------
   // Parent time synchronization
   //-----------------------------------------------------------------
   mGLOptionsButton = new wxButton(this, GL_OPTIONS, OFGLOptionsDialog::DIALOG_TITLE, wxDefaultPosition, wxDefaultSize, 0);

   wxFlexGridSizer *plotOptionSizer = new wxFlexGridSizer(2, 0, 0);
   plotOptionSizer->Add(mEnableStarsCheckBox, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   plotOptionSizer->Add(mStarsButton, 1, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   plotOptionSizer->AddSpacer(10);
   plotOptionSizer->AddSpacer(10);
   plotOptionSizer->Add(mShowPlotCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   plotOptionSizer->AddSpacer(0);
   plotOptionSizer->Add(mShowToolbarCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   plotOptionSizer->AddSpacer(0);
   plotOptionSizer->AddSpacer(10);
   plotOptionSizer->AddSpacer(10);
   plotOptionSizer->Add(timeSyncLabel, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   plotOptionSizer->Add(mSyncParentCombobox, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   plotOptionSizer->AddSpacer(10);
   plotOptionSizer->AddSpacer(10);
   plotOptionSizer->AddSpacer(0);
   plotOptionSizer->Add(mGLOptionsButton, 0, wxALIGN_RIGHT|wxALL, BORDER_SIZE);
   plotOptionSizer->AddSpacer(10);
   plotOptionSizer->AddSpacer(10);

   wxStaticBoxSizer *plotOptionStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Plot Option");
   plotOptionStaticSizer->Add(plotOptionSizer, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);

   //-----------------------------------------------------------------
   // Drawing option
   //-----------------------------------------------------------------
   mXYPlaneCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw XY Plane"), wxDefaultPosition, wxSize(-1, -1), 0);
   mEclipticCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw Ecliptic Plane"), wxDefaultPosition, wxSize(-1, -1), 0);
   mAxesCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw Axes"), wxDefaultPosition, wxSize(-1, -1), 0);
   mAxesCheckBoxSpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "1.0", wxDefaultPosition, wxDefaultSize, 0);
   mAxesCheckBoxSpinCtrl->SetRange(0.0, 99999999.0);
   mAxesCheckBoxSpinCtrl->SetDigits(4U);
   mShowAxesLabelsCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Show Axes Labels"), wxDefaultPosition, wxSize(-1, -1), BORDER_SIZE);
   mFrameLabelCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Label Reference Frame"), wxDefaultPosition, wxSize(-1, -1), 0);

   // Solver Iteration ComboBox
   wxStaticText *solverIterLabel = new wxStaticText(this, -1, wxT("Solver Iterations"), wxDefaultPosition, wxSize(-1, -1), 0);
   mSolverIterComboBox = new wxComboBox(this, ID_COMBOBOX, wxT(""), wxDefaultPosition, wxDefaultSize, emptyList, wxCB_READONLY);

   // Get Solver Iteration option list from the Subscriber
   const std::string *solverIterList = OpenFramesInterface::GetSolverIterOptionList();
   int count = OpenFramesInterface::GetSolverIterOptionCount();
   for (int i=0; i<count; i++)
      mSolverIterComboBox->Append(solverIterList[i]);
   wxBoxSizer *solverIterOptionSizer = new wxBoxSizer(wxHORIZONTAL);
   solverIterOptionSizer->Add(solverIterLabel, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);
   solverIterOptionSizer->Add(mSolverIterComboBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);

   wxStaticText *SILastNStaticText = new wxStaticText(this, -1, wxT("Last N Iterations "), wxDefaultPosition, wxSize(-1,-1), 0);
   mSILastNCtrl = new wxSpinCtrl(this, ID_SPINCTRL, wxT("0"), wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS|wxSP_WRAP);
   mSILastNCtrl->SetRange(0, 10000);
   wxFlexGridSizer *SILastNSizer = new wxFlexGridSizer(2, 0, 0);
   SILastNSizer->Add(SILastNStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   SILastNSizer->Add(mSILastNCtrl, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   mSILastNCtrl->Disable();

   
   wxFlexGridSizer *axesCheckBoxSizer = new wxFlexGridSizer(2, 0, 0);
   axesCheckBoxSizer->Add(mAxesCheckBox, 0, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL | wxALL, BORDER_SIZE);
   axesCheckBoxSizer->Add(mAxesCheckBoxSpinCtrl, 1, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, BORDER_SIZE);

   //-----------------------------------------------------------------
   // Draw option
   //-----------------------------------------------------------------
   wxBoxSizer *drawOptionSizer = new wxBoxSizer(wxVERTICAL);
   drawOptionSizer->Add(20, 2, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   drawOptionSizer->Add(mXYPlaneCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   drawOptionSizer->Add(mEclipticCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   drawOptionSizer->AddSpacer(10);
   //drawOptionSizer->Add(mAxesCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   drawOptionSizer->Add(axesCheckBoxSizer, 0, wxALIGN_LEFT | wxALL, BORDER_SIZE);
   drawOptionSizer->Add(mShowAxesLabelsCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   drawOptionSizer->Add(mFrameLabelCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   drawOptionSizer->AddSpacer(10);
   drawOptionSizer->Add(solverIterOptionSizer, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   drawOptionSizer->Add(SILastNSizer, 0, wxALIGN_CENTER, BORDER_SIZE);

   drawOptionSizer->Add(20, 2, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);

   wxStaticBoxSizer *drawOptionStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Drawing Option");
   drawOptionStaticSizer->Add(drawOptionSizer, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);

   //-----------------------------------------------------------------
   // Available spacecrafts and objects
   //-----------------------------------------------------------------
   wxSize fontSize = GetFont().GetPixelSize();
   int lbHeight = 10 * fontSize.GetHeight();
   wxStaticText *scAvailableLabel = new wxStaticText(this, -1, wxT("Spacecraft && Ground Station"), wxDefaultPosition, wxSize(-1,-1), 0);
   mSpacecraftListBox = new wxListBox(this, ID_LISTBOX, wxDefaultPosition, wxSize(150, lbHeight), emptyList, wxLB_SINGLE|wxLB_SORT|wxLB_HSCROLL);
   wxStaticText *coAvailableLabel = new wxStaticText(this, -1, wxT("Celestial Object"), wxDefaultPosition, wxSize(-1,-1), 0);
   mCelesPointListBox = new wxListBox(this, ID_LISTBOX, wxDefaultPosition, wxSize(150, lbHeight), emptyList, wxLB_SINGLE|wxLB_SORT|wxLB_HSCROLL);

   wxBoxSizer *availObjSizer = new wxBoxSizer(wxVERTICAL);
   availObjSizer->Add(scAvailableLabel, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   availObjSizer->Add(mSpacecraftListBox, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   availObjSizer->Add(coAvailableLabel, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   availObjSizer->Add(mCelesPointListBox, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);

   //-----------------------------------------------------------------
   // add, remove, clear buttons
   //-----------------------------------------------------------------
   addScButton = new wxBitmapButton(this, ADD_SP_BUTTON, BITMAP_RIGHT_ARROW, wxDefaultPosition, wxDefaultSize, 4);
   addScButton->SetToolTip("Add object or body to plot");
   removeScButton = new wxBitmapButton(this, REMOVE_SP_BUTTON, BITMAP_LEFT_ARROW, wxDefaultPosition, wxDefaultSize, 4);
   removeScButton->SetToolTip("Remove object or body from plot");
   clearScButton = new wxBitmapButton(this, CLEAR_SP_BUTTON, BITMAP_REMOVE, wxDefaultPosition, wxDefaultSize, 4);
   clearScButton->SetToolTip("Clear all objects or all bodies from plot");

   wxBoxSizer *arrowButtonsSizer = new wxBoxSizer(wxVERTICAL);
   arrowButtonsSizer->Add(addScButton, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   arrowButtonsSizer->Add(removeScButton, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   arrowButtonsSizer->Add(clearScButton, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);

   //-----------------------------------------------------------------
   // Selected spacecraft and objects
   //-----------------------------------------------------------------
   wxStaticText *titleSelectedSc = new wxStaticText(this, -1, wxT("Selected SC && GS"), wxDefaultPosition, wxSize(-1,-1), 0);
   wxStaticText *titleSelectedObj = new wxStaticText(this, -1, wxT("Selected Celestial Object"), wxDefaultPosition, wxSize(-1,-1), 0);
   mSelectedScListBox = new wxListBox(this, SC_SEL_LISTBOX, wxDefaultPosition, wxSize(150, lbHeight), emptyList, wxLB_SINGLE);
   mSelectedObjListBox = new wxListBox(this, OBJ_SEL_LISTBOX, wxDefaultPosition, wxSize(150, lbHeight), emptyList, wxLB_SINGLE);

   wxBoxSizer *mObjSelectedSizer = new wxBoxSizer(wxVERTICAL);
   mObjSelectedSizer->Add(titleSelectedSc, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   mObjSelectedSizer->Add(mSelectedScListBox, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   mObjSelectedSizer->Add(titleSelectedObj, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   mObjSelectedSizer->Add(mSelectedObjListBox, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);

   //-----------------------------------------------------------------
   // Draw object, orbit and target color
   //-----------------------------------------------------------------
   mDrawObjectCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw Object"), wxDefaultPosition, wxSize(-1, -1), 0);
   mDrawTrajectoryCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw Trajectory"), wxDefaultPosition, wxSize(-1, -1), 0);
   mDrawAxesCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw Axes"), wxDefaultPosition, wxSize(-1, -1), 0);
   mDrawXYPlaneCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw XY Plane"), wxDefaultPosition, wxSize(-1, -1), 0);
   mDrawGridCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw Grid"), wxDefaultPosition, wxSize(-1, -1), 0);
   mDrawBodyLabelBox = new wxCheckBox(this, CHECKBOX, wxT("Label Body"), wxDefaultPosition, wxSize(-1, -1), 0);
   mDrawPropLabelBox = new wxCheckBox(this, CHECKBOX, wxT("Label Segments"), wxDefaultPosition, wxSize(-1, -1), 0);
   mDrawCenterPointCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw Center Marker"), wxDefaultPosition, wxSize(-1, -1), 0);
   mDrawEndPointsCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw End Markers"), wxDefaultPosition, wxSize(-1, -1), 0);
   mDrawVelocityCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Draw Velocity"), wxDefaultPosition, wxSize(-1, -1), 0);
   mMarkerSizeStaticText = new wxStaticText(this, -1, wxT("Marker Size "), wxDefaultPosition, wxSize(-1,-1), 0);
   wxStaticText *lineWidthStaticText = new wxStaticText(this, -1, wxT("Line Width "), wxDefaultPosition, wxSize(-1,-1), 0);
   wxSize theSize;
   theSize.SetWidth(mDrawCenterPointCheckBox->GetSize().GetWidth() - mMarkerSizeStaticText->GetSize().GetWidth() - BORDER_SIZE);
   theSize.SetHeight(-1);
   mMarkerSizeTextCtrl = new wxSpinCtrl(this, ID_SPINCTRL, wxT("10"), wxDefaultPosition, theSize, wxSP_ARROW_KEYS|wxSP_WRAP);
   mMarkerSizeTextCtrl->SetRange(1, 10000);
   mLineWidthTextCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, wxT("2.0"), wxDefaultPosition, theSize, wxSP_ARROW_KEYS|wxSP_WRAP);
   mLineWidthTextCtrl->SetRange(1.0, 10000.0);
   mLineWidthTextCtrl->SetDigits(2U);
   mLineWidthTextCtrl->SetIncrement(0.5);

   wxStaticText *fontSizeText = new wxStaticText(this, -1, wxT("Font Size"), wxDefaultPosition, wxSize(-1, -1), 0);
   mFontSizeSpinCtrl = new wxSpinCtrl(this, ID_SPINCTRL, wxT("14"), wxDefaultPosition, theSize, wxSP_ARROW_KEYS | wxSP_WRAP);
   mFontSizeSpinCtrl->SetRange(1, 100);

   wxStaticText *fontPositionText = new wxStaticText(this, -1, wxT("Font Position"), wxDefaultPosition, wxSize(-1, -1), 0);
   mFontPositionComboBox = new wxComboBox(this, ID_COMBOBOX, "Top-Right", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_DROPDOWN | wxCB_READONLY);
   //Add options to the drop-down combo box
   wxString fontPositionList[4] = { "Top-Right", "Top-Left", "Bottom-Right", "Bottom-Left" };
   for (int i = 0; i < 4; i++)
     mFontPositionComboBox->Append(fontPositionList[i]);

   //mFontPositionComboBox->Select(0);
     
   wxFlexGridSizer *scOptionSizer1 = new wxFlexGridSizer(1, 0, 0);
   scOptionSizer1->Add(mDrawObjectCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   scOptionSizer1->Add(mDrawTrajectoryCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   scOptionSizer1->Add(mDrawAxesCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   scOptionSizer1->Add(mDrawXYPlaneCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   scOptionSizer1->Add(mDrawGridCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   scOptionSizer1->Add(mDrawBodyLabelBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   scOptionSizer1->Add(mDrawPropLabelBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   scOptionSizer1->Add(mDrawCenterPointCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   scOptionSizer1->Add(mDrawEndPointsCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   scOptionSizer1->Add(mDrawVelocityCheckBox, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);

   wxFlexGridSizer *lineWidthSizer = new wxFlexGridSizer(2, 0, 0);
   lineWidthSizer->Add(mMarkerSizeStaticText, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   lineWidthSizer->Add(mMarkerSizeTextCtrl, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   lineWidthSizer->Add(lineWidthStaticText, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   lineWidthSizer->Add(mLineWidthTextCtrl, 1, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   scOptionSizer1->Add(lineWidthSizer, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);
   //scOptionSizer1->Add(20, 10, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);

   wxFlexGridSizer *fontSizer = new wxFlexGridSizer(2, 0, 0);
   fontSizer->Add(fontSizeText, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, BORDER_SIZE);
   fontSizer->Add(mFontSizeSpinCtrl, 1, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, BORDER_SIZE);
   fontSizer->Add(fontPositionText, 0, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, BORDER_SIZE);
   fontSizer->Add(mFontPositionComboBox, 1, wxALIGN_LEFT | wxALIGN_CENTER_VERTICAL | wxALL, BORDER_SIZE);
   scOptionSizer1->Add(fontSizer, 0, wxALIGN_LEFT | wxALL, BORDER_SIZE);
   scOptionSizer1->Add(20, 10, 0, wxALIGN_LEFT | wxALL, BORDER_SIZE);

   mScOptionSizer = new wxBoxSizer(wxVERTICAL);
   mScOptionSizer->Add(scOptionSizer1, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);

   mObjectSizer = new wxFlexGridSizer(5, 0, 0);
   mObjectSizer->Add(availObjSizer, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   mObjectSizer->Add(arrowButtonsSizer, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   mObjectSizer->Add(mObjSelectedSizer, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   mObjectSizer->Add(mScOptionSizer, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);

   wxStaticBoxSizer *viewObjectStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Object Option");
   viewObjectStaticSizer->Add(mObjectSizer, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);

   //-----------------------------------------------------------------
   // Create the View Definition
   //-----------------------------------------------------------------
   wxStaticBoxSizer *viewDefStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "View Definition");

   // Create plot coordinate system
   // Done here so that the coord sys dropdown is selected in the right order when tabbing
   wxStaticText *coordSysLabel = new wxStaticText(this, -1, wxT("Coordinate System"), wxDefaultPosition, wxSize(-1,-1), 0);
   mCoordSysComboBox = new wxComboBox(this, ID_COMBOBOX, "Coordinate System", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_READONLY);
  
   // Create the listbox of views with the static text label "Views"
   wxStaticText *selectedViewsLabel = new wxStaticText(this, -1, wxT("Views"), wxDefaultPosition, wxSize(-1,-1), 0);
   mSelectedViewListBox = new wxListBox(this, VIEW_SEL_LISTBOX, wxDefaultPosition, wxSize(150, lbHeight), emptyList, wxLB_SINGLE|wxLB_HSCROLL);
   wxBoxSizer *selectedViewSelectSizer = new wxBoxSizer(wxVERTICAL);
   selectedViewSelectSizer->Add(selectedViewsLabel, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
   selectedViewSelectSizer->Add(mSelectedViewListBox, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);

   // Create a group of buttons to create and modify the views
   mViewDeleteButton = new wxButton(this, DELETE_VIEW_BUTTON, wxT("Delete Selected"), wxDefaultPosition, wxDefaultSize, 0);
   mViewDeleteButton->SetToolTip("Delete selected view");
   mViewNewButton = new wxButton(this, NEW_VIEW_BUTTON, wxT("New View"), wxDefaultPosition, mViewDeleteButton->GetSize(), 0);
   mViewNewButton->SetToolTip("Create new view");
   mViewAddButton = new wxButton(this, ADD_VIEW_BUTTON, wxT("Add Existing"), wxDefaultPosition, mViewDeleteButton->GetSize(), 0);
   mViewAddButton->SetToolTip("Add existing view");
   mViewEditButton = new wxButton(this, EDIT_VIEW_BUTTON, wxT("Edit Selected"), wxDefaultPosition, mViewDeleteButton->GetSize(), 0);
   mViewEditButton->SetToolTip("Edit selected view");
   wxBoxSizer *viewEditButtonSizer = new wxBoxSizer(wxVERTICAL);
   viewEditButtonSizer->Add(mViewNewButton, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);
   viewEditButtonSizer->Add(mViewAddButton, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);
   viewEditButtonSizer->Add(mViewDeleteButton, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);
   viewEditButtonSizer->Add(mViewEditButton, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);

   // Create new bitmap button for the bitmap UP and DOWN arrows with tooltips
   mViewUpButton = new wxBitmapButton(this, VIEW_UP_BUTTON, BITMAP_UP_ARROW, wxDefaultPosition, wxDefaultSize, 4);
   mViewUpButton->SetToolTip("Move selected view up in the order");
   mViewDownButton = new wxBitmapButton(this, VIEW_DOWN_BUTTON, BITMAP_DOWN_ARROW, wxDefaultPosition, wxDefaultSize, 4);
   mViewDownButton->SetToolTip("Move selected view down in the order");
   wxBoxSizer *viewsButtonSizer = new wxBoxSizer(wxVERTICAL);
   viewsButtonSizer->Add(mViewUpButton, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);
   viewsButtonSizer->Add(mViewDownButton, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);

   //Added Button Up and Down arrows to the Views Sizer to group them together
   //viewEditButtonSizer->Add(viewsButtonSizer, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);

   // Group all components of the View Definition together
   wxFlexGridSizer *viewsEditorSizer = new wxFlexGridSizer(3, 0, 0);
   viewsEditorSizer->AddSpacer(10);
   viewsEditorSizer->Add(coordSysLabel, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   viewsEditorSizer->Add(mCoordSysComboBox, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, BORDER_SIZE);
   viewsEditorSizer->Add(viewsButtonSizer, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);
   viewsEditorSizer->Add(selectedViewSelectSizer, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);
   viewsEditorSizer->Add(viewEditButtonSizer, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);

   // Add all Vew Definition components to the View Definition sizer
   viewDefStaticSizer->Add(viewsEditorSizer, 0, wxALIGN_LEFT|wxALL, BORDER_SIZE);

   UpdateViewButtons();

   //-----------------------------------------------------------------
   // Create the Vector Definition
   //-----------------------------------------------------------------
   wxStaticBoxSizer *vectorDefStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Vector Definition");

   // Create the listbox of views with the static text label "Vectors"
   wxStaticText *selectedVectorLabel = new wxStaticText(this, -1, wxT("Vectors"), wxDefaultPosition, wxSize(-1, -1), 0);
   mSelectedVectorListBox = new wxListBox(this, VECTOR_SEL_LISTBOX, wxDefaultPosition, wxSize(150, lbHeight), emptyList, wxLB_SINGLE | wxLB_HSCROLL);
   mSelectedVectorListBox->SetToolTip("List of created vectors");
   wxBoxSizer *selectedVectorSelectSizer = new wxBoxSizer(wxVERTICAL);
   selectedVectorSelectSizer->Add(selectedVectorLabel, 0, wxALIGN_CENTRE | wxALL, BORDER_SIZE);
   selectedVectorSelectSizer->Add(mSelectedVectorListBox, 0, wxALIGN_CENTRE | wxALL, BORDER_SIZE);

   // Create a group of buttons to create and modify the vectors
   mVectorDeleteButton = new wxButton(this, DELETE_VECTOR_BUTTON, wxT("Delete Selected"), wxDefaultPosition, wxDefaultSize, 0);
   mVectorDeleteButton->SetToolTip("Delete selected vector");
   mVectorNewButton = new wxButton(this, NEW_VECTOR_BUTTON, wxT("New Vector"), wxDefaultPosition, mVectorDeleteButton->GetSize(), 0);
   mVectorNewButton->SetToolTip("Create new vector");
   mVectorAddButton = new wxButton(this, ADD_VECTOR_BUTTON, wxT("Add Existing"), wxDefaultPosition, mVectorDeleteButton->GetSize(), 0);
   mVectorAddButton->SetToolTip("Add existing vector");
   mVectorEditButton = new wxButton(this, EDIT_VECTOR_BUTTON, wxT("Edit Selected"), wxDefaultPosition, mVectorDeleteButton->GetSize(), 0);
   mVectorEditButton->SetToolTip("Edit selected vector");
   wxBoxSizer *vectorEditButtonSizer = new wxBoxSizer(wxVERTICAL);
   vectorEditButtonSizer->Add(mVectorNewButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   vectorEditButtonSizer->Add(mVectorAddButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   vectorEditButtonSizer->Add(mVectorDeleteButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   vectorEditButtonSizer->Add(mVectorEditButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);

   // Create new bitmap button for the bitmap UP and DOWN arrows with tooltips
   mVectorUpButton = new wxBitmapButton(this, VECTOR_UP_BUTTON, BITMAP_UP_ARROW, wxDefaultPosition, wxDefaultSize, 4);
   mVectorUpButton->SetToolTip("Move selected vector up in the order");
   mVectorDownButton = new wxBitmapButton(this, VECTOR_DOWN_BUTTON, BITMAP_DOWN_ARROW, wxDefaultPosition, wxDefaultSize, 4);
   mVectorDownButton->SetToolTip("Move selected vector down in the order");
   wxBoxSizer *vectorsButtonSizer = new wxBoxSizer(wxVERTICAL);
   vectorsButtonSizer->Add(mVectorUpButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   vectorsButtonSizer->Add(mVectorDownButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);

   // Group all components of the Vector Definition together
   wxFlexGridSizer *vectorsEditorSizer = new wxFlexGridSizer(3, 0, 0);
   vectorsEditorSizer->Add(vectorsButtonSizer, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   vectorsEditorSizer->Add(selectedVectorSelectSizer, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   vectorsEditorSizer->Add(vectorEditButtonSizer, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);

   // Add all Vew Definition components to the View Definition sizer
   vectorDefStaticSizer->Add(vectorsEditorSizer, 0, wxALIGN_LEFT | wxALL, BORDER_SIZE);

   UpdateVectorButtons();

   //-----------------------------------------------------------------
   // Create the Sensor Mask Definition
   //-----------------------------------------------------------------
   wxStaticBoxSizer *maskDefStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Sensor Mask Definition");

   // Create the listbox of views with the static text label "Sensor Masks"
   wxStaticText *selectedMaskLabel = new wxStaticText(this, -1, wxT("Sensor Masks"), wxDefaultPosition, wxSize(-1, -1), 0);
   mSelectedMaskListBox = new wxListBox(this, MASK_SEL_LISTBOX, wxDefaultPosition, wxSize(150, lbHeight), emptyList, wxLB_SINGLE | wxLB_HSCROLL);
   mSelectedMaskListBox->SetToolTip("List of created sensor masks");
   wxBoxSizer *selectedMaskSelectSizer = new wxBoxSizer(wxVERTICAL);
   selectedMaskSelectSizer->Add(selectedMaskLabel, 0, wxALIGN_CENTRE | wxALL, BORDER_SIZE);
   selectedMaskSelectSizer->Add(mSelectedMaskListBox, 0, wxALIGN_CENTRE | wxALL, BORDER_SIZE);

   // Create a group of buttons to create and modify the masks
   mMaskDeleteButton = new wxButton(this, DELETE_MASK_BUTTON, wxT("Delete Selected"), wxDefaultPosition, wxDefaultSize, 0);
   mMaskDeleteButton->SetToolTip("Delete selected sensor mask");
   mMaskNewButton = new wxButton(this, NEW_MASK_BUTTON, wxT("New Mask"), wxDefaultPosition, mMaskDeleteButton->GetSize(), 0);
   mMaskNewButton->SetToolTip("Create new sensor mask");
   mMaskAddButton = new wxButton(this, ADD_MASK_BUTTON, wxT("Add Existing"), wxDefaultPosition, mMaskDeleteButton->GetSize(), 0);
   mMaskAddButton->SetToolTip("Add existing sensor mask");
   mMaskEditButton = new wxButton(this, EDIT_MASK_BUTTON, wxT("Edit Selected"), wxDefaultPosition, mMaskDeleteButton->GetSize(), 0);
   mMaskEditButton->SetToolTip("Edit selected sensor mask");
   wxBoxSizer *maskEditButtonSizer = new wxBoxSizer(wxVERTICAL);
   maskEditButtonSizer->Add(mMaskNewButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   maskEditButtonSizer->Add(mMaskAddButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   maskEditButtonSizer->Add(mMaskDeleteButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   maskEditButtonSizer->Add(mMaskEditButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);

   // Create new bitmap button for the bitmap UP and DOWN arrows with tooltips
   mMaskUpButton = new wxBitmapButton(this, MASK_UP_BUTTON, BITMAP_UP_ARROW, wxDefaultPosition, wxDefaultSize, 4);
   mMaskUpButton->SetToolTip("Move selected sensor mask up in the order");
   mMaskDownButton = new wxBitmapButton(this, MASK_DOWN_BUTTON, BITMAP_DOWN_ARROW, wxDefaultPosition, wxDefaultSize, 4);
   mMaskDownButton->SetToolTip("Move selected sensor mask down in the order");
   wxBoxSizer *masksButtonSizer = new wxBoxSizer(wxVERTICAL);
   masksButtonSizer->Add(mMaskUpButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   masksButtonSizer->Add(mMaskDownButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);

   // Group all components of the Sensor Mask Definition together
   wxFlexGridSizer *masksEditorSizer = new wxFlexGridSizer(3, 0, 0);
   masksEditorSizer->Add(masksButtonSizer, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   masksEditorSizer->Add(selectedMaskSelectSizer, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
   masksEditorSizer->Add(maskEditButtonSizer, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);

   // Add all Sensor Mask Definition components to the Sensor Mask Definition sizer
   maskDefStaticSizer->Add(masksEditorSizer, 0, wxALIGN_LEFT | wxALL, BORDER_SIZE);

   UpdateMaskButtons();
   
   //Create an empty sizer to pad space to the right of Object Options 
   //wxStaticBoxSizer *emptyStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "");

   //-----------------------------------------------------------------
   // Add to page sizer
   //-----------------------------------------------------------------

   wxFlexGridSizer *pageSizer1 = new wxFlexGridSizer(2, 3, 0, 0);
   pageSizer1->Add(plotOptionStaticSizer, 0, wxALIGN_CENTRE | wxGROW | wxALL, BORDER_SIZE);
   pageSizer1->Add(viewObjectStaticSizer, 0, wxALIGN_CENTRE | wxGROW | wxALL, BORDER_SIZE);
   pageSizer1->Add(maskDefStaticSizer, 0, wxALIGN_CENTRE | wxGROW | wxALL, BORDER_SIZE);
   pageSizer1->Add(drawOptionStaticSizer, 0, wxALIGN_CENTRE | wxGROW | wxALL, BORDER_SIZE);
   pageSizer1->Add(viewDefStaticSizer, 0, wxALIGN_CENTRE | wxGROW | wxALL, BORDER_SIZE);
   pageSizer1->Add(vectorDefStaticSizer, 0, wxALIGN_CENTRE | wxGROW | wxALL, BORDER_SIZE);
   
   //-----------------------------------------------------------------
   // Add to middle sizer
   //-----------------------------------------------------------------
   wxBoxSizer *pageSizer = new wxBoxSizer(wxVERTICAL);
   pageSizer->Add(pageSizer1, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);

   mMiddleSizer->Add(pageSizer, 0, wxALIGN_CENTRE|wxALL, BORDER_SIZE);
}


//------------------------------------------------------------------------------
// void Refresh()
//------------------------------------------------------------------------------
/**
 * Refreshes lists of objects within the panel.
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::Refresh()
{
   Moderator *theModerator = Moderator::Instance();

   // Coordinate system objects
   bool selectionRemoved = false;
   int sel = mCoordSysComboBox->GetSelection();
   const StringArray &items = theModerator->GetListOfObjects(Gmat::COORDINATE_SYSTEM);
   for (StringArray::const_iterator it = items.begin(); it < items.end(); it++)
   {
      if (mCoordSysComboBox->FindString(*it, true) == wxNOT_FOUND)
         mCoordSysComboBox->Append(*it);
   }
   for (int ii = mCoordSysComboBox->GetCount() - 1; ii >= 0; ii--)
   {
      bool found = false;
      wxString entry = mCoordSysComboBox->GetString(ii);
      for (StringArray::const_iterator it = items.begin(); it < items.end(); it++)
      {
         if (entry == *it)
            found = true;
      }
      if (!found)
      {
         mCoordSysComboBox->Remove(ii, ii);
         if (sel == ii)
            selectionRemoved = true;
      }
   }
   if (selectionRemoved)
      mCoordSysComboBox->Select(0);

   // Spacecraft objects
   wxString selected = mSelectedScListBox->GetStringSelection();
   StringArray artificials;
   const StringArray spacecrafts = theModerator->GetListOfObjects(Gmat::SPACECRAFT);
   artificials.insert(artificials.end(), spacecrafts.begin(), spacecrafts.end());
   const StringArray groundStations = theModerator->GetListOfObjects(Gmat::GROUND_STATION);
   artificials.insert(artificials.end(), groundStations.begin(), groundStations.end());
   for (StringArray::const_iterator sp = artificials.begin(); sp < artificials.end(); sp++)
   {
      // Add to list if not excluded
      bool exists = false;
      if (mExcludedScList.Index(*sp) != wxNOT_FOUND)
         exists = true;
      if (mSelectedScListBox->FindString(*sp, true) != wxNOT_FOUND)
         exists = true;
      if (mSpacecraftListBox->FindString(*sp, true) != wxNOT_FOUND)
         exists = true;
      if (!exists)
      {
         if (std::find(spacecrafts.begin(), spacecrafts.end(), *sp) != spacecrafts.end())
            mSpacecraftListBox->Append(*sp, &SPACECRAFT_CLIENT_DATA);
         else
            mSpacecraftListBox->Append(*sp, &GROUND_STATION_CLIENT_DATA);
      }
   }
   // Remove if no longer exists
   for (int ii = mExcludedScList.GetCount() - 1; ii >= 0; ii--)
   {
      if (find(artificials.begin(), artificials.end(), mExcludedScList[ii]) == artificials.end())
         mExcludedScList.Remove(mExcludedScList[ii]);
   }
   for (int ii = mSelectedScListBox->GetCount() - 1; ii >= 0; ii--)
   {
      if (find(artificials.begin(), artificials.end(), mSelectedScListBox->GetString(ii)) == artificials.end())
         mSelectedScListBox->Delete(ii);
   }
   for (int ii = mSpacecraftListBox->GetCount() - 1; ii >= 0; ii--)
   {
      if (find(artificials.begin(), artificials.end(), mSpacecraftListBox->GetString(ii)) == artificials.end())
         mSpacecraftListBox->Delete(ii);
   }
   if (selected.Length() > 0)
   {
      // Select the old selection or the first item if old selection was removed
      if (mSelectedScListBox->FindString(selected, true) == wxNOT_FOUND)
         mSelectedScListBox->Select(0);
      else
         mSelectedScListBox->SetStringSelection(selected);
      int sel = mSelectedScListBox->GetSelection();
      Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType *>(mSelectedScListBox->GetClientData(sel));
      ShowSpacePointOption(mSelectedScListBox->GetString(sel), *objType);
   }
   // else no need to select an item

   // Celestial objects
   selected = mSelectedObjListBox->GetStringSelection();
   const StringArray &bodies = theModerator->GetListOfObjects(Gmat::CELESTIAL_BODY);
   for (StringArray::const_iterator sp = bodies.begin(); sp < bodies.end(); sp++)
   {
      // Add to list if not excluded
      bool exists = false;
      if (mExcludedCelesPointList.Index(*sp) != wxNOT_FOUND)
         exists = true;
      if (mSelectedObjListBox->FindString(*sp, true) != wxNOT_FOUND)
         exists = true;
      if (mCelesPointListBox->FindString(*sp, true) != wxNOT_FOUND)
         exists = true;
      if (!exists)
         mCelesPointListBox->Append(*sp, &CELESTIAL_BODY_CLIENT_DATA);
   }
   // Remove if no longer exists
   for (int ii = mExcludedCelesPointList.GetCount() - 1; ii >= 0; ii--)
   {
      if (find(bodies.begin(), bodies.end(), mExcludedCelesPointList[ii]) == bodies.end())
         mExcludedCelesPointList.Remove(mExcludedCelesPointList[ii]);
   }
   for (int ii = mSelectedObjListBox->GetCount() - 1; ii >= 0; ii--)
   {
      if (find(bodies.begin(), bodies.end(), mSelectedObjListBox->GetString(ii)) == bodies.end())
         mSelectedObjListBox->Delete(ii);
   }
   for (int ii = mCelesPointListBox->GetCount() - 1; ii >= 0; ii--)
   {
      if (find(bodies.begin(), bodies.end(), mCelesPointListBox->GetString(ii)) == bodies.end())
         mCelesPointListBox->Delete(ii);
   }
   if (selected.Length() > 0)
   {
      // Select the old selection or the first item if old selection was removed
      if (mSelectedObjListBox->FindString(selected, true) == wxNOT_FOUND)
         mSelectedObjListBox->Select(0);
      else
         mSelectedObjListBox->SetStringSelection(selected);
      int sel = mSelectedObjListBox->GetSelection();
      Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType *>(mSelectedObjListBox->GetClientData(sel));
      ShowSpacePointOption(mSelectedObjListBox->GetString(sel), *objType);
   }
   // else no need to select an item

   // Possible parents for time sync
   RefreshPossibleParents();
}


//------------------------------------------------------------------------------
// void Refresh()
//------------------------------------------------------------------------------
/**
 * Refreshes lists of possible parents within the panel.
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::RefreshPossibleParents()
{
   Moderator *theModerator = Moderator::Instance();

   wxString selected = mSyncParentCombobox->GetStringSelection();
   StringArray interfaces = theModerator->GetListOfObjects("OpenFramesInterface"); // Copy into a modifiable StringArray
   for (int ii = interfaces.size() - 1; ii >= 0; ii--)
   {
      if (interfaces[ii] == mOFInterface->GetName())
         // Remove this object
         interfaces.erase(interfaces.begin() + ii);
      else
      {
         // Recurse through parents to remove objects with this object as a parent
         // Infinite recursion is protected by a for loop
         StringArray parents = { interfaces[ii] };
         for (int jj = 0; jj < interfaces.size(); jj++)
         {
            GmatBase *obj = theModerator->GetConfiguredObject(parents[jj]);
            if (obj != nullptr)
            {
               parents.push_back(static_cast<OpenFramesInterface *>(obj)->GetStringParameter("TimeSyncParent"));
               if (parents.back() == mOFInterface->GetName())
               {
                  // This interface is already a parent, remove and break
                  interfaces.erase(interfaces.begin() + ii);
                  break;
               }
            }
            else
               // No more parents
               break;
         }
      }
   }
   for (auto of = interfaces.begin(); of < interfaces.end(); of++)
   {
      if (mSyncParentCombobox->FindString(*of, true) == wxNOT_FOUND)
         mSyncParentCombobox->Append(*of);
   }
   for (int ii = mSyncParentCombobox->GetCount() - 1; ii >= 0; ii--)
   {
      if (mSyncParentCombobox->GetString(ii) != "Clock Time")
         if (find(interfaces.begin(), interfaces.end(), mSyncParentCombobox->GetString(ii)) == interfaces.end())
            mSyncParentCombobox->Delete(ii);
   }
   if (mSyncParentCombobox->GetCount() > 1)
   {
      if (selected.Length() > 0)
      {
         bool found = mSyncParentCombobox->SetStringSelection(selected);
         if (!found)
            mSyncParentCombobox->Select(0U);
      }
   }
   else
   {
      if (selected != "Clock Time")
      {
         // If there aren't any possible parents, then change the option
         mSyncParentCombobox->Select(0);
         mHasTimeSyncChanged = true;
         EnableUpdate(true);
      }
   }
}


//------------------------------------------------------------------------------
// void Show()
//------------------------------------------------------------------------------
/**
 * Shows the panel
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::Show()
{
   mPanelSizer->Add(mMiddleSizer, 1, wxGROW | wxALL, 1);
   mPanelSizer->Add(mBottomSizer, 0, wxGROW | wxALL, 1);

   SetAutoLayout(true); // tell the enclosing window to adjust to the size of the sizer
   SetSizer(mPanelSizer); //use the sizer for layout
   mPanelSizer->SetSizeHints(this); //set size hints to honour minimum size

   LoadData();

   // call Layout() to force layout of the children anew
   mPanelSizer->Layout();
}


//------------------------------------------------------------------------------
// void LoadData()
//------------------------------------------------------------------------------
/**
 * Loads data from mOFInterface to this instance's widgets
 */
void OFConfigurationPanel::LoadData()
{
   bool enableUpdate = false;

   try
   {
      // Load data from the core engine
      wxString str;
      Moderator *theModerator = Moderator::Instance();

      mBufferInterface = static_cast<OpenFramesInterface *>(mOFInterface->Clone());

      mShowPlotCheckBox->SetValue(mOFInterface->GetBooleanParameter("ShowPlot"));
      mShowToolbarCheckBox->SetValue(mOFInterface->GetBooleanParameter("ShowToolbar"));
      mXYPlaneCheckBox->SetValue(mOFInterface->GetOnOffParameter("XYPlane") == OpenFramesInterface::ON_STRING);
      mEclipticCheckBox->SetValue(mOFInterface->GetOnOffParameter("EclipticPlane") == OpenFramesInterface::ON_STRING);
      mAxesCheckBox->SetValue(mOFInterface->GetOnOffParameter("Axes") == OpenFramesInterface::ON_STRING);
      mShowAxesLabelsCheckBox->SetValue(mOFInterface->GetOnOffParameter("AxesLabels") == OpenFramesInterface::ON_STRING);
      mFrameLabelCheckBox->SetValue(mOFInterface->GetOnOffParameter("FrameLabel") == OpenFramesInterface::ON_STRING);
      mEnableStarsCheckBox->SetValue(mOFInterface->GetOnOffParameter("EnableStars") == OpenFramesInterface::ON_STRING);

      mSolverIterComboBox->SetStringSelection(mOFInterface->GetStringParameter("SolverIterations"));
      if (mSolverIterComboBox->GetStringSelection() == "LastN")
         mSILastNCtrl->Enable();
      else
         mSILastNCtrl->Disable();
      mSILastNCtrl->SetValue(mOFInterface->GetIntegerParameter("SolverIterLastN"));

      // Update start count and star size items
      mStarsButton->Enable(mEnableStarsCheckBox->GetValue());

      // Time sync option
      std::string parent = mOFInterface->GetStringParameter("TimeSyncParent");
      if (parent.size() == 0)
         mSyncParentCombobox->SetSelection(0);
      else
      {
         bool found = mSyncParentCombobox->SetStringSelection(parent);
         if (!found)
         {
            // If parent is invalid, then change the option
            mSyncParentCombobox->Select(0);
            mHasTimeSyncChanged = true;
            enableUpdate = true;
         }
      }

      // Update axes items
      if (mAxesCheckBox->GetValue())
         mShowAxesLabelsCheckBox->Enable();
      else
         mShowAxesLabelsCheckBox->Disable();

      mAxesCheckBoxSpinCtrl->SetValue(mOFInterface->GetRealParameter("AxesLength")); 

      mCoordSysComboBox->SetStringSelection(mOFInterface->GetStringParameter("CoordinateSystem"));

      //--------------------------------------------------------------
      // get SpacePoint list to plot
      //--------------------------------------------------------------
      const StringArray &spNameList = mOFInterface->GetStringArrayParameter("Add");
      int spCount = spNameList.size();

      StringArray scNameArray;
      StringArray nonScNameArray;

      // get spacecraft and non-spacecraft list
      for (int i=0; i<spCount; i++)
      {
         if (mSpacecraftListBox->FindString(spNameList[i]) == wxNOT_FOUND)
            nonScNameArray.push_back(spNameList[i]);
         else
            scNameArray.push_back(spNameList[i]);
      }

      mScCount = scNameArray.size();
      mNonScCount = nonScNameArray.size();

      //--------------------------------------------------------------
      // get object show, color
      //--------------------------------------------------------------
      if (mScCount > 0)
      {
         wxString *scNames = new wxString[mScCount];
         Gmat::ObjectType **scTypes = new Gmat::ObjectType*[mScCount];
         for (int i=0; i<mScCount; i++)
         {
            scNames[i] = scNameArray[i];
            mDrawObjectMap[scNameArray[i]] = mOFInterface->GetObjectBool("DrawObject", scNameArray[i]);
            mDrawTrajectoryMap[scNameArray[i]] = mOFInterface->GetObjectBool("DrawTrajectory", scNameArray[i]);
            mDrawAxesMap[scNameArray[i]] = mOFInterface->GetObjectBool("DrawAxes", scNameArray[i]);
            mDrawXYPlaneMap[scNameArray[i]] = mOFInterface->GetObjectBool("DrawXYPlane", scNameArray[i]);
            mDrawLabelMap[scNameArray[i]] = mOFInterface->GetObjectBool("DrawLabel", scNameArray[i]);
            mUsePropagateLabelMap[scNameArray[i]] = mOFInterface->GetObjectBool("DrawUsePropLabel", scNameArray[i]);
            mDrawCenterPointMap[scNameArray[i]] = mOFInterface->GetObjectBool("DrawCenterPoint", scNameArray[i]);
            mDrawEndPointsMap[scNameArray[i]] = mOFInterface->GetObjectBool("DrawEndPoints", scNameArray[i]);
            mDrawVelocityMap[scNameArray[i]] = mOFInterface->GetObjectBool("DrawVelocity", scNameArray[i]);
            mDrawGridMap[scNameArray[i]] = mOFInterface->GetObjectBool("DrawGrid", scNameArray[i]);
            mDrawLineWidthMap[scNameArray[i]] = mOFInterface->GetObjectReal("DrawLineWidth", scNameArray[i]);
            mDrawMarkerSizeMap[scNameArray[i]] = mOFInterface->GetObjectUInt("DrawMarkerSize", scNameArray[i]);
            mDrawFontSizeMap[scNameArray[i]] = mOFInterface->GetObjectUInt("DrawFontSize", scNameArray[i]);
            mDrawFontPositionMap[scNameArray[i]] = mOFInterface->GetObjectString("DrawFontPosition", scNameArray[i]);
            // Remove from the available ListBox
            int ind = mSpacecraftListBox->FindString(scNames[i]);
            scTypes[i] = reinterpret_cast<Gmat::ObjectType*>(mSpacecraftListBox->GetClientData(ind));
            mSpacecraftListBox->Delete(ind);
            // Add to excluded list
            mExcludedScList.Add(scNames[i]);
         }
         mSelectedScListBox->Set(mScCount, scNames, reinterpret_cast<void**>(scTypes));
         delete [] scNames;
         delete [] scTypes;
      }

      if (mNonScCount > 0)
      {
         wxString *nonScNames = new wxString[mNonScCount];
         Gmat::ObjectType **nonScTypes = new Gmat::ObjectType*[mNonScCount];
         for (int i=0; i<mNonScCount; i++)
         {
            nonScNames[i] = nonScNameArray[i];
            mDrawObjectMap[nonScNameArray[i]] = mOFInterface->GetObjectBool("DrawObject", nonScNameArray[i]);
            mDrawTrajectoryMap[nonScNameArray[i]] = mOFInterface->GetObjectBool("DrawTrajectory", nonScNameArray[i]);
            mDrawAxesMap[nonScNameArray[i]] = mOFInterface->GetObjectBool("DrawAxes", nonScNameArray[i]);
            mDrawXYPlaneMap[nonScNameArray[i]] = mOFInterface->GetObjectBool("DrawXYPlane", nonScNameArray[i]);
            mDrawLabelMap[nonScNameArray[i]] = mOFInterface->GetObjectBool("DrawLabel", nonScNameArray[i]);
            mUsePropagateLabelMap[nonScNameArray[i]] = mOFInterface->GetObjectBool("DrawUsePropLabel", nonScNameArray[i]);
            mDrawCenterPointMap[nonScNameArray[i]] = mOFInterface->GetObjectBool("DrawCenterPoint", nonScNameArray[i]);
            mDrawEndPointsMap[nonScNameArray[i]] = mOFInterface->GetObjectBool("DrawEndPoints", nonScNameArray[i]);
            mDrawVelocityMap[nonScNameArray[i]] = mOFInterface->GetObjectBool("DrawVelocity", nonScNameArray[i]);
            mDrawGridMap[nonScNameArray[i]] = mOFInterface->GetObjectBool("DrawGrid", nonScNameArray[i]);
            mDrawLineWidthMap[nonScNameArray[i]] = mOFInterface->GetObjectReal("DrawLineWidth", nonScNameArray[i]);
            mDrawMarkerSizeMap[nonScNameArray[i]] = mOFInterface->GetObjectUInt("DrawMarkerSize", nonScNameArray[i]);
            mDrawFontSizeMap[nonScNameArray[i]] = mOFInterface->GetObjectUInt("DrawFontSize", nonScNameArray[i]);
            mDrawFontPositionMap[nonScNameArray[i]] = mOFInterface->GetObjectString("DrawFontPosition", nonScNameArray[i]);

            // Remove from the available ListBox
            int ind = mCelesPointListBox->FindString(nonScNames[i]);
            nonScTypes[i] = reinterpret_cast<Gmat::ObjectType*>(mCelesPointListBox->GetClientData(ind));
            mCelesPointListBox->Delete(ind);
            // Add to excluded list
            mExcludedCelesPointList.Add(nonScNames[i]);
         }

         mSelectedObjListBox->Set(mNonScCount, nonScNames, reinterpret_cast<void**>(nonScTypes));
         delete [] nonScNames;
         delete [] nonScTypes;
      }

      // show spacecraft option
      if (mSelectedScListBox->GetCount() > 0)
      {
         mSelectedScListBox->SetSelection(0);
         Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType *>(mSelectedScListBox->GetClientData(0));
         ShowSpacePointOption(mSelectedScListBox->GetString(0), *objType);
      }
      else if(mSelectedObjListBox->GetCount() > 0)
      {
         mSelectedObjListBox->SetSelection(0);
         Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType *>(mSelectedObjListBox->GetClientData(0));
         ShowSpacePointOption(mSelectedObjListBox->GetString(0), *objType);
      }

      //--------------------------------------------------------------
      // get Views list
      //--------------------------------------------------------------
      const StringArray &viewList = mOFInterface->GetStringArrayParameter("View");
      for (StringArray::const_iterator view = viewList.begin(); view < viewList.end(); view++)
      {
         const StringArray &views = theModerator->GetListOfObjects("OpenFramesView");
         if (std::find(views.begin(), views.end(), *view) == views.end())
         {
            // not found in array
            mHasViewsChanged = true;
            EnableUpdate(true);
         }
         else
         {
            // found
            mSelectedViewListBox->Append(*view);
         }
      }

      //--------------------------------------------------------------
      // get Vectors list
      //--------------------------------------------------------------
      const StringArray &vectorList = mOFInterface->GetStringArrayParameter("Vector");
      for (StringArray::const_iterator vector = vectorList.begin(); vector < vectorList.end(); vector++)
      {
        const StringArray &vectors = theModerator->GetListOfObjects("OpenFramesVector");
        if (std::find(vectors.begin(), vectors.end(), *vector) == vectors.end())
        {
          // not found in array
          mHasVectorsChanged = true;
          EnableUpdate(true);
        }
        else
        {
          // found
          mSelectedVectorListBox->Append(*vector);
        }
      }
      
      //--------------------------------------------------------------
      // get Sensor Masks list
      //--------------------------------------------------------------
      const StringArray &maskList = mOFInterface->GetStringArrayParameter("SensorMask");
      for (StringArray::const_iterator mask = maskList.begin(); mask < maskList.end(); mask++)
      {
        const StringArray &masks = theModerator->GetListOfObjects("OpenFramesSensorMask");
        if (std::find(masks.begin(), masks.end(), *mask) == masks.end())
        {
          // not found in array
          mHasMasksChanged = true;
          EnableUpdate(true);
        }
        else
        {
          // found
          mSelectedMaskListBox->Append(*mask);
        }
      }

   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }

   // deselect available object list
   mSpacecraftListBox->Deselect(mSpacecraftListBox->GetSelection());
   mCelesPointListBox->Deselect(mCelesPointListBox->GetSelection());

   EnableUpdate(enableUpdate);
}


//------------------------------------------------------------------------------
// void SaveData()
//------------------------------------------------------------------------------
/**
 * Applies changes buffered by the widgets to the mOFInterface
 */
void OFConfigurationPanel::SaveData()
{
   //-----------------------------------------------------------------
   // save values to base, base code should do the range checking
   //-----------------------------------------------------------------
   try
   {
      bool hasChanges = false;

      if (mHasBufferChanged)
      {
         hasChanges = true;
         (*mOFInterface) = (*mBufferInterface);
         mHasBufferChanged = false;
      }

      if (mHasTimeSyncChanged)
      {
         hasChanges = true;
         if (mSyncParentCombobox->GetSelection() > 0)
            mOFInterface->SetStringParameter("TimeSyncParent", mSyncParentCombobox->GetStringSelection().WX_TO_STD_STRING);
         else
            mOFInterface->SetStringParameter("TimeSyncParent", "");
      }

      //--------------------------------------------------------------
      // save drawing options
      //--------------------------------------------------------------
      if (mHasDrawingOptionChanged)
      {
         hasChanges = true;
         mHasDrawingOptionChanged = false;

         mOFInterface->SetBooleanParameter("ShowPlot", mShowPlotCheckBox->IsChecked());
         mOFInterface->SetBooleanParameter("ShowToolbar", mShowToolbarCheckBox->IsChecked());

         if (mShowAxesLabelsCheckBox->IsChecked())
            mOFInterface->SetOnOffParameter("AxesLabels", OpenFramesInterface::ON_STRING);
         else
            mOFInterface->SetOnOffParameter("AxesLabels", OpenFramesInterface::OFF_STRING);

         if (mXYPlaneCheckBox->IsChecked())
            mOFInterface->SetOnOffParameter("XYPlane", OpenFramesInterface::ON_STRING);
         else
            mOFInterface->SetOnOffParameter("XYPlane", OpenFramesInterface::OFF_STRING);

         if (mEclipticCheckBox->IsChecked())
            mOFInterface->SetOnOffParameter("EclipticPlane", OpenFramesInterface::ON_STRING);
         else
            mOFInterface->SetOnOffParameter("EclipticPlane", OpenFramesInterface::OFF_STRING);

         if (mAxesCheckBox->IsChecked())
         {
           mOFInterface->SetOnOffParameter("Axes", OpenFramesInterface::ON_STRING);
           mOFInterface->SetRealParameter("AxesLength", mAxesCheckBoxSpinCtrl->GetValue());
         }  
         else
            mOFInterface->SetOnOffParameter("Axes", OpenFramesInterface::OFF_STRING);

         if (mFrameLabelCheckBox->IsChecked())
            mOFInterface->SetOnOffParameter("FrameLabel", OpenFramesInterface::ON_STRING);
         else
            mOFInterface->SetOnOffParameter("FrameLabel", OpenFramesInterface::OFF_STRING);

         mOFInterface->SetStringParameter("SolverIterations", mSolverIterComboBox->GetValue().WX_TO_STD_STRING);

      }

      //--------------------------------------------------------------
      // save integer spinner data
      //--------------------------------------------------------------
      if (mHasIntegerDataChanged)
      {
         hasChanges = true;
         unsigned long value;
         mHasIntegerDataChanged = false;

         mOFInterface->SetIntegerParameter("SolverIterLastN", mSILastNCtrl->GetValue());
      }

      //--------------------------------------------------------------
      // save real spinner data
      //--------------------------------------------------------------
      if (mHasRealDataChanged)
      {
         hasChanges = true;
         Real value;
         mHasRealDataChanged = false;
      }

      //--------------------------------------------------------------
      // save star options
      //--------------------------------------------------------------
      if (mHasStarOptionChanged)
      {
         hasChanges = true;
         mHasStarOptionChanged = false;
         if (mEnableStarsCheckBox->IsChecked())
            mOFInterface->SetOnOffParameter("EnableStars", OpenFramesInterface::ON_STRING);
         else
            mOFInterface->SetOnOffParameter("EnableStars", OpenFramesInterface::OFF_STRING);
      }

      //--------------------------------------------------------------
      // save spacecraft list
      //--------------------------------------------------------------
      if (mHasSpChanged)
      {
         hasChanges = true;
         mHasSpChanged = false;
         mScCount = mSelectedScListBox->GetCount();
         mNonScCount = mSelectedObjListBox->GetCount();

         // clear the list first
         mOFInterface->TakeAction("Clear", "");

         // add spacecraft
         for (int i=0; i < mScCount; i++)
         {
            mSelSpName = mSelectedScListBox->GetString(i).WX_TO_STD_STRING;
            mOFInterface->SetStringParameter("Add", mSelSpName, i);
         }

         // add non-spacecraft
         for (int i=0; i < mNonScCount; i++)
         {
            mSelSpName = mSelectedObjListBox->GetString(i).WX_TO_STD_STRING;
            mOFInterface->SetStringParameter("Add", mSelSpName, mScCount+i);
         }
      }

      //--------------------------------------------------------------
      // save draw object
      //--------------------------------------------------------------
      if (mHasShowObjectChanged)
      {
         hasChanges = true;
         mHasShowObjectChanged = false;

         // change draw spacecraft
         for (int i=0; i<mScCount; i++)
         {
            mSelSpName = mSelectedScListBox->GetString(i).WX_TO_STD_STRING;
            mOFInterface->SetObjectBool("DrawObject", mSelSpName, mDrawObjectMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawTrajectory", mSelSpName, mDrawTrajectoryMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawAxes", mSelSpName, mDrawAxesMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawXYPlane", mSelSpName, mDrawXYPlaneMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawLabel", mSelSpName, mDrawLabelMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawUsePropLabel", mSelSpName, mUsePropagateLabelMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawCenterPoint", mSelSpName, mDrawCenterPointMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawEndPoints", mSelSpName, mDrawEndPointsMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawVelocity", mSelSpName, mDrawVelocityMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawGrid", mSelSpName, mDrawGridMap[mSelSpName]);
            mOFInterface->SetObjectReal("DrawLineWidth", mSelSpName, mDrawLineWidthMap[mSelSpName]);
            mOFInterface->SetObjectUInt("DrawMarkerSize", mSelSpName, mDrawMarkerSizeMap[mSelSpName]);
            mOFInterface->SetObjectUInt("DrawFontSize", mSelSpName, mDrawFontSizeMap[mSelSpName]);
            mOFInterface->SetObjectString("DrawFontPosition", mSelSpName, mDrawFontPositionMap[mSelSpName]);

         }

         // change draw non-spacecraft
         for (int i=0; i<mNonScCount; i++)
         {
            mSelSpName = mSelectedObjListBox->GetString(i).WX_TO_STD_STRING;
            mOFInterface->SetObjectBool("DrawObject", mSelSpName, mDrawObjectMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawTrajectory", mSelSpName, mDrawTrajectoryMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawAxes", mSelSpName, mDrawAxesMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawXYPlane", mSelSpName, mDrawXYPlaneMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawLabel", mSelSpName, mDrawLabelMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawUsePropLabel", mSelSpName, mUsePropagateLabelMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawCenterPoint", mSelSpName, mDrawCenterPointMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawEndPoints", mSelSpName, mDrawEndPointsMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawVelocity", mSelSpName, mDrawVelocityMap[mSelSpName]);
            mOFInterface->SetObjectBool("DrawGrid", mSelSpName, mDrawGridMap[mSelSpName]);
            mOFInterface->SetObjectReal("DrawLineWidth", mSelSpName, mDrawLineWidthMap[mSelSpName]);
            mOFInterface->SetObjectUInt("DrawMarkerSize", mSelSpName, mDrawMarkerSizeMap[mSelSpName]);
            mOFInterface->SetObjectUInt("DrawFontSize", mSelSpName, mDrawFontSizeMap[mSelSpName]);
            mOFInterface->SetObjectString("DrawFontPosition", mSelSpName, mDrawFontPositionMap[mSelSpName]);

         }
      }

      //--------------------------------------------------------------
      // save coordinate system
      //--------------------------------------------------------------
      if (mHasCoordSysChanged)
      {
         hasChanges = true;
         mHasCoordSysChanged = false;
         mOFInterface->SetStringParameter("CoordinateSystem", mCoordSysComboBox->GetStringSelection().WX_TO_STD_STRING);
      }

      //--------------------------------------------------------------
      // save views
      //--------------------------------------------------------------
      if (mHasViewsChanged)
      {
         hasChanges = true;
         wxArrayString viewsArray = mSelectedViewListBox->GetStrings();
         std::string viewsString = "{";
         for (Integer i = 0; i < viewsArray.Count(); i++)
         {
            if (i > 0)
               viewsString += ",";
            viewsString += viewsArray[i];
         }
         viewsString += "}";
         mOFInterface->SetStringParameter("View", viewsString);
      }
      

      //--------------------------------------------------------------
      // save vectors
      //--------------------------------------------------------------
      if (mHasVectorsChanged)
      {        
        hasChanges = true;
        wxArrayString vectorsArray = mSelectedVectorListBox->GetStrings();
        std::string vectorsString = "{";
        for (Integer i = 0; i < vectorsArray.Count(); i++)
        {
          if (i > 0)
            vectorsString += ",";
          vectorsString += vectorsArray[i];
        }
        vectorsString += "}";
        mOFInterface->SetStringParameter("Vector", vectorsString);
      }
      
      //--------------------------------------------------------------
      // save sensor masks
      //--------------------------------------------------------------
      if (mHasMasksChanged)
      {
        hasChanges = true;
        wxArrayString masksArray = mSelectedMaskListBox->GetStrings();
        std::string masksString = "{";
        for (Integer i = 0; i < masksArray.Count(); i++)
        {
          if (i > 0)
            masksString += ",";
          masksString += masksArray[i];
        }
        masksString += "}";
        mOFInterface->SetStringParameter("SensorMask", masksString);
      }

      //--------------------------------------------------------------
      // clone changes into a new buffer object
      //--------------------------------------------------------------
      if (mBufferInterface != nullptr)
         delete mBufferInterface;
      mBufferInterface = static_cast<OpenFramesInterface *>(mOFInterface->Clone());

      //--------------------------------------------------------------
      // push changes to the sandbox
      //--------------------------------------------------------------
      Moderator *theModerator = Moderator::Instance();
      Sandbox *sandbox = theModerator->GetSandbox();
      if (sandbox != nullptr)
      {
         OpenFramesInterface *target = GetCloneFromSandbox();
         if (target != nullptr)
         {
            OFWindowProxyPanel *widget = target->GetWidget();
            if (widget != nullptr)
            {
               // Get options from the configured object
               const OpenGLOptions &glOptions = mOFInterface->GetGLOptions();
               const CoordinateOptions &frameOptions = mOFInterface->GetFrameOptions();
               const StarOptions &starOptions = mOFInterface->GetStarOptions();
               // Get the time sync parent inside the sandbox
               GmatBase *tsp;
               OpenFramesInterface *timeSyncParent;
               std::string parentName = mOFInterface->GetStringParameter("TimeSyncParent");
               if (parentName.size() > 0)
                  try
                  {
                     tsp = sandbox->GetInternalObject(parentName);
                  }
                  catch (SandboxException &e)
                  {
                     tsp = nullptr;
                  }
               else
                  tsp = nullptr;
               if (tsp != nullptr && tsp->IsOfType("OpenFramesInterface"))
                  timeSyncParent = static_cast<OpenFramesInterface *>(tsp);
               else
                  timeSyncParent = nullptr;

               // Get latest configured views
               std::vector<OpenFramesView *> views;
               const StringArray &viewNames = mOFInterface->GetStringArrayParameter("View");
               for (auto v = viewNames.begin(); v < viewNames.end(); v++)
               {
                  GmatBase *view_obj = theModerator->GetConfiguredObject(*v);
                  if (view_obj != nullptr && view_obj->IsOfType("OpenFramesView"))
                     views.push_back(static_cast<OpenFramesView *>(view_obj));
               }

               // Get latest configured vectors
               std::vector<OpenFramesVector *> vectors;
               const StringArray &vectorNames = mOFInterface->GetStringArrayParameter("Vector");
               for (const auto& v : vectorNames)
               {
                  GmatBase *vector_obj = theModerator->GetConfiguredObject(v);
                  if (vector_obj != nullptr && vector_obj->IsOfType("OpenFramesVector"))
                     vectors.push_back(static_cast<OpenFramesVector *>(vector_obj));
               }
               
               // Get latest configured sensor masks
               std::vector<OpenFramesSensorMask *> masks;
               const StringArray &maskNames = mOFInterface->GetStringArrayParameter("SensorMask");
               for (const auto& m : maskNames)
               {
                  GmatBase *mask_obj = theModerator->GetConfiguredObject(m);
                  if (mask_obj != nullptr && mask_obj->IsOfType("OpenFramesSensorMask"))
                     masks.push_back(static_cast<OpenFramesSensorMask *>(mask_obj));
               }

               // Populate space point options with configured space points
               std::vector<SpacePointOptions> spacePoints = mOFInterface->GetSpacePointOptions();
               for (Integer ii = spacePoints.size() - 1; ii >= 0; ii--)
               {
                  GmatBase *sp_obj = theModerator->GetConfiguredObject(spacePoints[ii].GetName());
                  if (sp_obj != nullptr && sp_obj->IsOfType("SpacePoint"))
                     spacePoints[ii].SetSpacePoint(static_cast<SpacePoint *>(sp_obj));
               }
               
               // Figure out how many solution segments should be shown
               Integer numOldTrajectories;
               if (mSolverIterComboBox->GetStringSelection() == "Current")
                  numOldTrajectories = 1;
               else
                  numOldTrajectories = mSILastNCtrl->GetValue() * OpenFramesInterface::TRAJ_PER_SOLVER_ITER;
               
               // Finally reconfigure the target window
               widget->ShowToolbar(mOFInterface->GetBooleanParameter("ShowToolbar"));
               widget->GetScene().Reconfigure(glOptions, frameOptions, starOptions, timeSyncParent, spacePoints,
                     mOFInterface->GetName(), views, vectors, masks, mCoordSysComboBox->GetStringSelection().WX_TO_STD_STRING,
                     numOldTrajectories);
            }
         }
      }

      if (hasChanges)
         Moderator::Instance()->ConfigurationChanged(mOFInterface, true);
      EnableUpdate(false);
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }
}


OpenFramesInterface *OFConfigurationPanel::GetCloneFromSandbox()
{
   OpenFramesInterface *clone = nullptr;
   Moderator *theModerator = Moderator::Instance();
   Sandbox *sandbox = theModerator->GetSandbox();
   if (sandbox != nullptr)
   {
      GmatBase *obj;
      try
      {
         obj = sandbox->GetInternalObject(mOFInterface->GetName());
      }
      catch (SandboxException)
      {
         obj = nullptr;
      }
      if (obj != nullptr && obj->IsOfType("OpenFramesInterface"))
      {
         clone = static_cast<OpenFramesInterface *>(obj);
      }
   }

   return clone;
}


//------------------------------------------------------------------------------
// void OnAddSpacePoint(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback that adds the highlighted excluded spacecraft or body to the lists
 * of selected spacecraft and bodies
 *
 * @param <event> Details of the event
 */
void OFConfigurationPanel::OnAddSpacePoint(wxCommandEvent& event)
{
   if (mSpacecraftListBox->GetSelection() != -1)
   {
      // get string in first list and then search for it
      // in the second list
      wxString str = mSpacecraftListBox->GetStringSelection();
      int strId = mSpacecraftListBox->FindString(str);
      int found = mSelectedScListBox->FindString(str);

      // if the string wasn't found in the second list, insert it
      if (found == wxNOT_FOUND)
      {
         Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType*>(mSpacecraftListBox->GetClientData(mSpacecraftListBox->GetSelection()));
         mSelectedScListBox->Append(str, objType);
         mSelectedScListBox->SetStringSelection(str);

         // Removed from available list
         mSpacecraftListBox->Delete(strId);

         // select next available item
         if (strId == 0)
            mSpacecraftListBox->SetSelection(0);
         else if (strId > 0)
            mSpacecraftListBox->SetSelection(strId - 1);

         // deselect selected other object
         mSelectedObjListBox->Deselect(mSelectedObjListBox->GetSelection());

         // Add to excluded list
         mExcludedScList.Add(str);

         if (*objType == GROUND_STATION_CLIENT_DATA)
         {
            mDrawObjectMap[str.WX_TO_STD_STRING] = false;
            mDrawTrajectoryMap[str.WX_TO_STD_STRING] = false;
            mDrawEndPointsMap[str.WX_TO_STD_STRING] = false;
         }
         else
         {
            mDrawObjectMap[str.WX_TO_STD_STRING] = true;
            mDrawTrajectoryMap[str.WX_TO_STD_STRING] = true;
            mDrawEndPointsMap[str.WX_TO_STD_STRING] = true;
         }
         mDrawAxesMap[str.WX_TO_STD_STRING] = false;
         mDrawXYPlaneMap[str.WX_TO_STD_STRING] = false;
         mDrawLabelMap[str.WX_TO_STD_STRING] = true;
         mUsePropagateLabelMap[str.WX_TO_STD_STRING] = false;
         mDrawCenterPointMap[str.WX_TO_STD_STRING] = true;
         mDrawVelocityMap[str.WX_TO_STD_STRING] = false;
         mDrawGridMap[str.WX_TO_STD_STRING] = false;
         mDrawLineWidthMap[str.WX_TO_STD_STRING] = 2.0;
         mDrawMarkerSizeMap[str.WX_TO_STD_STRING] = 10U;
         mDrawFontSizeMap[str.WX_TO_STD_STRING] = 14U;
         mDrawFontPositionMap[str.WX_TO_STD_STRING] = "Top-Right";
         ShowSpacePointOption(str, *objType);
         mHasSpChanged = true;
         //mHasDrawingOptionChanged = true;
         mHasShowObjectChanged = true;
         EnableUpdate(true);
         
      }
   }
   else if (mCelesPointListBox->GetSelection() != -1)
   {
      wxString str = mCelesPointListBox->GetStringSelection();
      int strId = mCelesPointListBox->FindString(str);
      int found = mSelectedObjListBox->FindString(str);

      // if the string wasn't found in the second list, insert it
      if (found == wxNOT_FOUND)
      {
         // Add to selected list
         Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType*>(mCelesPointListBox->GetClientData(mCelesPointListBox->GetSelection()));
         mSelectedObjListBox->Append(str, objType);
         mSelectedObjListBox->SetStringSelection(str);

         // Removed from available list
         mCelesPointListBox->Delete(strId);

         // select next available item
         if (strId == 0)
            mCelesPointListBox->SetSelection(0);
         else if (strId > 0)
            mCelesPointListBox->SetSelection(strId - 1);

         // deselect selected spacecraft
         mSelectedScListBox->Deselect(mSelectedScListBox->GetSelection());

         // Add to excluded list
         mExcludedCelesPointList.Add(str);

         mDrawObjectMap[str.WX_TO_STD_STRING] = true;
         mDrawTrajectoryMap[str.WX_TO_STD_STRING] = true;
         mDrawAxesMap[str.WX_TO_STD_STRING] = false;
         mDrawXYPlaneMap[str.WX_TO_STD_STRING] = false;
         mDrawLabelMap[str.WX_TO_STD_STRING] = true;
         mUsePropagateLabelMap[str.WX_TO_STD_STRING] = false;
         mDrawCenterPointMap[str.WX_TO_STD_STRING] = false;
         mDrawEndPointsMap[str.WX_TO_STD_STRING] = false;
         mDrawVelocityMap[str.WX_TO_STD_STRING] = false;
         mDrawGridMap[str.WX_TO_STD_STRING] = false;
         mDrawLineWidthMap[str.WX_TO_STD_STRING] = 2.0;
         mDrawMarkerSizeMap[str.WX_TO_STD_STRING] = 10U;
         mDrawFontSizeMap[str.WX_TO_STD_STRING] = 14U;
         mDrawFontPositionMap[str.WX_TO_STD_STRING] = "Top-Right";
         ShowSpacePointOption(str, *objType);

         //mHasDrawingOptionChanged = true;
         mHasShowObjectChanged = true;
         mHasSpChanged = true;
         EnableUpdate(true);
      }
   }

   Layout();
   Update();
}


//------------------------------------------------------------------------------
// void OnRemoveSpacePoint(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback that removes the highlighted selected spacecraft or body from the
 * list of selected spacecraft and bodies
 *
 * @param <event> Details of the event
 */
void OFConfigurationPanel::OnRemoveSpacePoint(wxCommandEvent& event)
{
   if (mSelectedScListBox->GetSelection() != -1)
   {
      wxString str = mSelectedScListBox->GetStringSelection();
      int sel = mSelectedScListBox->GetSelection();
      Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType*>(mSelectedScListBox->GetClientData(sel));

      // Add to available list
      mSpacecraftListBox->Append(str, objType);

      // Remove from selected list
      mSelectedScListBox->Delete(sel);

      // Remove from excluded list
      mExcludedScList.Remove(str);

      if (sel-1 < 0)
      {
         mSelectedScListBox->SetSelection(0);
         if (mSelectedScListBox->GetCount() == 0)
            ShowSpacePointOption("", SPACECRAFT_CLIENT_DATA); // hide spacecraft color, etc
         else
         {
            Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType *>(mSelectedScListBox->GetClientData(0));
            ShowSpacePointOption(mSelectedScListBox->GetString(0), *objType);
         }
      }
      else
      {
         mSelectedScListBox->SetSelection(sel-1);
         Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType *>(mSelectedScListBox->GetClientData(sel-1));
         ShowSpacePointOption(mSelectedScListBox->GetString(sel-1), *objType);
      }
   }
   else if (mSelectedObjListBox->GetSelection() != -1)
   {
      wxString str = mSelectedObjListBox->GetStringSelection();
      int sel = mSelectedObjListBox->GetSelection();
      Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType*>(mSelectedObjListBox->GetClientData(sel));

      // Add to available list
      mCelesPointListBox->Append(str, objType);

      // Remove from selected list
      mSelectedObjListBox->Delete(sel);

      // Remove from excluded list
      mExcludedCelesPointList.Remove(str);

      if (sel-1 < 0)
      {
         mSelectedObjListBox->SetSelection(0);
         if (mSelectedObjListBox->GetCount() == 0)
            ShowSpacePointOption("", CELESTIAL_BODY_CLIENT_DATA);
         else
         {
            Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType *>(mSelectedObjListBox->GetClientData(0));
            ShowSpacePointOption(mSelectedObjListBox->GetString(0), *objType);
         }
      }
      else
      {
         mSelectedObjListBox->SetSelection(sel-1);
            Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType *>(mSelectedObjListBox->GetClientData(sel-1));
         ShowSpacePointOption(mSelectedObjListBox->GetString(sel-1), *objType);
      }
   }

   mHasSpChanged = true;
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnClearSpacePoint(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback that removes all selected spacecraft or body from the lists of
 * selected spacecraft and bodies
 *
 * @param <event> Details of the event
 */
void OFConfigurationPanel::OnClearSpacePoint(wxCommandEvent& event)
{
   if (mSelectedScListBox->GetSelection() != -1)
   {
      Integer count = mSelectedScListBox->GetCount();

      if (count == 0)
         return;

      for (Integer i = 0; i < count; i++)
      {
         Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType*>(mSelectedScListBox->GetClientData(i));
         mSpacecraftListBox->Append(mSelectedScListBox->GetString(i), objType);
      }

      // Clear selected spacecraft
      mSelectedScListBox->Clear();
      mExcludedScList.Clear();
   }
   else if (mSelectedObjListBox->GetSelection() != -1)
   {
      Integer count = mSelectedObjListBox->GetCount();

      if (count == 0)
         return;

      for (Integer i = 0; i < count; i++)
      {
         Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType*>(mSelectedObjListBox->GetClientData(i));
         mCelesPointListBox->Append(mSelectedObjListBox->GetString(i), objType);
      }

      // Clear selected bodies
      mSelectedObjListBox->Clear();
      mExcludedCelesPointList.Clear();
   }

   ShowSpacePointOption("", CELESTIAL_BODY_CLIENT_DATA);
   mHasSpChanged = true;
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnSelectAvailObject(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a different excluded spacecraft or body is highlighted
 *
 * @param <event> Details of the event
 */
void OFConfigurationPanel::OnSelectAvailObject(wxCommandEvent& event)
{
   if (event.GetEventObject() == mSpacecraftListBox)
      mCelesPointListBox->Deselect(mCelesPointListBox->GetSelection());
   else if (event.GetEventObject() == mCelesPointListBox)
      mSpacecraftListBox->Deselect(mSpacecraftListBox->GetSelection());
}


//------------------------------------------------------------------------------
// void OnSelectSpacecraft(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a different selected spacecraft is highlighted
 *
 * @param <event> Details of the event
 */
void OFConfigurationPanel::OnSelectSpacecraft(wxCommandEvent& event)
{
   int sel = mSelectedScListBox->GetSelection();
   Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType*>(mSelectedScListBox->GetClientData(sel));
   ShowSpacePointOption(mSelectedScListBox->GetString(sel), *objType);
   mSelectedObjListBox->Deselect(mSelectedObjListBox->GetSelection());
}


//------------------------------------------------------------------------------
// void OnSelectOtherObject(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a different selected celestial body is highlighted
 *
 * @param <event> Details of the event
 */
void OFConfigurationPanel::OnSelectOtherObject(wxCommandEvent& event)
{
   int sel = mSelectedObjListBox->GetSelection();
   Gmat::ObjectType *objType = reinterpret_cast<Gmat::ObjectType*>(mSelectedObjListBox->GetClientData(sel));
   ShowSpacePointOption(mSelectedObjListBox->GetStringSelection(), *objType);
   mSelectedScListBox->Deselect(mSelectedScListBox->GetSelection());
}


//------------------------------------------------------------------------------
// void OnCheckBoxChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when checkbox is changed
 *
 * @param <event> Details of the event
 */
void OFConfigurationPanel::OnCheckBoxChange(wxCommandEvent& event)
{
   if (event.GetEventObject() == mDrawObjectCheckBox)
   {
      if (mSelectedScListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawObjectMap[mSelSpName] = mDrawObjectCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawObjectMap[mSelSpName] = mDrawObjectCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   else if (event.GetEventObject() == mDrawTrajectoryCheckBox)
   {
      if (mSelectedScListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawTrajectoryMap[mSelSpName] = mDrawTrajectoryCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawTrajectoryMap[mSelSpName] = mDrawTrajectoryCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   else if (event.GetEventObject() == mDrawAxesCheckBox)
   {
      if (mSelectedScListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawAxesMap[mSelSpName] = mDrawAxesCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawAxesMap[mSelSpName] = mDrawAxesCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   else if (event.GetEventObject() == mDrawXYPlaneCheckBox)
   {
      if (mSelectedScListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawXYPlaneMap[mSelSpName] = mDrawXYPlaneCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawXYPlaneMap[mSelSpName] = mDrawXYPlaneCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   else if (event.GetEventObject() == mDrawCenterPointCheckBox)
   {
      if (mSelectedScListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawCenterPointMap[mSelSpName] = mDrawCenterPointCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawCenterPointMap[mSelSpName] = mDrawCenterPointCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   else if (event.GetEventObject() == mDrawEndPointsCheckBox)
   {
      if (mSelectedScListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawEndPointsMap[mSelSpName] = mDrawEndPointsCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawEndPointsMap[mSelSpName] = mDrawEndPointsCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   else if (event.GetEventObject() == mDrawVelocityCheckBox)
   {
      if (mSelectedScListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawVelocityMap[mSelSpName] = mDrawVelocityCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawVelocityMap[mSelSpName] = mDrawVelocityCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   else if (event.GetEventObject() == mDrawGridCheckBox)
   {
      if (mSelectedScListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawGridMap[mSelSpName] = mDrawGridCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawGridMap[mSelSpName] = mDrawGridCheckBox->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   else if (event.GetEventObject() == mDrawBodyLabelBox)
   {
      if (mSelectedScListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawLabelMap[mSelSpName] = mDrawBodyLabelBox->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawLabelMap[mSelSpName] = mDrawBodyLabelBox->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   else if (event.GetEventObject() == mDrawPropLabelBox)
   {
      if (mSelectedScListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mUsePropagateLabelMap[mSelSpName] = mDrawPropLabelBox->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != -1)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mUsePropagateLabelMap[mSelSpName] = mDrawPropLabelBox->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   else if (event.GetEventObject() == mEnableStarsCheckBox)
   {
      mStarsButton->Enable(mEnableStarsCheckBox->GetValue());
      mHasStarOptionChanged = true;
   }
   else if (event.GetEventObject() == mAxesCheckBox)
   {
     if (mAxesCheckBox->GetValue())
     {
       mShowAxesLabelsCheckBox->Enable();
       mAxesCheckBoxSpinCtrl->Enable();
     }       
     else
     {
       mShowAxesLabelsCheckBox->Disable();
       mAxesCheckBoxSpinCtrl->Disable();
     }  
      mHasDrawingOptionChanged = true;
   }
   else
   {
      mHasDrawingOptionChanged = true;
   }

   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnComboBoxChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a combo box selection changes
 *
 * @param <event> Details of the event
 */
void OFConfigurationPanel::OnComboBoxChange(wxCommandEvent& event)
{
   if (event.GetEventObject() == mCoordSysComboBox)
   {
      mHasCoordSysChanged = true;
   }
   else if (event.GetEventObject() == mFontPositionComboBox)
   {
     if (mSelectedScListBox->GetSelection() != wxNOT_FOUND)
     {
       mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
       mDrawFontPositionMap[mSelSpName] = mFontPositionComboBox->GetValue();
       mHasShowObjectChanged = true;
     }
     else if (mSelectedObjListBox->GetSelection() != wxNOT_FOUND)
     {
       mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
       mDrawFontPositionMap[mSelSpName] = mFontPositionComboBox->GetValue();
       mHasShowObjectChanged = true;
     }
   }
   else if (event.GetEventObject() == mSolverIterComboBox)
   {
      mHasDrawingOptionChanged = true;
      if (mSolverIterComboBox->GetStringSelection() == "LastN")
      {
         mSILastNCtrl->Enable();
         if (mSILastNCtrl->GetValue() == 0)
         {
            mSILastNCtrl->SetValue(1);
            mHasIntegerDataChanged = true;
         }
      }
      else if (mSolverIterComboBox->GetStringSelection() == "Current")
      {
         mSILastNCtrl->Disable();
         if (mSILastNCtrl->GetValue() != 1)
         {
            mSILastNCtrl->SetValue(1);
            mHasIntegerDataChanged = true;
         }
      }
      else
      {
         mSILastNCtrl->Disable();
         if (mSILastNCtrl->GetValue() != 0)
         {
            mSILastNCtrl->SetValue(0);
            mHasIntegerDataChanged = true;
         }
      }

   }
   else if (event.GetEventObject() == mSyncParentCombobox)
   {
      mHasTimeSyncChanged = true;
   }
   // call Layout() to force layout of the children anew
   mPanelSizer->Layout();

   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnComboBoxDropdown(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a combo box drops down
 *
 * @param <event> Details of the event
 */
void OFConfigurationPanel::OnComboBoxDropdown(wxCommandEvent& event)
{
   if (event.GetEventObject() == mSyncParentCombobox)
      RefreshPossibleParents();

   if (event.GetEventObject() == mFontPositionComboBox)
   {
     if (mSelectedScListBox->GetSelection() != wxNOT_FOUND)
     {
       mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
       mDrawFontPositionMap[mSelSpName] = mFontPositionComboBox->GetValue();
       mHasShowObjectChanged = true;
     }
     else if (mSelectedObjListBox->GetSelection() != wxNOT_FOUND)
     {
       mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
       mDrawFontPositionMap[mSelSpName] = mFontPositionComboBox->GetValue();
       mHasShowObjectChanged = true;
     }
   }

}


//------------------------------------------------------------------------------
// void OnSpinChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a spin controller changes
 *
 * @param <event> Details of the event
 */
void OFConfigurationPanel::OnSpinChange(wxSpinEvent& event)
{
   wxObject *obj = event.GetEventObject();

   /*if (obj == mLineWidthTextCtrl)
   {
      if (mSelectedScListBox->GetSelection() != wxNOT_FOUND)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawLineWidthMap[mSelSpName] = mLineWidthTextCtrl->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != wxNOT_FOUND)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawLineWidthMap[mSelSpName] = mLineWidthTextCtrl->GetValue();
         mHasShowObjectChanged = true;
      }
   }*/
   if (obj == mMarkerSizeTextCtrl)
   {
      if (mSelectedScListBox->GetSelection() != wxNOT_FOUND)
      {
         mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawMarkerSizeMap[mSelSpName] = mMarkerSizeTextCtrl->GetValue();
         mHasShowObjectChanged = true;
      }
      else if (mSelectedObjListBox->GetSelection() != wxNOT_FOUND)
      {
         mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
         mDrawMarkerSizeMap[mSelSpName] = mMarkerSizeTextCtrl->GetValue();
         mHasShowObjectChanged = true;
      }
   }
   if (obj == mFontSizeSpinCtrl)
   {
     if (mSelectedScListBox->GetSelection() != wxNOT_FOUND)
     {
       mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
       mDrawFontSizeMap[mSelSpName] = mFontSizeSpinCtrl->GetValue();
       mHasShowObjectChanged = true;
     }
     else if (mSelectedObjListBox->GetSelection() != wxNOT_FOUND)
     {
       mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
       mDrawFontSizeMap[mSelSpName] = mFontSizeSpinCtrl->GetValue();
       mHasShowObjectChanged = true;
     }
   }
   else if (obj == mSILastNCtrl)
   {
      mHasIntegerDataChanged = true;
   }
   /*else if (obj == mAxesCheckBoxSpinCtrl)
   {
     mHasDrawingOptionChanged = true;
   }*/
   else
   {
      mHasRealDataChanged = true;
   }

   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnSpinChangeDouble(wxSpinDoubleEvent& event)
//------------------------------------------------------------------------------
/**
* Callback when a spin controller changes
*
* @param <event> Details of the event
*/
void OFConfigurationPanel::OnSpinChangeDouble(wxSpinDoubleEvent& event)
{
  wxObject *obj = event.GetEventObject();

  if (obj == mLineWidthTextCtrl)
  {
    if (mSelectedScListBox->GetSelection() != wxNOT_FOUND)
    {
      mSelSpName = mSelectedScListBox->GetStringSelection().WX_TO_STD_STRING;
      mDrawLineWidthMap[mSelSpName] = mLineWidthTextCtrl->GetValue();
      mHasShowObjectChanged = true;
    }
    else if (mSelectedObjListBox->GetSelection() != wxNOT_FOUND)
    {
      mSelSpName = mSelectedObjListBox->GetStringSelection().WX_TO_STD_STRING;
      mDrawLineWidthMap[mSelSpName] = mLineWidthTextCtrl->GetValue();
      mHasShowObjectChanged = true;
    }
  }
  
  else if (obj == mAxesCheckBoxSpinCtrl)
  {
    mHasDrawingOptionChanged = true;
  }
  else
  {
    mHasRealDataChanged = true;
  }

  EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnNewView(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
 * Callback that creates a new view in the editor
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnNewView(wxCommandEvent &event)
{
   Moderator *theModerator = Moderator::Instance();
   unsigned int i;
   const unsigned int limit = 999999U;
   std::string baseName = "NewView";
   std::string name;

   for (i = 1; i < limit; i++)
   {
      name = baseName + std::to_string(i);
      if (theModerator->GetConfiguredObject(name) == nullptr)
         break;
   }

   if (i < limit)
      CreateNewView(name);
}


//------------------------------------------------------------------------------
// void OnNewVector(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that creates a new vector in the editor
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnNewVector(wxCommandEvent &event)
{
	Moderator *theModerator = Moderator::Instance();
	unsigned int i;
	const unsigned int limit = 999999U;
	std::string baseName = "NewVector";
	std::string name;

	for (i = 1; i < limit; i++)
	{
		name = baseName + std::to_string(i);
		if (theModerator->GetConfiguredObject(name) == nullptr)
			break;
	}

	if (i < limit)
		CreateNewVector(name);

}


//------------------------------------------------------------------------------
// void OnNewMask(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that creates a new sensor mask in the editor
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnNewMask(wxCommandEvent &event)
{
   Moderator *theModerator = Moderator::Instance();
   unsigned int i;
   const unsigned int limit = 999999U;
   std::string baseName = "NewMask";
   std::string name;

   for (i = 1; i < limit; i++)
   {
      name = baseName + std::to_string(i);
      if (theModerator->GetConfiguredObject(name) == nullptr)
         break;
   }

   if (i < limit)
      CreateNewMask(name);
}


//------------------------------------------------------------------------------
// void OnAddView(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
 * Callback that adds an existing view
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnAddView(wxCommandEvent &event)
{
   Moderator *theModerator = Moderator::Instance();
   mExistingViews = theModerator->GetListOfObjects("OpenFramesView");

   for (int ii = mExistingViews.size() - 1; ii >= 0; ii--)
   {
      int sel = mSelectedViewListBox->FindString(mExistingViews[ii]);
      if (sel != wxNOT_FOUND)
         mExistingViews.erase(mExistingViews.begin() + ii);
   }

   wxMenu menu(0L);
   for (int ii = 0; ii < mExistingViews.size() && ii < MAX_EXISTING_VIEWS; ii++)
      menu.Append(START_VIEW_MENU + ii, mExistingViews[ii]);

   if (menu.GetMenuItemCount() == 0)
   {
      wxMenuItem *item = menu.Append(START_VIEW_MENU, "No other existing views");
      item->Enable(false);
   }
   PopupMenu(&menu);
}


//------------------------------------------------------------------------------
// void OnAddVector(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that adds an existing vector
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnAddVector(wxCommandEvent &event)
{
  Moderator *theModerator = Moderator::Instance();
  mExistingVectors = theModerator->GetListOfObjects("OpenFramesVector");

  for (int ii = mExistingVectors.size() - 1; ii >= 0; ii--)
  {
    int sel = mSelectedVectorListBox->FindString(mExistingVectors[ii]);
    if (sel != wxNOT_FOUND)
      mExistingVectors.erase(mExistingVectors.begin() + ii);
  }
  wxMenu menu(0L);
  for (int ii = 0; ii < mExistingVectors.size() && ii < MAX_EXISTING_VECTORS; ii++)
    menu.Append(START_VECTOR_MENU + ii, mExistingVectors[ii]);

  if (menu.GetMenuItemCount() == 0)
  {
    wxMenuItem *item = menu.Append(START_VECTOR_MENU, "No other existing vectors");
    item->Enable(false);
  }
  PopupMenu(&menu);
}

//------------------------------------------------------------------------------
// void OnAddMask(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that adds an existing sensor mask
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnAddMask(wxCommandEvent &event)
{
  Moderator *theModerator = Moderator::Instance();
  mExistingMasks = theModerator->GetListOfObjects("OpenFramesSensorMask");

  for (int ii = mExistingMasks.size() - 1; ii >= 0; ii--)
  {
    int sel = mSelectedMaskListBox->FindString(mExistingMasks[ii]);
    if (sel != wxNOT_FOUND)
      mExistingMasks.erase(mExistingMasks.begin() + ii);
  }
  wxMenu menu(0L);
  for (int ii = 0; ii < mExistingMasks.size() && ii < MAX_EXISTING_MASKS; ii++)
    menu.Append(START_MASK_MENU + ii, mExistingMasks[ii]);

  if (menu.GetMenuItemCount() == 0)
  {
    wxMenuItem *item = menu.Append(START_MASK_MENU, "No other existing masks");
    item->Enable(false);
  }
  PopupMenu(&menu);
}

//------------------------------------------------------------------------------
// void OnExistingView(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
 * Callback when an existing view is selected
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnExistingView(wxCommandEvent &event)
{
   mSelectedViewListBox->Append(mExistingViews[event.GetId() - START_VIEW_MENU]);
   mSelectedViewListBox->SetStringSelection(mExistingViews[event.GetId() - START_VIEW_MENU]);

   UpdateViewButtons();
   mHasViewsChanged = true;
   EnableUpdate(true);
}

//------------------------------------------------------------------------------
// void OnExistingVector(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback when an existing vector is selected
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnExistingVector(wxCommandEvent &event)
{
  mSelectedVectorListBox->Append(mExistingVectors[event.GetId() - START_VECTOR_MENU]);
  mSelectedVectorListBox->SetStringSelection(mExistingVectors[event.GetId() - START_VECTOR_MENU]);

  UpdateVectorButtons();
  mHasVectorsChanged = true;
  EnableUpdate(true);
}

//------------------------------------------------------------------------------
// void OnExistingMask(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback when an existing sensor mask is selected
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnExistingMask(wxCommandEvent &event)
{
  mSelectedMaskListBox->Append(mExistingMasks[event.GetId() - START_MASK_MENU]);
  mSelectedMaskListBox->SetStringSelection(mExistingMasks[event.GetId() - START_MASK_MENU]);

  UpdateMaskButtons();
  mHasMasksChanged = true;
  EnableUpdate(true);
}

//------------------------------------------------------------------------------
// void OnDeleteView(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
 * Callback that deletes selected view from this interface
 *
 * Also deletes the object from GMAT if it is not used by any other objects.
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnDeleteView(wxCommandEvent &event)
{
   int sel = mSelectedViewListBox->GetSelection();
   if (sel != wxNOT_FOUND)
   {
      std::string selString = mSelectedViewListBox->GetString(sel).WX_TO_STD_STRING;
      mSelectedViewListBox->SetSelection(wxNOT_FOUND);
      mSelectedViewListBox->Delete(sel);
      StringArray views = mOFInterface->GetStringArrayParameter("View");
      for (int i = views.size() - 1; i >= 0; i--)
      {
         if (views[i] == selString)
            views.erase(views.begin() + i);
      }
      mOFInterface->SetStringArrayParameter("View", views);

      Moderator *theModerator = Moderator::Instance();
      theModerator->RemoveObject(GmatType::GetTypeId("OpenFramesView"), selString, true);

      UpdateViewButtons();
   }
}


//------------------------------------------------------------------------------
// void OnDeleteVector(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that deletes selected vector from this interface
*
* Also deletes the object from GMAT if it is not used by any other objects.
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnDeleteVector(wxCommandEvent &event)
{
  int sel = mSelectedVectorListBox->GetSelection();
  if (sel != wxNOT_FOUND)
  {
    std::string selString = mSelectedVectorListBox->GetString(sel).WX_TO_STD_STRING;
    mSelectedVectorListBox->SetSelection(wxNOT_FOUND);
    mSelectedVectorListBox->Delete(sel);
    StringArray vectors = mOFInterface->GetStringArrayParameter("Vector");
    for (int i = vectors.size() - 1; i >= 0; i--)
    {
      if (vectors[i] == selString)
        vectors.erase(vectors.begin() + i);
    }
    mOFInterface->SetStringArrayParameter("Vector", vectors);

    Moderator *theModerator = Moderator::Instance();
    theModerator->RemoveObject(GmatType::GetTypeId("OpenFramesVector"), selString, true);

    UpdateVectorButtons();
  }
}


//------------------------------------------------------------------------------
// void OnDeleteMask(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that deletes selected sensor mask from this interface
*
* Also deletes the object from GMAT if it is not used by any other objects.
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnDeleteMask(wxCommandEvent &event)
{
  int sel = mSelectedMaskListBox->GetSelection();
  if (sel != wxNOT_FOUND)
  {
    std::string selString = mSelectedMaskListBox->GetString(sel).WX_TO_STD_STRING;
    mSelectedMaskListBox->SetSelection(wxNOT_FOUND);
    mSelectedMaskListBox->Delete(sel);
    StringArray masks = mOFInterface->GetStringArrayParameter("SensorMask");
    for (int i = masks.size() - 1; i >= 0; i--)
    {
      if (masks[i] == selString)
        masks.erase(masks.begin() + i);
    }
    mOFInterface->SetStringArrayParameter("SensorMask", masks);

    Moderator *theModerator = Moderator::Instance();
    theModerator->RemoveObject(GmatType::GetTypeId("OpenFramesSensorMask"), selString, true);

    UpdateMaskButtons();
  }
}

//------------------------------------------------------------------------------
// void OnEditView(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
 * Callback that edits selected view from the views
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnEditView(wxCommandEvent &event)
{
   Moderator *theModerator = Moderator::Instance();

   int sel = mSelectedViewListBox->GetSelection();
   if (sel != wxNOT_FOUND)
   {
      std::string name = mSelectedViewListBox->GetString(sel).WX_TO_STD_STRING;
      GmatBase *obj = theModerator->GetConfiguredObject(name);

      if (obj != nullptr && obj->IsOfType("OpenFramesView"))
      {
         OpenFramesView &view = *static_cast<OpenFramesView *>(obj);
         if (EditViewDialog(view, false) == wxID_OK)
         {
            if (view.GetName() != name)
               mSelectedViewListBox->SetString(sel, view.GetName());
            // Enable apply button if there is a clone in the sandbox
            OpenFramesInterface *clone = OFConfigurationPanel::GetCloneFromSandbox();
            if (clone != nullptr)
               EnableUpdate(true);
         }
         UpdateViewButtons();
      }
      else
      {
         CreateNewView(name);
      }
   }
}


//------------------------------------------------------------------------------
// void OnEditVector(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that edits selected vector from the vectors
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnEditVector(wxCommandEvent &event)
{
  Moderator *theModerator = Moderator::Instance();

  int sel = mSelectedVectorListBox->GetSelection();
  if (sel != wxNOT_FOUND)
  {
    std::string name = mSelectedVectorListBox->GetString(sel).WX_TO_STD_STRING;
    GmatBase *obj = theModerator->GetConfiguredObject(name);

    if (obj != nullptr && obj->IsOfType("OpenFramesVector"))
    {
      OpenFramesVector &vector = *static_cast<OpenFramesVector *>(obj);
      if (EditVectorDialog(vector, false) == wxID_OK)
      {
        if (vector.GetName() != name)
          mSelectedVectorListBox->SetString(sel, vector.GetName());
        // Enable apply button if there is a clone in the sandbox
        OpenFramesInterface *clone = OFConfigurationPanel::GetCloneFromSandbox();
        if (clone != nullptr)
          EnableUpdate(true);
      }
      UpdateVectorButtons();
    }
    else
    {
      CreateNewVector(name);
    }
  }
}


//------------------------------------------------------------------------------
// void OnEditMask(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that edits selected sensor mask from the masks
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnEditMask(wxCommandEvent &event)
{
  Moderator *theModerator = Moderator::Instance();

  int sel = mSelectedMaskListBox->GetSelection();
  if (sel != wxNOT_FOUND)
  {
    std::string name = mSelectedMaskListBox->GetString(sel).WX_TO_STD_STRING;
    GmatBase *obj = theModerator->GetConfiguredObject(name);

    if (obj != nullptr && obj->IsOfType("OpenFramesSensorMask"))
    {
      OpenFramesSensorMask &mask = *static_cast<OpenFramesSensorMask *>(obj);
      if (EditMaskDialog(mask, false) == wxID_OK)
      {
        if (mask.GetName() != name)
          mSelectedMaskListBox->SetString(sel, mask.GetName());
        // Enable apply button if there is a clone in the sandbox
        OpenFramesInterface *clone = OFConfigurationPanel::GetCloneFromSandbox();
        if (clone != nullptr)
          EnableUpdate(true);
      }
      UpdateMaskButtons();
    }
    else
    {
      CreateNewMask(name);
    }
  }
}

//------------------------------------------------------------------------------
// void OnSelectView(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
 * Callback when the highlighted view changes
 *
 * Enables/Disables Edit and Remove buttons
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnSelectView(wxCommandEvent &event)
{
   UpdateViewButtons();
}

//------------------------------------------------------------------------------
// void OnSelectVector(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback when the highlighted vector changes
*
* Enables/Disables Edit and Remove buttons
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnSelectVector(wxCommandEvent &event)
{
  UpdateVectorButtons();
}

//------------------------------------------------------------------------------
// void OnSelectMask(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback when the highlighted sensor mask changes
*
* Enables/Disables Edit and Remove buttons
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnSelectMask(wxCommandEvent &event)
{
  UpdateMaskButtons();
}

//------------------------------------------------------------------------------
// void OnViewUp(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
 * Callback that moves selected view up in the list
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnViewUp(wxCommandEvent &event)
{
   int index = mSelectedViewListBox->GetSelection();
   if (index > 0)
   {
      wxString value = mSelectedViewListBox->GetString(index).WX_TO_STD_STRING;
      mSelectedViewListBox->Delete(index);
      index--;
      mSelectedViewListBox->Insert(value, index);
      mSelectedViewListBox->SetSelection(index);
      mHasViewsChanged = true;
      UpdateViewButtons();
      EnableUpdate(true);
   }
}

//------------------------------------------------------------------------------
// void OnVectorUp(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that moves selected vector up in the list
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnVectorUp(wxCommandEvent &event)
{
   int index = mSelectedVectorListBox->GetSelection();
   if (index > 0)
   {
      wxString value = mSelectedVectorListBox->GetString(index).WX_TO_STD_STRING;
      mSelectedVectorListBox->Delete(index);
      index--;
      mSelectedVectorListBox->Insert(value, index);
      mSelectedVectorListBox->SetSelection(index);
      mHasVectorsChanged = true;
      UpdateVectorButtons();
      EnableUpdate(true);
   }
}

//------------------------------------------------------------------------------
// void OnMaskUp(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that moves selected sensor mask up in the list
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnMaskUp(wxCommandEvent &event)
{
   int index = mSelectedMaskListBox->GetSelection();
   if (index > 0)
   {
      wxString value = mSelectedMaskListBox->GetString(index).WX_TO_STD_STRING;
      mSelectedMaskListBox->Delete(index);
      index--;
      mSelectedMaskListBox->Insert(value, index);
      mSelectedMaskListBox->SetSelection(index);
      mHasMasksChanged = true;
      UpdateMaskButtons();
      EnableUpdate(true);
   }
}

//------------------------------------------------------------------------------
// void OnViewDown(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
 * Callback that moves selected view down in the list
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnViewDown(wxCommandEvent &event)
{
   int index = mSelectedViewListBox->GetSelection();
   if (index != wxNOT_FOUND && index < (mSelectedViewListBox->GetCount() - 1))
   {
      wxString value = mSelectedViewListBox->GetString(index).WX_TO_STD_STRING;
      mSelectedViewListBox->Delete(index);
      index++;
      mSelectedViewListBox->Insert(value, index);
      mSelectedViewListBox->SetSelection(index);
      mHasViewsChanged = true;
      UpdateViewButtons();
      EnableUpdate(true);
   }
}

//------------------------------------------------------------------------------
// void OnVectorDown(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that moves selected vector down in the list
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnVectorDown(wxCommandEvent &event)
{
  int index = mSelectedVectorListBox->GetSelection();
  if (index != wxNOT_FOUND && index < (mSelectedVectorListBox->GetCount() - 1))
  {
    wxString value = mSelectedVectorListBox->GetString(index).WX_TO_STD_STRING;
    mSelectedVectorListBox->Delete(index);
    index++;
    mSelectedVectorListBox->Insert(value, index);
    mSelectedVectorListBox->SetSelection(index);
    mHasVectorsChanged = true;
    UpdateVectorButtons();
    EnableUpdate(true);
  }
}

//------------------------------------------------------------------------------
// void OnMaskDown(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
* Callback that moves selected sensor mask down in the list
*
* @param <event> Event details
*/
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnMaskDown(wxCommandEvent &event)
{
  int index = mSelectedMaskListBox->GetSelection();
  if (index != wxNOT_FOUND && index < (mSelectedMaskListBox->GetCount() - 1))
  {
    wxString value = mSelectedMaskListBox->GetString(index).WX_TO_STD_STRING;
    mSelectedMaskListBox->Delete(index);
    index++;
    mSelectedMaskListBox->Insert(value, index);
    mSelectedMaskListBox->SetSelection(index);
    mHasMasksChanged = true;
    UpdateMaskButtons();
    EnableUpdate(true);
  }
}


//------------------------------------------------------------------------------
// void OnStarOptions(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
 * Callback that shows the star options modal dialog
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnStarOptions(wxCommandEvent &event)
{
   OFStarsDialog *stars = new OFStarsDialog(mBufferInterface, this);
   stars->Centre();
   if (stars->ShowModal() == wxID_OK)
   {
      mHasBufferChanged = true;
      EnableUpdate(true);
   }
   stars->Destroy();
}


//------------------------------------------------------------------------------
// void OnGLOptions(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback that shows an OpenGL options modal dialog
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnGLOptions(wxCommandEvent& event)
{
   OFGLOptionsDialog *options = new OFGLOptionsDialog(mBufferInterface, this);
   options->Centre();
   if (options->ShowModal() == wxID_OK)
   {
      mHasBufferChanged = true;
      EnableUpdate(true);
   }
   options->Destroy();
}


//------------------------------------------------------------------------------
// void OnApply()
//------------------------------------------------------------------------------
/**
 * Callback that saves the data and remain unclosed
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnApply(wxCommandEvent &event)
{
   if (mDataChanged)
   {
      SaveData();
      if (mCanClose)
      {
         EnableUpdate(false);
      }
   }
}


//------------------------------------------------------------------------------
// void OnOk()
//------------------------------------------------------------------------------
/**
 * Callback that saves the data and closes the page
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnOK(wxCommandEvent &event)
{
   if (mDataChanged)
   {
      SaveData();
   }

   if (mCanClose)
   {
      GmatMdiChildFrame* mdichild = (GmatMdiChildFrame*)(GetParent()->GetParent());
      mdichild->Close();
   }
}


//------------------------------------------------------------------------------
// void OnCancel()
//------------------------------------------------------------------------------
/**
 * Callback that closes the page
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnCancel(wxCommandEvent &event)
{
   GmatMdiChildFrame* mdichild = (GmatMdiChildFrame*)(GetParent()->GetParent());
   mdichild->SetDirty(false);
   mdichild->Close();
}


//------------------------------------------------------------------------------
// void OnAbout()
//------------------------------------------------------------------------------
/**
 * Callback that shows About box
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnAbout(wxCommandEvent &event)
{
   // General info
   wxAboutDialogInfo info;
   info.SetName("OpenFramesInterface");
   std::string buildTime = "Built on ";
   buildTime += __DATE__;
   buildTime += ", ";
   buildTime += __TIME__;
   info.SetVersion("R2022a v1");
   info.SetDescription("Modern Visualizations in GMAT\n" + buildTime);
   info.SetCopyright("(C) 2022 Emergent Space Technologies, Inc.");
   info.AddDeveloper("Ravi Mathur");
   info.AddDeveloper("\nMatthew Ruschmann");
   info.AddDeveloper("\nJohn McGreevy");
   info.AddDeveloper("\nDarrel Conway (Thinking Systems, Inc.)");
   info.SetWebSite("http://gitlab.com/EmergentSpaceTechnologies/OpenFramesInterface/wikis/home",
                   "OpenFramesInterface Wiki (check for updates)");
   
   // License info
   info.SetLicense("Released under the Apache 2.0 Open Source License\n"
                   "Developed under SBIR, all SBIR data rights apply\n"
                   "See Wiki for dependencies and licence details");

   // Icon
   FileManager *fm = FileManager::Instance();
   std::string iconFile = fm->FindPath("OFIAboutIcon.png", "ICON_PATH", true);
   wxIcon theIcon(iconFile, wxBITMAP_TYPE_PNG);
   info.SetIcon(theIcon);
   
   // Show standard wx about box
   wxAboutBox(info);
}


//------------------------------------------------------------------------------
// void OnHelp()
//------------------------------------------------------------------------------
/**
 * Callback that shows helps
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnHelp(wxCommandEvent &event)
{
	wxLaunchDefaultBrowser("https://gitlab.com/EmergentSpaceTechnologies/OpenFramesInterface/wikis/docs/user/current/Configuration-Panel");
}


//------------------------------------------------------------------------------
// void OnScript()
//------------------------------------------------------------------------------
/**
 * Callback that shows script
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFConfigurationPanel::OnScript(wxCommandEvent &event)
{
   Moderator *theModerator = Moderator::Instance();
   std::string name = "Scripting for " + mOFInterface->GetName();
   std::string script;

   // Get Views
   for (int sel = 0; sel < mSelectedViewListBox->GetCount(); sel++)
   {
      std::string name = mSelectedViewListBox->GetString(sel).WX_TO_STD_STRING;
      GmatBase *obj = theModerator->GetConfiguredObject(name);

      if (obj != nullptr && obj->IsOfType("OpenFramesView"))
      {
         OpenFramesView &view = *static_cast<OpenFramesView *>(obj);
         script += view.GetGeneratingString() + "\n";
      }
   }

   // Get Vectors
   for (int sel = 0; sel < mSelectedVectorListBox->GetCount(); sel++)
   {
     std::string name = mSelectedVectorListBox->GetString(sel).WX_TO_STD_STRING;
     GmatBase *obj = theModerator->GetConfiguredObject(name);
     if (obj != nullptr && obj->IsOfType("OpenFramesVector"))
     {
       OpenFramesVector &vector = *static_cast<OpenFramesVector *>(obj);
       script += vector.GetGeneratingString() + "\n";
     }
   }
   
   // Get Sensor Masks
   for (int sel = 0; sel < mSelectedMaskListBox->GetCount(); sel++)
   {
     std::string name = mSelectedMaskListBox->GetString(sel).WX_TO_STD_STRING;
     GmatBase *obj = theModerator->GetConfiguredObject(name);
     if (obj != nullptr && obj->IsOfType("OpenFramesSensorMask"))
     {
       OpenFramesSensorMask &mask = *static_cast<OpenFramesSensorMask *>(obj);
       script += mask.GetGeneratingString() + "\n";
     }
   }

   script += mOFInterface->GetGeneratingString();
   ShowScriptDialog(name, script);
}


//------------------------------------------------------------------------------
// virtual void EnableUpdate(bool enable = true)
//------------------------------------------------------------------------------
/**
 * Changes the mode of the dialog based on whether there are data changes to
 * apply
 *
 * @param <enable> If true, then application of data changes is enabled
 */
void OFConfigurationPanel::EnableUpdate(bool enable)
{
   mDataChanged = enable;
   GmatMdiChildFrame* mdichild = (GmatMdiChildFrame*)GetParent()->GetParent();
   mdichild->SetDirty(enable);
   if (mApplyButton != nullptr)
      mApplyButton->Enable(enable);
}


//------------------------------------------------------------------------------
// void UpdateViewButtons()
//------------------------------------------------------------------------------
/**
 * Changes the enabled/disabled state of panel elements based on the selected
 * view
 */
void OFConfigurationPanel::UpdateViewButtons()
{
   int index = mSelectedViewListBox->GetSelection();

   if (index == wxNOT_FOUND)
   {
      mViewDeleteButton->Disable();
      mViewUpButton->Disable();
      mViewDownButton->Disable();
      mViewEditButton->Disable();
   }
   else
   {
      int count = mSelectedViewListBox->GetCount();
      mViewDeleteButton->Enable();
      mViewUpButton->Enable(index > 0);
      mViewDownButton->Enable(index < (count - 1));
      mViewEditButton->Enable();
   }
}

//------------------------------------------------------------------------------
// void UpdateVectorButtons()
//------------------------------------------------------------------------------
/**
* Changes the enabled/disabled state of panel elements based on the selected
* vector
*/
void OFConfigurationPanel::UpdateVectorButtons()
{
	int index = mSelectedVectorListBox->GetSelection();

	if (index == wxNOT_FOUND)
	{
		mVectorDeleteButton->Disable();
		mVectorUpButton->Disable();
		mVectorDownButton->Disable();
		mVectorEditButton->Disable();
	}
	else
	{
		int count = mSelectedVectorListBox->GetCount();
		mVectorDeleteButton->Enable();
		mVectorUpButton->Enable(index > 0);
		mVectorDownButton->Enable(index < (count - 1));
		mVectorEditButton->Enable();
	}
}

//------------------------------------------------------------------------------
// void UpdateMaskButtons()
//------------------------------------------------------------------------------
/**
* Changes the enabled/disabled state of panel elements based on the selected
* sensor mask
*/
void OFConfigurationPanel::UpdateMaskButtons()
{
   int index = mSelectedMaskListBox->GetSelection();

   if (index == wxNOT_FOUND)
   {
      mMaskDeleteButton->Disable();
      mMaskUpButton->Disable();
      mMaskDownButton->Disable();
      mMaskEditButton->Disable();
   }
   else
   {
      int count = mSelectedMaskListBox->GetCount();
      mMaskDeleteButton->Enable();
      mMaskUpButton->Enable(index > 0);
      mMaskDownButton->Enable(index < (count - 1));
      mMaskEditButton->Enable();
   }
}

//------------------------------------------------------------------------------
// void ShowSpacePointOption(const wxString &name, bool show = true,
//                           bool isSc = true)
//------------------------------------------------------------------------------
/**
 * Loads options regarding a specific spacecraft or celestial body into the
 * object configuration page
 *
 * @param <name> Name of the highlight spacecraft to load
 * @param <isSc> True if is a spacecraft, false if is a celestial body
 */
void OFConfigurationPanel::ShowSpacePointOption(const wxString &name, Gmat::ObjectType objType)
{
   wxString str;
   if (!name.IsSameAs(""))
   {
      mSelSpName = name.WX_TO_STD_STRING;

      if (objType == SPACECRAFT_CLIENT_DATA)
      {
         mDrawVelocityCheckBox->Enable();
      }
      mDrawObjectCheckBox->SetValue(mDrawObjectMap[mSelSpName]);
      mDrawTrajectoryCheckBox->SetValue(mDrawTrajectoryMap[mSelSpName]);
      mDrawAxesCheckBox->SetValue(mDrawAxesMap[mSelSpName]);
      mDrawXYPlaneCheckBox->SetValue(mDrawXYPlaneMap[mSelSpName]);
      mDrawBodyLabelBox->SetValue(mDrawLabelMap[mSelSpName]);
      mDrawPropLabelBox->SetValue(mUsePropagateLabelMap[mSelSpName]);
      mDrawCenterPointCheckBox->SetValue(mDrawCenterPointMap[mSelSpName]);
      mDrawEndPointsCheckBox->SetValue(mDrawEndPointsMap[mSelSpName]);
      mDrawVelocityCheckBox->SetValue(mDrawVelocityMap[mSelSpName]);
      mDrawGridCheckBox->SetValue(mDrawGridMap[mSelSpName]);
      str.Printf("%0.1f", mDrawLineWidthMap[mSelSpName]);
      mLineWidthTextCtrl->SetValue(str);
      str.Printf("%u", mDrawMarkerSizeMap[mSelSpName]);
      mMarkerSizeTextCtrl->SetValue(str);
      str.Printf("%u", mDrawFontSizeMap[mSelSpName]);
      mFontSizeSpinCtrl->SetValue(str);
      mFontPositionComboBox->SetValue(mDrawFontPositionMap[mSelSpName]);


      if (objType == CELESTIAL_BODY_CLIENT_DATA)
      {
         mDrawVelocityCheckBox->Disable();
         mDrawVelocityCheckBox->SetValue(false);
      }
      else if (objType == GROUND_STATION_CLIENT_DATA)
      {
         mDrawVelocityCheckBox->Disable();
         mDrawVelocityCheckBox->SetValue(false);
      }

      mObjectSizer->Show(mScOptionSizer, true);
   }
   else
   {
      mObjectSizer->Show(mScOptionSizer, false);
   }
}


//------------------------------------------------------------------------------
// int EditView(OpenFramesView &view)
//------------------------------------------------------------------------------
/**
 * Launches a dialog for editing the specified view
 *
 * @param <view> The view to edit
 * @param <enableUpdate> Apply button enabled as soon as window opens (for new views)
 *
 * @return wxID_CANCEL if no changes applied
 * @return wxID_OK if changes applied
 */
int OFConfigurationPanel::EditViewDialog(OpenFramesView &view, bool enableUpdate)
{
   int modified = wxID_CANCEL;

   wxArrayString objects = mExcludedScList;
   for (wxArrayString::const_iterator s = mExcludedCelesPointList.begin(); s < mExcludedCelesPointList.end(); s++)
   {
      objects.Add(*s);
   }

   OFViewDialog *viewEditor = new OFViewDialog(&view, objects, this, enableUpdate);
   viewEditor->Centre();
   modified = viewEditor->ShowModal();
   viewEditor->Destroy();

   return modified;
}


//------------------------------------------------------------------------------
// int EditVectorDialog(OpenFramesVector &vector)
//------------------------------------------------------------------------------
/**
* Launches a dialog for editing the specified vector
*
* @param <vector> The vector to edit
* @param <enableUpdate> Apply button enabled as soon as window opens (for new vectors)
*
* @return wxID_CANCEL if no changes applied
* @return wxID_OK if changes applied
*/
int OFConfigurationPanel::EditVectorDialog(OpenFramesVector &vector, bool enableUpdate)
{
	int modified = wxID_CANCEL;

	wxArrayString objects = mExcludedScList;
	for (wxArrayString::const_iterator s = mExcludedCelesPointList.begin(); s < mExcludedCelesPointList.end(); s++)
	{
		objects.Add(*s);
	}

	OFVectorDialog *vectorDialog = new OFVectorDialog(&vector, objects, this, enableUpdate);
	vectorDialog->Centre();
	modified = vectorDialog->ShowModal();

	vectorDialog->Destroy();

	return modified;
}


//------------------------------------------------------------------------------
// int EditMaskDialog(OpenFramesSensorMask &mask)
//------------------------------------------------------------------------------
/**
* Launches a dialog for editing the specified sensor mask
*
* @param <mask> The sensor mask to edit
* @param <enableUpdate> Apply button enabled as soon as window opens (for new masks)
*
* @return wxID_CANCEL if no changes applied
* @return wxID_OK if changes applied
*/
int OFConfigurationPanel::EditMaskDialog(OpenFramesSensorMask &mask, bool enableUpdate)
{
   int modified = wxID_CANCEL;

   wxArrayString objects = mExcludedScList;
   for (wxArrayString::const_iterator s = mExcludedCelesPointList.begin(); s < mExcludedCelesPointList.end(); s++)
   {
      objects.Add(*s);
   }

   OFSensorMaskDialog *maskDialog = new OFSensorMaskDialog(&mask, objects, this, enableUpdate);
   if(maskDialog->isValid())
   {
      maskDialog->Centre();
      modified = maskDialog->ShowModal();
   }
   else
   {
      wxMessageBox("Creating or editing a Sensor Mask requires at least one object with a Field of View.",
                   "No Available FOVs", wxOK | wxCENTRE | wxICON_ERROR);
   }

   maskDialog->Destroy();

   return modified;
}


//------------------------------------------------------------------------------
// void CreateNewView(std::string name)
//------------------------------------------------------------------------------
/**
 * Creates a new view object in GMAT
 *
 * @param <name> The name of the new view
 */
void OFConfigurationPanel::CreateNewView(std::string name)
{
   Moderator *theModerator = Moderator::Instance();
   GmatBase *obj = theModerator->CreateObject(GmatType::GetTypeId("OpenFramesView"), "OpenFramesView", name);
   if (obj != nullptr)
   {
      OpenFramesView &view = *static_cast<OpenFramesView *>(obj);
      view.SetStringParameter("ViewFrame", OpenFramesView::ROOT_FRAME_STRING);
      if (EditViewDialog(view, true) == wxID_OK)
      {
         mSelectedViewListBox->Append(obj->GetName());
         mSelectedViewListBox->SetStringSelection(obj->GetName());

         UpdateViewButtons();
         mHasViewsChanged = true;
         EnableUpdate(true);
      }
      else
      {
         theModerator->RemoveObject(GmatType::GetTypeId("OpenFramesView"), name, false);
      }
   }
}

//------------------------------------------------------------------------------
// void CreateNewVector(std::string name)
//------------------------------------------------------------------------------
/**
* Creates a new vector object in GMAT
*
* @param <name> The name of the new view
*/
void OFConfigurationPanel::CreateNewVector(std::string name)
{
	Moderator *theModerator = Moderator::Instance();
	GmatBase *obj = theModerator->CreateObject(GmatType::GetTypeId("OpenFramesVector"), "OpenFramesVector", name);
	if (obj != nullptr)
	{
		OpenFramesVector &vector = *static_cast<OpenFramesVector *>(obj);
		if (EditVectorDialog(vector, true) == wxID_OK)
		{
			mSelectedVectorListBox->Append(obj->GetName());
			mSelectedVectorListBox->SetStringSelection(obj->GetName());

			UpdateVectorButtons();
			mHasVectorsChanged = true;
			EnableUpdate(true);
		}
		else
		{
			theModerator->RemoveObject(GmatType::GetTypeId("OpenFramesVector"), name, false);
		}
	}
}

//------------------------------------------------------------------------------
// void CreateNewMask(std::string name)
//------------------------------------------------------------------------------
/**
* Creates a new sensor mask object in GMAT
*
* @param <name> The name of the new sensor mask
*/
void OFConfigurationPanel::CreateNewMask(std::string name)
{
   Moderator *theModerator = Moderator::Instance();
   GmatBase *obj = theModerator->CreateObject(GmatType::GetTypeId("OpenFramesSensorMask"), "OpenFramesSensorMask", name);
   if (obj != nullptr)
   {
      OpenFramesSensorMask &mask = *static_cast<OpenFramesSensorMask *>(obj);
      if (EditMaskDialog(mask, true) == wxID_OK)
      {
         mSelectedMaskListBox->Append(obj->GetName());
         mSelectedMaskListBox->SetStringSelection(obj->GetName());

         UpdateMaskButtons();
         mHasMasksChanged = true;
         EnableUpdate(true);
      }
      else
      {
         theModerator->RemoveObject(GmatType::GetTypeId("OpenFramesSensorMask"), name, false);
      }
   }
}

void OFConfigurationPanel::ShowScriptDialog(const std::string &title, const std::string &script)
{
	// Create script dialog
	wxDialog *dialog = new wxDialog(this, -1, title.c_str(), wxDefaultPosition, wxDefaultSize,
								   wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER|wxMAXIMIZE_BOX);
	
	// Create script text
	wxTextCtrl *text = new wxTextCtrl(dialog, -1, script.c_str(), wxDefaultPosition, wxSize(500, 550),
									  wxTE_MULTILINE|wxTE_READONLY|wxHSCROLL|wxTE_RICH);
	
	// Create action buttons
	wxButton *okButton = new wxButton(dialog, wxID_OK, "OK");
	okButton->SetToolTip("Close Window");
	wxButton *helpButton = new wxButton(dialog, ID_BUTTON_HELP, GUI_ACCEL_KEY"Help");
	helpButton->SetToolTip("OpenFramesInterface Help (F1)");
	dialog->Bind(wxEVT_BUTTON, &OFConfigurationPanel::OnHelp, this, ID_BUTTON_HELP); // Share help button action with the main configuration panel
	
	// Create sizers to contain everything
	wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);
	buttonSizer->Add(okButton, 0, wxALIGN_CENTER | wxALL, BORDER_SIZE);
	buttonSizer->Add(helpButton, 0, wxALIGN_RIGHT | wxALL, BORDER_SIZE);
	wxFlexGridSizer *sizer = new wxFlexGridSizer(1);
	sizer->Add(text, 1, wxEXPAND|wxALIGN_CENTER|wxALL, BORDER_SIZE);
	sizer->Add(buttonSizer, 0, wxALIGN_CENTER|wxALL, BORDER_SIZE);
	sizer->AddGrowableCol(0, 1);
	sizer->AddGrowableRow(0, 1);
	dialog->SetAutoLayout(true);
	dialog->SetSizer(sizer);
	sizer->SetSizeHints(dialog);
	
	// Set shortcut keys
	wxAcceleratorEntry entries[2];
	entries[0].Set(wxACCEL_NORMAL, WXK_F1, ID_BUTTON_HELP);
	entries[1].Set(wxACCEL_CTRL, static_cast<int>('W'), wxID_OK);
	wxAcceleratorTable accel(2, entries);
	dialog->SetAcceleratorTable(accel);
	
	// Show dialog
	dialog->Centre();
	dialog->ShowModal();
	dialog->Destroy();
}
