//$Id$
//------------------------------------------------------------------------------
//                                  OFVectorDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Yasir Majeed Khan, Emergent Space Technologies, Inc.
// Created: September 24th, 2018
/**
*  @class OFVectorDialog
*  Dialog to configure OpenFramesVector objects
*/
//------------------------------------------------------------------------------

#ifndef OFVECTORDIALOG_hpp
#define OFVECTORDIALOG_hpp

#include "wx/wx.h"
#include "wx/spinctrl.h"
#include <wx/clrpicker.h>           // for wxColorPickerCtrl
#include "RgbColor.hpp"             // for RgbColor


// Forward declaration of referenced types
class OpenFramesVector;


class OFVectorDialog : public wxDialog
{
public:
	/// Title for the dialog titlebar
	static const wxString DIALOG_TITLE;

  OFVectorDialog(OpenFramesVector *forObject, const wxArrayString &objects, wxWindow *parent, bool enableUpdate);
	virtual ~OFVectorDialog();

protected:
  /// Combo box to select the vector source object
  wxComboBox *mVectorSourceComboBox;
  /// Combo box to select the vector type
  wxComboBox *mVectorTypeComboBox;
  /// Combo box to select the vector destination object
  wxComboBox *mVectorDestinationComboBox;
  // vector destination label
  wxStaticText *mVectorDestinationLabel;
  /// Combo box to select the vector length options
  wxComboBox *mVectorLengthComboBox;
  // Body-fixed start point label
  wxStaticText *mBFStartPointLabel;
  // Body-fixed start point X input value (double)
  wxSpinCtrlDouble *mBFStartPointXSpinCtrl;
  // Body-fixed start point Y input value (double)
  wxSpinCtrlDouble *mBFStartPointYSpinCtrl;
  // Body-fixed start point Z input value (double)
  wxSpinCtrlDouble *mBFStartPointZSpinCtrl; 
  // Body-fixed direction label
  wxStaticText *mBFDirectionLabel;
  // Body-fixed direction X input value (double)
  wxSpinCtrlDouble *mBFDirectionXSpinCtrl;
  // Body-fixed direction Y input value (double)
  wxSpinCtrlDouble *mBFDirectionYSpinCtrl;
  // Body-fixed direction Z input value (double)
  wxSpinCtrlDouble *mBFDirectionZSpinCtrl;

  //OPTIONS
  // Text box for renaming the vector
  wxTextCtrl *mVectorName;
  /// Text box for renaming the vector label
  wxTextCtrl *mVectorLabel;
  //Color Pickers
  wxColourPickerCtrl *mVectorColorCtrl;
  wxColour mVectorColor;
  // Vector Length input value (double)
  wxSpinCtrlDouble *mVectorLengthCtrl;

  /// True when the dialog is in apply mode because there are data changes to
  /// be applied
  bool mDataChanged;
  /// The when the dialog is able to close (currently always true)
  bool mCanClose;

  // The object for storing vector data 
  OpenFramesVector *mVector;
  // Possible source and destination objects for the vectors
  wxArrayString mObjects;
  
	/// A sizer to separate dialog buttons from the content page
	wxBoxSizer *mPanelSizer;
	/// A sizer for the content page
	wxSizer *mMiddleSizer;
	/// The top-level sizer for the dialog
	wxStaticBoxSizer *mBottomSizer;
  //Destination Object Drop-down sizer
  wxFlexGridSizer *mDestinationObjectSizer;
  //Body-fixed start point and direction sizer
  wxFlexGridSizer *mBFSizer;

	/// OK button, applies and closes
	wxButton *mOkButton;
	/// Cancel button, prompts to apply then closes
	wxButton *mCancelButton;
   /// Help button displays help
   wxButton *mHelpButton;

	// for initializing data
	void InitializeData();

	// methods based on GmatPanel
	virtual void Create();
	void Refresh();
	virtual void Show();
	void LoadData();
	bool SaveData();
   
	void OnOK(wxCommandEvent &event);
	void OnCancel(wxCommandEvent &event);
   void OnHelp(wxCommandEvent &event);
	void OnSystemClose(wxCloseEvent& event);
   
  void OnComboBoxVectorTypeChange(wxCommandEvent& event);
  void OnComboBoxSourceObjectChange(wxCommandEvent& event);
  void OnComboBoxDestinationObjectChange(wxCommandEvent& event);
  void OnComboBoxVectorLengthChange(wxCommandEvent& event);
  void OnTextChange(wxCommandEvent& event);
	void EnableUpdate(bool enable);
  void OnColorPickerChange(wxColourPickerEvent& event);
  void OnSpinChange(wxCommandEvent& event);


	DECLARE_EVENT_TABLE();

	// IDs for the controls and the menu commands
	enum
	{
		ID_BUTTON_OK = 8000,
		ID_BUTTON_CANCEL,
      ID_BUTTON_HELP,
	};
	enum
	{
		ID_COMBOBOX = 15600,
    ID_COMBOBOX_VECTOR_TYPE,
    ID_COMBOBOX_VECTOR_SOURCE,
    ID_COMBOBOX_VECTOR_DESTINATION,
    ID_COMBOBOX_VECTOR_LENGTH,
    ID_COLOR_CTRL,
		CHECKBOX,
		RADIOBUTTON,
		STATIC_TEXT,
		ID_TEXTCTRL,
    ID_TEXTCTRL_VECTOR_NAME,
		ID_SPINCTRL,
		COPY_UP_BUTTON,
	};

private:
	/// Up arrow image
	//static const wxBitmap BITMAP_UP_ARROW;
};

#endif
