//$Id$
//------------------------------------------------------------------------------
//                                  TimeDilator
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: February 13, 2018
/**
 *  @class TimeDilator
 *  A widget for constraining limits of the time slider.
 */
//------------------------------------------------------------------------------

#ifndef d
#define TIMEDILATOR_hpp

#include "OpenFramesInterface_defs.hpp"

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif


/// Triggered when the current time is changed by user input
wxDECLARE_EVENT(DIALATE_CURRENT_TIME, wxCommandEvent);


class OpenFramesInterface_API TimeDilator : public wxControl
{
public:
   TimeDilator();
   TimeDilator(wxWindow *parent, wxWindowID winid, const wxPoint &pos = wxDefaultPosition,
                const wxSize &size = wxDefaultSize, long style = wxBORDER_NONE, const wxValidator &val = wxDefaultValidator,
                const wxString &name = "TimeDilator");
   virtual ~TimeDilator();

   bool Create(wxWindow *parent,  wxWindowID winid, const wxPoint &pos = wxDefaultPosition,
               const wxSize &size = wxDefaultSize, long style = wxBORDER_NONE, const wxValidator &val = wxDefaultValidator,
               const wxString &name = "TimeDilator");

   Real GetCurrent() const { return mCurrentTime; }
   void SetLimits(Real start, Real end);
   void SetCurrent(Real current);

protected:
   void Init();
   virtual wxSize DoGetBestSize() const;
   void OnPaint(wxPaintEvent &);
   void OnSize(wxSizeEvent &);
   void TriggerRedraw();
   void Draw(wxDC &dc);
   void OnMouseMove(wxMouseEvent &event);
   void OnMouseLeftDown(wxMouseEvent &event);
   void OnMouseLeftUp(wxMouseEvent &event);
   void OnMouseRightUp(wxMouseEvent &event);
   void OnMouseWheel(wxMouseEvent &event);
   void OnMenuSelection(wxCommandEvent &event);

   enum {
      ID_ZOOM_LEVEL = 15600,
      ID_ZOOM_IN,
      ID_ZOOM_OUT,
      ID_ZOOM_NORMAL,
      ID_CURRENT_TIME,
      ID_START_TIME,
      ID_END_TIME,
   };

private:
   enum GrabbedMouseState
   {
      IDLE, //< Left mouse button is up
      MOVING_START, //< Left mouse button selected the start limit
      MOVING_END, //< Left mouse button selected the end limit
      MOVING_CURRENT, //< Left mouse button selected current time in bottom
      TRANSLATING, //< Left mouse button selected the dialated time range
      SETTING_CURRENT, //< Left mouse button selected the current time handle in top
   };
   enum CursorState
   {
      TIME_SLIDER,
      DILATOR_MOVE_TIME,
      DILATOR_MOVE_WINDOW,
      DILATOR_MOVE_TIME_WINDOW,
      DILATOR_SIZE_WINDOW,
   };
   class TimeFraction
   {
   public:
      double fraction;
      bool inRange;
   };

   static const wxString TOOLTIP_TIME_SLIDER;
   static const wxString TOOLTIP_DILATOR_MOVE_TIME;
   static const wxString TOOLTIP_DILATOR_MOVE_WINDOW;
   static const wxString TOOLTIP_DILATOR_MOVE_TIME_WINDOW;
   static const wxString TOOLTIP_DILATOR_SIZE_WINDOW;

   wxDECLARE_DYNAMIC_CLASS(TimeDilator);
   wxDECLARE_EVENT_TABLE();

   void SetDilation(Real start, Real end, bool scaleCurrentTime = true);
   wxRect GetRangeInLimitsPixelRect(const wxSize &clientSize);
   int GetCurrentInLimitsPixelPos(const wxSize &clientSize);
   int GetCurrentInRangePixelPos(const wxSize &clientSize);
   void SetCursorState(CursorState state);
   void TriggerCurrentTimeEvent(Real time);
   TimeFraction GetCurrentTimeFraction() const;
   void SetCurrentTimeFraction(TimeFraction fraction);
   void ZoomTime(double zoomAmount);

   wxBitmap *mBuffer;
   GrabbedMouseState mMouseState;
   Real mStartLimit;
   Real mEndLimit;
   Real mStartTime;
   Real mEndTime;
   Real mCurrentTime;
   std::string mTimeFormat; // The GMAT time format used by OFDateTimeDialog when selecting a time
   Real mMouseInitial;
   Real mStartTimeAtMouseDown;
   TimeFraction mTimeFracAtMouseDown;
   bool mCurrentTimeSet;
   CursorState mCursorState;
};

#endif
