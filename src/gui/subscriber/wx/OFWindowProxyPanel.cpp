//$Id$
//------------------------------------------------------------------------------
//                                  OFWindowProxyPanel
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 1, 2015
/**
*  OFWindowProxyPanel class
*  Subclasses wxFrame to implement the window that OpenFrames will render onto.
*/
//------------------------------------------------------------------------------

#include "OFScene.hpp"
#include "OFGLCanvas.hpp"
#include "OFWindowProxyPanel.hpp"
#include "OpenFramesInterface.hpp"
#include "TimeDilator.hpp"

#include "MessageInterface.hpp"
#include "Moderator.hpp"
#include "gmatwxdefs.hpp"

#include "bitmaps/RunAnimation.xpm"
#include "bitmaps/PauseAnimation.xpm"
#include "bitmaps/FasterAnimation.xpm"
#include "bitmaps/SlowerAnimation.xpm"
#include "bitmaps/forward.xpm"
#include "bitmaps/back.xpm"
#include "t0.xpm"
#include "tf.xpm"
#include "resetview.xpm"
#include "pause_link.xpm"
#include "run_link.xpm"

#include <float.h>

wxBEGIN_EVENT_TABLE(OFWindowProxyPanel, wxPanel)
EVT_BUTTON(ID_PLAY_PAUSE, OFWindowProxyPanel::OnToolbar)
EVT_BUTTON(ID_SLOWER, OFWindowProxyPanel::OnToolbar)
EVT_BUTTON(ID_FASTER, OFWindowProxyPanel::OnToolbar)
EVT_BUTTON(ID_SCALE_SIGN, OFWindowProxyPanel::OnToolbar)
EVT_BUTTON(ID_RESET_VIEW, OFWindowProxyPanel::OnToolbar)
EVT_TEXT(ID_SCALE_TEXT, OFWindowProxyPanel::OnToolbar)
EVT_COMBOBOX(ID_VIEW_SELECT, OFWindowProxyPanel::OnViewSelect)
EVT_COMMAND(ID_TIME_DILATOR, DIALATE_CURRENT_TIME, OFWindowProxyPanel::OnTimeSlider)
EVT_MOUSEWHEEL(OFWindowProxyPanel::MouseWheel)
wxEND_EVENT_TABLE()


const wxBitmap OFWindowProxyPanel::FORWARD_BITMAP(forward_xpm);
const wxBitmap OFWindowProxyPanel::BACK_BITMAP(back_xpm);

const std::string OFWindowProxyPanel::TOOLTIP_PLAY_ANIMATION("Play animation (p)");
const std::string OFWindowProxyPanel::TOOLTIP_PAUSE_ANIMATION("Pause animation (p)");
const std::string OFWindowProxyPanel::TOOLTIP_PLAY_LINKED("Play linked animations (p)");
const std::string OFWindowProxyPanel::TOOLTIP_PAUSE_LINKED("Pause linked animation (p)");
const std::string OFWindowProxyPanel::TOOLTIP_POSITIVE_SCALE("Toggle time scale sign to positive (n)");
const std::string OFWindowProxyPanel::TOOLTIP_NEGATIVE_SCALE("Toggle time scale sign to negative (n)");


/**
 * Constructor
 *
 * @param <forObject> @ref OpenFramesInterface object that defines the plot
 */
OFWindowProxyPanel::OFWindowProxyPanel(GmatBase *forObject, wxWindow *parent) :
   wxPanel(parent),
   mForObject(static_cast<OpenFramesInterface *>(forObject)),
   mScene(nullptr),
   mViewList(nullptr),
   mTimeDilator(nullptr),
   mScale(0.0),
   mScaleSign(1),
   mIsPlaying(true),
   mShowSyncedButtons(false),
   mScaleValid(true)
{
   minimizeOnRun = false; // OpenFrames window should not be minimized at each mission run

   wxArrayString emptyList;

   wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

   mPlayPauseButton = new wxButton(this, ID_PLAY_PAUSE, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxBU_NOTEXT | wxBU_EXACTFIT);
   mPlayPauseButton->SetBitmap(wxBitmap(RunAnimation_xpm));
   mPlayPauseButton->SetToolTip(TOOLTIP_PLAY_ANIMATION);
   mSlowerButton = new wxButton(this, ID_SLOWER, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxBU_NOTEXT | wxBU_EXACTFIT);
   mSlowerButton->SetBitmap(wxBitmap(SlowerAnimation_xpm));
   mSlowerButton->SetToolTip("Slower animation (-)");
   mFasterButton = new wxButton(this, ID_FASTER, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxBU_NOTEXT | wxBU_EXACTFIT);
   mFasterButton->SetBitmap(wxBitmap(FasterAnimation_xpm));
   mFasterButton->SetToolTip("Faster animation (+)");

   // On Linux with wx3.0.2, wxTextCtrl() triggers an event if called
   // with non-default text. This causes a segfault in the event handler.
   // So create it with default text here, and initialize later.
   mScaleText = new wxTextCtrl(this, ID_SCALE_TEXT, wxEmptyString, wxDefaultPosition,  wxSize(50,wxDefaultSize.GetY()), wxTE_LEFT);
   mScaleText->SetToolTip("Time scale (+ and - keys perform fine adjustments)");

   mScaleSignButton = new wxButton(this, ID_SCALE_SIGN, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxBU_NOTEXT | wxBU_EXACTFIT);
   mScaleSignButton->SetBitmap(FORWARD_BITMAP);
   mScaleSignButton->SetToolTip(TOOLTIP_NEGATIVE_SCALE);
   mViewList = new wxComboBox(this, ID_VIEW_SELECT, wxEmptyString, wxDefaultPosition, wxDefaultSize, emptyList, wxCB_READONLY);
   mViewList->SetToolTip("Change current view (v or V)");
   wxButton *resetViewButton = new wxButton(this, ID_RESET_VIEW, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxBU_NOTEXT | wxBU_EXACTFIT);
   resetViewButton->SetBitmap(wxBitmap(resetview_xpm));
   resetViewButton->SetToolTip("Reset view to default (spacebar)");

   mTimeDilator = new TimeDilator(this, ID_TIME_DILATOR);

   mToolSizer = new wxBoxSizer(wxHORIZONTAL);
   mToolSizer->Add(mPlayPauseButton, 0, wxALIGN_CENTER_VERTICAL | wxALL, TOOL_BORDER_WIDTH);
   mToolSizer->Add(mScaleSignButton, 0, wxALIGN_CENTER_VERTICAL | wxALL, TOOL_BORDER_WIDTH);
   mToolSizer->Add(mSlowerButton, 0, wxALIGN_CENTER_VERTICAL | wxALL, TOOL_BORDER_WIDTH);
   mToolSizer->Add(mFasterButton, 0, wxALIGN_CENTER_VERTICAL | wxALL, TOOL_BORDER_WIDTH);
   mToolSizer->Add(mScaleText, 0, wxALIGN_CENTER_VERTICAL | wxALL, TOOL_BORDER_WIDTH);
   mToolSizer->Add(mTimeDilator, 1, wxALIGN_CENTER_VERTICAL | wxALL, TOOL_BORDER_WIDTH);
   mToolSizer->Add(mViewList, 0, wxALIGN_CENTER_VERTICAL | wxALL, TOOL_BORDER_WIDTH);
   mToolSizer->Add(resetViewButton, 0, wxEXPAND | wxALL, TOOL_BORDER_WIDTH);

   sizer->Add(mToolSizer, 0, wxEXPAND);
   SetSizer(sizer);

   mScene = new OFScene(this);

   // Initialize text after all class objects have been initialized
   mScaleText->SetValue(wxT("-00000x"));

   sizer->SetSizeHints(this);
   SetAutoLayout(true);
}


/**
 * Destructor
 */
OFWindowProxyPanel::~OFWindowProxyPanel()
{
   // In wxWidgets, Child windows (mScene, etc.) are deleted from within the parent destructor.
   // http://docs.wxwidgets.org/3.1/overview_windowdeletion.html#overview_windowdeletion_deletion
   RemoveObject(mForObject);
   if (mScene != nullptr)
      delete mScene;
}


std::string OFWindowProxyPanel::GetForObjectName() const
{
   if (mForObject == nullptr)
      return "";
   else
      return mForObject->GetName();
}


void OFWindowProxyPanel::SetObject(OpenFramesInterface *object)
{
   // Set object is not being called from OpenFramesInterface, so calling mForObject->RemoveWidget() is okay
   if (mForObject != nullptr && mForObject != object)
      RemoveObject(mForObject);
   mForObject = object;
}


void OFWindowProxyPanel::RemoveObject(const OpenFramesInterface *object)
{
   if (mForObject != nullptr && mForObject == object)
   {
      mScene->RemoveTimeSynchronization();
      mForObject->RemoveWidget();
      mForObject = nullptr;
   }
}


/**
 * Adds a canvas to the panel
 *
 * OFScene uses this callback to provide a canvas that adheres to the
 * OpenFramesInterface's settings. If the aspect ratio of the canvas is fixed,
 * then the parent window of this panel is resized to fit the canvas as base as
 * possible.
 *
 * @param canvas The canvas to add to the window
 * @param shaped Set true to fix the canvas aspect ratio
 */
void OFWindowProxyPanel::SetCanvas(OFGLCanvas *canvas, bool shaped)
{
   // add the canvas
   GetSizer()->Add(canvas, 1, wxEXPAND | static_cast<int>(shaped)*wxSHAPED | wxALIGN_CENTER);
   GetSizer()->Layout();

   // if not maximized and using VR, then resize to crop empty borders
   wxMDIChildFrame *parent = static_cast<wxMDIChildFrame *>(GetParent()->GetParent());
   if (shaped && !parent->IsMaximized())
   {
      wxSize clientSize = parent->GetClientSize();
      wxSize canvasSize = canvas->GetSize();
      wxSize minSize = GetSizer()->GetMinSize();
      wxSize toolSize = mToolSizer->GetMinSize();
      if (canvasSize.GetWidth() < clientSize.GetWidth())
      {
         if (canvasSize.GetWidth() < minSize.GetWidth())
            parent->SetClientSize(minSize.GetWidth(), clientSize.GetHeight());
         else
            parent->SetClientSize(canvasSize.GetWidth(), clientSize.GetHeight());
      }
      else if (canvasSize.GetHeight() < clientSize.GetHeight() - toolSize.GetHeight())
      {
         if (canvasSize.GetHeight() < minSize.GetHeight())
            parent->SetClientSize(clientSize.GetWidth(), minSize.GetHeight());
         else
            parent->SetClientSize(clientSize.GetWidth(), canvasSize.GetHeight() + toolSize.GetHeight());
      }
   }

   // finally give the canvas focus
   canvas->SetFocus();
}


/**
 * Provides a reference to the underlying scene
 *
 * @return The scene
 *
 * Used by OpenFramesInterface to call functions in OFGLCanvas directly
 */
OFScene &OFWindowProxyPanel::GetScene()
{
   return *mScene;
}


/**
 * Shows or hides the sizer containing our makeshift toolbar at the top of the
 * panel
 *
 * @param <show> Toolbar shown if true, hidden otherwise
 */
void OFWindowProxyPanel::ShowToolbar(bool show)
{
   mToolSizer->Show(show);
   Layout();
}


/**
 * Sets the values in the views combobox, which clears existing values
 *
 * @param <views> List of views to set as entries in the combobox
 * @param <current> Index of the view to select in the combobox after the entries
 *                  are set
 */
void OFWindowProxyPanel::SetListOfViews(const wxArrayString &views)
{
   int maxWidth = -1;

   mViewList->Clear();
   mViewList->Append(views);
   mViewList->SetSelection(0);
   ResizeViewComboBox();
}


/**
 * Sets the values in the views combobox
 *
 * @param <views> List of views to set as entries in the combobox
 */
void OFWindowProxyPanel::AppendToListOfViews(const wxString &view)
{
   mViewList->Append(view);
   ResizeViewComboBox();
}


/**
 * Resize the combobox to fit the contents
 */
void OFWindowProxyPanel::ResizeViewComboBox()
{
   int maxWidth = -1;
   for (int i = 0; i < mViewList->GetCount(); i++)
   {
      int width;
      mViewList->GetTextExtent(mViewList->GetString(i), &width, nullptr);
      if (width > maxWidth)
         maxWidth = width;
   }
   mViewList->SetSize(wxSize(maxWidth + 24, -1));
   mToolSizer->Layout();
}


/**
 * Sets the current view in the combobox
 *
 * @param <current> Index of the view to select in the combobox
 */
void OFWindowProxyPanel::SetCurrentViewByIndex(int current)
{
   mViewList->SetSelection(current);
}


wxString OFWindowProxyPanel::GetCurrentViewString()
{
   return mViewList->GetStringSelection();
}


void OFWindowProxyPanel::SelectViewByString(const wxString &view)
{
   bool found = mViewList->SetStringSelection(view);
   if (found)
   {
      int sel = mViewList->GetCurrentSelection();
      mScene->SetCurrentView(static_cast<unsigned int>(sel));
   }
}


/**
 * Sets the time limits of the slider
 *
 * @param <initialTime> Initial time for the slider
 * @param <finalTime> Final time for the slider
 */
void OFWindowProxyPanel::SetTimeSliderLimits(Real initialTime, Real finalTime)
{
   mTimeDilator->SetLimits(initialTime, finalTime);
}


/**
 * Sets the value of the slider
 *
 * @param <timeOffset> The time to move the slider to
 */
void OFWindowProxyPanel::SetTimeSliderValue(Real timeOffset)
{
   mTimeDilator->SetCurrent(timeOffset);
}


/**
 * Sets the time scale without pushing an update to OpenFrames
 *
 * This functionality is used for setting the scale to a know value within
 * OpenFrames or OFScene.
 *
 * @param <scale> The time scale to move the slider to
 */
void OFWindowProxyPanel::SetTimeScaleValue(Real scale, bool updateText)
{
   if (scale < 0)
   {
      if (mScaleSign != -1)
      {
         mScaleSignButton->SetToolTip(TOOLTIP_POSITIVE_SCALE);
         mScaleSignButton->SetBitmap(BACK_BITMAP);
         mScaleSign = -1;
      }
   }
   else
   {
      if (mScaleSign != 1)
      {
         mScaleSignButton->SetToolTip(TOOLTIP_NEGATIVE_SCALE);
         mScaleSignButton->SetBitmap(FORWARD_BITMAP);
         mScaleSign = 1;
      }
   }

   // Set the text
   if (updateText && !mScaleText->HasFocus())
      SetScaleText(scale);
}


/**
 * Increases the value of the time scale and updates slider
 */
void OFWindowProxyPanel::IncreaseScale()
{
   Real scale = mScene->GetTimeScaleFromWindowProxy();

   int sign = (scale > 0.0) - (scale < 0.0);

   if (sign == 1)
   {
      if (scale < 1.0)
      {
         scale = 1.0 / DownByTwo(1.0 / scale);
      }
      else
      {
         scale = UpByTwo(scale);
      }
   }
   else if (sign == -1)
   {
      if (scale > -1.0)
      {
         scale = 1.0 / -DownByTwo(1.0 / -scale);
      }
      else
      {
         scale = -UpByTwo(-scale);
      }
   }
   else // (sign == 0)
   {
      scale = 1.0;
   }

   mScene->SetTimeScaleToWindowProxy(scale);
   SetTimeScaleValue(scale);
}


/**
 * Decreases the value of the time scale and updates slider
 */
void OFWindowProxyPanel::DecreaseScale()
{
   Real scale = mScene->GetTimeScaleFromWindowProxy();

   int sign = (scale > 0.0) - (scale < 0.0);

   if (sign == 1)
   {
      if (scale < 1.0)
      {
         scale = 1.0 / UpByTwo(1.0 / scale);
      }
      else
      {
         scale = DownByTwo(scale);
      }
   }
   else if (sign == -1)
   {
      if (scale > -1.0)
      {
         scale = 1.0 / -UpByTwo(1.0 / -scale);
      }
      else
      {
         scale = -DownByTwo(-scale);
      }
   }
   else // (sign == 0)
   {
      scale = -1.0;
   }

   mScene->SetTimeScaleToWindowProxy(scale);
   SetTimeScaleValue(scale);
}


Real OFWindowProxyPanel::UpByTwo(Real init)
{
   if (init < 500000.0)
      return ( init * 2.0 );
   else
      return init;
}


Real OFWindowProxyPanel::DownByTwo(Real init)
{
   if (init >= 0.00002)
      return ( init / 2.0 );
   else
      return init;
}


/**
 * Sets the time scale text to the given value
 */
void OFWindowProxyPanel::SetScaleText(Real scaleFactor)
{
   if (mScale != scaleFactor)
   {
      if (!mScaleValid)
      {
         mScaleValid = true;
         mScaleText->SetBackgroundColour(mScaleBackgroundColor);
         mScaleText->SetForegroundColour(mScaleForegroundColor);
         mScaleText->Refresh();
         mScaleText->Update();
      }

      // Update the static text
      mScale = scaleFactor;
      std::string str = std::to_string(scaleFactor);
      int newLength = str.size();
      bool foundDecimal = false;
      bool seenNonZero = false;
      for (int ii = newLength - 1; ii > 0; ii--)
      {
         if (!seenNonZero)
         {
            if (str[ii] == '0' || str[ii] == '.')
               newLength = ii;
            else
               seenNonZero = true;
         }
         if (str[ii] == '.')
         {
            foundDecimal = true;
            break;
         }
      }
      if (foundDecimal)
         str.erase(newLength, std::string::npos);
      str.append("x");
      if (mScaleText->GetValue().WX_TO_STD_STRING != str)
         mScaleText->ChangeValue(str);
   }
}


/**
 * Changes the UI between play mode and pause mode
 *
 * @param <setToPlay> UI set to play mode if true, otherwise pause
 */
void OFWindowProxyPanel::SetPlayPauseMode(bool setToPlay)
{
   bool forceButtonUpdate = false;
   if (!mShowSyncedButtons && mScene->IsTimeSynced())
   {
      mShowSyncedButtons = true;
      forceButtonUpdate = true;
   }
   else if (mShowSyncedButtons && !mScene->IsTimeSynced())
   {
      mShowSyncedButtons = false;
      forceButtonUpdate = true;
   }

   if (forceButtonUpdate || mIsPlaying != setToPlay)
   {
      mIsPlaying = setToPlay;
      if (mIsPlaying)
      {
         if (mShowSyncedButtons)
         {
            mPlayPauseButton->SetToolTip(TOOLTIP_PLAY_LINKED);
            mPlayPauseButton->SetBitmap(wxBitmap(run_link_xpm));
         }
         else
         {
            mPlayPauseButton->SetToolTip(TOOLTIP_PLAY_ANIMATION);
            mPlayPauseButton->SetBitmap(wxBitmap(RunAnimation_xpm));
         }
      }
      else
      {
         if (mShowSyncedButtons)
         {
            mPlayPauseButton->SetToolTip(TOOLTIP_PAUSE_LINKED);
            mPlayPauseButton->SetBitmap(wxBitmap(pause_link_xpm));
         }
         else
         {
            mPlayPauseButton->SetToolTip(TOOLTIP_PAUSE_ANIMATION);
            mPlayPauseButton->SetBitmap(wxBitmap(PauseAnimation_xpm));
         }
      }
   }
}


void OFWindowProxyPanel::ToggleScaleSign()
{
   Real scale = mScene->GetTimeScaleFromWindowProxy();
   scale = -1.0 * scale;
   mScene->SetTimeScaleToWindowProxy(scale);
   SetTimeScaleValue(scale);
}


/**
 * Callback that handles button events in our makeshift toolbar
 *
 * @param <event> Event details
 */
void OFWindowProxyPanel::OnToolbar(wxCommandEvent &event)
{
   switch (event.GetId())
   {
   case ID_PLAY_PAUSE:
      mScene->TogglePlayback();
      mScene->GetCanvas().SetFocus();
      break;
   case ID_SLOWER:
      DecreaseScale();
      mScene->GetCanvas().SetFocus();
      break;
   case ID_FASTER:
      IncreaseScale();
      mScene->GetCanvas().SetFocus();
      break;
   case ID_SCALE_SIGN:
   {
      ToggleScaleSign();
      mScene->GetCanvas().SetFocus();
      break;
   }
   case ID_RESET_VIEW:
   {
      mScene->ResetTrackBall();
      mScene->GetCanvas().SetFocus();
      break;
   }
   case ID_SCALE_TEXT:
   {
      double scale;
      wxString text = mScaleText->GetValue();
      if (text.Length() > 0 && text.Last() == 'x')
         text = text.BeforeLast('x');
      if (text.ToDouble(&scale))
      {
         mScene->SetTimeScaleToWindowProxy(scale);
         SetTimeScaleValue(scale, false);
         if (!mScaleValid)
         {
            mScaleValid = true;
            mScaleText->SetBackgroundColour(mScaleBackgroundColor);
            mScaleText->SetForegroundColour(mScaleForegroundColor);
            mScaleText->Refresh();
            mScaleText->Update();
         }
      }
      else
      {
         if (text.Length() > 0)
         {
            if (mScaleValid)
            {
               mScaleValid = false;
               mScaleForegroundColor = mScaleText->GetForegroundColour();
               mScaleBackgroundColor = mScaleText->GetBackgroundColour();
               mScaleText->SetBackgroundColour(*wxRED);
               mScaleText->SetForegroundColour(*wxWHITE);
               mScaleText->Refresh();
               mScaleText->Update();
            }
         }
      }
      break;
   }
   default:
   {
      mScene->GetCanvas().SetFocus();
      break;
   }
   }
}


/**
 * Callback that handles selections in the views combobox
 *
 * @param <event> Event details
 */
void OFWindowProxyPanel::OnViewSelect(wxCommandEvent &event)
{
   int sel = mViewList->GetSelection();
   if (sel != wxNOT_FOUND)
   {
      mScene->SetCurrentView(static_cast<unsigned int>(sel));
   }
   mScene->GetCanvas().SetFocus();
}


/**
 * Callback that handles changes in the time slider
 *
 * @param <event> Event details
 */
void OFWindowProxyPanel::OnTimeSlider(wxCommandEvent &event)
{
   if (event.GetEventObject() == mTimeDilator)
   {
      Real offset = mTimeDilator->GetCurrent();
      mScene->SetTimeInWindowProxy(offset);
   }
   mScene->GetCanvas().SetFocus();
}


/**
 * Callback that forwards mouse wheel event to canvas
 *
 * It seems like wxGLCanvas will not receive scroll wheel events when its
 * parent is a wxFrame. This has been reported in 2007, and maybe it remains
 * unchanged:
 * http://wxwidgets.10942.n7.nabble.com/Mousewheel-and-GL-canvas-td16159.html
 *
 * @param <event> Event details
 */
void OFWindowProxyPanel::MouseWheel(wxMouseEvent& event)
{
   mScene->GetCanvas().MouseWheel(event);
}


/**
 * Overrides function to destroy the window
 *
 * This function is called as soon as possible and before the object's
 * destructor.
 */
bool OFWindowProxyPanel::Destroy()
{
   RemoveObject(mForObject);
   delete mScene;
   mScene = nullptr;
   return wxPanel::Destroy();
}
