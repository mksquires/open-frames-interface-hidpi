//$Id$
//------------------------------------------------------------------------------
//                                  OFViewDialog
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
//------------------------------------------------------------------------------
/**
 * Dialog to configure OpenFramesView objects
 */
//------------------------------------------------------------------------------

#include "OFViewDialog.hpp"
#include "OpenFramesView.hpp"

#include "gmatwxdefs.hpp"
#include "MessageInterface.hpp"
#include "Moderator.hpp"

#include "bitmaps/up.xpm"

#include <float.h>


//------------------------------
// event tables for wxWindows
//------------------------------
BEGIN_EVENT_TABLE(OFViewDialog, wxDialog)
   EVT_BUTTON(ID_BUTTON_OK, OFViewDialog::OnOK)
   EVT_BUTTON(ID_BUTTON_CANCEL, OFViewDialog::OnCancel)
   EVT_BUTTON(ID_BUTTON_HELP, OFViewDialog::OnHelp)
   EVT_CLOSE(OFViewDialog::OnSystemClose)

   EVT_CHECKBOX(CHECKBOX, OFViewDialog::OnCheckBoxChange)
   EVT_RADIOBUTTON(RADIOBUTTON, OFViewDialog::OnRadioButtonChange)
   EVT_COMBOBOX(ID_COMBOBOX, OFViewDialog::OnComboBoxChange)
   EVT_TEXT(ID_COMBOBOX, OFViewDialog::OnComboBoxChange)
   EVT_TEXT(ID_TEXTCTRL, OFViewDialog::OnTextChange)
   EVT_TEXT(ID_SPINCTRL, OFViewDialog::OnSpinChange)
   EVT_BUTTON(COPY_UP_BUTTON, OFViewDialog::OnCopyUp)
END_EVENT_TABLE()


const wxString OFViewDialog::DIALOG_TITLE = "View Definition";


const wxBitmap OFViewDialog::BITMAP_UP_ARROW(up_xpm);


//------------------------------------------------------------------------------
// OFViewDialog(wxWindow *parent, const wxString &subscriberName)
//------------------------------------------------------------------------------
/**
 * Constructs OFViewDialog object.
 *
 * @param <forObject> The view to change settings for
 * @param <objects> List of possible objects to point the view at
 * @param <parent> input parent
 * @param <enableUpdate> Enable apply mode immediately (for new views)
 *
 * @note Creates the OFViewDialog GUI
 */
//------------------------------------------------------------------------------
OFViewDialog::OFViewDialog(OpenFramesView *forObject, const wxArrayString &objects, wxWindow *parent, bool enableUpdate) :
    wxDialog(parent, -1, DIALOG_TITLE, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxMAXIMIZE_BOX),
    mView(forObject),
    mObjects(objects),
    mRealDataChanged(false),
    mDataChanged(enableUpdate),
    mCanClose(true)
{
   InitializeData();
   Create();
   Refresh();
   Show();
   EnableUpdate(enableUpdate);

   // shortcut keys
   wxAcceleratorEntry entries[2];
   entries[0].Set(wxACCEL_NORMAL, WXK_F1, ID_BUTTON_HELP);
   entries[1].Set(wxACCEL_CTRL, static_cast<int>('W'), ID_BUTTON_CANCEL);
   wxAcceleratorTable accel(2, entries);
   this->SetAcceleratorTable(accel);
}


//------------------------------------------------------------------------------
// ~OFViewDialog()
//------------------------------------------------------------------------------
/**
 * Destructor
 */
OFViewDialog::~OFViewDialog()
{
   // nothing to clean up here
}


//------------------------------------------------------------------------------
// void InitializeData()
//------------------------------------------------------------------------------
/**
 * Initializes data that is not wxWidgets
 */
void OFViewDialog::InitializeData()
{
   mDataChanged = false;
   mCanClose = true;
}


//------------------------------------------------------------------------------
// void Create()
//------------------------------------------------------------------------------
/**
 * Creates wxWidgets and adds them to the panel
 */
void OFViewDialog::Create()
{
   int bsize = 2; // border size

   // create axis array
   wxArrayString emptyList;

   //-----------------------------------------------------------------
   // Main sizers and buttons
   //-----------------------------------------------------------------
   mPanelSizer = new wxBoxSizer(wxVERTICAL);
   mMiddleSizer = (wxSizer*)(new wxStaticBoxSizer(wxVERTICAL, this));
   mBottomSizer = new wxStaticBoxSizer(wxVERTICAL, this );
   wxBoxSizer *buttonSizer = new wxBoxSizer(wxHORIZONTAL);

   // create bottom buttons
   mOkButton = new wxButton(this, ID_BUTTON_OK, "OK", wxDefaultPosition, wxDefaultSize, 0);
   mOkButton->SetToolTip("Apply Changes and Close");
   mCancelButton = new wxButton(this, ID_BUTTON_CANCEL, "Cancel", wxDefaultPosition, wxDefaultSize, 0);
   mCancelButton->SetToolTip("Close Without Applying Changes");
   mHelpButton = new wxButton(this, ID_BUTTON_HELP, GUI_ACCEL_KEY"Help", wxDefaultPosition, wxDefaultSize, 0);
   mHelpButton->SetToolTip("OpenFramesInterface Help (F1)");
   mOkButton->SetDefault();

   buttonSizer->Add(mOkButton, 0, wxALIGN_CENTER | wxALL, bsize);
   buttonSizer->Add(mCancelButton, 0, wxALIGN_CENTER | wxALL, bsize);
   buttonSizer->Add(0, 1, wxALIGN_RIGHT);
   buttonSizer->Add(mHelpButton, 0, wxALIGN_RIGHT | wxALL, bsize);

   mBottomSizer->Add(buttonSizer, 0, wxGROW | wxALL, bsize);

   //-----------------------------------------------------------------
   // Create the viewalyzer
   //-----------------------------------------------------------------
   wxStaticText *viewNameLabel = new wxStaticText(this, -1, wxT("Name of View"), wxDefaultPosition, wxSize(-1,-1), 0);
   mViewName = new wxTextCtrl(this, ID_TEXTCTRL, mView->GetName(), wxDefaultPosition, wxDefaultSize, 0);
   wxFlexGridSizer *viewNameSizer = new wxFlexGridSizer(2, 0, 0);
   viewNameSizer->Add(viewNameLabel, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   viewNameSizer->Add(mViewName, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);

   wxStaticText *viewObjLabel = new wxStaticText(this, -1, wxT("View Point Reference"), wxDefaultPosition, wxSize(-1,-1), 0);
   mViewObjComboBox = new wxComboBox(this, ID_COMBOBOX, "View Frame", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_DROPDOWN | wxCB_READONLY);
   viewNameSizer->Add(viewObjLabel, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   viewNameSizer->Add(mViewObjComboBox, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);

   mViewRelativeRadio = new wxRadioButton(this, RADIOBUTTON, wxT("Body-fixed frame"), wxDefaultPosition, wxSize(-1, -1), wxRB_GROUP);
   mViewAbsoluteRadio = new wxRadioButton(this, RADIOBUTTON, wxT("Coordinate frame"), wxDefaultPosition, wxSize(-1, -1), 0);
   mViewRelativeRadio->SetToolTip("This view rotates with the selected body");
   mViewAbsoluteRadio->SetToolTip("This view is fixed in the coordinate frame");
   wxBoxSizer *viewRelAbsSizer = new wxBoxSizer(wxHORIZONTAL);
   viewRelAbsSizer->Add(mViewRelativeRadio, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   viewRelAbsSizer->Add(mViewAbsoluteRadio, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);

   mViewFitObjectRadio = new wxRadioButton(this, RADIOBUTTON, wxT("Fit to object"), wxDefaultPosition, wxSize(-1, -1), wxRB_GROUP);
   mViewFitTrajRadio = new wxRadioButton(this, RADIOBUTTON, wxT("Fit to trajectory"), wxDefaultPosition, wxSize(-1, -1), 0);
   mViewFitObjectRadio->SetToolTip("This view is centered on and sized to the object by default");
   mViewFitTrajRadio->SetToolTip("This view is centered on and sized to the trajectory by default");
   wxBoxSizer *viewFitSizer = new wxBoxSizer(wxHORIZONTAL);
   viewFitSizer->Add(mViewFitObjectRadio, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   viewFitSizer->Add(mViewFitTrajRadio, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);

   mViewLookAtObjCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Maintain view direction toward object"), wxDefaultPosition, wxSize(-1, -1), 0);
   wxStaticText *viewTgtLabel = new wxStaticText(this, -1, wxT("View Direction"), wxDefaultPosition, wxSize(-1,-1), 0);
   mViewTgtComboBox = new wxComboBox(this, ID_COMBOBOX, "View Direction", wxDefaultPosition, wxDefaultSize, emptyList, wxCB_DROPDOWN | wxCB_READONLY);
   wxFlexGridSizer *viewTgtSizer = new wxFlexGridSizer(2, 0, 0);
   viewTgtSizer->Add(viewTgtLabel, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   viewTgtSizer->Add(mViewTgtComboBox, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);

   mViewZUpRadio = new wxRadioButton(this, RADIOBUTTON, wxT("Maintain View \"Z-Up\""), wxDefaultPosition, wxSize(-1, -1), wxRB_GROUP);
   mViewDirectRadio = new wxRadioButton(this, RADIOBUTTON, wxT("Use Shortest Angle"), wxDefaultPosition, wxSize(-1, -1), 0);
   wxBoxSizer *viewUpDirSizer = new wxBoxSizer(wxHORIZONTAL);
   viewUpDirSizer->Add(mViewZUpRadio, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);
   viewUpDirSizer->Add(mViewDirectRadio, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL|wxALL, bsize);

   wxBoxSizer *viewConfigSizer = new wxBoxSizer(wxVERTICAL);
   viewConfigSizer->Add(viewNameSizer, 0, wxALIGN_LEFT|wxALL, bsize);
   viewConfigSizer->Add(viewRelAbsSizer, 0, wxALIGN_LEFT|wxALL, bsize);
   viewConfigSizer->Add(viewFitSizer, 0, wxALIGN_LEFT|wxALL, bsize);
   viewConfigSizer->AddSpacer(10);
   viewConfigSizer->Add(mViewLookAtObjCheckBox, 0, wxALIGN_LEFT|wxALL, bsize);
   viewConfigSizer->Add(viewTgtSizer, 0, wxALIGN_LEFT|wxALL, bsize);
   viewConfigSizer->Add(viewUpDirSizer, 0, wxALIGN_LEFT|wxALL, bsize);

   wxStaticBoxSizer *viewDefStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "View Definition");
   viewDefStaticSizer->Add(viewConfigSizer, 0, wxALIGN_LEFT|wxALL, bsize);

   mViewTgtComboBox->Disable();
   mViewRelativeRadio->SetValue(true);
   mViewFitObjectRadio->SetValue(true);
   mViewZUpRadio->SetValue(true);
   mViewZUpRadio->Disable();
   mViewDirectRadio->Disable();

   //-----------------------------------------------------------------
   // Camera location
   //-----------------------------------------------------------------

   mCameraOnCheckBox = new wxCheckBox(this, CHECKBOX, wxT("Specify Default Location"), wxDefaultPosition, wxSize(-1, -1), 0);
   mEyeStaticText = new wxStaticText(this, STATIC_TEXT, wxT("Eye"), wxDefaultPosition, wxDefaultSize, 0);
   mEye1SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mEye1SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mEye1SpinCtrl->SetDigits(8U);
   mEye2SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "-1.0", wxDefaultPosition, wxDefaultSize, 0);
   mEye2SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mEye2SpinCtrl->SetDigits(8U);
   mEye3SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mEye3SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mEye3SpinCtrl->SetDigits(8U);
   mCenterStaticText = new wxStaticText(this, STATIC_TEXT, wxT("Center"), wxDefaultPosition, wxDefaultSize, 0);
   mCenter1SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mCenter1SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCenter1SpinCtrl->SetDigits(8U);
   mCenter2SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mCenter2SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCenter2SpinCtrl->SetDigits(8U);
   mCenter3SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mCenter3SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCenter3SpinCtrl->SetDigits(8U);
   mUpStaticText = new wxStaticText(this, STATIC_TEXT, wxT("Up"), wxDefaultPosition, wxDefaultSize, 0);
   mUp1SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mUp1SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mUp1SpinCtrl->SetDigits(8U);
   mUp2SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mUp2SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mUp2SpinCtrl->SetDigits(8U);
   mUp3SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "1.0", wxDefaultPosition, wxDefaultSize, 0);
   mUp3SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mUp3SpinCtrl->SetDigits(8U);

   wxFlexGridSizer *matrixSizer = new wxFlexGridSizer(3, 4, 0, 0);
   matrixSizer->Add(mEyeStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxGROW|wxALL, bsize);
   matrixSizer->Add(mEye1SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   matrixSizer->Add(mEye2SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   matrixSizer->Add(mEye3SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   matrixSizer->Add(mCenterStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxGROW|wxALL, bsize);
   matrixSizer->Add(mCenter1SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   matrixSizer->Add(mCenter2SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   matrixSizer->Add(mCenter3SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   matrixSizer->Add(mUpStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxGROW|wxALL, bsize);
   matrixSizer->Add(mUp1SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   matrixSizer->Add(mUp2SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   matrixSizer->Add(mUp3SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);

   mCurrentEyeStaticText = new wxStaticText(this, STATIC_TEXT, wxT("Eye"), wxDefaultPosition, wxDefaultSize, 0);
   mCurrentEyeStaticText->Disable();
   mCurrentEye1SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mCurrentEye1SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCurrentEye1SpinCtrl->SetDigits(8U);
   mCurrentEye1SpinCtrl->Disable();
   mCurrentEye2SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "-1.0", wxDefaultPosition, wxDefaultSize, 0);
   mCurrentEye2SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCurrentEye2SpinCtrl->SetDigits(8U);
   mCurrentEye2SpinCtrl->Disable();
   mCurrentEye3SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mCurrentEye3SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCurrentEye3SpinCtrl->SetDigits(8U);
   mCurrentEye3SpinCtrl->Disable();
   mCurrentCenterStaticText = new wxStaticText(this, STATIC_TEXT, wxT("Center"), wxDefaultPosition, wxDefaultSize, 0);
   mCurrentCenterStaticText->Disable();
   mCurrentCenter1SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mCurrentCenter1SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCurrentCenter1SpinCtrl->SetDigits(8U);
   mCurrentCenter1SpinCtrl->Disable();
   mCurrentCenter2SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mCurrentCenter2SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCurrentCenter2SpinCtrl->SetDigits(8U);
   mCurrentCenter2SpinCtrl->Disable();
   mCurrentCenter3SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mCurrentCenter3SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCurrentCenter3SpinCtrl->SetDigits(8U);
   mCurrentCenter3SpinCtrl->Disable();
   mCurrentUpStaticText = new wxStaticText(this, STATIC_TEXT, wxT("Up"), wxDefaultPosition, wxDefaultSize, 0);
   mCurrentUpStaticText->Disable();
   mCurrentUp1SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mCurrentUp1SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCurrentUp1SpinCtrl->SetDigits(8U);
   mCurrentUp1SpinCtrl->Disable();
   mCurrentUp2SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "0.0", wxDefaultPosition, wxDefaultSize, 0);
   mCurrentUp2SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCurrentUp2SpinCtrl->SetDigits(8U);
   mCurrentUp2SpinCtrl->Disable();
   mCurrentUp3SpinCtrl = new wxSpinCtrlDouble(this, ID_SPINCTRL, "1.0", wxDefaultPosition, wxDefaultSize, 0);
   mCurrentUp3SpinCtrl->SetRange(-DBL_MAX, DBL_MAX);
   mCurrentUp3SpinCtrl->SetDigits(8U);
   mCurrentUp3SpinCtrl->Disable();

   wxStaticText *currentStaticText = new wxStaticText(this, STATIC_TEXT, wxT("Current Location"), wxDefaultPosition, wxDefaultSize, 0);
   mCopyUpButton = new wxBitmapButton(this, COPY_UP_BUTTON, BITMAP_UP_ARROW, wxDefaultPosition, wxDefaultSize, 4);

   wxFlexGridSizer *currentMatrixSizer = new wxFlexGridSizer(3, 4, 0, 0);
   currentMatrixSizer->Add(mCurrentEyeStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentEye1SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentEye2SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentEye3SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentCenterStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentCenter1SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentCenter2SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentCenter3SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentUpStaticText, 0, wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentUp1SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentUp2SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   currentMatrixSizer->Add(mCurrentUp3SpinCtrl, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);

   EnableCameraSettings(false);

   wxBoxSizer *cameraConfigSizer = new wxBoxSizer(wxVERTICAL);
   cameraConfigSizer->Add(mCameraOnCheckBox, 0, wxALIGN_LEFT|wxALL, bsize);
   cameraConfigSizer->Add(matrixSizer);
   cameraConfigSizer->Add(mCopyUpButton, 0, wxALIGN_CENTER_HORIZONTAL|wxALL, bsize);
   cameraConfigSizer->Add(currentStaticText, 0, wxALIGN_LEFT|wxALL, bsize);
   cameraConfigSizer->Add(currentMatrixSizer);

   wxStaticBoxSizer *cameraStaticSizer = new wxStaticBoxSizer(wxVERTICAL, this, "Camera Location");
   cameraStaticSizer->Add(cameraConfigSizer, 0, wxALIGN_LEFT|wxALL, bsize);

   //-----------------------------------------------------------------
   // Add to page sizer
   //-----------------------------------------------------------------

   wxFlexGridSizer *pageSizer1 = new wxFlexGridSizer(3, 2, 0, 0);
   pageSizer1->Add(viewDefStaticSizer, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);
   pageSizer1->Add(cameraStaticSizer, 0, wxALIGN_CENTRE|wxGROW|wxALL, bsize);

   //-----------------------------------------------------------------
   // Add to middle sizer
   //-----------------------------------------------------------------
   wxBoxSizer *pageSizer = new wxBoxSizer(wxVERTICAL);
   pageSizer->Add(pageSizer1, 0, wxALIGN_CENTRE|wxALL, bsize);

   mMiddleSizer->Add(pageSizer, 0, wxALIGN_CENTRE|wxALL, bsize);
}


//------------------------------------------------------------------------------
// void Refresh()
//------------------------------------------------------------------------------
/**
 * Refreshes lists of objects within the panel.
 */
//------------------------------------------------------------------------------
void OFViewDialog::Refresh()
{
   // Remember view combo box selections
   wxString selectedView = mViewObjComboBox->GetStringSelection();
   wxString selectedTarget = mViewTgtComboBox->GetStringSelection();
   mViewObjComboBox->Clear();
   mViewTgtComboBox->Clear();
   mViewObjComboBox->Append(OpenFramesView::ROOT_FRAME_STRING);
   mViewTgtComboBox->Append(OpenFramesView::ROOT_FRAME_STRING);

   // Spacecraft objects
   mViewObjComboBox->Append(mObjects);
   mViewTgtComboBox->Append(mObjects);

   // Reselect old selections for view comboboxes or first item if old selection removed
   if (selectedView.size() == 0)
      mViewObjComboBox->Select(wxNOT_FOUND);
   else if (mViewObjComboBox->FindString(selectedView) == wxNOT_FOUND)
      mViewObjComboBox->Select(0);
   else
      mViewObjComboBox->SetValue(selectedView);

   if (selectedTarget.size() == 0)
      mViewTgtComboBox->Select(wxNOT_FOUND);
   else if (mViewTgtComboBox->FindString(selectedTarget) == wxNOT_FOUND)
      mViewTgtComboBox->Select(0);
   else
      mViewTgtComboBox->SetValue(selectedTarget);
}


//------------------------------------------------------------------------------
// void Show()
//------------------------------------------------------------------------------
/**
 * Shows the panel.
 */
//------------------------------------------------------------------------------
void OFViewDialog::Show()
{
   mPanelSizer->Add(mMiddleSizer, 1, wxGROW | wxALL, 1);
   mPanelSizer->Add(mBottomSizer, 0, wxGROW | wxALL, 1);

   SetAutoLayout(true); // tell the enclosing window to adjust to the size of the sizer
   SetSizer(mPanelSizer); //use the sizer for layout
   mPanelSizer->SetSizeHints(this); //set size hints to honour minimum size

   LoadData();
   EnableUpdate(false);

   // call Layout() to force layout of the children anew
   mPanelSizer->Layout();
   Centre();
}


//------------------------------------------------------------------------------
// void LoadData()
//------------------------------------------------------------------------------
/**
 * Loads data from mOFInterface to this instance's widgets
 */
void OFViewDialog::LoadData()
{
   try
   {
      // Load data from the core engine
      wxString lookAtFrame = mView->GetStringParameter("LookAtFrame");
      mViewObjComboBox->SetValue(mView->GetStringParameter("ViewFrame"));

      if (mView->GetOnOffParameter("InertialFrame") == OpenFramesView::ON_STRING)
         mViewAbsoluteRadio->SetValue(true);
      else
         mViewRelativeRadio->SetValue(true);

      if (mView->GetOnOffParameter("ViewTrajectory") == OpenFramesView::ON_STRING)
         mViewFitTrajRadio->SetValue(true);
      else
         mViewFitObjectRadio->SetValue(true);

      bool hasTarget = ( lookAtFrame.size() > 0 );
      mViewLookAtObjCheckBox->SetValue(hasTarget);
      mViewTgtComboBox->Enable(hasTarget);
      mViewDirectRadio->Enable(hasTarget);
      mViewZUpRadio->Enable(hasTarget);
      if (lookAtFrame.size() > 0)
         mViewTgtComboBox->SetValue(lookAtFrame);
      else
         mViewTgtComboBox->Select(wxNOT_FOUND);

      if (mView->GetOnOffParameter("ShortestAngle") == OpenFramesView::ON_STRING)
         mViewDirectRadio->SetValue(true);
      else
         mViewZUpRadio->SetValue(true);

      if (mView->GetOnOffParameter("SetDefaultLocation") == OpenFramesView::ON_STRING)
      {
         mCameraOnCheckBox->SetValue(true);
         EnableCameraSettings(true);
      }
      else
      {
         mCameraOnCheckBox->SetValue(false);
         EnableCameraSettings(false);
      }
      mEye1SpinCtrl->SetValue(mView->GetRealParameter("DefaultEye", 0));
      mEye2SpinCtrl->SetValue(mView->GetRealParameter("DefaultEye", 1));
      mEye3SpinCtrl->SetValue(mView->GetRealParameter("DefaultEye", 2));
      mCenter1SpinCtrl->SetValue(mView->GetRealParameter("DefaultCenter", 0));
      mCenter2SpinCtrl->SetValue(mView->GetRealParameter("DefaultCenter", 1));
      mCenter3SpinCtrl->SetValue(mView->GetRealParameter("DefaultCenter", 2));
      mUp1SpinCtrl->SetValue(mView->GetRealParameter("DefaultUp", 0));
      mUp2SpinCtrl->SetValue(mView->GetRealParameter("DefaultUp", 1));
      mUp3SpinCtrl->SetValue(mView->GetRealParameter("DefaultUp", 2));

      mCurrentEye1SpinCtrl->SetValue(mView->GetRealParameter("CurrentEye", 0));
      mCurrentEye2SpinCtrl->SetValue(mView->GetRealParameter("CurrentEye", 1));
      mCurrentEye3SpinCtrl->SetValue(mView->GetRealParameter("CurrentEye", 2));
      mCurrentCenter1SpinCtrl->SetValue(mView->GetRealParameter("CurrentCenter", 0));
      mCurrentCenter2SpinCtrl->SetValue(mView->GetRealParameter("CurrentCenter", 1));
      mCurrentCenter3SpinCtrl->SetValue(mView->GetRealParameter("CurrentCenter", 2));
      mCurrentUp1SpinCtrl->SetValue(mView->GetRealParameter("CurrentUp", 0));
      mCurrentUp2SpinCtrl->SetValue(mView->GetRealParameter("CurrentUp", 1));
      mCurrentUp3SpinCtrl->SetValue(mView->GetRealParameter("CurrentUp", 2));
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
   }

   EnableUpdate(false);
}


//------------------------------------------------------------------------------
// void SaveData()
//------------------------------------------------------------------------------
/**
 * Applies changes buffered by the widgets to the mOFInterface
 */
bool OFViewDialog::SaveData()
{
   bool success = true;

   //-----------------------------------------------------------------
   // save values to base, base code should do the range checking
   //-----------------------------------------------------------------
   try
   {
      if (mDataChanged)
      {
         mView->SetStringParameter("ViewFrame", mViewObjComboBox->GetValue().WX_TO_STD_STRING);

         if (mViewAbsoluteRadio->GetValue())
            mView->SetOnOffParameter("InertialFrame", OpenFramesView::ON_STRING);
         else
            mView->SetOnOffParameter("InertialFrame", OpenFramesView::OFF_STRING);

         if (mViewFitTrajRadio->GetValue())
            mView->SetOnOffParameter("ViewTrajectory", OpenFramesView::ON_STRING);
         else
            mView->SetOnOffParameter("ViewTrajectory", OpenFramesView::OFF_STRING);

         if (mViewLookAtObjCheckBox->GetValue())
            mView->SetStringParameter("LookAtFrame", mViewTgtComboBox->GetValue().WX_TO_STD_STRING);
         else
            mView->SetStringParameter("LookAtFrame", "");

         if (mViewDirectRadio->GetValue())
            mView->SetOnOffParameter("ShortestAngle", OpenFramesView::ON_STRING);
         else
            mView->SetOnOffParameter("ShortestAngle", OpenFramesView::OFF_STRING);
         
         if (mCameraOnCheckBox->GetValue())
            mView->SetOnOffParameter("SetDefaultLocation", OpenFramesView::ON_STRING);
         else
            mView->SetOnOffParameter("SetDefaultLocation", OpenFramesView::OFF_STRING);

         if (mRealDataChanged)
         {
            mView->SetRealParameter("DefaultEye", mEye1SpinCtrl->GetValue(), 0);
            mView->SetRealParameter("DefaultEye", mEye2SpinCtrl->GetValue(), 1);
            mView->SetRealParameter("DefaultEye", mEye3SpinCtrl->GetValue(), 2);
            mView->SetRealParameter("DefaultCenter", mCenter1SpinCtrl->GetValue(), 0);
            mView->SetRealParameter("DefaultCenter", mCenter2SpinCtrl->GetValue(), 1);
            mView->SetRealParameter("DefaultCenter", mCenter3SpinCtrl->GetValue(), 2);
            mView->SetRealParameter("DefaultUp", mUp1SpinCtrl->GetValue(), 0);
            mView->SetRealParameter("DefaultUp", mUp2SpinCtrl->GetValue(), 1);
            mView->SetRealParameter("DefaultUp", mUp3SpinCtrl->GetValue(), 2);
            mRealDataChanged = false;
         }

         Moderator::Instance()->ConfigurationChanged(mView, true);
         EnableUpdate(false);
      }

      if (mView->GetName() != mViewName->GetValue().WX_TO_STD_STRING)
      {
         Moderator *theModerator = Moderator::Instance();
         success = theModerator->RenameObject(GmatType::GetTypeId("OpenFramesView"), mView->GetName(),
                                              mViewName->GetValue().WX_TO_STD_STRING);
      }
   }
   catch (BaseException &e)
   {
      MessageInterface::PopupMessage(Gmat::ERROR_, e.GetFullMessage().c_str());
      success = false;
   }

   return success;
}


//------------------------------------------------------------------------------
// void EnableCameraSettings(bool enable)
//------------------------------------------------------------------------------
/**
 * Enables/disables widgets for the camera view position and angle settings
 *
 * @param enable Enables widgets if true, disables widgets otherwise
 */
//------------------------------------------------------------------------------
void OFViewDialog::EnableCameraSettings(bool enable)
{
   mEyeStaticText->Enable(enable);
   mCenterStaticText->Enable(enable);
   mUpStaticText->Enable(enable);
   mEye1SpinCtrl->Enable(enable);
   mEye2SpinCtrl->Enable(enable);
   mEye3SpinCtrl->Enable(enable);
   mCenter1SpinCtrl->Enable(enable);
   mCenter2SpinCtrl->Enable(enable);
   mCenter3SpinCtrl->Enable(enable);
   mUp1SpinCtrl->Enable(enable);
   mUp2SpinCtrl->Enable(enable);
   mUp3SpinCtrl->Enable(enable);
}


//------------------------------------------------------------------------------
// void OnCheckBoxChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a check box changes
 *
 * @param <event> Details of the event
 */
void OFViewDialog::OnCheckBoxChange(wxCommandEvent& event)
{
   if (event.GetEventObject() == mViewLookAtObjCheckBox)
   {
      bool hasTarget = mViewLookAtObjCheckBox->GetValue();
      mViewTgtComboBox->Enable(hasTarget);
      mViewDirectRadio->Enable(hasTarget);
      mViewZUpRadio->Enable(hasTarget);
   }
   else if (event.GetEventObject() == mCameraOnCheckBox)
   {
      EnableCameraSettings(mCameraOnCheckBox->GetValue());
   }

   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnRadioButtonChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a radio button changes
 *
 * @param <event> Details of the event
 */
void OFViewDialog::OnRadioButtonChange(wxCommandEvent& event)
{
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnComboBoxChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a combo box changes
 *
 * @param <event> Details of the event
 */
void OFViewDialog::OnComboBoxChange(wxCommandEvent& event)
{
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnTextChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a text box changes
 *
 * @param <event> Details of the event
 */
void OFViewDialog::OnTextChange(wxCommandEvent& event)
{
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnSpinChange(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when a spin control changes
 *
 * @param <event> Details of the event
 */
void OFViewDialog::OnSpinChange(wxCommandEvent& event)
{
   mRealDataChanged = true;
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnCopyUp(wxCommandEvent& event)
//------------------------------------------------------------------------------
/**
 * Callback when the copy up button is pressed
 *
 * @param <event> Details of the event
 */
void OFViewDialog::OnCopyUp(wxCommandEvent& event)
{
   mEye1SpinCtrl->SetValue(mCurrentEye1SpinCtrl->GetValue());
   mEye2SpinCtrl->SetValue(mCurrentEye2SpinCtrl->GetValue());
   mEye3SpinCtrl->SetValue(mCurrentEye3SpinCtrl->GetValue());
   mCenter1SpinCtrl->SetValue(mCurrentCenter1SpinCtrl->GetValue());
   mCenter2SpinCtrl->SetValue(mCurrentCenter2SpinCtrl->GetValue());
   mCenter3SpinCtrl->SetValue(mCurrentCenter3SpinCtrl->GetValue());
   mUp1SpinCtrl->SetValue(mCurrentUp1SpinCtrl->GetValue());
   mUp2SpinCtrl->SetValue(mCurrentUp2SpinCtrl->GetValue());
   mUp3SpinCtrl->SetValue(mCurrentUp3SpinCtrl->GetValue());

   mCameraOnCheckBox->SetValue(true);
   EnableCameraSettings(true);

   mRealDataChanged = true;
   EnableUpdate(true);
}


//------------------------------------------------------------------------------
// void OnOk()
//------------------------------------------------------------------------------
/**
 * Saves the data and closes the page
 *
 * @param <event> Details of the event
 */
//------------------------------------------------------------------------------
void OFViewDialog::OnOK(wxCommandEvent &event)
{
   if (mDataChanged)
   {
      bool success = SaveData();
      if (mCanClose && success)
         EndModal(wxID_OK);
   }
   else
   {
      if (mCanClose)
         EndModal(wxID_CANCEL);
   }

}


//------------------------------------------------------------------------------
// void OnCancel()
//------------------------------------------------------------------------------
/**
 * Close page without prompting about changes
 *
 * @param <event> Details of the event
 */
//------------------------------------------------------------------------------
void OFViewDialog::OnCancel(wxCommandEvent &event)
{
   EndModal(wxID_CANCEL);
}

//------------------------------------------------------------------------------
// void OnHelp()
//------------------------------------------------------------------------------
/**
 * Callback that shows help
 *
 * @param <event> Event details
 */
//------------------------------------------------------------------------------
void OFViewDialog::OnHelp(wxCommandEvent &event)
{
   wxLaunchDefaultBrowser("https://gitlab.com/EmergentSpaceTechnologies/OpenFramesInterface/wikis/docs/user/current/View-Definition");
}


//------------------------------------------------------------------------------
// void OnSystemClose(wxCloseEvent& even)
//------------------------------------------------------------------------------
/**
 * Close page after prompting about changes.
 *
 * @param <event> Details of the event
 */
//------------------------------------------------------------------------------
void OFViewDialog::OnSystemClose(wxCloseEvent& event)
{
   // Prompt user if changes were made
   if (mDataChanged)
   {
      if (wxMessageBox(_T("There were changes made to \"" + GetTitle() + "\" panel"
         " which will be lost on Close. \nDo you want to close anyway?"),
         _T("Please Confirm Close"),
         wxICON_QUESTION | wxYES_NO) != wxYES)
      {
         event.Veto();
         return;
      }
   }

   EndModal(wxID_CANCEL);
}


//------------------------------------------------------------------------------
// virtual void EnableUpdate(bool enable = true)
//------------------------------------------------------------------------------
/**
 * Changes the mode of the dialog based on whether there are data changes to
 * apply
 *
 * @param <enable> If true, then application of data changes is enabled
 */
void OFViewDialog::EnableUpdate(bool enable)
{
   mDataChanged = enable;
   if (mOkButton != nullptr)
      mOkButton->Enable(enable);
}
