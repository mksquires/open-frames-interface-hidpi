//$Id: OFRenderPool.hpp rmathur $
//------------------------------------------------------------------------------
//                                  OFRenderPool
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  @class OFRenderPool
 *  Singleton that tracks all instances of OFRendererIF and provides static
 *  functions that deal OpenFrames callbacks to the OFRendererIF containing an
 *  OpenFrames::WindowProxy with the given window ID.
 */
//------------------------------------------------------------------------------

#ifndef OFRENDERPOOL_H
#define OFRENDERPOOL_H

#include "OpenFramesInterface_defs.hpp"
#include <vector>

namespace OpenFrames
{
class WindowProxy;
class OpenVREvent;
}
class OFScene;


class OpenFramesInterface_API OFRenderPool
{
public:
   static OFRenderPool &Instance();

   OFRenderPool();
   virtual ~OFRenderPool();

   void AddScene(OFScene &renderer, OpenFrames::WindowProxy &winproxy);
   void RemoveScene(const OFScene &renderer);
   unsigned int GetNextID();
   bool IsTimeSyncTarget(const OpenFrames::WindowProxy *winproxy) const;
   OFScene *FindInstanceWithWinID(unsigned int *winID);
   void LockScenesWithRoot(const std::string &coordinateFrameName);
   void UnlockScenesWithRoot(const std::string &coordinateFrameName);
   void LockAllScenes();
   void UnlockAllScenes();

   static void DealKeyPressCallback(unsigned int *winID, unsigned int *row, unsigned int *col, int *key);
   static void DealVREventCallback(unsigned int *winID, unsigned int *row, unsigned int *col, const OpenFrames::OpenVREvent *vrEvent);
   static void DealMakeCurrent(unsigned int *winID, bool *success);
   static void DealSwapBuffers(unsigned int *winID);

private:
   unsigned int mNextIDToUse;
   /// A vector for tracking all active instances of OFRendererIF
   std::vector<OFScene *> mPool;

   void SetCallbacks(OpenFrames::WindowProxy &winproxy);
};

#endif
