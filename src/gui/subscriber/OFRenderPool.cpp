//$Id: OFRenderPool.hpp rmathur $
//------------------------------------------------------------------------------
//                                  OFRenderPool
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  OFRenderPool class
 *  Static class that tracks all instances of OFRendererIF and provides static
 *  functions that deal OpenFrames callbacks to the OFRendererIF containing an
 *  OpenFrames::WindowProxy with the given window ID.
 */
//------------------------------------------------------------------------------

#include <OpenFrames/WindowProxy.hpp> // before windows.h define min and max macros

#include "OFRenderPool.hpp"
#include "OFScene.hpp"


// Initialize static members
OFRenderPool renderPoolInstance;


/**
 * Gets singleton
 *
 * @return The singleteon render pool
 */
OFRenderPool &OFRenderPool::Instance()
{
   return renderPoolInstance;
}


/**
 * Default constructor
 */
OFRenderPool::OFRenderPool() :
   mNextIDToUse(0U),
   mPool()
{
   // Nothing to do here
}


/**
 * Destructor
 */
OFRenderPool::~OFRenderPool()
{
   // Nothing to do here
}


/**
 * Adds an OFRendererIF to the @ref POOL of all instances
 *
 * @param <renderer> The instance to add
 * @param <winproxy> The OpenFrames::WindowProxy associated with renderer
 */
void OFRenderPool::AddScene(OFScene &renderer, OpenFrames::WindowProxy &winproxy)
{
   mPool.push_back(&renderer);
   renderer.SetWinProxyID(OFRenderPool::GetNextID());
   SetCallbacks(winproxy);
}


/**
 * Removes an instance from the POOL of all instances
 *
 * @param <renderer> The instance to remove
 */
void OFRenderPool::RemoveScene(const OFScene &renderer)
{
   for (int ii = mPool.size() - 1; ii >= 0; ii--)
   {
      if (mPool[ii] == &renderer)
      {
         std::vector<OFScene *>::iterator it = mPool.begin() + ii;
         mPool.erase(it);
      }
   }
}


/**
 * Returns a unique ID to assign to a OpenFrames::WindowProxy in the pool
 *
 * @return The unique identifier
 */
unsigned int OFRenderPool::GetNextID()
{
   return mNextIDToUse++;
}


/**
 * Sets callbacks to the render pool for winproxy
 *
 * @param <winproxy> The instance to set callbacks for
 */
void OFRenderPool::SetCallbacks(OpenFrames::WindowProxy &winproxy)
{
   winproxy.setKeyPressCallback(OFRenderPool::DealKeyPressCallback);
   winproxy.setVREventCallback(OFRenderPool::DealVREventCallback);
   winproxy.setMakeCurrentFunction(OFRenderPool::DealMakeCurrent);
   winproxy.setSwapBuffersFunction(OFRenderPool::DealSwapBuffers);
#ifdef __APPLE__
   winproxy.setUpdateContextFunction(OFRenderPool::DealMakeCurrent);
#endif
}


/**
 * Deals a keypress callback from OpenFrames to the OFRendererIF with the
 * provided winID
 *
 * @param[in] <winID> Unique identifier for the OpenFrames::WindowProxy that
 *                    caused this callback
 * @param[in] <row> The row of the viewport grid in which the keypress was
 *                  registered
 * @param[in] <col> The column of the viewport grid in which the keypress was
 *                  registered
 * @param[in] <key> Keycode of the pressed key
 */
void OFRenderPool::DealKeyPressCallback(unsigned int *winID, unsigned int *row, unsigned int *col, int *key)
{
   OFScene *renderer = Instance().FindInstanceWithWinID(winID);

   // Pass key press to the appropriate thread
   if (renderer != nullptr)
   {
      if (row != nullptr && col != nullptr && key != nullptr)
      {
         renderer->KeyPressCallback(*row, *col, *key);
      }
   }
}

/**
* Deals a VR event callback from OpenFrames to the OFRendererIF with the
* provided winID
*
* @param[in] <winID> Unique identifier for the OpenFrames::WindowProxy that
*                    caused this callback
* @param[in] <row> The row of the viewport grid in which the keypress was
*                  registered
* @param[in] <col> The column of the viewport grid in which the keypress was
*                  registered
* @param[in] <vrEvent> Details of the VR event
*/
void OFRenderPool::DealVREventCallback(unsigned int *winID, unsigned int *row, unsigned int *col, const OpenFrames::OpenVREvent *vrEvent)
{
  OFScene *renderer = Instance().FindInstanceWithWinID(winID);

  // Pass key press to the appropriate thread
  if (renderer != nullptr)
  {
    if (row != nullptr && col != nullptr && vrEvent != nullptr)
    {
      renderer->VREventCallback(*row, *col, vrEvent);
    }
  }
}

/**
 * Deals a makecurrent callback from OpenFrames to the OFRendererIF with the
 * provided winID
 *
 * @param[in] <winID> Unique identifier for the OpenFrames::WindowProxy that
 *                    caused this callback
 * @param[out] <success>
 */
void OFRenderPool::DealMakeCurrent(unsigned int *winID, bool *success)
{
   OFScene *renderer = Instance().FindInstanceWithWinID(winID);
   bool theSuccess;

   // Pass make current to the appropriate thread
   if (renderer != nullptr)
   {
      *success = renderer->MakeCurrent();
   }
   else
   {
      *success = false;
   }
}


/**
 * Deals a swapbuffers callback from OpenFrames to the OFRendererIF with the provided winID
 *
 * @param[in] <winID> Unique identifier for the OpenFrames::WindowProxy that
 *                    caused this callback
 */
void OFRenderPool::DealSwapBuffers(unsigned int *winID)
{
   OFScene *renderer = Instance().FindInstanceWithWinID(winID);

   // Pass swapbuffers to the appropriate thread
   if (renderer != nullptr)
   {
      renderer->SwapBuffers();
   }
}


/**
 * Iterate through the pool to determine if winproxy is a target for time sync
 *
 * @param[in] <winproxy> The 
 */
bool OFRenderPool::IsTimeSyncTarget(const OpenFrames::WindowProxy *winproxy) const
{
   bool found;
   for (auto scene = mPool.begin(); scene < mPool.end(); scene++)
   {
      found = (*scene)->IsTimeSyncedTo(winproxy);
      if (found)
         return true;
   }
   return false;
}


/**
 * Finds the OFRendererIF that is using the specified OpenFrames::WindowProxy
 *
 * @param <winID> The unique ID of the OpenFrames::WindowProxy to find
 * @return The renderer that winID belongs to
 */
OFScene *OFRenderPool::FindInstanceWithWinID(unsigned int *winID)
{
   OFScene *renderer = nullptr;

   if (winID != nullptr)
   {
      // locate the thread that contains winID in the pool
      for (auto it = mPool.begin(); it < mPool.end(); it++)
      {
         if ((*it)->GetWinProxyID() == *winID)
         {
            renderer = (*it);
            break;
         }
      }
   }

   return renderer;
}


void OFRenderPool::LockScenesWithRoot(const std::string &coordinateFrameName)
{
   for (auto it = mPool.begin(); it < mPool.end(); it++)
   {
      if ((*it)->IsRootCoordinateFrameNamed(coordinateFrameName))
         (*it)->LockFrameManager();
   }
}


void OFRenderPool::UnlockScenesWithRoot(const std::string &coordinateFrameName)
{
   for (auto it = mPool.begin(); it < mPool.end(); it++)
   {
      if ((*it)->IsRootCoordinateFrameNamed(coordinateFrameName))
         (*it)->UnlockFrameManager();
   }
}


void OFRenderPool::LockAllScenes()
{
   for (auto it = mPool.begin(); it < mPool.end(); it++)
   {
      (*it)->LockFrameManager();
   }
}


void OFRenderPool::UnlockAllScenes()
{
   for (auto it = mPool.begin(); it < mPool.end(); it++)
   {
      (*it)->UnlockFrameManager();
   }
}
