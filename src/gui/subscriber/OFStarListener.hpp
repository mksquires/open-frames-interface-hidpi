//$Id$
//------------------------------------------------------------------------------
//                                  OFStarListener
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  @class OFStarListener
 *  Assigns trajectories to the trajectory follower for the stars.
 */
//------------------------------------------------------------------------------

#ifndef OFSTARLISTENER_H
#define OFSTARLISTENER_H

#include "OpenFramesInterface_defs.hpp"
#include "TrajectoryListener.hpp"

#include <osg/ref_ptr>

// Forward declaration of OpenFrames classes
namespace OpenFrames
{
class ReferenceFrame;
class TrajectoryFollower;
}


class OpenFramesInterface_API OFStarListener : public TrajectoryListener
{
public:
   static const std::string NAME;

   OFStarListener();
   virtual ~OFStarListener();

   void AssignSkySphere(OpenFrames::ReferenceFrame *skySphere, const std::string frameName,
                        Integer numberOfSolverTrajectories);

   virtual void ResetTrajectories();
   virtual bool IsSpacecraft() const { return false; }
   virtual void UpdateColor() { }
   virtual void AddSegment(const std::string &propName, OpenFrames::Trajectory *trajectory);
   virtual void RemoveSegment(OpenFrames::Trajectory *trajectory);
   virtual bool HasSegment(OpenFrames::Trajectory *trajectory);
   virtual void AddSolution(OpenFrames::Trajectory *trajectory);
   virtual Integer FinalizeSolverTrajectory();
   virtual void RemoveLastSolverTrajectory(Integer numTrajToRemove);
   virtual void RemoveAllSolverTrajectories();

private:
   /// The Sky Sphere reference frame
   osg::ref_ptr<OpenFrames::ReferenceFrame> mSkySphereRefFrame;
   /// The trajectory follower of the stars
   osg::ref_ptr<OpenFrames::TrajectoryFollower> mStarTrajectoryFollower;
   /// A collection of all segments
   std::vector<osg::ref_ptr<OpenFrames::Trajectory>> mTrajectories;
   /// A collection of all solutions
   std::vector<osg::ref_ptr<OpenFrames::Trajectory>> mSolverTrajs;
   /// The number of solver trajectories that this object should keep
   Integer mNumberOfSolverTrajectories;

   /// Reserve the copy constructor
   OFStarListener(const OFStarListener &source) = delete;
   /// Reserve the assignment operator
   OFStarListener& operator=(const OFStarListener &rhs) = delete;
};

#endif
