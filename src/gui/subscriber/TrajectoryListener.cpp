//$Id$
//------------------------------------------------------------------------------
//                                  TrajectoryListener
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  TrajectoryListener class
 *  Conveniently wraps all OpenFrames objects that implement a single model for
 *  a GMAT spacecraft object.
 */
//------------------------------------------------------------------------------

#include "TrajectoryListener.hpp"
#include "TrajectoryDealer.hpp"

#include <OpenFrames/Trajectory.hpp>


/**
 * Constructor
 *
 * @param options Configuration that defines the appearance of this instance
 * @param timeScale The initial time scale for the trajectory follower
 */
TrajectoryListener::TrajectoryListener()
{
   TrajectoryDealer::Instance().RegisterListener(*this);
}


/**
 * Destructor
 */
TrajectoryListener::~TrajectoryListener()
{
   TrajectoryDealer::Instance().UnregisterListener(*this);
}
