//$Id$
//------------------------------------------------------------------------------
//                                  OFStarListener
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  OFStarListener class
 *  Assigns trajectories to the trajectory follower for the stars.
 */
//------------------------------------------------------------------------------

#include "OFStarListener.hpp"
#include "TrajectoryDealer.hpp"

#include <OpenFrames/FrameTransform.hpp>
#include <OpenFrames/ReferenceFrame.hpp>

#include <OpenFrames/TrajectoryFollower.hpp>

const std::string OFStarListener::NAME(".Stars");


/**
 * Constructor
 *
 * @param options Configuration that defines the appearance of this instance
 * @param timeScale The initial time scale for the trajectory follower
 */
OFStarListener::OFStarListener() :
   mNumberOfSolverTrajectories(0)
{
   SetObjectName(NAME);
   mStarTrajectoryFollower = new OpenFrames::TrajectoryFollower();
   mStarTrajectoryFollower->setFollowType(OpenFrames::TrajectoryFollower::ATTITUDE, OpenFrames::TrajectoryFollower::LIMIT);
}


/**
 * Destructor
 */
OFStarListener::~OFStarListener()
{
   // Remove update callback from the assigned sky sphere
   if(mSkySphereRefFrame.valid())
   {
      mSkySphereRefFrame->getTransform()->setUpdateCallback(nullptr);
   }
}


void OFStarListener::AssignSkySphere(OpenFrames::ReferenceFrame *skySphere, const std::string frameName,
                                     Integer numberOfSolverTrajectories)
{
   SetFrameName(frameName);
   mNumberOfSolverTrajectories = numberOfSolverTrajectories;
   mSkySphereRefFrame = skySphere;
   if(mSkySphereRefFrame.valid())
   {
      mSkySphereRefFrame->getTransform()->setUpdateCallback(mStarTrajectoryFollower.get());
      mSkySphereRefFrame->setPosition(0.0, 0.0, 0.0);
      mSkySphereRefFrame->setAttitude(0.0, 0.0, 0.0, 1.0);
   }
}


void OFStarListener::ResetTrajectories()
{
   for (auto t = mTrajectories.begin(); t < mTrajectories.end(); t++)
      mStarTrajectoryFollower->removeTrajectory(t->get());

   // clear drawables
   mSolverTrajs.clear();
   mTrajectories.clear();

   if(mSkySphereRefFrame.valid())
   {
      mSkySphereRefFrame->setPosition(0.0, 0.0, 0.0);
      mSkySphereRefFrame->setAttitude(0.0, 0.0, 0.0, 1.0);
   }
}


/**
 * Adds a new segment to this object
 *
 * @param propName The name of the Propagate command creating this trajectory
 * @param trajectory The trajectory containing segment data
 */
void OFStarListener::AddSegment(const std::string &propName, OpenFrames::Trajectory *trajectory)
{
   mTrajectories.emplace_back(trajectory);
   if (mTrajectories.size() == 1)
      mStarTrajectoryFollower->setTrajectory(mTrajectories.back());
   else
      mStarTrajectoryFollower->addTrajectory(mTrajectories.back());
}


/**
 * Remove a new segment from this object
 *
 * @param trajectory The trajectory representing the segment to remove
 */
void OFStarListener::RemoveSegment(OpenFrames::Trajectory *trajectory)
{
   for (int ii = mTrajectories.size() - 1; ii >= 0; ii--)
   {
      if (mTrajectories[ii] == trajectory)
      {
         mStarTrajectoryFollower->removeTrajectory(mTrajectories[ii].get());
         mTrajectories.erase(mTrajectories.begin() + ii);
      }
   }
}


bool OFStarListener::HasSegment(OpenFrames::Trajectory *trajectory)
{
   bool found = false;
   for (auto traj = mTrajectories.begin(); traj < mTrajectories.end(); traj++)
   {
      if ((*traj).get() == trajectory)
      {
         found = true;
         break;
      }
   }
   return found;
}


/**
 * Adds a new solution segment to this object
 *
 * @param trajectory The trajectory containing segment data
 */
void OFStarListener::AddSolution(OpenFrames::Trajectory *trajectory)
{
   mSolverTrajs.push_back(trajectory);
}


/**
 * Finalizes a solver trajectories and displays it
 *
 * @param removeOldTrajectories If true, then all old solver trajectories are
 *                              removed
 *
 * @return The number of solver trajectories kept
 *
 * First, the latest solver trajectory is displayed. Second, any old solver
 * trajectories that need to be removed are removed.
 *
 * This function displays the new trajectory before removing older trajectories
 * because this order makes the visualization a little smoother.
 */
Integer OFStarListener::FinalizeSolverTrajectory()
{
   if (mSolverTrajs.size() > 0)
   {
      if (mSolverTrajs.back()->getNumPos() > 0)
      {
         // Follow the latest solver trajectory if no regular segments
         if (mTrajectories.size() == 0)
         {
            mStarTrajectoryFollower->addTrajectory(mSolverTrajs.back());
            if (mSolverTrajs.size() > 1)
               mStarTrajectoryFollower->removeTrajectory(mSolverTrajs.at(mSolverTrajs.size()-1));
         }
         // Remove old trajectories
         if (mNumberOfSolverTrajectories > 0)
         {
            for (Integer ii = mSolverTrajs.size() - 1 - mNumberOfSolverTrajectories; ii >= 0; ii--)
               mSolverTrajs.erase(mSolverTrajs.begin());
         }
      }
      // else Trajectory is empty, no need to display it
   }

   return mSolverTrajs.size();
}


/**
 * Removes just the last solver trajectory
 *
 * This is actually the next to the last trajectory because there is always an
 * empty (or in progress) trajectory as the "real" final trajectory. Of course,
 * that last trajectory should be empty when this function is called, but that
 * is not checked because its irrelevant to this function.
 */
void OFStarListener::RemoveLastSolverTrajectory(Integer numTrajToRemove)
{
   // Always unfollow last trajectory before discarding.
   if (mSolverTrajs.size() > 0)
      mStarTrajectoryFollower->removeTrajectory(mSolverTrajs.back());

   // Discard the last N trajectories:
   for (Integer ii = 0; ii < numTrajToRemove; ii++)
   {
      if (mSolverTrajs.size() > 0)
      {
         int last_i = mSolverTrajs.size() - 1;
         mSolverTrajs.erase(mSolverTrajs.begin() + last_i);
      }
      // else Nothing to remove
   }
}


/**
 * Removes all last solver trajectories
 *
 * This actually removes all but one trajectory because the last trajectory
 * is empty, unassigned, and waiting to be used.
 */
void OFStarListener::RemoveAllSolverTrajectories()
{
   // Always unfollow last trajectory before discarding. Do not check
   // mSegments.size() because trajectories may have been added.
   if (mSolverTrajs.size() > 0)
      mStarTrajectoryFollower->removeTrajectory(mSolverTrajs.back());

   // Discard all but the last trajectory
   for (Integer ii = mSolverTrajs.size() - 1; ii >= 0; ii--)
      mSolverTrajs.erase(mSolverTrajs.begin() + ii);
}
