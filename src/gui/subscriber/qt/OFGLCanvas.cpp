//$Id$
//------------------------------------------------------------------------------
//                                  OFGLCanvas
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 1, 2015
/**
 *  OFGLCanvas class
 *  Subclasses wxFrame to implement the OpenGL Canvas that OpenFrames will render
 *  onto, and manages the OpenFrames objects that draw the scene.
 */
//------------------------------------------------------------------------------

#include "OFGLCanvas.hpp"
#include "OFScene.hpp"
#include "RgbColorFloat.hpp"
#include "OpenFramesView.hpp"
#include "OpenFramesInterface.hpp"

#include "SpacePoint.hpp"
#include "Spacecraft.hpp"
#include "Planet.hpp"
#include "MessageInterface.hpp"
#include "GmatDefaults.hpp"
#include "TextParser.hpp"
#include "FileManager.hpp"
#include "AttitudeConversionUtility.hpp"
#include "Moderator.hpp"

#include <QCoreApplication>
#include <QDebug>

#include <iostream>
#include <sstream>
#include <float.h>

const bool OFGLCanvas::VERBOSE_CONSOLE = false;

//wxBEGIN_EVENT_TABLE(OFGLCanvas, wxGLCanvas)
//EVT_MOTION(OFGLCanvas::MouseMoved)
//EVT_LEFT_DOWN(OFGLCanvas::MouseDownLeft)
//EVT_LEFT_UP(OFGLCanvas::MouseUpLeft)
//EVT_RIGHT_DOWN(OFGLCanvas::MouseDownRight)
//EVT_RIGHT_UP(OFGLCanvas::MouseUpRight)
//EVT_MIDDLE_DOWN(OFGLCanvas::MouseDownMiddle)
//EVT_MIDDLE_UP(OFGLCanvas::MouseUpMiddle)
////EVT_MOUSEWHEEL(OFGLCanvas::MouseWheel) // Forwarded by the parent
//EVT_MOUSE_AUX1_DOWN(OFGLCanvas::MouseAux1) // _UP always sends AUX1
//EVT_MOUSE_AUX2_DOWN(OFGLCanvas::MouseAux2) // _UP always sends AUX1
//EVT_SIZE(OFGLCanvas::Resized)
//EVT_CHAR(OFGLCanvas::KeyPressed)
//EVT_CHAR_HOOK(OFGLCanvas::KeyPressedHook)
//EVT_KEY_DOWN(OFGLCanvas::KeyDown) // For Macbook trackpad middle-button emulation
//EVT_KEY_UP(OFGLCanvas::KeyUp)
//wxEND_EVENT_TABLE()


/**
 * Constructor
 *
 * @param <parent> The parent of this object, which may be nullptr
 * @param <args> Attribute arguments passed to the underlying wxGLCanvas
 */
OFGLCanvas::OFGLCanvas(OFScene& scene, QWindow* parent) :
   QWindow(parent),
   mScene(scene),
   mAutoPauseAnimation(true),
   mContext(nullptr)
   //mAltKeyPressed(false),
   //mShiftKeyPressed(false),
   //lastButtonPressed(0)
{
   setSurfaceType(QSurface::OpenGLSurface);
}


/**
 * Destructor
 */
OFGLCanvas::~OFGLCanvas()
{
   // nothing to do here
}


//------------------------------------------------------------------------------
// Callbacks from OpenFrames
//------------------------------------------------------------------------------

/**
 * Callback function for OpenFrames to makecurrent on the GL context
 *
 * @return True if the context is set successfully
 * @return False otherwise
 *
 * @warning OpenFrames calls this function from another thread. Therefore, GUI
 * functionality cannot be called directly.
 *
 */
bool OFGLCanvas::MakeCurrent()
{
   bool success = false;
   
   // Create OpenGL context the first time
   if(mContext == nullptr)
   {
      mContext = new QOpenGLContext();
      if(mContext != nullptr) mContext->create();
   }

   if(mContext != nullptr) success = mContext->makeCurrent(this);

   return success;
}

/**
 * Callback function for OpenFrames to swap buffers on the GL context
 *
 * @warning OpenFrames calls this function from another thread. Therefore, GUI
 * functionality cannot be called directly.
 *
 */
void OFGLCanvas::SwapBuffers()
{
   if(mContext != nullptr) mContext->swapBuffers(this);
}

//------------------------------------------------------------------------------
// wxWidgets callbacks
//------------------------------------------------------------------------------


/**
 * Callback that resizes the OpenFrames::WindowProxy window
 *
 * @param <event> Event details
 */
void OFGLCanvas::resizeEvent(QResizeEvent *event) 
{
   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
   if(windowProxy != nullptr && windowProxy->isAnimating())
   {
      windowProxy->resizeWindow(0, 0, event->size().width(), event->size().height());
   }
   QWindow::resizeEvent(event);
}

/**
 * Callback that starts/pauses the OpenFrames::WindowProxy animation rendering (per QWindow documentation)
 *
 * @param <event> Event details
 */
void OFGLCanvas::exposeEvent(QExposeEvent *event)
{
   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
   if(windowProxy == nullptr) return;

   // Enable rendering when window is exposed
   if(isExposed()) 
   {
      // Unpause animation state
      if(mAutoPauseAnimation) windowProxy->pauseAnimation(false);
   }
   else
   {
      // Pause animation state
      if(mAutoPauseAnimation) windowProxy->pauseAnimation(true);
   }
   // else Do not attempt to start the renderer twice!
}

/**
 * Callback that sends a mouse movement to OpenFrames::WindowProxy
 *
 * @param <event> Event details
 */
void OFGLCanvas::mouseMoveEvent(QMouseEvent *event)
{
   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
   if(windowProxy != nullptr && windowProxy->isAnimating()) 
   {
      if(VERBOSE_CONSOLE) {
         qDebug() << "mouseMoved to (" << event->x() << ", " << event->y() << ")";
      }
      windowProxy->mouseMotion(event->x(), event->y());
   }
}

///**
// * Callback that sends a button press to OpenFrames::WindowProxy
// *
// * @param <event> Event details
// */
//void OFGLCanvas::MouseDownLeft(wxMouseEvent& event)
//{
//   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
//   if (windowProxy != nullptr && windowProxy->isAnimating())
//   {
//      // Check if alt key was pressed to emulate middle mouse button
//      if(mAltKeyPressed)
//         lastButtonPressed = 2U; // Middle-click
//      else
//         lastButtonPressed = 1U;              // Left-click
//      windowProxy->buttonPress(event.GetX(), event.GetY(), lastButtonPressed);
//   }
//   CaptureMouse();
//   event.Skip();
//}
//
//
///**
// * Callback that sends a button press to OpenFrames::WindowProxy
// *
// * @param <event> Event details
// */
//void OFGLCanvas::MouseUpLeft(wxMouseEvent& event)
//{
//   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
//   if (windowProxy != nullptr && windowProxy->isAnimating())
//   {
//      windowProxy->buttonRelease(event.GetX(), event.GetY(), lastButtonPressed);
//      lastButtonPressed = 0;
//   }
//   mScene.SaveCurrentTrackBall();
//   ReleaseMouse();
//   event.Skip();
//}
//
//
///**
// * Callback that sends a button press to OpenFrames::WindowProxy
// *
// * @param <event> Event details
// */
//void OFGLCanvas::MouseDownRight(wxMouseEvent& event)
//{
//   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
//   if (windowProxy != nullptr && windowProxy->isAnimating())
//      windowProxy->buttonPress(event.GetX(), event.GetY(), 3U);
//   CaptureMouse();
//   event.Skip();
//}
//
//
///**
// * Callback that sends a button press to OpenFrames::WindowProxy
// *
// * @param <event> Event details
// */
//void OFGLCanvas::MouseUpRight(wxMouseEvent& event)
//{
//   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
//   if (windowProxy != nullptr && windowProxy->isAnimating())
//      windowProxy->buttonRelease(event.GetX(), event.GetY(), 3U);
//   mScene.SaveCurrentTrackBall();
//   ReleaseMouse();
//   event.Skip();
//}
//
//
///**
// * Callback that sends a button press to OpenFrames::WindowProxy
// *
// * @param <event> Event details
// */
//void OFGLCanvas::MouseDownMiddle(wxMouseEvent& event)
//{
//   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
//   if (windowProxy != nullptr && windowProxy->isAnimating())
//      windowProxy->buttonPress(event.GetX(), event.GetY(), 2U);
//   CaptureMouse();
//   event.Skip();
//}
//
//
///**
// * Callback that sends a button press to OpenFrames::WindowProxy
// *
// * @param <event> Event details
// */
//void OFGLCanvas::MouseUpMiddle(wxMouseEvent& event)
//{
//   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
//   if (windowProxy != nullptr && windowProxy->isAnimating())
//      windowProxy->buttonRelease(event.GetX(), event.GetY(), 2U);
//   mScene.SaveCurrentTrackBall();
//   ReleaseMouse();
//   event.Skip();
//}

/**
 * Callback that sends a button press to OpenFrames::WindowProxy
 *
 * @param <event> Event details
 */
void OFGLCanvas::mousePressEvent(QMouseEvent *event)
{
   unsigned int button = mapQtButtonToOFButton(event->button());
   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();

   if(windowProxy != nullptr && windowProxy->isAnimating()) {
      if(button != 0) {
         if(VERBOSE_CONSOLE) {
            qDebug() << "mouseDown " << button << " at (" << event->x() << ", " << event->y() << ")";
         }
         windowProxy->buttonPress(event->x(), event->y(), button);
      }
   }
}

/**
 * Callback that sends a button release to OpenFrames::WindowProxy
 *
 * @param <event> Event details
 */
void OFGLCanvas::mouseReleaseEvent(QMouseEvent *event)
{
   unsigned int button = mapQtButtonToOFButton(event->button());
   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();

   if(windowProxy != nullptr && windowProxy->isAnimating()) {
      if(button != 0) {
         if(VERBOSE_CONSOLE) {
            qDebug() << "mouseUp " << button << " at (" << event->x() << ", " << event->y() << ")";
         }
         windowProxy->buttonRelease(event->x(), event->y(), button);
      }
   }
}

///**
// * Callback that scrolls time with the mouse wheel
// *
// * @param <event> Event details
// */
//void OFGLCanvas::MouseWheel(wxMouseEvent& event)
//{
//   if (event.GetWheelAxis() == wxMOUSE_WHEEL_VERTICAL)
//   {
//      double amount = static_cast<double>(event.GetWheelRotation()) / static_cast<double>(event.GetWheelDelta());
//      double offset;
//      if (mAltKeyPressed)
//         offset = amount * mScene.GetTimeScaleFromWindowProxy() / 100.0 / GmatTimeConstants::SECS_PER_DAY;
//      else if (mShiftKeyPressed)
//         offset = amount * mScene.GetTimeScaleFromWindowProxy()  / GmatTimeConstants::SECS_PER_DAY;
//      else
//         offset = amount * mScene.GetTimeScaleFromWindowProxy()  / 10.0 / GmatTimeConstants::SECS_PER_DAY;
//      mScene.SetTimeOffset(mScene.GetTimeOffsetFromWindowProxy() + offset);
//   }
//}

/**
 * Callback that scrolls time with the mouse wheel
 *
 * @param <event> Event details
 */
void OFGLCanvas::wheelEvent(QWheelEvent *event) {
   QWindow::wheelEvent(event);
}

///**
// * Callback that scrolls time with the mouse wheel
// *
// * @param <event> Event details
// */
//void OFGLCanvas::MouseAux1(wxMouseEvent& event)
//{
//   mScene.NextView();
//}


///**
// * Callback that scrolls time with the mouse wheel
// *
// * @param <event> Event details
// */
//void OFGLCanvas::MouseAux2(wxMouseEvent& event)
//{
//   mScene.PreviousView();
//}


///**
// * Callback that sends a button press to OpenFrames::WindowProxy
// *
// * @param <event> Event details
// */
//void OFGLCanvas::KeyPressed(wxKeyEvent& event)
//{
//   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
//   int key = event.GetKeyCode();
//   bool processed = mScene.PreProcessKeyPress(event);
//
//   if (!processed && windowProxy != nullptr && windowProxy->isAnimating())
//      windowProxy->keyPress(key);
//}


///**
// * Handles key presses before GMAT grabs them
// *
// * Right and left are used by KeyPressCallback(). Up and down are also grabbed
// * so that the wxWidgets interface is more consistent.
// *
// * @param <event> Event details
// */
//void OFGLCanvas::KeyPressedHook(wxKeyEvent& event)
//{
//   int key = event.GetKeyCode();
//   switch (key)
//   {
//   case WXK_LEFT:
//   case WXK_RIGHT:
//   case WXK_UP:
//   case WXK_DOWN:
//   {
//      OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
//      if (windowProxy != nullptr && windowProxy->isAnimating())
//      {
//         double offset;
//         if (event.AltDown())
//            offset = mScene.GetTimeScaleFromWindowProxy() / 100.0 / GmatTimeConstants::SECS_PER_DAY;
//         else if (event.ShiftDown())
//            offset = mScene.GetTimeScaleFromWindowProxy() / GmatTimeConstants::SECS_PER_DAY;
//         else
//            offset = mScene.GetTimeScaleFromWindowProxy() / 10.0 / GmatTimeConstants::SECS_PER_DAY;
//         // Shift time forward
//         if (key == WXK_RIGHT)
//            mScene.SetTimeOffset(mScene.GetTimeOffsetFromWindowProxy() + offset);
//         // Shift time backward
//         else if (key == WXK_LEFT)
//            mScene.SetTimeOffset(mScene.GetTimeOffsetFromWindowProxy() - offset);
//         // Otherwise pass to openframes for processing
//         else
//            windowProxy->keyPress(key);
//      }
//      break;
//   }
//   default:
//      event.Skip();
//      break;
//   }
//}

///**
// * Handles key down events before GMAT grabs them
// *
// * Macbooks have 1-button mouse so we have to manually emulate middle-click
// * Other trackpads have 2 buttons and can always do hardware middle-click,
// * but we enable middle-click emulation anyway for consistency across platforms
// *
// * @param <event> Event details
// */
//void OFGLCanvas::KeyDown(wxKeyEvent& event)
//{
//   // Indicate that alt key is pressed
//   if (event.GetKeyCode() == WXK_ALT)
//      mAltKeyPressed = true;
//   else if (event.GetKeyCode() == WXK_SHIFT)
//      mShiftKeyPressed = true;
//   else
//      event.Skip();
//}

///**
// * Handles key up events before GMAT grabs them
// *
// * Macbooks have 1-button mouse so we have to manually emulate middle-click
// *
// * @param <event> Event details
// */
//void OFGLCanvas::KeyUp(wxKeyEvent& event)
//{
//   // Indicate that alt key is not pressed
//   if (event.GetKeyCode() == WXK_ALT)
//      mAltKeyPressed = false;
//   else if (event.GetKeyCode() == WXK_SHIFT)
//      mShiftKeyPressed = false;
//   else
//      event.Skip();
//}


/**
 * Handles key down events before GMAT grabs them
 *
 * @param <event> Event details
 */
void OFGLCanvas::keyPressEvent(QKeyEvent *event)
{
   OpenFrames::WindowProxy *windowProxy = mScene.GetWinProxy();
   bool processed = mScene.PreProcessKeyPress(*event);
   
   if (!processed && windowProxy != nullptr && windowProxy->isAnimating())
   {
      int key = mapQtKeyEventToOsgKey(event);
      if(VERBOSE_CONSOLE) {
         qDebug() << "keyPressed " << key << " (" << (char)key << ")";
      }
      windowProxy->keyPress(key);
   }
}

unsigned int OFGLCanvas::mapQtButtonToOFButton(Qt::MouseButtons qButton)
{
   unsigned int button;

   switch(qButton) {
   case Qt::LeftButton: {
      button = 1;
      break;
   }
   case Qt::RightButton: {
      button = 3;
      break;
   }
   case Qt::MiddleButton: {
      button = 2;
      break;
   }
   case Qt::BackButton: {
      button = 6;
      break;
   }
   case Qt::ForwardButton: {
      button = 7;
      break;
   }
   default: {
      button = 0;
      break;
   }
   }

   return button;
}

int OFGLCanvas::mapQtKeyEventToOsgKey(QKeyEvent *event) 
{
   int key;

   // Convert uppercase to lowercase as necessary
   if(Qt::Key_A && event->key() <= Qt::Key_Z) {
      if(event->modifiers() & Qt::ShiftModifier) {
         key = event->key();
      }
      else {
         key = event->key() + 0x20;
      }
   }
   else {
      key = event->key();
   }

   return key;
}