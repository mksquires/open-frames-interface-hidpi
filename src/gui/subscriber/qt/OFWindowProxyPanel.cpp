//$Id$
//------------------------------------------------------------------------------
//                                  OFWindowProxyPanel
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 1, 2015
/**
*  OFWindowProxyPanel class
*  Subclasses wxFrame to implement the window that OpenFrames will render onto.
*/
//------------------------------------------------------------------------------

#include "OFScene.hpp"
#include "OFGLCanvas.hpp"
#include "OFWindowProxyPanel.hpp"
#include "OpenFramesInterface.hpp"

#include "MessageInterface.hpp"
#include "Moderator.hpp"

#include "bitmaps/RunAnimation.xpm"
#include "bitmaps/PauseAnimation.xpm"
#include "bitmaps/FasterAnimation.xpm"
#include "bitmaps/SlowerAnimation.xpm"
#include "bitmaps/forward.xpm"
#include "bitmaps/back.xpm"
#include "t0.xpm"
#include "tf.xpm"
#include "resetview.xpm"
#include "pause_link.xpm"
#include "run_link.xpm"

#include <QGridLayout>

#include <float.h>

const QPixmap OFWindowProxyPanel::FORWARD_BITMAP(forward_xpm);
const QPixmap OFWindowProxyPanel::BACK_BITMAP(back_xpm);

const std::string OFWindowProxyPanel::TOOLTIP_PLAY_ANIMATION("Play animation (p)");
const std::string OFWindowProxyPanel::TOOLTIP_PAUSE_ANIMATION("Pause animation (p)");
const std::string OFWindowProxyPanel::TOOLTIP_PLAY_LINKED("Play linked animations (p)");
const std::string OFWindowProxyPanel::TOOLTIP_PAUSE_LINKED("Pause linked animation (p)");
const std::string OFWindowProxyPanel::TOOLTIP_POSITIVE_SCALE("Toggle time scale sign to positive (n)");
const std::string OFWindowProxyPanel::TOOLTIP_NEGATIVE_SCALE("Toggle time scale sign to negative (n)");


/**
 * Constructor
 *
 * @param <forObject> @ref OpenFramesInterface object that defines the plot
 */
OFWindowProxyPanel::OFWindowProxyPanel(GmatBase *forObject, QWidget *parent) :
   QWidget(parent),
   m_sizeHint(400, 350),
   mForObject(static_cast<OpenFramesInterface *>(forObject)),
   mScene(nullptr),
   mScale(0.0),
   mScaleSign(1),
   mIsPlaying(true),
   mShowSyncedButtons(false),
   mScaleValid(true)
{
   minimizeOnRun = false; // OpenFrames window should not be minimized at each mission run

   // Place the container widget inside of this widget
   // Set a vertical layout for this widget
   setLayout(new QGridLayout());
   layout()->setContentsMargins(0, 0, 0, 0);

   mScene = new OFScene(this);

   /*
   // Initialize text after all class objects have been initialized
   mScaleText->SetValue(wxT("-00000x"));

   sizer->SetSizeHints(this);
   SetAutoLayout(true);
   */
}


/**
 * Destructor
 */
OFWindowProxyPanel::~OFWindowProxyPanel()
{
   // In wxWidgets, Child windows (mScene, etc.) are deleted from within the parent destructor.
   // http://docs.wxwidgets.org/3.1/overview_windowdeletion.html#overview_windowdeletion_deletion
   RemoveObject(mForObject);
   if (mScene != nullptr)
      delete mScene;
}

QSize OFWindowProxyPanel::sizeHint() const
{
   return m_sizeHint;
}

void OFWindowProxyPanel::setSizeHint(int width, int height)
{
   m_sizeHint.setWidth(width);
   m_sizeHint.setHeight(height);
}

std::string OFWindowProxyPanel::GetForObjectName() const
{
   if (mForObject == nullptr)
      return "";
   else
      return mForObject->GetName();
}


void OFWindowProxyPanel::SetObject(OpenFramesInterface *object)
{
   // Set object is not being called from OpenFramesInterface, so calling mForObject->RemoveWidget() is okay
   if (mForObject != nullptr && mForObject != object)
      RemoveObject(mForObject);
   mForObject = object;
}


void OFWindowProxyPanel::RemoveObject(const OpenFramesInterface *object)
{
   if (mForObject != nullptr && mForObject == object)
   {
      mScene->RemoveTimeSynchronization();
      mForObject->RemoveWidget();
      mForObject = nullptr;
   }
}


/**
 * Adds a canvas to the panel
 *
 * OFScene uses this callback to provide a canvas that adheres to the
 * OpenFramesInterface's settings. If the aspect ratio of the canvas is fixed,
 * then the parent window of this panel is resized to fit the canvas as best as
 * possible.
 *
 * @param canvas The canvas to add to the window
 * @param shaped Set true to fix the canvas aspect ratio
 */
void OFWindowProxyPanel::SetCanvas(OFGLCanvas *canvas, bool shaped)
{
   mGLCanvasContainer = QWidget::createWindowContainer(canvas, this);
   layout()->addWidget(mGLCanvasContainer);
}


/**
 * Provides a reference to the underlying scene
 *
 * @return The scene
 *
 * Used by OpenFramesInterface to call functions in OFGLCanvas directly
 */
OFScene &OFWindowProxyPanel::GetScene()
{
   return *mScene;
}


/**
 * Shows or hides the sizer containing our makeshift toolbar at the top of the
 * panel
 *
 * @param <show> Toolbar shown if true, hidden otherwise
 */
void OFWindowProxyPanel::ShowToolbar(bool show)
{
}


/**
 * Sets the values in the views combobox, which clears existing values
 *
 * @param <views> List of views to set as entries in the combobox
 * @param <current> Index of the view to select in the combobox after the entries
 *                  are set
 */
void OFWindowProxyPanel::SetListOfViews(const QStringList &views)
{
   int maxWidth = -1;

   ResizeViewComboBox();
}


/**
 * Sets the values in the views combobox
 *
 * @param <views> List of views to set as entries in the combobox
 */
void OFWindowProxyPanel::AppendToListOfViews(const QString &view)
{
   ResizeViewComboBox();
}


/**
 * Resize the combobox to fit the contents
 */
void OFWindowProxyPanel::ResizeViewComboBox()
{
}


/**
 * Sets the current view in the combobox
 *
 * @param <current> Index of the view to select in the combobox
 */
void OFWindowProxyPanel::SetCurrentViewByIndex(int current)
{
}


QString OFWindowProxyPanel::GetCurrentViewString()
{
   return QString();
}


void OFWindowProxyPanel::SelectViewByString(const QString &view)
{
   bool found = false;
   if (found)
   {
      int sel = 0;
      mScene->SetCurrentView(static_cast<unsigned int>(sel));
   }
}


/**
 * Increases the value of the time scale and updates slider
 */
void OFWindowProxyPanel::IncreaseScale()
{
   Real scale = mScene->GetTimeScaleFromWindowProxy();

   int sign = (scale > 0.0) - (scale < 0.0);

   if(sign == 1)
   {
      if(scale < 1.0)
      {
         scale = 1.0 / DownByTwo(1.0 / scale);
      }
      else
      {
         scale = UpByTwo(scale);
      }
   }
   else if(sign == -1)
   {
      if(scale > -1.0)
      {
         scale = 1.0 / -DownByTwo(1.0 / -scale);
      }
      else
      {
         scale = -UpByTwo(-scale);
      }
   }
   else // (sign == 0)
   {
      scale = 1.0;
   }

   mScene->SetTimeScaleToWindowProxy(scale);
   //SetTimeScaleValue(scale);
}


/**
 * Decreases the value of the time scale and updates slider
 */
void OFWindowProxyPanel::DecreaseScale()
{
   Real scale = mScene->GetTimeScaleFromWindowProxy();

   int sign = (scale > 0.0) - (scale < 0.0);

   if(sign == 1)
   {
      if(scale < 1.0)
      {
         scale = 1.0 / UpByTwo(1.0 / scale);
      }
      else
      {
         scale = DownByTwo(scale);
      }
   }
   else if(sign == -1)
   {
      if(scale > -1.0)
      {
         scale = 1.0 / -UpByTwo(1.0 / -scale);
      }
      else
      {
         scale = -DownByTwo(-scale);
      }
   }
   else // (sign == 0)
   {
      scale = -1.0;
   }

   mScene->SetTimeScaleToWindowProxy(scale);
   //SetTimeScaleValue(scale);
}


Real OFWindowProxyPanel::UpByTwo(Real init)
{
   if (init < 500000.0)
      return ( init * 2.0 );
   else
      return init;
}


Real OFWindowProxyPanel::DownByTwo(Real init)
{
   if (init >= 0.00002)
      return ( init / 2.0 );
   else
      return init;
}


void OFWindowProxyPanel::ToggleScaleSign()
{
   Real scale = mScene->GetTimeScaleFromWindowProxy();
   scale = -1.0 * scale;
   mScene->SetTimeScaleToWindowProxy(scale);
   //SetTimeScaleValue(scale);
}

/**
 * Overrides function to destroy the window
 *
 * This function is called as soon as possible and before the object's
 * destructor.
 */
bool OFWindowProxyPanel::Destroy()
{
   RemoveObject(mForObject);
   delete mScene;
   mScene = nullptr;
   return true;
   //return wxPanel::Destroy();
}
