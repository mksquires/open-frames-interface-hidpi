//$Id$
//------------------------------------------------------------------------------
//                                  OFScene
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: December 8, 2017
/**
 *  OFScene class
 *  Subclasses wxFrame to implement the OpenGL Canvas that OpenFrames will render
 *  onto, and manages the OpenFrames objects that draw the scene.
 */
//------------------------------------------------------------------------------

#define NOMINMAX // So that Windows.h doesn't redefine min/max

#include "OFScene.hpp"
#include "OFGLCanvas.hpp"
#include "OFWindowProxyPanel.hpp"
#include "OFRenderPool.hpp"
#include "OFSpaceObject.hpp"
#include "OFStarListener.hpp"
#include "OFEclipticPlane.hpp"
#include "RgbColorFloat.hpp"
#include "OpenFramesView.hpp"
#include "OpenFramesVector.hpp"
#include "OpenFramesSensorMask.hpp"
#include "OpenFramesInterface.hpp"
#include "SpacePointOptions.hpp"
#include "CoordinateOptions.hpp"
#include "TrajectoryDealer.hpp"

#include "SpacePoint.hpp"
#include "Spacecraft.hpp"
#include "Planet.hpp"
#include "MessageInterface.hpp"
#include "GmatDefaults.hpp"
#include "TextParser.hpp"
#include "FileManager.hpp"
#include "Moderator.hpp"
#include "CoordinateSystem.hpp"
#include "ConicalFOV.hpp"
#include "RectangularFOV.hpp"
#include "CustomFOV.hpp"

#include <osgDB/FileNameUtils>
#include <OpenFrames/FramePointer.hpp>
#include <OpenFrames/Model.hpp>
#include <OpenFrames/EllipticCone.hpp>
#include <OpenFrames/RectangularCone.hpp>

#include <iostream>
#include <sstream>
#include <float.h>
#include <algorithm>

#include <QCoreApplication>

#ifdef USE_OPENVR
#include <openvr.h>
#endif


const Real OFScene::EARTH_RADIUS = GmatSolarSystemDefaults::PLANET_EQUATORIAL_RADIUS[GmatSolarSystemDefaults::EARTH];


/**
 * Constructor
 *
 * @param <parent> The parent of this object, which may be nullptr
 * @param <args> Attribute arguments passed to the underlying wxGLCanvas
 */
OFScene::OFScene(OFWindowProxyPanel* parent) :
   mParent(parent),
   mCanvas(nullptr),
   mFrameManagerLockCount(0),
   mEarliestTime(DBL_MAX),
   mLatestTime(-DBL_MAX),
   mStarOptions(StarOptions::DISABLED),
   mGLOptions()
{
   int width = 320;
   int height = 240;
   bool shaped = false;
   if (mGLOptions.GetEnableVR())
   {
      width = 216;
      height = 240;
      shaped = true;
   }
   mCanvas = new OFGLCanvas(*this);
   mParent->SetCanvas(mCanvas, shaped);
}


/**
 * Destructor
 */
OFScene::~OFScene()
{
   StopOpenFrames();
   OFRenderPool::Instance().RemoveScene(*this);

   // Remove all views before objects
   ClearViews();

   if (mRadialPlane.valid())
   {
      mPrimaryRefFrame->removeChild(mRadialPlane);
   }
   if (mPrimaryAxes.valid())
   {
      mPrimaryRefFrame->removeChild(mPrimaryAxes);
   }

   OFRenderPool::Instance().LockAllScenes();
   mWinProxy = nullptr;
   OFRenderPool::Instance().UnlockAllScenes();
}


//------------------------------------------------------------------------------
// Callbacks from OpenFrames
//------------------------------------------------------------------------------

/**
 * Gets the identifier of the underlying instance of OpenFrames::WindowProxy
 * that draws the canvas
 *
 * @return The underlying window proxy
 *
 * @warning This may be called from the OpenFrames render thread. Therefore, GUI
 * functionality cannot be called directly. Use of the wxWidgets event system is
 * recommended for inter-process communication back to the GUI thread.
 *
 * @see https://wiki.wxwidgets.org/Inter-Thread_and_Inter-Process_communication
 */
unsigned int OFScene::GetWinProxyID()
{
   if (mWinProxy.valid())
      return mWinProxy->getID();
   else
      return 0U;
}


/**
 * Sets the identifier of the underlying instance of OpenFrames::WindowProxy
 * that draws the canvas
 *
 * @param id The new id
 *
 * @warning This may be called from the OpenFrames render thread. Therefore, GUI
 * functionality cannot be called directly. Use of the wxWidgets event system is
 * recommended for inter-process communication back to the GUI thread.
 *
 * @see https://wiki.wxwidgets.org/Inter-Thread_and_Inter-Process_communication
 */
void OFScene::SetWinProxyID(unsigned int id)
{
   if (mWinProxy.valid())
      mWinProxy->setID(id);
}


/**
 * Callback function for OpenFrames to makecurrent on the GL context
 *
 * @return True if the context is set successfully
 * @return False otherwise
 *
 * @warning OpenFrames calls this function from another thread. Therefore, GUI
 * functionality cannot be called directly. Use of the wxWidgets event system is
 * recommended for inter-process communication back to the GUI thread.
 *
 * @see https://wiki.wxwidgets.org/Inter-Thread_and_Inter-Process_communication
 */
bool OFScene::MakeCurrent()
{
   return mCanvas->MakeCurrent();
}


/**
 * Callback function for OpenFrames to swapbuffers on the GL canvas
 *
 * @warning OpenFrames calls this function from another thread. Therefore, GUI
 * functionality cannot be called directly. Use of the wxWidgets event system is
 * recommended for inter-process communication back to the GUI thread.
 *
 * @see https://wiki.wxwidgets.org/Inter-Thread_and_Inter-Process_communication
 */
void OFScene::SwapBuffers()
{
   mCanvas->SwapBuffers();
}


/**
 * Preprocesses a keypress
 *
 * @param event The key event
 * @return true if the key was preprocessed and should not be further processed
 * @return false if the key should be further processed
 *
 */
bool OFScene::PreProcessKeyPress(QKeyEvent &event)
{
   bool processed = true;

   if (mWinProxy.valid() && mWinProxy->isAnimating())
   {
      QString text = event.text();
      int key = event.key();
      Qt::KeyboardModifiers keyMod = event.modifiers();

      // Pause/unpause animation
      if (text == 'p')
         TogglePlayback();

      // Reverse time direction
      else if (text == 'n')
         mParent->ToggleScaleSign();

      // Reset time to epoch. All ReferenceFrames that are following
      // a Trajectory will return to their starting positions.
      else if (text == 'r')
         ResetPlayback(false);
      else if (text == 'R')
         ResetPlayback(true);

      // Speed up time
      else if ((text == '+') || (text == '='))
         mParent->IncreaseScale();

      // Slow down time
      else if ((text == '-') || (text == '_'))
         mParent->DecreaseScale();

      // Change view
      else if (text == 'v')
         NextView();
      else if (text == 'V')
         PreviousView();

      // Change FOVy
      // TODO: This needs to be generalized with GUI elements and defaults
      else if (text == 'z') // Reduce FOVy (zoom in)
      {
         double fovy = GetFOVy();
         if ((int)fovy > 1.0) fovy = (int)fovy - 1;
         else if ((fovy -= 0.05) < 0.1) fovy = 0.1;
         SetFOVy(fovy);
         MessageInterface::ShowMessage("FOVy = %f\n", fovy);
      }
      else if (text == 'Z') // Increase FOVy (zoom out)
      {
         double fovy = GetFOVy();
         if ((fovy + 1.0) < 2.0) fovy += 0.05;
         else if (++fovy > 90.0) fovy = 90.0;
         SetFOVy(fovy);
         MessageInterface::ShowMessage("FOVy = %f\n", fovy);
      }

      // Reset the view trackball
      else if (text == ' ')
         ResetTrackBall();

      // Change animation time
      else if((key == Qt::Key_Left) || (key == Qt::Key_Right))
      {
         // Compute time offset
         double offset;
         if(keyMod & Qt::AltModifier)
            offset = GetTimeScaleFromWindowProxy() / 100.0 / GmatTimeConstants::SECS_PER_DAY;
         else if(keyMod & Qt::ShiftModifier)
            offset = GetTimeScaleFromWindowProxy() / GmatTimeConstants::SECS_PER_DAY;
         else
            offset = GetTimeScaleFromWindowProxy() / 10.0 / GmatTimeConstants::SECS_PER_DAY;

         // Shift time forward
         if(key == Qt::Key_Right)
            SetTimeOffset(GetTimeOffsetFromWindowProxy() + offset);

         // Shift time backward
         else
            SetTimeOffset(GetTimeOffsetFromWindowProxy() - offset);
      }

      // Otherwise pass to openframes for processing
      else
         processed = false;
   }
   else
      processed = false;

   processed = false;
   return processed;
}


/**
 * The function called when the user presses a key
 *
 * @warning OpenFrames calls this function from another thread. Therefore, GUI
 * functionality cannot be called directly. Use of the wxWidgets event system is
 * recommended for inter-process communication back to the GUI thread.
 *
 * @see https://wiki.wxwidgets.org/Inter-Thread_and_Inter-Process_communication
 */
void OFScene::KeyPressCallback(unsigned int row, unsigned int col, int key)
{
  // Key presses are handled during before sending to OpenFrames
}

/**
* The function called when a VR event is generated by OpenVR, e.g. via VR controllers
*
* @warning OpenFrames calls this function from another thread. Therefore, GUI
* functionality cannot be called directly. Use of the wxWidgets event system is
* recommended for inter-process communication back to the GUI thread.
*
* @see https://wiki.wxwidgets.org/Inter-Thread_and_Inter-Process_communication
*/
void OFScene::VREventCallback(unsigned int row, unsigned int col, const OpenFrames::OpenVREvent *vrEvent)
{
#ifdef USE_OPENVR
  // Get the OpenFrames::OpenVRDevice object that handles OpenVR rendering & interaction
  const OpenFrames::OpenVRDevice* ovrDevice = mWinProxy->getOpenVRDevice();

  // Get OpenVR event data; see openvr.h and online documentation/examples for details
  const vr::VREvent_t *ovrEvent = vrEvent->_ovrEvent;  // OpenVR data type
  vr::TrackedDeviceIndex_t deviceID = ovrEvent->trackedDeviceIndex; // Device index that generated event
  const OpenFrames::OpenVRDevice::DeviceModel* deviceModel = ovrDevice->getDeviceModel(deviceID);

  // Only process event if it's from a controller
  if ((deviceModel == nullptr) || (deviceModel->_class != OpenFrames::OpenVRDevice::CONTROLLER)) return;

  // Get current controller state
  const vr::VRControllerState_t *state = ovrDevice->getDeviceModel(deviceID)->_controllerState;

  // Convert controller event types to view changes in VR space
  switch (ovrEvent->eventType)
  {
  case(vr::VREvent_ButtonPress):
  {
    if (state->ulButtonPressed & vr::ButtonMaskFromId(vr::k_EButton_SteamVR_Touchpad))
    {
      // Switch views if grip and touchpad are pressed simultaneously
      if (state->ulButtonPressed & vr::ButtonMaskFromId(vr::k_EButton_Grip))
      {
        mWinProxy->getGridPosition(row, col)->nextView();
        //this->CallAfter(&OFScene::UpdateParentViewSelection);
      }

      // Toggle animation play/pause state when only touchpad is pressed
      else
      {
        //this->CallAfter(&OFScene::TogglePlayback);
      }
    }

    break;
  }
  }
#endif

  // TODO Change GUI elements as needed based on VR controller events
}


//------------------------------------------------------------------------------
// Non-callbacks
//------------------------------------------------------------------------------

/**
 * Start OpenFrames in a separate thread
 *
 * @param <options> OpenGL related options
 */
void OFScene::StartOpenFrames(const OpenGLOptions &options)
{
   double currentTime = 0.0;
   osg::ref_ptr<OpenFrames::WindowProxy> timeSyncTarget;

   if (!mGLOptions.EqualTo(options))
   {
      currentTime = GetTimeOffsetFromWindowProxy();
      if (mWinProxy.valid())
         timeSyncTarget = mWinProxy->getTimeSyncWindow();
      StopOpenFrames();
      ReleaseWindowProxy();
      //mCanvas->Destroy();
      mGLOptions = options;
      mStarOptions.SetStarCatalog("invalidcatalog"); // Force star options to be reprocessed
      int width = 320;
      int height = 240;
      bool shaped = false;
      if (mGLOptions.GetEnableVR())
      {
         width = 216; // htc vive aspect ratio
         height = 240;
         shaped = true;
      }
      mCanvas = new OFGLCanvas(*this);
      mParent->SetCanvas(mCanvas, shaped);
   }

   CreateWindowProxy(mGLOptions.GetEnableVR());

   if (!mWinProxy->isAnimating())
   {
      SetTimeOffset(currentTime);
      mWinProxy->synchronizeTime(timeSyncTarget);
      mWinProxy->startThread();
      while (!mWinProxy->isAnimating())
      {
         QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
      }
   }
}


/**
 * Stop OpenFrames thread
 */
void OFScene::StopOpenFrames()
{
   if (mWinProxy.valid())
   {
      if (mWinProxy->isAnimating())
      {
         mWinProxy->shutdown();
         mWinProxy->join();
      }
   }
}

/**
 * OSG callback that updates HUD text during the update traversal
*/
class HUDUpdateCallback : public osg::Callback
{
public:
   HUDUpdateCallback()
      : mPrimaryRefFrameName("no PrimaryRefFrameName"), mTimeOffset(std::numeric_limits<double>::max())
   {}

   void setPrimaryRefFrameName(std::string primaryRefFrameName)
   {
      mPrimaryRefFrameName = primaryRefFrameName;
   }

   /// Inherited from osg::Callback, implements the callback
   virtual bool run(osg::Object* object, osg::Object* data)
   {
      double newTimeOffset = 0.0;

      // Get simulation time
      osg::NodeVisitor *nv = data ? data->asNodeVisitor() : 0;
      if (nv) newTimeOffset = nv->getFrameStamp()->getSimulationTime();

      // Only compute new HUD text if simulation time changed
      if (mTimeOffset != newTimeOffset)
      {
         mTimeOffset = newTimeOffset;

         // Set HUD text based on simulation time
         osgText::Text* hudText = dynamic_cast<osgText::Text*>(object);
         if (hudText)
         {
            Real toMjd = -999;
            std::string utcGregorian;
            TimeSystemConverter::Instance()->Convert("A1ModJulian", mTimeOffset, "",
               "UTCGregorian", toMjd, utcGregorian);
            std::string hudTextStr = " " + mPrimaryRefFrameName + "\n Epoch: " + utcGregorian;
            hudText->setText(hudTextStr);
         }
      }

      // Call nested callbacks and traverse rest of scene graph
      return osg::Callback::traverse(object, data);
   }

protected:
   std::string mPrimaryRefFrameName;
   double mTimeOffset;
};

/**
 * Creates the windows proxy and configures it to its initial state
 *
 * @param <useVR> Enable virtual reality
 */
void OFScene::CreateWindowProxy(bool useVR)
{
   // Create a set of Coordinate Axes for time history plot
   if (!mPrimaryRefFrame.valid())
   {
      mPrimaryRefFrame = new OpenFrames::ReferenceFrame("axes", 1.0, 1.0, 1.0, 1.0);
      mPrimaryRefFrame->setXLabel("X");
      mPrimaryRefFrame->setYLabel("Y");
      mPrimaryRefFrame->setZLabel("Z");
      mPrimaryRefFrame->showAxes(OpenFrames::ReferenceFrame::NO_AXES);
      mPrimaryRefFrame->showAxesLabels(OpenFrames::ReferenceFrame::NO_AXES);
      mPrimaryRefFrame->showNameLabel(false);
   }

   // Create a set of Coordinate Axes for time history plot
   if (!mPrimaryAxes.valid())
   {
      mPrimaryAxes = new OpenFrames::CoordinateAxes("axes", 0.0, 0.8, 0.8, 1.0);
      mPrimaryAxes->setTickSize(5U, 3U);
      mPrimaryAxes->setXLabel("X");
      mPrimaryAxes->setYLabel("Y");
      mPrimaryAxes->setZLabel("Z");
      mPrimaryAxes->showContents(false);
      mPrimaryRefFrame->addChild(mPrimaryAxes);
   }

   // Create a RadialPlane to show equatorial plane
   if (!mRadialPlane.valid())
   {
      mRadialPlane = new OpenFrames::RadialPlane("xy_plane", 0.0, 0.0, 0.5, 1.0);
      mRadialPlane->setLineColor(osg::Vec4(0.0, 0.0, 0.5, 1.0));
      mRadialPlane->setPlaneColor(osg::Vec4(0.0, 0.0, 0.5, 0.2));
      mRadialPlane->showAxes(OpenFrames::ReferenceFrame::NO_AXES);
      mRadialPlane->showAxesLabels(OpenFrames::ReferenceFrame::NO_AXES);
      mRadialPlane->showNameLabel(false);
      mRadialPlane->setParameters(15*EARTH_RADIUS, EARTH_RADIUS, M_PI / 6.0);
      mRadialPlane->showContents(false);
      mPrimaryRefFrame->addChild(mRadialPlane);
   }

   // Create a RadialPlane to show equatorial plane
   if (!mEclipticPlane.valid())
   {
      mEclipticPlane = new OFEclipticPlane();
      mPrimaryRefFrame->addChild(mEclipticPlane->ReferenceFrame());
   }

   if (!mFrameManager.valid())
   {
      mFrameManager = new OpenFrames::FrameManager;
      mFrameManager->setFrame(mPrimaryRefFrame);
   }

   if (!mHUDText_BottomLeft.valid())
   {
     // Define potential fonts for each OS
     StringArray fontFileList;

#if defined _WIN32
    fontFileList = {
        "courbd.ttf"
     };
#elif defined __linux__
    fontFileList = {
        "courbd.ttf", "msttcore/courbd.ttf",
        "/usr/share/fonts/msttcore/courbd.ttf", // CentOS, Fedora, Red Hat
        "/usr/share/fonts/truetype/msttcorefonts/courbd.ttf", // Ubuntu, Debian
        "LiberationMono-Bold.ttf", "liberation/LiberationMono-Bold.ttf",
        "/usr/share/fonts/liberation/LiberationMono-Bold.ttf", // CentOS, Fedora, Red Hat
        "/usr/share/fonts/truetype/liberation/LiberationMono-Bold.ttf" // Ubuntu, Debian
     };
#elif defined __APPLE__
    fontFileList = {
        "Courier New Bold.ttf"
     };
#endif

     // Setup HUD text font by trying each possible font
     mHUDText_BottomLeft = new osgText::Text;
     for(auto fontFile = fontFileList.begin(); fontFile < fontFileList.end(); fontFile++)
     {
        mHUDText_BottomLeft->setFont(*fontFile);
        const osgText::Font *font = mHUDText_BottomLeft->getFont();
        if(font != nullptr)
           break;
     }

      mHUDText_BottomLeft->setColor(osg::Vec4(0.949, 0.427, 0.129, 1)); // Coral Orange for better contrast on light & dark backgrounds
      mHUDText_BottomLeft->setCharacterSizeMode(osgText::Text::SCREEN_COORDS);
      mHUDText_BottomLeft->setCharacterSize(16.0);
      mHUDText_BottomLeft->setFontResolution(16, 16);
      mHUDText_BottomLeft->setLineSpacing(0.25);

      // Position HUD
      // Screen coordinates go from (0,0) bottom-left to (1,1) top-right
      mHUDText_BottomLeft->setAlignment(osgText::Text::LEFT_BOTTOM);
      mHUDText_BottomLeft->setPosition(osg::Vec3(0.0, 0.0, 0.0));
      
      // Some graphics drivers have a bug where text can't be properly changed.
      // Get around this by initializing text using all likely characters.
      std::string dummyText("the quick brown fox jumps over the lazy dog");
      dummyText += "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG";
      dummyText += "1234567890";
      dummyText += "!@#$%^&*";
      dummyText += "[]{}()<>";
      dummyText += ",.!?;:";
      dummyText += "+-*/=";
      dummyText += "_|\\~`'\"";
      mHUDText_BottomLeft->setText(dummyText); // Allocates memory appropriately
      mHUDText_BottomLeft->setText(" Epoch:"); // Reset to default HUD text

      // Set callback to update HUD text
      mHUDText_BottomLeft->setUpdateCallback(new HUDUpdateCallback());
   }

   if (!mWinProxy.valid())
   {
      // Create embedded WindowProxy that handles all OpenFrames drawing
      mWinProxy = new OpenFrames::WindowProxy(0, 0, mCanvas->width(), mCanvas->height(),
                                              1U, 1U, true, useVR);
      // Add instance to pool, specify static/global callbacks, set ID
      OFRenderPool::Instance().AddScene(*this, *mWinProxy);

      // GMAT models are usually huge, so start with a large VR scale
      mWinProxy->setWorldUnitsPerMeter(1000.0);

      // Set up the scene
      mWinProxy->setScene(mFrameManager, 0U, 0U);

      // Set black background color for spacecraft window (and remember that stars are now disabled)
      mStarOptions.SetEnableStars(false);
      mWinProxy->getGridPosition(0U, 0U)->getSceneView()->getCamera()->setClearColor(osg::Vec4(0.0, 0.0, 0.0, 1.0));

      // Attach HUD text
      osg::Geode* geode = new osg::Geode;
      geode->addDrawable(mHUDText_BottomLeft);
      mWinProxy->getGridPosition(0U, 0U)->getHUD()->addChild(geode);
   }
}


void OFScene::ReleaseWindowProxy()
{
   StopOpenFrames();
   if (mWinProxy.valid())
      mWinProxy->getGridPosition(0U, 0U)->removeAllViews();
   mWinProxy = nullptr;
   OFRenderPool::Instance().RemoveScene(*this);
}


/**
 * Pauses OpenFrames animating so that the scene can be modified
 */
void OFScene::LockFrameManager()
{
   if (mWinProxy.valid())
   {
      if (mFrameManagerLockCount <= 0)
      {
         int status = mWinProxy->getGridPosition(0U, 0U)->getFrameManager()->lock();
         if (status == 0)
            mFrameManagerLockCount = 1;
         else
            MessageInterface::ShowMessage("Error locking OpenFrames FrameManager.\n");
      }
      else
        mFrameManagerLockCount++;
   }
}


/**
 * Resumes OpenFrames animating after all pausing functions have resumed
 */
void OFScene::UnlockFrameManager()
{
   if (mWinProxy.valid())
   {
      if (mFrameManagerLockCount <= 1)
      {
         mWinProxy->getGridPosition(0U, 0U)->getFrameManager()->unlock();
         mFrameManagerLockCount = 0;
      }
      else
         mFrameManagerLockCount--;
   }
}


void OFScene::AddTimeSyncChild(OpenFrames::WindowProxy *child)
{
   if (mWinProxy.valid())
   {
      bool success = child->synchronizeTime(mWinProxy);
      if (!success)
         MessageInterface::ShowMessage("Detected circular time synchronization dependency referencing %s.\n", mParent->GetForObjectName().c_str());
   }
   else
   {
      child->synchronizeTime(nullptr);
      MessageInterface::ShowMessage("Attempt to set time synchronization to non existant parent %s.\n", mParent->GetForObjectName().c_str());
   }
}


/**
 * Triggered when the parent has its OpenFramesInterface object removed
 *
 * This function only takes actions essential to avoiding defects. Right now,
 * all that it does is dettach from a time sync parent to avoid accidental
 * setting of a time scale from an old parent.
 */
void OFScene::RemoveTimeSynchronization()
{
   if (mWinProxy.valid())
      mWinProxy->synchronizeTime(nullptr);
}


bool OFScene::IsTimeSynced()
{
   bool synched = false;
   if (mWinProxy.valid())
   {
      synched = ( mWinProxy->getTimeSyncWindow() != nullptr );
      if (!synched)
         synched = OFRenderPool::Instance().IsTimeSyncTarget(mWinProxy.get());
   }

   return synched;
}


bool OFScene::IsTimeSyncedTo(const OpenFrames::WindowProxy *windowProxy) const
{
   bool synched = false;
   if (mWinProxy.valid())
      synched = ( mWinProxy->getTimeSyncWindow() == windowProxy );

   return synched;
}

void OFScene::SetFOVy(double newFOVy)
{
   if (mWinProxy.valid() && mWinProxy->isAnimating())
   {
      OpenFrames::View* currView = mWinProxy->getGridPosition(0U, 0U)->getCurrentView();

      // Get current FOVy and aspect ratio
      double currFOVy, currRatio;
      currView->getPerspective(currFOVy, currRatio);

      // Set new FOVy as needed
      currView->setPerspective(newFOVy, currRatio);
      mWinProxy->getGridPosition(0U, 0U)->applyCurrentViewProjection();

      // Save FOVy to sandbox
      Integer sel = GetIndexOfCurrentView();
      if (sel != -1)
      {
         GmatBase *view = Moderator::Instance()->GetConfiguredObject(mViews[sel].name);
         if (view != nullptr && view->GetTypeName() == "OpenFramesView")
         {
            static_cast<OpenFramesView *>(view)->SetRealParameter("FOVy", newFOVy);
         }
      }
   }
}

double OFScene::GetFOVy() const
{
   if (mWinProxy.valid() && mWinProxy->isAnimating())
   {
      OpenFrames::View* currView = mWinProxy->getGridPosition(0U, 0U)->getCurrentView();

      // Get current FOVy and aspect ratio
      double fovy, ratio;
      currView->getPerspective(fovy, ratio);
      return fovy;
   }
   else return -1.0;
}


/**
 * Command OpenFrames to change to the next view
 */
void OFScene::NextView()
{
   if (mWinProxy.valid() && mWinProxy->isAnimating())
   {
      mWinProxy->getGridPosition(0U, 0U)->nextView();
      UpdateParentViewSelection();
   }
}


/**
 * Command OpenFrames to change to the previous view
 */
void OFScene::PreviousView()
{
   if (mWinProxy.valid() && mWinProxy->isAnimating())
   {
      mWinProxy->getGridPosition(0U, 0U)->previousView();
      UpdateParentViewSelection();
   }
}


/**
 * Command OpenFrames to change to a specified view
 */
void OFScene::SetCurrentView(unsigned int viewIndex)
{
   if (mWinProxy.valid() && mWinProxy->isAnimating())
   {
      mWinProxy->getGridPosition(0U, 0U)->selectView(viewIndex);
      UpdateParentViewSelection();
   }
}


/**
 * Command OpenFrames to reset the trackball for the current view
 */
void OFScene::ResetTrackBall()
{
   if (mWinProxy.valid() && mWinProxy->isAnimating())
   {
      Integer viewIdx = GetIndexOfCurrentView();
      if (viewIdx != -1)
      {
         ViewDefinition &viewDef = mViews[viewIdx];
         viewDef.view->resetView(); // Reset view so that any active throwing is stopped
         if (viewDef.hasDefaultState) // Set view to user-specified default
         {
            viewDef.view->getTrackball()->setTransformation(viewDef.defaultEye,
                                                            viewDef.defaultCenter,
                                                            viewDef.defaultUp);
         }
         SaveCurrentTrackBall();
      }
      else
      {
         mWinProxy->getGridPosition(0U, 0U)->getCurrentView()->resetView();
      }
   }
}


/**
 * Saves the trackball to the home position and saves to the active
 * OpenFramesView in the Moderator
 *
 * Appropriate checks are in place to verify that the named GMAT objects
 * exists, and that it is an OpenFramesView. If the view does not exist, then
 * there is no feedback to the user, but the trackball is still saved to the
 * current OpenFrames session.
 */
void OFScene::SaveCurrentTrackBall()
{
   if (mWinProxy.valid() && mWinProxy->isAnimating())
   {
      Moderator *theModerator = Moderator::Instance();
      osg::Vec3d eye, center, up;
      Rvector viewEye(3), viewCenter(3), viewUp(3);
      OpenFrames::FollowingTrackball *ball = mWinProxy->getGridPosition(0U, 0U)->getCurrentView()->getTrackball();
      double distance = ball->getDistance();
      ball->TrackballManipulator::getInverseMatrix().getLookAt(eye, center, up, distance);
      for (Integer row = 0; row < 3; row++)
      {
         viewEye.SetElement(row, eye[row]);
         viewCenter.SetElement(row, center[row]);
         viewUp.SetElement(row, up[row]);
      }
      Integer sel = GetIndexOfCurrentView();
      if (sel != -1)
      {
         GmatBase *view = theModerator->GetConfiguredObject(mViews[sel].name);
         if (view != nullptr && view->GetTypeName() == "OpenFramesView")
         {
            static_cast<OpenFramesView *>(view)->SetOnOffParameter("SetCurrentLocation", OpenFramesView::ON_STRING);
            static_cast<OpenFramesView *>(view)->SetRvectorParameter("CurrentEye", viewEye);
            static_cast<OpenFramesView *>(view)->SetRvectorParameter("CurrentCenter", viewCenter);
            static_cast<OpenFramesView *>(view)->SetRvectorParameter("CurrentUp", viewUp);
         }
      }
   }
}


/**
 * Determines the index of the current view within mPrimaryViews
 *
 * @return The index of the view if found
 * @return -1 otherwise
 */
Integer OFScene::GetIndexOfCurrentView()
{
   Integer sel = -1;

   if (mWinProxy.valid())
   {
      OpenFrames::View *view = mWinProxy->getGridPosition(0U, 0U)->getCurrentView();
      for (Integer i = 0; i < mViews.size(); i++)
      {
         if (view == mViews[i].view)
            sel = i;
      }
   }

   return sel;
}


// -----------------------------------------------------------------------------
// Functions that setup the OpenFrames scene
// -----------------------------------------------------------------------------

void OFScene::Initialize(const OpenGLOptions &glOptions, const CoordinateOptions &frameOptions,
                         const StarOptions &starOptions, OpenFramesInterface *timeSyncParent,
                         std::vector<SpacePointOptions> &spacePoints, const std::string &interfaceName,
                         std::vector<OpenFramesView *> &views, std::vector<OpenFramesVector *> &vectors,
                         std::vector<OpenFramesSensorMask *> &masks,
                         const CoordinateSystem *frame, Integer numberOfSolverTrajectories)
{
   mRootCoordinateFrameName = frame->GetName();
   StartOpenFrames(glOptions);
   LockFrameManager();
   SetReferenceFrame(frameOptions, starOptions, timeSyncParent, numberOfSolverTrajectories);
   ResetScene(frame->AreAxesOfType("BodyFixedAxes"), frame->GetOriginName());
   SetObjects(spacePoints, numberOfSolverTrajectories);
   SetVectors(vectors);
   SetSensorMasks(masks);
   SetViews(interfaceName, views);
   UnlockFrameManager();
}


void OFScene::Reconfigure(const OpenGLOptions &glOptions, const CoordinateOptions &frameOptions,
                          const StarOptions &starOptions, OpenFramesInterface *timeSyncParent,
                          std::vector<SpacePointOptions> &spacePoints, const std::string &interfaceName,
                          std::vector<OpenFramesView *> &views, std::vector<OpenFramesVector *> &vectors,
                          std::vector<OpenFramesSensorMask *> &masks,
                          const std::string &frameName, Integer numberOfSolverTrajectories)
{
   bool valid;
   mRootCoordinateFrameName = frameName;
   QString selectedView = mParent->GetCurrentViewString();
   Real originalTime = GetTimeOffsetFromWindowProxy();
   StartOpenFrames(glOptions);
   LockFrameManager();
   SetReferenceFrame(frameOptions, starOptions, timeSyncParent, numberOfSolverTrajectories);
   ClearViews();
   SetObjects(spacePoints, numberOfSolverTrajectories);
   SetVectors(vectors);
   SetSensorMasks(masks);
   SetViews(interfaceName, views);
   for (auto seg = mSegViewDefs.begin(); seg < mSegViewDefs.end(); seg++)
   {
      // TODO Add these views back in the order that the segments were created
      const std::string &viewName = ProcessOpenFramesView(*seg->definition, false, valid);
      if (valid && viewName.size() > 0)
         mParent->AppendToListOfViews(QString::fromStdString(viewName));
   }
   mParent->SelectViewByString(selectedView);
   UnlockFrameManager();

   // Force an update by changing time
   SetTimeOffset(originalTime+1.0);
   SetTimeOffset(originalTime);
}


/**
 * Remove all views from OpenFrames
 */
void OFScene::ClearViews()
{
   LockFrameManager();
   if (mWinProxy.valid())
      mWinProxy->getGridPosition(0, 0)->removeAllViews();
   mViews.clear();
   mSegViewDefs.clear();
   UnlockFrameManager();
}


/**
 * Resets objects in preparation for a new plot
 */
void OFScene::ResetScene(bool frameIsBodyFixed, const std::string &bodyName)
{
   // Reset related variables
   mEarliestTime = DBL_MAX;
   mLatestTime = -DBL_MAX;
   if (mWinProxy.valid())
      mWinProxy->setTime(0.0);

   // Remove old views before spacecraft are removed
   ClearViews();

   // Clear trajectories of objects
   mPrimaryRefFrame->setPosition(0.0, 0.0, 0.0);
   mPrimaryRefFrame->setAttitude(0.0, 0.0, 0.0, 1.0);
   TrajectoryDealer::Instance().ClearAllTrajectories();

   // Make sure that objects centered on the reference frame are reset
   if (frameIsBodyFixed)
   {
      for (auto sc = mSpacecraft.begin(); sc < mSpacecraft.end(); sc++)
      {
         if ((*sc)->GetObjectName() == bodyName)
            (*sc)->ResetState();
      }
      for (auto body = mBodies.begin(); body < mBodies.end(); body++)
      {
         if ((*body)->GetObjectName() == bodyName)
            (*body)->ResetState();
      }
   }
}


/**
 * Set options that apply to the entire plot, such as the reference frame
 *
 * @param <referenceFrameName> The name of the reference frame at the origin of
 *                             the plot, which is used for labeling
 * @param <enableAxes> Shows the axes of the reference frame at the origin
 * @param <enableAxesLabes> Shows the X, Y, Z labels on axes of the reference
 *                          frame at the origin
 * @param <enableFrameLabel> Shows the referenceFrameName at the origin or
 *                           above the reference frame axes if shown
 * @param <enableXYPlane> Draws a plane on the X-Y plane of the reference frame
 * @param <enableEclipticPlane> Draws a plane at the origin along the ecliptic
 * @param <enableStars> Draws a star field
 * @param <starCount> Maximum number of stars to load from the library
 * @param <minStarMag> Lower bound on the magnitude of stars loaded
 * @param <maxStarMag> Upper bound on the magnitude of stars loaded
 * @param <minStarPixels> Number of pixels to draw stars of minStarMag
 * @param <maxStarPixels> Number of pixels to draw stars of maxStarMag
 * @param <minStarDimRatio> Determines amount star dimming in OpenFrames
 * @param <playbackTimeScale> The initial time scale to use during playback
 *
 * This function is part of a multi-step process that must be called to
 * properly setup the OpenFrames ReferenceFrames, Vectors, Views, and
 * RenderRectangles. These are implemented separately not only for clarity, but
 * also because they could be called at different times in the future.
 *
 * 1. OFScene::SetReferenceFrame()
 * 2. OFScene::SetObjects()
 * 3. OFScene::SetVectors()
 * 4. OFScene::SetViews()
 */
void OFScene::SetReferenceFrame(const CoordinateOptions &frameOptions, const StarOptions &starOptions,
                                OpenFramesInterface *timeSyncParent, Integer numberOfSolverTrajectories)
{
   mPrimaryRefFrame->setName(frameOptions.GetCoordSysName());
   mPrimaryAxes->setName(frameOptions.GetCoordSysName());

   if (mWinProxy.valid())
   {
      mWinProxy->pauseTime(true);
      SetTimeScaleToWindowProxy(frameOptions.GetPlaybackTimeScale());
   }

   // Update text parameters of the HUD update callback
   if (mHUDText_BottomLeft.valid())
   {
      HUDUpdateCallback* hudUpdateCallback = dynamic_cast<HUDUpdateCallback*>(mHUDText_BottomLeft->getUpdateCallback());
      if (hudUpdateCallback != nullptr)
      {
         hudUpdateCallback->setPrimaryRefFrameName(mPrimaryRefFrame->getName());
      }
   }

   //mParent->SetTimeScaleValue(frameOptions.GetPlaybackTimeScale());

   if (frameOptions.GetEnableAxes())
   {
      mPrimaryAxes->showContents(true);

      mPrimaryRefFrame->showNameLabel(false);
      if (frameOptions.GetEnableFrameLabel())
         mPrimaryAxes->showNameLabel(true);
      else
         mPrimaryAxes->showNameLabel(false);

      mPrimaryAxes->showAxes(OpenFrames::ReferenceFrame::X_AXIS | OpenFrames::ReferenceFrame::Y_AXIS | OpenFrames::ReferenceFrame::Z_AXIS);
      if (frameOptions.GetEnableAxesLabels())
         mPrimaryAxes->showAxesLabels(OpenFrames::ReferenceFrame::X_AXIS | OpenFrames::ReferenceFrame::Y_AXIS | OpenFrames::ReferenceFrame::Z_AXIS);
      else
         mPrimaryAxes->showAxesLabels(OpenFrames::ReferenceFrame::NO_AXES);
      
      Real axesLength = frameOptions.GetAxesLength();
      Real majorTickSpacing = std::pow(10.0, std::floor(std::log10(axesLength)));
      Real minorTickSpacing = majorTickSpacing/5.0;
      mPrimaryAxes->setAxisLength(axesLength);
      mPrimaryAxes->setTickSpacing(majorTickSpacing, minorTickSpacing);
   }
   else
   {
      mPrimaryAxes->showContents(false);
      mPrimaryAxes->showNameLabel(false);
      mPrimaryAxes->showAxesLabels(0);
      mPrimaryAxes->showAxes(0);

      if (frameOptions.GetEnableFrameLabel())
         mPrimaryRefFrame->showNameLabel(true);
      else
         mPrimaryRefFrame->showNameLabel(false);
   }

   mRadialPlane->showContents(frameOptions.GetEnableXYPlane());

   mEclipticPlane->ReferenceFrame()->showContents(frameOptions.GetEnableEclipticPlane());
   if (frameOptions.GetEnableEclipticPlane())
      mEclipticPlane->Configure(mRootCoordinateFrameName, numberOfSolverTrajectories);

   if (!mStarListener.valid())
      mStarListener = new OFStarListener();
   mStarListener->AssignSkySphere(mWinProxy->getGridPosition(0U, 0U)->getSkySphere(),
                                  mRootCoordinateFrameName,
                                  numberOfSolverTrajectories);
   
   if (!starOptions.EqualTo(mStarOptions))
   {
      mStarOptions = starOptions;
      if (mStarOptions.GetPrintOverride()) // Enable printer mode
      {
         mWinProxy->getGridPosition(0U, 0U)->setBackgroundColor(1.0f, 1.0f, 1.0f); // White background
         mWinProxy->getGridPosition(0U, 0U)->setSkySphereStarData("", mStarOptions.GetMinStarMag(),
                                                                  mStarOptions.GetMaxStarMag(),
                                                                  mStarOptions.GetStarCount(),
                                                                  mStarOptions.GetMinStarPixels(),
                                                                  mStarOptions.GetMaxStarPixels(),
                                                                  mStarOptions.GetMinStarDimRatio());
         mHUDText_BottomLeft->setColor(osg::Vec4(0, 0, 0, 1)); // Black HUD text
      }
      else if (mStarOptions.GetEnableStars()) // Non-printer mode (Monitor, VR, Custom, etc.) with stars
      {
         mWinProxy->getGridPosition(0U, 0U)->setBackgroundColor(0.0f, 0.0f, 0.0f);
         FileManager *fm = FileManager::Instance();
         std::string starCatalogLoc = fm->FindPath(mStarOptions.GetStarCatalog(), "STAR_PATH", true, true, false);
         bool success = mWinProxy->getGridPosition(0U, 0U)->setSkySphereStarData(starCatalogLoc,
                                                                                 mStarOptions.GetMinStarMag(),
                                                                                 mStarOptions.GetMaxStarMag(),
                                                                                 mStarOptions.GetStarCount(),
                                                                                 mStarOptions.GetMinStarPixels(),
                                                                                 mStarOptions.GetMaxStarPixels(),
                                                                                 mStarOptions.GetMinStarDimRatio());
         if (!success)
         {
            MessageInterface::ShowMessage("Unable to load %s. Stars are disabled.\n", mStarOptions.GetStarCatalog().c_str());
         }
      }
      else // Non-printer mode without stars
      {
         mWinProxy->getGridPosition(0U, 0U)->setBackgroundColor(0.0f, 0.0f, 0.0f);
         mWinProxy->getGridPosition(0U, 0U)->setSkySphereStarData("", mStarOptions.GetMinStarMag(),
                                                                  mStarOptions.GetMaxStarMag(),
                                                                  mStarOptions.GetStarCount(),
                                                                  mStarOptions.GetMinStarPixels(),
                                                                  mStarOptions.GetMaxStarPixels(),
                                                                  mStarOptions.GetMinStarDimRatio());
      }
   }

   // Set the parent for time synchronization
   if (timeSyncParent == nullptr)
   {
      mSyncParentName = "";
      mWinProxy->synchronizeTime(nullptr);
   }
   else
   {
      mSyncParentName = timeSyncParent->GetName();
      timeSyncParent->AddTimeSyncChild(mWinProxy.get());
   }
}


/**
 * Set up OpenFrames models and trajectories for releveant spacecraft and
 * celestial bodies
 *
 * @param <spacePoints> A list of spacepoints (spacecraft and celestial bodies)
 *                      to have in the plot
 *
 * This function is part of a multi-step process that must be called to
 * properly setup the OpenFrames ReferenceFrames, Vectors, Views, and
 * RenderRectangles. These are implemented separately not only for clarity, but
 * also because they could be called at different times in the future.
 *
 * 1. OFScene::SetReferenceFrame()
 * 2. OFScene::SetObjects()
 * 3. OFScene::SetVectors()
 * 4. OFScene::SetViews()
 */
void OFScene::SetObjects(std::vector<SpacePointOptions> &spacePoints, Integer numberOfSolverTrajectories)
{
   LockFrameManager();

   // Add the new object or keep an old object for each spacepoint
   std::vector<osg::ref_ptr<OFSpaceObject>> newSpacecraft;
   std::vector<osg::ref_ptr<OFSpaceObject>> newBodies;
   std::vector<osg::ref_ptr<OFSpaceObject>> newGroundStations;
   for (auto spo = spacePoints.begin(); spo < spacePoints.end(); spo++)
   {
      if (spo->GetSpacePointConst() != nullptr)
      {
         if (spo->GetSpacePointConst()->IsOfType(Gmat::SPACECRAFT))
         {
            bool kept = false;
            // Check for a mSpacecraft that can be reused
            for (auto sc = mSpacecraft.begin(); sc < mSpacecraft.end(); sc++)
            {
               bool reconfigured = (*sc)->AttemptReconfigure(*spo, mRootCoordinateFrameName, true,
                                                             numberOfSolverTrajectories);
               if (reconfigured)
               {
                  kept = true;
                  newSpacecraft.push_back(*sc);
                  break;
               }
            }
            // Otherwise add a new one
            if (!kept)
               newSpacecraft.push_back(new OFSpaceObject(*spo, mRootCoordinateFrameName, true,
                                                         numberOfSolverTrajectories));
            // Add object to the reference frame
            newSpacecraft.back()->AddPrimaryToParentFrame(mPrimaryRefFrame);
         }
         else if (spo->GetSpacePointConst()->IsOfType(Gmat::CELESTIAL_BODY))
         {
            bool kept = false;
            // Check for a mBodies that can be reused
            for (auto bd = mBodies.begin(); bd < mBodies.end(); bd++)
            {
               bool reconfigured = (*bd)->AttemptReconfigure(*spo, mRootCoordinateFrameName, false,
                                                             numberOfSolverTrajectories);
               if (reconfigured)
               {
                  kept = true;
                  newBodies.push_back(*bd);
                  break;
               }
            }
            // Otherwise add a new one
            if (!kept)
               newBodies.push_back(new OFSpaceObject(*spo, mRootCoordinateFrameName, false,
                                                     numberOfSolverTrajectories));
            // Add object to the reference frame
            newBodies.back()->AddPrimaryToParentFrame(mPrimaryRefFrame);
         }
         else if (spo->GetSpacePointConst()->IsOfType(Gmat::GROUND_STATION))
         {
            bool kept = false;
            // Check for a mGroundStations that can be reused
            for (auto gs = mGroundStations.begin(); gs < mGroundStations.end(); gs++)
            {
               bool reconfigured = (*gs)->AttemptReconfigure(*spo, mRootCoordinateFrameName, false,
                                                             numberOfSolverTrajectories);
               if (reconfigured)
               {
                  kept = true;
                  newGroundStations.push_back(*gs);
                  break;
               }
            }
            // Otherwise add a new one
            if (!kept)
               newGroundStations.push_back(new OFSpaceObject(*spo, mRootCoordinateFrameName, false,
                                                             numberOfSolverTrajectories));
            // Add object to the reference frame
            newGroundStations.back()->AddPrimaryToParentFrame(mPrimaryRefFrame);
         }
         else
         {
            MessageInterface::ShowMessage("OpenFramesInterface: Unrecognized derived type of SpacePoint.\n");
         }
      }
   }
   mSpacecraft = newSpacecraft;
   mBodies = newBodies;
   mGroundStations = newGroundStations;

   // Disable lights on all osgEarth nodes so the Sun correctly lights up celestial bodies
   ConsolidateCelestialBodyLights();

   UnlockFrameManager();
}

/**
* Sets the vectors from a list of objects
*
* @param <vectors> A list of vectors to create
*
* This function is part of a multi-step process that must be called to
* properly setup the OpenFrames ReferenceFrames, Vectors, Views, and
* RenderRectangles. These are implemented separately not only for clarity, but
* also because they could be called at different times in the future.
*
* 1. OFScene::SetReferenceFrame()
* 2. OFScene::SetObjects()
* 3. OFScene::SetVectors()
* 4. OFScene::SetViews()
*/
void OFScene::SetVectors(std::vector<OpenFramesVector *> &vectors)
{
   LockFrameManager();

   std::vector<VectorDefinition> newVectors;

   for (auto v : vectors)
   {
      if (v == nullptr)
      {
         MessageInterface::ShowMessage("OpenFramesInterface ERROR: Invalid OpenFramesVector!\n");
         continue;
      }

      // Get ReferenceFrame associated with source object
      std::string source = v->GetStringParameter("SourceObject");
      OpenFrames::ReferenceFrame* sourceFrame = GetFrameOnTrajectoryByName(source);
      if (sourceFrame == nullptr)
      {
        MessageInterface::ShowMessage("OpenFramesInterface ERROR: Vector source frame not found!\n");
        continue;
      }

      // Check if an existing vector can be reused
      std::string vectorName = v->GetName();
      VectorDefinition currVector;
      bool vectorExists = false;
      for (auto& oldVector : mVectors)
      {
         if (oldVector.isSameNameAs(vectorName))
         {
            // Use existing vector
            currVector.vector = oldVector.vector;
            vectorExists = true;
            break;
         }
      }

      // If vector does not exist then create it
      if (!vectorExists)
      {
         // Create ReferenceFrame, only show x-axis (the pointing vector)
         currVector.vector = new OpenFrames::ReferenceFrame(vectorName);
         currVector.vector->showAxes(OpenFrames::ReferenceFrame::X_AXIS);
         currVector.vector->showAxesLabels(OpenFrames::ReferenceFrame::X_AXIS);
         currVector.vector->showNameLabel(false);
      }
      
      // Compute vector length
      // If Auto length then set it relative to the size of the source frame
      std::string lengthType = v->GetStringParameter("VectorLengthType");
      osg::BoundingSphere sourceBound = sourceFrame->getBound();
      if (!sourceBound.valid()) sourceBound.set(osg::Vec3(), 1.0); // Account for empty source frame (e.g. ground stations without a model)
      double vectorLength = (lengthType == "Auto") ? sourceBound.radius() : v->GetRealParameter("VectorLength");

      // Add vector to source frame
      // Must be done before FramePointer is created
      currVector.source = sourceFrame;
      currVector.add();

      std::string vectorType = v->GetStringParameter("VectorType");

      // Type = Relative Position: Point frame from source to destination
      if (vectorType == "Relative Position")
      {
        std::string destination = v->GetStringParameter("DestinationObject");
        if (source == destination) continue; // Only create vector if source and destination are different

        // Reset vector to origin and move its x-axis outside the source frame's bounds
        currVector.vector->setPosition(0.0, 0.0, 0.0);
        currVector.vector->setAttitude(osg::Quat());
        currVector.vector->moveXAxis(sourceBound.center() + osg::Vec3(sourceBound.radius(), 0.0, 0.0), vectorLength);

        OpenFrames::ReferenceFrame* destinationFrame = GetFrameOnTrajectoryByName(destination);
        if (destinationFrame == nullptr)
        {
          MessageInterface::ShowMessage("OpenFramesInterface ERROR: Vector destination frame not found!\n");
          continue;
        }

        // Update vector pointing callback
        OpenFrames::FramePointer* fp = dynamic_cast<OpenFrames::FramePointer*>(currVector.vector->getTransform()->getUpdateCallback());
        if (fp == nullptr)
        {
          // Create update callback that will point from source to destination
          fp = new OpenFrames::FramePointer();
          currVector.vector->getTransform()->setUpdateCallback(fp);
        }
        fp->setPointingFrames(mPrimaryRefFrame, currVector.vector, destinationFrame);
      }

      // Type = Body-Fixed: Set specified frame origin and direction
      else if (vectorType == "Body-Fixed")
      {
        // Remove vector pointing callback
        currVector.vector->getTransform()->setUpdateCallback(nullptr);

        // Move x-axis to origin so that it will be properly positioned
        currVector.vector->moveXAxis(osg::Vec3d(), vectorLength);

        // Get origin and direction
        Rvector startRVector = v->GetRvectorParameter("BFStartPoint");
        osg::Vec3d startPoint(startRVector[0], startRVector[1], startRVector[2]);
        Rvector directionRVector = v->GetRvectorParameter("BFDirection");
        osg::Vec3d direction(directionRVector[0], directionRVector[1], directionRVector[2]);

        // Set vector position and orientation
        osg::Quat quat;
        quat.makeRotate(osg::Vec3d(1.0, 0.0, 0.0), direction); // Point x-axis in desired direction
        currVector.vector->setPosition(startPoint);
        currVector.vector->setAttitude(quat);
      }

      // Update common vector properties
      RgbColorFloat color(RgbColor::ToIntColor(v->GetStringParameter("VectorColor")));
      currVector.vector->setColor(color.RedFloat(), color.GreenFloat(), color.BlueFloat(), 0.8f);
      std::string label = v->GetStringParameter("VectorLabel");
      currVector.vector->setXLabel(label);

      newVectors.emplace_back(currVector); // Add vector to list of vectors
   }
   
   // Disconnect unused vectors from their source frames (i.e. in mVectors but not in newVectors)
   for (auto& oldVector : mVectors)
   {
      bool shouldKeep = false;
      for (auto& newVector : newVectors)
      {
         if (oldVector == newVector)
         {
            shouldKeep = true;
            break;
         }
      }
      
      if (!shouldKeep)
      {
         oldVector.remove();
      }
   }
   
   // Update vector list
   mVectors = newVectors;
   
   UnlockFrameManager();
}

/**
* Sets the sensor masks from a list of objects
*
* @param <vectors> A list of sensor masks to create
*
* This function is part of a multi-step process that must be called to
* properly setup the OpenFrames ReferenceFrames, Vectors, Views
* Sensor Masks. These are implemented separately not only for clarity, but
* also because they could be called at different times in the future.
*
* 1. OFScene::SetReferenceFrame()
* 2. OFScene::SetObjects()
* 3. OFScene::SetVectors()
* 4. OFScene::SetViews()
*/
void OFScene::SetSensorMasks(std::vector<OpenFramesSensorMask *> &masks)
{
   LockFrameManager();
   
   std::vector<SensorMaskDefinition> newSensorMasks;

   for (auto m : masks)
   {
      std::string maskName = m->GetName();

      if (m == nullptr)
      {
         MessageInterface::ShowMessage("OpenFramesInterface ERROR: Invalid OpenFramesSensorMask!\n");
         continue;
      }

      // Get ReferenceFrame associated with source object
      std::string maskSource = m->GetSensorMaskSource();
      OpenFrames::ReferenceFrame* sourceFrame = GetFrameOnTrajectoryByName(maskSource);
      if (sourceFrame == nullptr)
      {
        MessageInterface::ShowMessage("OpenFramesInterface ERROR: Sensor Mask source frame \"" + maskSource + "\" not found!\n");
        continue;
      }

      // Get the appropriate FieldOfView object for this mask
      Moderator *theModerator = Moderator::Instance();
      std::string hwName = m->GetSensorMaskHardware();
      Hardware *hwObj = dynamic_cast<Hardware*>(theModerator->GetConfiguredObject(hwName));
      std::string fovName = hwObj ? hwObj->GetRefObjectName(Gmat::FIELD_OF_VIEW) : "";
      GmatBase *fovObjBase = theModerator->GetConfiguredObject(fovName);
      FieldOfView *fovObj = fovObjBase ? dynamic_cast<FieldOfView*>(fovObjBase->Clone()) : nullptr;
      if(fovObj == nullptr)
      {
         MessageInterface::ShowMessage("Could not find FieldOfView \"" + fovName +
                                       "\" attached to Hardware \"" + hwName +
                                       "\" for use with Sensor Mask \"" + maskName + "\"\n");
         continue;
      }
      
      // Check if an existing sensor mask can be reused
      SensorMaskDefinition currMask;
      bool maskExists = false;
      for (auto& oldMask : mSensorMasks)
      {
         if (oldMask.isSameNameAs(maskName))
         {
            // Use existing mask
            currMask.mask = oldMask.mask;
            maskExists = true;
            break;
         }
      }

      // If mask does not exist then create it
      if (!maskExists)
      {
         // Create a conical FOV
         if(fovObj->GetType() == Gmat::CONICAL_FOV)
         {
            ConicalFOV *fov = static_cast<ConicalFOV*>(fovObj);
            OpenFrames::EllipticCone *cone = new OpenFrames::EllipticCone(maskName);
            currMask.mask = cone;
            
            // Set cone angle
            Rvector coneAngle = fov->GetMaskConeAngles();
            cone->setPrimaryAngles(coneAngle(0), coneAngle(0));
         }
         else if(fovObj->GetType() == Gmat::RECTANGULAR_FOV)
         {
            RectangularFOV *fov = static_cast<RectangularFOV*>(fovObj);
            OpenFrames::RectangularCone *cone = new OpenFrames::RectangularCone(maskName);
            currMask.mask = cone;
            
            // Set width/height cone angle
            Real heightAngle = fov->GetAngleHeight();
            Real widthAngle = fov->GetAngleWidth();
            cone->setPrimaryAngles(widthAngle, heightAngle);
         }
         else if(fovObj->GetType() == Gmat::CUSTOM_FOV)
         {
            CustomFOV *fov = static_cast<CustomFOV*>(fovObj);
            OpenFrames::PolyhedralCone *cone = new OpenFrames::PolyhedralCone(maskName);
            currMask.mask = cone;
            fov->Initialize(); // [31] Custom FOV can modify points during Initialize()
            
            // Set clock/cone angles
            Rvector coneAnglesRvec = fov->GetMaskConeAngles();
            Rvector clockAnglesRvec = fov->GetMaskClockAngles();
            const Real *coneAnglesArray = coneAnglesRvec.GetDataVector();
            Integer coneAnglesSize = coneAnglesRvec.GetSize();
            const Real *clockAnglesArray = clockAnglesRvec.GetDataVector();
            Integer clockAnglesSize = clockAnglesRvec.GetSize();
            std::vector<double> coneAngles(coneAnglesArray, coneAnglesArray + coneAnglesSize);
            std::vector<double> clockAngles(clockAnglesArray, clockAnglesArray + clockAnglesSize);
            cone->setVertexAngles(clockAngles, coneAngles);
         }
         else
         {
            MessageInterface::ShowMessage("Object " + fovName + " unhandled type\n");
            continue;
         }
      }

      // Add mask to source frame
      currMask.source = sourceFrame;
      currMask.add();

      // Set mask color
      // TODO: GMAT FieldOfView holds color & alpha separately. Combine them.
      RgbColor colorRGB(fovObj->GetColor());
      RgbColorFloat color(colorRGB.Red(), colorRGB.Green(), colorRGB.Blue(), fovObj->GetAlpha());
      currMask.mask->setConeColor(color.RedFloat(), color.GreenFloat(), color.BlueFloat(), color.AlphaFloat());
      
      // Set mask length
      // If Auto length then set it relative to the size of the source frame
      OpenFramesSensorMask::MaskLengthType lengthType = m->GetSensorMaskAxisLengthType();
      osg::BoundingSphere sourceBound = sourceFrame->getBound();
      if (!sourceBound.valid()) sourceBound.set(osg::Vec3(), 1.0); // Account for empty source frame (e.g. ground stations without a model)
      double maskLength = (lengthType == OpenFramesSensorMask::AUTO) ? sourceBound.radius() : m->GetSensorMaskAxisLength();
      currMask.mask->setConeLength(maskLength);
      
      // Set mask origin & orientation
      // Use hardware direction as mask boresight (-Z axis)
      // Use hardware second direction as mask "up" direction
      // This is consistent with 
      Rvector3 originRvec = hwObj->GetLocation();
      osg::Vec3d origin(originRvec(0), originRvec(1), originRvec(2));
      Rvector3 directionRvec = hwObj->GetDirection();
      osg::Vec3d direction(directionRvec(0), directionRvec(1), directionRvec(2));
      Rvector3 upRvec = hwObj->GetSecondDirection();
      osg::Vec3d up(upRvec(0), upRvec(1), upRvec(2));
      osg::Matrixd maskRot;
      maskRot.makeLookAt(osg::Vec3(), direction, up);
      currMask.mask->setPosition(origin);
      currMask.mask->setAttitude(maskRot.getRotate().inverse());
      
      // Set mask label
      std::string label = m->GetSensorMaskLabel();
      currMask.mask->setZLabel(label);
      currMask.mask->showAxes(OpenFrames::ReferenceFrame::NO_AXES);
      currMask.mask->showAxesLabels(OpenFrames::ReferenceFrame::Z_AXIS);
      currMask.mask->showNameLabel(false);

      newSensorMasks.emplace_back(currMask); // Add mask to list of masks
   }
   
   // Disconnect unused masks from their source frames (i.e. in mSensorMasks but not in newSensorMasks)
   for (auto& oldMask : mSensorMasks)
   {
      bool shouldKeep = false;
      for (auto& newMask : newSensorMasks)
      {
         if (oldMask == newMask)
         {
            shouldKeep = true;
            break;
         }
      }
      
      if (!shouldKeep)
      {
         oldMask.remove();
      }
   }
   
   // Update mask list
   mSensorMasks = newSensorMasks;
   
   UnlockFrameManager();
}
/**
 * Sets the views from a list of objects
 *
 * @param <views>
 *
 * If no valid views are provided, then a default set of views is created. The
 * default set places a view on the origin as well as views on each spacecraft
 * and on each celestrial body.
 *
 * This function is part of a multi-step process that must be called to
 * properly setup the OpenFrames ReferenceFrames, Vectors, Views, and
 * RenderRectangles. These are implemented separately not only for clarity, but
 * also because they could be called at different times in the future.
 *
 * 1. OFScene::SetReferenceFrame()
 * 2. OFScene::SetObjects()
 * 3. OFScene::SetVectors()
 * 4. OFScene::SetViews()
 */
void OFScene::SetViews(const std::string &interfaceName, std::vector<OpenFramesView *> &views)
{
   LockFrameManager();
   QStringList viewNames;

   // Default views
   if (views.size() == 0)
   {
      Moderator *theModerator = Moderator::Instance();
      GmatBase *obj = theModerator->GetConfiguredObject(interfaceName);
      if (obj != nullptr)
      {
         OpenFramesView *view = CreateViewForObject(OpenFramesView::ROOT_FRAME_STRING);
         if (view != nullptr)
         {
            obj->SetStringParameter("View", view->GetName());
            views.push_back(view);
         }
         for (auto cb = mBodies.begin(); cb < mBodies.end(); cb++)
         {
            OpenFramesView *view = CreateViewForObject((*cb)->GetSpacecraftName());
            if (view != nullptr)
            {
               obj->SetStringParameter("View", view->GetName());
               views.push_back(view);
            }
         }
         for (auto sc = mSpacecraft.begin(); sc < mSpacecraft.end(); sc++)
         {
            OpenFramesView *view = CreateViewForObject((*sc)->GetSpacecraftName());
            if (view != nullptr)
            {
               obj->SetStringParameter("View", view->GetName());
               views.push_back(view);
            }
         }
         for (auto gs = mGroundStations.begin(); gs < mGroundStations.end(); gs++)
         {
            OpenFramesView *view = CreateViewForObject((*gs)->GetSpacecraftName());
            if (view != nullptr)
            {
               obj->SetStringParameter("View", view->GetName());
               views.push_back(view);
            }
         }
      }
   }

   // User views
   for (std::vector<OpenFramesView *>::const_iterator v = views.begin(); v < views.end(); v++)
   {
      if ((*v) != nullptr)
      {
         bool valid;
         // Add views of spacepoints
         const std::string &viewName = ProcessOpenFramesView(*(*v), true, valid);
         if (valid && viewName.size() > 0)
               viewNames.append(QString::fromStdString(viewName));
      }
   }

   if (mParent != nullptr)
      mParent->SetListOfViews(viewNames);

   UnlockFrameManager();
}

/**
 * Called when a segment has finished drawing
 *
 * @param <spacecraft> The name of the spacecraft associated with the segment
 * @param <provider> The name of the provide (Propagate) associated with the
 *                   segment
 *
 * If no valid views are provided, then a default set of views is created. The
 * default set places a view on the origin as well as views on each spacecraft
 * and on each celestrial body.
 */
void OFScene::UpdateSegmentViews(const std::string &spacecraft, const std::string &propagate)
{
   // Check for views that need to be reset
   for (auto viewDef = mViews.begin(); viewDef < mViews.end(); viewDef++)
   {
      if (viewDef->isOfTrajectory && !viewDef->hasDefaultState && viewDef->frameName == spacecraft)
      {
         if (!viewDef->hasCurrentState)
            viewDef->view->resetView();
      }
   }

   // Check for views that need to be added
   for (auto seg = mSegViewDefs.begin(); seg < mSegViewDefs.end(); seg++)
   {
      if (seg->propagate == propagate && seg->spacecraft == spacecraft)
      {
         bool found = false;
         for (std::vector<ViewDefinition>::const_iterator v = mViews.begin(); v < mViews.end(); v++)
         {
            if (v->name == seg->definition->GetName())
            {
               found = true;
               break;
            }
         }
         if (!found)
         {
            LockFrameManager();
            bool valid;
            const std::string &viewName = ProcessOpenFramesView(*seg->definition, false, valid);
            UnlockFrameManager();
            if (valid && viewName.size() > 0)
               mParent->AppendToListOfViews(QString::fromStdString(viewName));
         }
      }
   }
}


/**
 * Creates an OpenFrames::View and adds it to the scene
 *
 * @param <view> Specifications of the view to add
 * @param <initialSetup> True if this is during initialization
 * @param[out] <valid> Set to true if a valid view is created
 *
 * @return The name of the created view if valid is true
 * @return Empty string if valid is false
 *
 * When intialSetup is true, views of specific segments are not added
 * immediately. Instead, their specs are saved to an array, and the views are
 * created after the segments are created.
 */
std::string OFScene::ProcessOpenFramesView(OpenFramesView &view, bool initialSetup, bool &valid)
{
   TextParser parser;
   OpenFrames::ReferenceFrame *viewFrame = nullptr;
   std::string viewFrameName = view.GetStringParameter("ViewFrame");
   StringArray parts = parser.SeparateBy(viewFrameName, ".");
   bool useTrajectory = ( view.GetOnOffParameter("ViewTrajectory") == OpenFramesView::ON_STRING );

   // Determine the frame at the center of the view
   valid = false;
   if (parts.size() > 1)
   {
      // This is a segment view (more than one parts)
      // Process it differently during initialization
      if (initialSetup)
      {
         // Add to list of views to be checked after segments finish
         mSegViewDefs.emplace_back();
         mSegViewDefs.back().spacecraft = parts[0];
         mSegViewDefs.back().propagate = parts[1];
         mSegViewDefs.back().definition = &view;
         viewFrame = nullptr;
         valid = true;
      }
      else
      {
         // Its time to add the actual view
         if (useTrajectory)
            viewFrame = GetTrajectoryFrameByName(parts[0], parts[1]);
         else
            viewFrame = GetFrameOnTrajectoryByName(parts[0], parts[1]);
         if (viewFrame == nullptr)
            MessageInterface::ShowMessage("OpenFramesInterface could not find target named \"%s\" for OpenFramesView \"%s\".\n",
                                          viewFrameName.c_str(), view.GetName().c_str());
      }
   }
   else if (parts.size() > 0)
   {
      // Not a segment view
      if (useTrajectory)
         viewFrame = GetTrajectoryFrameByName(parts[0]);
      else
         viewFrame = GetFrameOnTrajectoryByName(parts[0]);
      if (viewFrame == nullptr)
         MessageInterface::ShowMessage("OpenFramesInterface could not find target named \"%s\" for OpenFramesView \"%s\".\n",
                                       viewFrameName.c_str(), view.GetName().c_str());
   }
   else
      viewFrame = nullptr;

   // Create the OpenFrames::View and add it
   if (viewFrame != nullptr)
   {
      // Found the frame for the view
      valid = true;
      bool useInertial = ( view.GetOnOffParameter("InertialFrame") == OpenFramesView::ON_STRING );
      OpenFrames::View::ViewFrameType frame = useInertial ? OpenFrames::View::ABSOLUTE_FRAME : OpenFrames::View::RELATIVE_FRAME;
      std::string lookatFrameName = view.GetStringParameter("LookAtFrame");
      OpenFrames::ReferenceFrame *lookAtFrame = GetFrameOnTrajectoryByName(lookatFrameName);
      if (lookatFrameName.size() > 0 && lookAtFrame != nullptr)
      {
         // Has a valid look at frame
         bool useShortestAngle = ( view.GetOnOffParameter("ShortestAngle") == OpenFramesView::ON_STRING );
         OpenFrames::View::ViewRotationType rotation = useShortestAngle ? OpenFrames::View::DIRECT : OpenFrames::View::AZEL;
         mViews.emplace_back();
         mViews.back().view = new OpenFrames::View(mPrimaryRefFrame, viewFrame, lookAtFrame, frame, rotation);
         mViews.back().isOfTrajectory = useTrajectory;
      }
      else
      {
         // Does not have a valid look at frame
         mViews.emplace_back();
         mViews.back().view = new OpenFrames::View(mPrimaryRefFrame, viewFrame, frame);
         mViews.back().isOfTrajectory = useTrajectory;
         if (viewFrame == mPrimaryRefFrame && view.GetOnOffParameter("SetDefaultLocation") != OpenFramesView::ON_STRING)
         {
            // Size the view of the origin appropriately
            mViews.back().view->setDefaultViewDistance(12.0*EARTH_RADIUS);
            mViews.back().view->resetView();
         }
      }
      mViews.back().name = view.GetName();
      mViews.back().frameName = parts[0];

      // Add new view to window
      if (mWinProxy.valid())
      {
         mWinProxy->getGridPosition(0U, 0U)->addView(mViews.back().view);
      }

      if (view.GetOnOffParameter("SetCurrentLocation") == OpenFramesView::ON_STRING)
      {
         mViews.back().hasCurrentState = true;
         const Rvector &viewEye = view.GetRvectorParameter("CurrentEye");
         const Rvector &viewCenter = view.GetRvectorParameter("CurrentCenter");
         const Rvector &viewUp = view.GetRvectorParameter("CurrentUp");
         osg::Vec3d eye(viewEye[0], viewEye[1], viewEye[2]);
         osg::Vec3d center(viewCenter[0], viewCenter[1], viewCenter[2]);
         osg::Vec3d up(viewUp[0], viewUp[1], viewUp[2]);
         mViews.back().view->getTrackball()->setTransformation(eye, center, up);
      }
      else
         mViews.back().hasCurrentState = false;

      if (view.GetOnOffParameter("SetDefaultLocation") == OpenFramesView::ON_STRING)
      {
         mViews.back().hasDefaultState = true;
         const Rvector &viewEye = view.GetRvectorParameter("DefaultEye");
         const Rvector &viewCenter = view.GetRvectorParameter("DefaultCenter");
         const Rvector &viewUp = view.GetRvectorParameter("DefaultUp");
         mViews.back().defaultEye.set(osg::Vec3d(viewEye[0], viewEye[1], viewEye[2]));
         mViews.back().defaultCenter.set(osg::Vec3d(viewCenter[0], viewCenter[1], viewCenter[2]));
         mViews.back().defaultUp.set(osg::Vec3d(viewUp[0], viewUp[1], viewUp[2]));

         if (view.GetOnOffParameter("SetCurrentLocation") != OpenFramesView::ON_STRING)
         {
            mViews.back().view->getTrackball()->setTransformation(mViews.back().defaultEye,
                                                                  mViews.back().defaultCenter,
                                                                  mViews.back().defaultUp);
         }
      }
      else
         mViews.back().hasDefaultState = false;

      // Set FOVy
      double viewFOVy = view.GetRealParameter("FOVy");
      double currFOVy, currRatio;
      mViews.back().view->getPerspective(currFOVy, currRatio);
      mViews.back().view->setPerspective(viewFOVy, currRatio);

      // Reset view if not using default or current camera location
      if (!mViews.back().hasDefaultState && !mViews.back().hasCurrentState) mViews.back().view->resetView();

      return view.GetName();
   }
   else
   {
      return "";
   }
}


/**
 * Creates an OpenFramesView object for the named object
 *
 * @param<bodyName> The object to create a view of
 */
OpenFramesView *OFScene::CreateViewForObject(const std::string &bodyName) {
   Moderator *theModerator = Moderator::Instance();
   unsigned int i;
   const unsigned int limit = 999999U;
   std::string baseName = bodyName + "View";
   std::string name;
   GmatBase *view = nullptr;

   for (i = 1; i < limit; i++)
   {
      name = baseName + std::to_string(i);
      if (theModerator->GetConfiguredObject(name) == nullptr)
         break;
   }
   if (i < limit)
   {
      view = theModerator->CreateObject(GmatType::GetTypeId("OpenFramesView"), "OpenFramesView", name);
      view->SetStringParameter("ViewFrame", bodyName);
   }

   return static_cast<OpenFramesView *>(view);
}


// -----------------------------------------------------------------------------
// Functions that manipulate the trajectories of spacecraft and celestial bodies
// -----------------------------------------------------------------------------

/**
 * Updates the time limits from the subscriber
 *
 * @param <t> The time associated with this state (days)
 */
void OFScene::UpdateTimeLimits(Real t, bool isSolving)
{
   if (!isSolving)
   {
      bool timeRangeUpdated = false;
      if (t > mLatestTime)
      {
         mLatestTime = t;
         timeRangeUpdated = true;
      }
      if (t < mEarliestTime)
      {
         mEarliestTime = t;
         timeRangeUpdated = true;
      }
      if (timeRangeUpdated)
      {
         //mParent->SetTimeSliderLimits(mEarliestTime, mLatestTime);
         if (mWinProxy.valid())
            mWinProxy->setTimeRange(mEarliestTime, mLatestTime);
      }
      SetTimeOffset(t);
   }
}


/**
 * Delegates command to set the color of the current segment
 *
 * @param name The name of the object to set
 * @param useDefaultColor If true, then resets to default instead of using color
 * @param color The color to set if useDefaultColor if false
 */
void OFScene::SetSegmentColor(const std::string &name, bool useDefaultColor, UnsignedInt color)
{
   for (auto sc = mSpacecraft.begin(); sc < mSpacecraft.end(); sc++)
   {
      if ((*sc)->IsNameSameAs(name))
      {
         if (useDefaultColor)
         {
            (*sc)->SetDefaultSegmentColor();
         }
         else
         {
            RgbColorFloat rgb = RgbColorFloat(color);
            (*sc)->SetSegmentColor(rgb);
         }
      }
   }
}


/**
 * Delegates command to set the color of the orbit trajectory
 *
 * @param name The name of the object to set
 * @param color The color to set if useDefaultColor if false
 */
void OFScene::SetOrbitColor(const std::string &name, UnsignedInt color)
{
   for (auto sc = mSpacecraft.begin(); sc < mSpacecraft.end(); sc++)
   {
      if ((*sc)->IsNameSameAs(name))
      {
         RgbColorFloat rgb = RgbColorFloat(color);
         (*sc)->SetOrbitColor(rgb);
      }
   }
}


/**
 * Delegates command to set the color of the target orbit trajectory
 *
 * @param name The name of the object to set
 * @param color The color to set if useDefaultColor if false
 */
void OFScene::SetTargetColor(const std::string &name, UnsignedInt color)
{
   for (auto sc = mSpacecraft.begin(); sc < mSpacecraft.end(); sc++)
   {
      if ((*sc)->IsNameSameAs(name))
      {
         RgbColorFloat rgb = RgbColorFloat(color);
         (*sc)->SetTargetColor(rgb);
      }
   }
}


/**
 * Gets the reference frame associated with a named object
 *
 * @param name The name of the object to retreive
 */
OpenFrames::ReferenceFrame *OFScene::GetFrameOnTrajectoryByName(const std::string &objectName, const std::string &propName)
{
   if (objectName == mPrimaryRefFrame->getName() || objectName == OpenFramesView::ROOT_FRAME_STRING)
   {
      return mPrimaryRefFrame;
   }
   else
   {
      for (auto sc = mSpacecraft.begin(); sc < mSpacecraft.end(); sc++)
      {
         if ((*sc)->IsNameSameAs(objectName))
         {
            if (propName.size() > 0)
               return (*sc)->TrajectorySegment(propName);
            else
               return (*sc)->FrameOnWholeTrajectory();
         }
      }
      for (auto cb = mBodies.begin(); cb < mBodies.end(); cb++)
      {
         if ((*cb)->IsNameSameAs(objectName))
            return (*cb)->FrameOnWholeTrajectory();
      }
      for (auto gs = mGroundStations.begin(); gs < mGroundStations.end(); gs++)
      {
         if ((*gs)->IsNameSameAs(objectName))
            return (*gs)->FrameOnWholeTrajectory();
      }
   }

   return nullptr;
}


/**
 * Gets the reference frame associated with a named object
 *
 * @param name The name of the object to retreive
 */
OpenFrames::ReferenceFrame *OFScene::GetTrajectoryFrameByName(const std::string &objectName, const std::string &propName)
{
   if (objectName == mPrimaryRefFrame->getName() || objectName == OpenFramesView::ROOT_FRAME_STRING)
   {
      return mPrimaryRefFrame;
   }
   else
   {
      for (auto sc = mSpacecraft.begin(); sc < mSpacecraft.end(); sc++)
      {
         if ((*sc)->IsNameSameAs(objectName))
         {
            if (propName.size() > 0)
               return (*sc)->TrajectorySegment(propName);
            else
               return (*sc)->WholeTrajectory();
         }
      }
      for (auto cb = mBodies.begin(); cb < mBodies.end(); cb++)
      {
         if ((*cb)->IsNameSameAs(objectName))
            return (*cb)->WholeTrajectory();
      }
      for (auto gs = mGroundStations.begin(); gs < mGroundStations.end(); gs++)
      {
         if ((*gs)->IsNameSameAs(objectName))
            return (*gs)->WholeTrajectory();
      }
   }

   return nullptr;
}


// -----------------------------------------------------------------------------
// Functions for manipulating the scene after it is setup
// -----------------------------------------------------------------------------

/**
 * Sets the time offset
 *
 * @param timeOffset The new time offset
 */
void OFScene::SetTimeOffset(Real timeOffset)
{
   SetTimeInWindowProxy(timeOffset);  // does everything but change the slider
   //mParent->SetTimeSliderValue(timeOffset);
}


/**
 * Sets the time offset based on a fraction without a callback to the parent's
 * slider
 *
 * Playback shall pause when the slider moves, and the time follower shall be
 * reset so that the time offset is from zero again
 *
 * @param timeOffset The time offset
 */
void OFScene::SetTimeInWindowProxy(Real timeOffset)
{
   PausePlayback();
   if (mWinProxy.valid())
      mWinProxy->setTime(timeOffset);
}


/**
 * Gets the time offset for animation
 *
 * @return The time offset
 */
Real OFScene::GetTimeOffsetFromWindowProxy()
{
   if (mWinProxy.valid())
      return mWinProxy->getTime();
   else
      return 0.0;
}


/**
 * Resumes playback of the scene
 */
void OFScene::ResumePlayback()
{
   if (mWinProxy.valid())
   {
      if (mWinProxy->isTimePaused())
         TogglePlayback();
   }
}


/**
 * Pauses playback of the scene
 */
void OFScene::PausePlayback()
{
   if (mWinProxy.valid())
   {
      if (!mWinProxy->isTimePaused())
         TogglePlayback();
   }
}


/**
 * Toggles playback of the scene
 */
void OFScene::TogglePlayback()
{
   if (mWinProxy.valid() && mWinProxy->isAnimating())
   {
      //wxTimerEvent event;
      double tMin, tMax;
      mWinProxy->getTimeRange(tMin, tMax);
      if (GetTimeScaleFromWindowProxy() >= 0.0)
      {
         if (mWinProxy->isTimePaused() && GetTimeOffsetFromWindowProxy() == tMax)
            ResetPlayback(false);
      }
      else
      {
         if (mWinProxy->isTimePaused() && GetTimeOffsetFromWindowProxy() == tMin)
            ResetPlayback(true);
      }
      mWinProxy->pauseTime(!mWinProxy->isTimePaused());
      //OnUpdateTimer(event);
   }
}


/**
 * Resets playback of the scene
 */
void OFScene::ResetPlayback(bool toTheEnd)
{
   double tMin, tMax;
   mWinProxy->getTimeRange(tMin, tMax);
   if (toTheEnd)
      SetTimeOffset(tMax);
   else
      SetTimeOffset(tMin);
}


/**
 * Gets the time scale factor for playback
 */
Real OFScene::GetTimeScaleFromWindowProxy()
{
   double scale = 1.0;
   if (mWinProxy.valid())
      scale = mWinProxy->getTimeScale() * GmatTimeConstants::SECS_PER_DAY;
   return scale;
}


/**
 * Sets the time scale factor for playback
 *
 * @param newScaleFactor The new time scale as an index of OFWindowProxyPanel::TIME_SCALES
 */
void OFScene::SetTimeScaleToWindowProxy(Real newScaleFactor)
{
   if (mWinProxy.valid())
      mWinProxy->setTimeScale(newScaleFactor / GmatTimeConstants::SECS_PER_DAY);
}


/**
 * Inform our parent that the view has changed.
 *
 * This function can be safely called from a non-GUI thread (the OpenFrames
 * thread.)
 */
void OFScene::UpdateParentViewSelection()
{
   if (mParent != nullptr)
   {
      int sel = GetIndexOfCurrentView();
      mParent->SetCurrentViewByIndex(sel);
   }
}


//------------------------------------------------------------------------------
// wxWidgets callbacks
//------------------------------------------------------------------------------

/**
 * Callback that updates the time slider and play/pause button
 *
 * @param <event> Event details
 */
//void OFScene::OnUpdateTimer()
//{
//   double timeOffset = GetTimeOffsetFromWindowProxy();
//
//   if (mParent != nullptr)
//   {
//   }
//}

/**
* Find a LightSource node in an osgEarth subgraph. This assumes that an osgEarth LightSource
* node comes from its Sky driver, and has a cull callback attached to it.
* IMPORTANT NOTE: This assumption might fail in the future if osgEarth adds other uses 
* for a LightSource or changes how its LightSource is set up (e.g. removes the NodeCallback).
*/
class FindLightSourceNode : public osg::NodeVisitor
{
public:
   FindLightSourceNode()
      : osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN)
   {
      setNodeMaskOverride(~0);
      reset();
   }

   void reset()
   {
      mLightSource = nullptr;
      mNumLights = 0;
   }

   void apply(osg::LightSource& ls)
   {
      // osgEarth SkyNode light sources are assumed to have a cull callback
      // Save the first SkyNode light source
      if (ls.getCullCallback() && (mLightSource == nullptr)) mLightSource = &ls;

      // Disable the light source and continue traversing
      ++mNumLights;
      ls.setNodeMask(0x0);
      traverse(ls);
   }

   osg::LightSource* mLightSource;
   int mNumLights;
};

/**
* Disables lights on osgEarth nodes and updates the Sun's light so it will work on osgEarth nodes. 
* Required because osgEarth's Sky drivers (simple & GL sky) install a local light on the celestial body node.
*/
void OFScene::ConsolidateCelestialBodyLights()
{
   FindLightSourceNode lsVisitor; // NodeVisitor that finds lights in a node's subgraph
   osg::LightSource *oeLightSource = nullptr;
   osg::LightSource *sunLightSource = nullptr;

   // Disable light source for each celestial body
   // Save the first found osgEarth light source since its CullCallback should be transferred to the Sun
   for (auto &currBody : mBodies)
   {
      // Save Sun for future use
      if (currBody->IsNameSameAs("Sun"))
      {
         sunLightSource = currBody->FrameOnWholeTrajectory()->getLightSource();
         continue;
      }

      // Make sure celestial body is an osgEarth model
      OpenFrames::Model *bodyModel = dynamic_cast<OpenFrames::Model*>(currBody->FrameOnWholeTrajectory());
      if (bodyModel)
      {
         std::string modelFileName = bodyModel->getModel() ? bodyModel->getModel()->getName() : "invalid";
         std::string modelFileExt = osgDB::getFileExtension(modelFileName);
         std::transform(modelFileExt.begin(), modelFileExt.end(), modelFileExt.begin(), ::tolower); // Convert extension to lowercase

         if (modelFileExt == "earth") // Model loaded from osgEarth file (.earth extension)
         {
            // Search the osgEarth node for a light source
            // This also disables all light sources in the node's subgraph
            lsVisitor.reset();
            bodyModel->getModel()->accept(lsVisitor);

            // Save first found LightSource for use with the Sun
            if (lsVisitor.mLightSource && (oeLightSource == nullptr)) oeLightSource = lsVisitor.mLightSource;
         }
      }
   }

   // Copy properties from the osgEarth light source to the Sun's light source
   if (sunLightSource && oeLightSource)
   {
      // Use osgEarth's LightSourceGL3UniformGenerator cull callback
      sunLightSource->setCullCallback(oeLightSource->getCullCallback());

      // Copy Sun's light properties into osgEarth's light
      oeLightSource->getLight()->setPosition(sunLightSource->getLight()->getPosition());
      oeLightSource->getLight()->setAmbient(sunLightSource->getLight()->getAmbient());
      oeLightSource->getLight()->setDiffuse(sunLightSource->getLight()->getDiffuse());
      oeLightSource->getLight()->setSpecular(sunLightSource->getLight()->getSpecular());

      // Use osgEarth's light, which is compatible with GL3
      sunLightSource->setLight(oeLightSource->getLight());
   }
}
