//$Id$
//------------------------------------------------------------------------------
//                                  OFGLCanvas
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 1, 2015
/**
 *  @class OFGLCanvas
 *  Subclasses wxFrame to implement the OpenGL Canvas that OpenFrames will render
 *  onto, and manages the OpenFrames objects that draw the scene.
 */
//------------------------------------------------------------------------------

#ifndef OFGLCANVAS_hpp
#define OFGLCANVAS_hpp

#include <OpenFrames/WindowProxy.hpp> // before windows.h define min and max macros

#include "OpenFramesInterface_defs.hpp"

#include <QOpenGLContext>
#include <QWindow>

class OFScene;

class OpenFramesInterface_API OFGLCanvas : public QWindow
{
public:
   OFGLCanvas(OFScene& scene, QWindow* parent = nullptr);
   virtual ~OFGLCanvas();

   bool MakeCurrent();
   void SwapBuffers();

   static const bool VERBOSE_CONSOLE;

   // wxWidgets events
   //void MouseDownLeft(wxMouseEvent& event);
   //void MouseUpLeft(wxMouseEvent& event);
   //void MouseDownRight(wxMouseEvent& event);
   //void MouseUpRight(wxMouseEvent& event);
   //void MouseDownMiddle(wxMouseEvent& event);
   //void MouseUpMiddle(wxMouseEvent& event);
   //void MouseWheel(wxMouseEvent& event);
   //void MouseAux1(wxMouseEvent& event);
   //void MouseAux2(wxMouseEvent& event);
   //void KeyPressed(wxKeyEvent& event);
   //void KeyPressedHook(wxKeyEvent& event);
   //void KeyDown(wxKeyEvent& event);
   //void KeyUp(wxKeyEvent& event);

   //wxDECLARE_EVENT_TABLE();

protected:
   void mousePressEvent(QMouseEvent *event) override;
   void mouseReleaseEvent(QMouseEvent *event) override;
   void mouseMoveEvent(QMouseEvent *event) override;
   void wheelEvent(QWheelEvent *event) override;
   void keyPressEvent(QKeyEvent *event) override;
   void resizeEvent(QResizeEvent *event) override;
   void exposeEvent(QExposeEvent *event) override;
   //void timerEvent(QTimerEvent *event) override;

private:
   /// Map Qt input to OpenFrames input
   unsigned int mapQtButtonToOFButton(Qt::MouseButtons qButton);
   int mapQtKeyEventToOsgKey(QKeyEvent *event);

   /// The associated scene
   OFScene &mScene;
   /// Whether to automatically pause animation when window is hidden
   bool mAutoPauseAnimation;

   /// The OpenGL context drawn to by the WindowProxy
   //wxGLContext mContext;
   QOpenGLContext* mContext;

   /// Reserve copy constructor
   OFGLCanvas(const OFGLCanvas &) = delete;
   /// Reserve assignment operator
   OFGLCanvas& operator=(const OFGLCanvas&) = delete;
   
   /// Whether alt key is currently pressed (for middle-click emulation)
   //bool mAltKeyPressed;
   /// Whether alt key is currently pressed
   //bool mShiftKeyPressed;
   // Last emulated mouse button
   //unsigned int lastButtonPressed;
};

#endif
