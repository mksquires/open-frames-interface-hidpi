//$Id: GmatPluginFunctions.hpp 9460 2011-04-21 22:03:28Z ravimathur $
//------------------------------------------------------------------------------
//                            GmatPluginFunctions
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 1, 2015
//
/**
 * Definition for library code interfaces.
 *
 * This is prototype code.
 */
//------------------------------------------------------------------------------
#ifndef GmatPluginFunctions_hpp
#define GmatPluginFunctions_hpp

#include "OpenFramesInterface_defs.hpp"

// GUI Plugin Interface
#include "GuiFactory.hpp"

class Factory;
class MessageReceiver;

extern "C"
{
   Integer    OpenFramesInterface_API GetFactoryCount();
   Factory    OpenFramesInterface_API *GetFactoryPointer(Integer index);
   void       OpenFramesInterface_API SetMessageReceiver(MessageReceiver* mr);

   // GUI elements
   Integer                 OpenFramesInterface_API GetMenuEntryCount();
   Gmat::PluginResource    OpenFramesInterface_API *GetMenuEntry(Integer index);
   std::string             OpenFramesInterface_API GetGuiToolkitName();
   Integer                 OpenFramesInterface_API GetGuiFactoryCount();
   GuiFactory              OpenFramesInterface_API *GetGuiFactory(const Integer whichOne);

   void                    OpenFramesInterface_API ProcessEvent(Integer event);
};


#endif /*GmatPluginFunctions_hpp*/
