//------------------------------------------------------------------------------
//                           OpenFramesEventHandler
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 16, 2017
/**
 * Handle GUI events for the OpenFramesInterface GMAT plugin
 */
//------------------------------------------------------------------------------

#include "OpenFramesEventHandler.hpp"
#include "OpenFramesInterface.hpp"
#include "wx/wx.h"
#include "MessageInterface.hpp"
#include "GuiInterface.hpp"
#include "Moderator.hpp"

//#define DEBUG_EVENT_HANDLER

// Forward declaration of creation handler
void HandleOpenFramesInterface(wxCommandEvent &event);

//------------------------------------------------------------------------------
// OpenFramesEventHandler()
//------------------------------------------------------------------------------
/**
 * Constructor
 */
//------------------------------------------------------------------------------
OpenFramesEventHandler::OpenFramesEventHandler()
{
}

//------------------------------------------------------------------------------
// ~OpenFramesEventHandler()
//------------------------------------------------------------------------------
/**
 * Destructor
 */
//------------------------------------------------------------------------------
OpenFramesEventHandler::~OpenFramesEventHandler()
{
}

//------------------------------------------------------------------------------
// OpenFramesEventHandler(const OpenFramesEventHandler& peh)
//------------------------------------------------------------------------------
/**
 * Copy constructor
 *
 * @param peh The object used to make this copy
 */
//------------------------------------------------------------------------------
OpenFramesEventHandler::OpenFramesEventHandler(const OpenFramesEventHandler& peh)
{
}

//------------------------------------------------------------------------------
// OpenFramesEventHandler& operator =(const OpenFramesEventHandler& peh)
//------------------------------------------------------------------------------
/**
 * Assignment operator
 *
 * Nothing to see here
 *
 * @param peh Object used to reconfigure this one
 *
 * @return This object configured to match peh
 */
//------------------------------------------------------------------------------
OpenFramesEventHandler& OpenFramesEventHandler::operator =(
      const OpenFramesEventHandler& peh)
{
   if (this != &peh)
   {
   }
   return *this;
}

//------------------------------------------------------------------------------
// bool ConnectEvent(Gmat::PluginResource& rec, void *parent, Integer messageID)
//------------------------------------------------------------------------------
/**
 * Method used to attach plugin code to the wx based GMAT GUI
 *
 * @param rec The resource structure providing the needed data
 * @param parent The GUI component that owns the message
 * @param messageID The ID associated with the message
 *
 * @return true if the message was bound
 */
//------------------------------------------------------------------------------
bool OpenFramesEventHandler::ConnectEvent(Gmat::PluginResource& rec, void *parent,
      Integer messageID)
{
   bool retval = false;

   if (messageID == -1)
   {
      messageID = 10001;
   }

   if ((rec.subtype == "OpenFramesInterface") && (parent != nullptr))
   {
      ((wxWindow*)parent)->Bind(wxEVT_COMMAND_MENU_SELECTED,
            &(HandleOpenFramesInterface), messageID);
      retval = true;
   }

   return retval;
}


//------------------------------------------------------------------------------
// void HandleOpenFramesInterface(wxCommandEvent &event)
//------------------------------------------------------------------------------
/**
 * Creation handler used for the HandleOpenFramesInterface object
 *
 * @param event The event that triggers this handler
 */
//------------------------------------------------------------------------------
void HandleOpenFramesInterface(wxCommandEvent &event)
{
   GuiInterface *theInterface = GuiInterface::Instance();

   if (theInterface == nullptr)
   {
      MessageInterface::ShowMessage("Unable to create a OpenFramesInterface; the "
            "GuiInterface is nullptr\n");
   }
   else
   {
      Moderator *theModerator = Moderator::Instance();
      unsigned int i;
      const unsigned int limit = 999999U;
      std::string baseName = "OpenFrames";
      std::string name;

      for (i = 1; i < limit; i++)
      {
         name = baseName + std::to_string(i);
         if (theModerator->GetConfiguredObject(name) == nullptr)
            break;
      }

      GmatBase *obj = nullptr;
      if (i < limit)
         obj = theInterface->CreateObject("OpenFramesInterface", name);
      if (obj != nullptr)
      {
         OpenFramesInterface *ofi = static_cast<OpenFramesInterface *>(obj);
         Moderator *theModerator = Moderator::Instance();
         StringArray soConfigList = theModerator->GetListOfObjects(Gmat::SPACECRAFT);
         if (soConfigList.size() > 0)
            ofi->SetStringParameter("Add", soConfigList[0]);
         ofi->SetStringParameter("Add", "Earth");
         ofi->SetObjectBool("DrawCenterPoint", "Earth", false); // default for planets
         ofi->SetObjectBool("DrawEndPoints", "Earth", false);   // default for planets
         ofi->SetStringParameter("CoordinateSystem", "EarthMJ2000Eq");
      }
   }
}
