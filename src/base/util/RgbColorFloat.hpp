//$Id: RgbColorFloat.hpp mruschmann $
//------------------------------------------------------------------------------
//                                  RgbColorFloat
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 21, 2017
/**
 *  @class RgbColorFloat
 *  Sits on top of GMAT's RgbColor class to provide floating point output the
 *  range of zero to one.
 */
//------------------------------------------------------------------------------

#ifndef RGBCOLORFLOAT_H
#define RGBCOLORFLOAT_H

#include "OpenFramesInterface_defs.hpp"
#include "RgbColor.hpp"


class OpenFramesInterface_API RgbColorFloat : public RgbColor
{
public:
   RgbColorFloat();
   RgbColorFloat(const Byte red, const Byte green, const Byte blue, const Byte alpha = 0);
   RgbColorFloat(const UnsignedInt intColor);
   RgbColorFloat(const RgbColorFloat &rgbColor);
   RgbColorFloat& operator=(const RgbColorFloat &rgbColor);
   virtual ~RgbColorFloat();

   float RedFloat();
   float GreenFloat();
   float BlueFloat();
   float AlphaFloat();

private:
   static float Integer255ToFloat1(Integer input);
};

#endif
