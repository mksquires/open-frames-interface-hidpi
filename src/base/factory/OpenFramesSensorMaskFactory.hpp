//$Id$
//------------------------------------------------------------------------------
//                            OpenFramesSensorMaskFactory
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under SBIR contract 80NSSC19C0044
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: January 10, 2020
/**
*  Declaration code for the OpenFramesSensorMask class.
*/
//------------------------------------------------------------------------------
#ifndef OPENFRAMESSENSORMASKFACTORY_hpp
#define OPENFRAMESSENSORMASKFACTORY_hpp

#include "OpenFramesInterface_defs.hpp"
#include "Factory.hpp"


class OpenFramesInterface_API OpenFramesSensorMaskFactory : public Factory
{
public:
  virtual GmatBase* CreateObject(const std::string &ofType,
    const std::string &withName = "");

  // default constructor
  OpenFramesSensorMaskFactory();
  // copy constructor
  OpenFramesSensorMaskFactory(const OpenFramesSensorMaskFactory& fact);
  // assignment operator
  OpenFramesSensorMaskFactory& operator=(const OpenFramesSensorMaskFactory& fact);

  virtual ~OpenFramesSensorMaskFactory();

};

#endif
