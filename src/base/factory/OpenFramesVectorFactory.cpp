//$Id$
//------------------------------------------------------------------------------
//                            OpenFramesVectorFactory
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Yasir Majeed Khan, Emergent Space Technologies, Inc.
// Created: October 16, 2018
/**
*  Implementation code for the OpenFramesVectorFactory class, responsible for
*  creating OpenFramesInterface Generic Objects.
*/
//------------------------------------------------------------------------------
#include "OpenFramesVectorFactory.hpp"
#include "OpenFramesVector.hpp"


//---------------------------------
//  public methods
//---------------------------------

//------------------------------------------------------------------------------
//  CreateObject(const std::string &ofType, const std::string &withName)
//------------------------------------------------------------------------------
/**
* This method creates and returns an object of the requested Generic Object
* class in generic way.
*
* @param <ofType> the Generic Object to create and return.
* @param <withName> the name to give the newly-created Generic Object.
*
*/
//------------------------------------------------------------------------------
GmatBase* OpenFramesVectorFactory::CreateObject(const std::string &ofType,
  const std::string &withName)
{
  if (ofType == "OpenFramesVector")
  {
    return new OpenFramesVector(ofType, withName);
  }

  return nullptr;
}


//------------------------------------------------------------------------------
//  OpenFramesVectorFactory()
//------------------------------------------------------------------------------
/**
* This method creates an object of the class OpenFramesVectorFactory
* (default constructor).
*
*
*/
//------------------------------------------------------------------------------
OpenFramesVectorFactory::OpenFramesVectorFactory() :
Factory(GmatType::RegisterType("OpenFramesVector"))
{
  if (creatables.empty())
  {
    creatables.push_back("OpenFramesVector");
  }
}


//------------------------------------------------------------------------------
//  OpenFramesVectorFactory(const OpenFramesVectorFactory& fact)
//------------------------------------------------------------------------------
/**
* This method creates an object of the class OpenFramesVectorFactory
* (copy constructor).
*
* @param <fact> the factory object to copy to "this" factory.
*/
//------------------------------------------------------------------------------
OpenFramesVectorFactory::OpenFramesVectorFactory(const OpenFramesVectorFactory& fact) :
Factory(fact)
{
  if (creatables.empty())
  {
    creatables.push_back("OpenFramesVector");
  }
}


//------------------------------------------------------------------------------
// OpenFramesVectorFactory& operator= (const OpenFramesVectorFactory& fact)
//------------------------------------------------------------------------------
/**
* Assignment operator for the OpenFramesVectorFactory class.
*
* @param <fact> the OpenFramesVectorFactory object whose data to assign to "this"
*               factory.
*
* @return "this" OpenFramesVectorFactory with data of input factory fact.
*/
//------------------------------------------------------------------------------
OpenFramesVectorFactory& OpenFramesVectorFactory::operator= (const OpenFramesVectorFactory& fact)
{
  if (this != &fact)
  {
    Factory::operator=(fact);
  }
  return *this;
}


//------------------------------------------------------------------------------
// ~OpenFramesVectorFactory()
//------------------------------------------------------------------------------
/**
* Destructor for the OpenFramesVectorFactory base class.
*
*/
//------------------------------------------------------------------------------
OpenFramesVectorFactory::~OpenFramesVectorFactory()
{
  // deletes handled by Factory destructor
}
