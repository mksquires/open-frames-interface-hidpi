//$Id$
//------------------------------------------------------------------------------
//                            OpenFramesInterfaceFactory
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur (Emergent Space Technologies, Inc.)
// Created: December 1, 2015
/**
 *  Declaration code for the OpenFramesInterfaceFactory class. This class demos
 *  the use of OpenFrames visualizations with GMAT.
 */
//------------------------------------------------------------------------------
#ifndef OpenFramesInterfaceFactory_hpp
#define OpenFramesInterfaceFactory_hpp

#include "OpenFramesInterface_defs.hpp"
#include "Factory.hpp"
#include "Subscriber.hpp"

class OpenFramesInterface_API OpenFramesInterfaceFactory : public Factory
{
public:
   virtual GmatBase* CreateObject(const std::string &ofType,
                                  const std::string &withName = "");
   virtual Subscriber* CreateSubscriber(const std::string &ofType,
                                        const std::string &withName = "");

   // default constructor
   OpenFramesInterfaceFactory();
   // copy constructor
   OpenFramesInterfaceFactory(const OpenFramesInterfaceFactory& fact);
   // assignment operator
   OpenFramesInterfaceFactory& operator=(const OpenFramesInterfaceFactory& fact);

   virtual ~OpenFramesInterfaceFactory();

};

#endif // OpenFramesInterfaceFactory_hpp
