//$Id$
//------------------------------------------------------------------------------
//                            OpenFramesSensorMaskFactory
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under SBIR contract 80NSSC19C0044
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: January 10, 2020
/**
*  Implementation code for the OpenFramesSensorMaskFactory class, responsible for
*  creating OpenFramesInterface Sensor Mask Objects.
*/
//------------------------------------------------------------------------------
#include "OpenFramesSensorMaskFactory.hpp"
#include "OpenFramesSensorMask.hpp"


//---------------------------------
//  public methods
//---------------------------------

//------------------------------------------------------------------------------
//  CreateObject(const std::string &ofType, const std::string &withName)
//------------------------------------------------------------------------------
/**
* This method creates and returns an object of the requested Generic Object
* class in generic way.
*
* @param <ofType> the Generic Object to create and return.
* @param <withName> the name to give the newly-created Generic Object.
*
*/
//------------------------------------------------------------------------------
GmatBase* OpenFramesSensorMaskFactory::CreateObject(const std::string &ofType,
  const std::string &withName)
{
  if (ofType == "OpenFramesSensorMask")
  {
    return new OpenFramesSensorMask(ofType, withName);
  }

  return nullptr;
}


//------------------------------------------------------------------------------
//  OpenFramesSensorMaskFactory()
//------------------------------------------------------------------------------
/**
* This method creates an object of the class OpenFramesSensorMaskFactory
* (default constructor).
*
*
*/
//------------------------------------------------------------------------------
OpenFramesSensorMaskFactory::OpenFramesSensorMaskFactory() :
Factory(GmatType::RegisterType("OpenFramesSensorMask"))
{
  if (creatables.empty())
  {
    creatables.push_back("OpenFramesSensorMask");
  }
}


//------------------------------------------------------------------------------
//  OpenFramesSensorMaskFactory(const OpenFramesSensorMaskFactory& fact)
//------------------------------------------------------------------------------
/**
* This method creates an object of the class OpenFramesSensorMaskFactory
* (copy constructor).
*
* @param <fact> the factory object to copy to "this" factory.
*/
//------------------------------------------------------------------------------
OpenFramesSensorMaskFactory::OpenFramesSensorMaskFactory(const OpenFramesSensorMaskFactory& fact) :
Factory(fact)
{
  if (creatables.empty())
  {
    creatables.push_back("OpenFramesSensorMask");
  }
}


//------------------------------------------------------------------------------
// OpenFramesSensorMaskFactory& operator= (const OpenFramesSensorMaskFactory& fact)
//------------------------------------------------------------------------------
/**
* Assignment operator for the OpenFramesSensorMaskFactory class.
*
* @param <fact> the OpenFramesSensorMaskFactory object whose data to assign to "this"
*               factory.
*
* @return "this" OpenFramesSensorMaskFactory with data of input factory fact.
*/
//------------------------------------------------------------------------------
OpenFramesSensorMaskFactory& OpenFramesSensorMaskFactory::operator= (const OpenFramesSensorMaskFactory& fact)
{
  if (this != &fact)
  {
    Factory::operator=(fact);
  }
  return *this;
}


//------------------------------------------------------------------------------
// ~OpenFramesSensorMaskFactory()
//------------------------------------------------------------------------------
/**
* Destructor for the OpenFramesSensorMaskFactory base class.
*
*/
//------------------------------------------------------------------------------
OpenFramesSensorMaskFactory::~OpenFramesSensorMaskFactory()
{
  // deletes handled by Factory destructor
}
