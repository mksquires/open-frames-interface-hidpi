//$Id$
//------------------------------------------------------------------------------
//                            OpenFramesVectorFactory
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Yasir Majeed Khan (Emergent Space Technologies, Inc.)
// Created: October 16, 2018
/**
*  Declaration code for the OpenFramesVectorFactory class. This class demos
*  the use of OpenFrames visualizations with GMAT.
*/
//------------------------------------------------------------------------------
#ifndef OpenFramesVectorFactory_hpp
#define OpenFramesVectorFactory_hpp

#include "OpenFramesInterface_defs.hpp"
#include "Factory.hpp"


class OpenFramesInterface_API OpenFramesVectorFactory : public Factory
{
public:
  virtual GmatBase* CreateObject(const std::string &ofType,
    const std::string &withName = "");

  // default constructor
  OpenFramesVectorFactory();
  // copy constructor
  OpenFramesVectorFactory(const OpenFramesVectorFactory& fact);
  // assignment operator
  OpenFramesVectorFactory& operator=(const OpenFramesVectorFactory& fact);

  virtual ~OpenFramesVectorFactory();

};

#endif // OpenFramesVectorFactory_hpp
