//$Id$
//------------------------------------------------------------------------------
//                                  OpenFramesSensorMask
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http ://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under SBIR contract 80NSSC19C0044
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: January 10, 2020
/**
*  OpenFramesSensorMask class
*  Manages parameters associated with an OpenFrames sensor mask.
*/
//------------------------------------------------------------------------------

#include "OpenFramesSensorMask.hpp"

#include "SubscriberException.hpp"
#include "TextParser.hpp"


/// Names of parameters for OpenFramesSensorMask
const std::string
OpenFramesSensorMask::PARAMETER_TEXT[OpenFramesSensorMaskParamCount - GmatBaseParamCount] =
{
   "Source",
   "Hardware",
   "Label",
   "LengthType",
   "Length",
};


/// Gmat interpreter types for the parameters in @ref OpenFramesSensorMask::PARAMETER_TEXT
const Gmat::ParameterType
OpenFramesSensorMask::PARAMETER_TYPE[OpenFramesSensorMaskParamCount - GmatBaseParamCount] =
{
   Gmat::OBJECT_TYPE,           //"Source"
   Gmat::OBJECT_TYPE,           //"Hardware"
   Gmat::STRING_TYPE,           //"Label"
   Gmat::ENUMERATION_TYPE,      //"LengthType"
   Gmat::REAL_TYPE,             //"Length"
};

/// Names of MaskLengthType parameters
const StringArray
OpenFramesSensorMask::MaskLengthTypeStrings =
{
   "Auto",        // AUTO
   "Manual",      // MANUAL
   "",            // Invalid (NUM_MASK_LENGTH_TYPES)
};

//------------------------------------------------------------------------------
// OpenFramesSensorMask(const std::string &type, const std::string &name)
//------------------------------------------------------------------------------
/**
*  Constructor
*/
//------------------------------------------------------------------------------
OpenFramesSensorMask::OpenFramesSensorMask(const std::string &type, const std::string &name) :
GmatBase(GmatType::GetTypeId("OpenFramesSensorMask"), type, name),
mSensorMaskSource("None Selected"),
mSensorMaskHardware("None Selected"),
mSensorMaskLabel("DefaultSensorMask"),
mSensorMaskLengthType(AUTO),
mSensorMaskLength(1.0)
{
  objectTypeNames.push_back("OpenFramesSensorMask");
  objectTypes.push_back(GmatBase::type);

  parameterCount = OpenFramesSensorMaskParamCount;
}


//------------------------------------------------------------------------------
// ~OpenFramesSensorMask(void)
//------------------------------------------------------------------------------
/**
* Destructor.
*/
//------------------------------------------------------------------------------
OpenFramesSensorMask::~OpenFramesSensorMask(void)
{
}


//------------------------------------------------------------------------------
// OpenFramesSensorMask(const OpenFramesSensorMask &rf)
//------------------------------------------------------------------------------
/**
* Copy Constructor
*/
//------------------------------------------------------------------------------
OpenFramesSensorMask::OpenFramesSensorMask(const OpenFramesSensorMask &dc) :
GmatBase(dc)
{
   mSensorMaskSource = dc.mSensorMaskSource;
   mSensorMaskHardware = dc.mSensorMaskHardware;
   mSensorMaskLabel = dc.mSensorMaskLabel;
   mSensorMaskLengthType = dc.mSensorMaskLengthType;
   mSensorMaskLength = dc.mSensorMaskLength;

   parameterCount = OpenFramesSensorMaskParamCount;
}


//------------------------------------------------------------------------------
// OpenFramesSensorMask& OpenFramesSensorMask::operator=(const OpenFramesSensorMask& dc)
//------------------------------------------------------------------------------
/**
* Assignment operator
*/
//------------------------------------------------------------------------------
OpenFramesSensorMask& OpenFramesSensorMask::operator=(const OpenFramesSensorMask& dc)
{
   if (this == &dc) return *this;

   GmatBase::operator=(dc);

   mSensorMaskSource = dc.mSensorMaskSource;
   mSensorMaskHardware = dc.mSensorMaskHardware;
   mSensorMaskLabel = dc.mSensorMaskLabel;
   mSensorMaskLengthType = dc.mSensorMaskLengthType;
   mSensorMaskLength = dc.mSensorMaskLength;

   return *this;
}

//------------------------------------------------------------------------------
// MaskLengthType GetMaskLengthType(const std::string& str)
// const std::string& GetMaskLengthTypeString(MaskLengthType type)
//------------------------------------------------------------------------------
/**
* Convert between MaskLengthType and its corresponding string
*/
//------------------------------------------------------------------------------
OpenFramesSensorMask::MaskLengthType OpenFramesSensorMask::GetMaskLengthType(const std::string& typeStr) const
{
   for(int i = 0; i < NUM_MASK_LENGTH_TYPES; ++i)
   {
      if(typeStr == MaskLengthTypeStrings[i]) return (MaskLengthType)i;
   }
   return NUM_MASK_LENGTH_TYPES;
}

const std::string& OpenFramesSensorMask::GetMaskLengthTypeString(MaskLengthType type) const
{
   return MaskLengthTypeStrings[type];
}

//------------------------------------------------------------------------------
// SetSensorMaskAxisLengthType
//------------------------------------------------------------------------------
/**
* Set the sensor mask axis length type
*/
//------------------------------------------------------------------------------
void OpenFramesSensorMask::SetSensorMaskAxisLengthType(MaskLengthType type)
{
   if(type != NUM_MASK_LENGTH_TYPES) mSensorMaskLengthType = type;
}

void OpenFramesSensorMask::SetSensorMaskAxisLengthType(const std::string& typeStr)
{
   // Throw error if mask length type is invalid
   MaskLengthType type = GetMaskLengthType(typeStr);
   if(type == NUM_MASK_LENGTH_TYPES)
   {
      std::string validMaskLengthTypes = MaskLengthTypeStrings[0];
      for(int i = 1; i < NUM_MASK_LENGTH_TYPES; ++i) validMaskLengthTypes += ", " + MaskLengthTypeStrings[i];
      SubscriberException se;
      se.SetDetails(errorMessageFormat.c_str(), typeStr.c_str(),
        GetParameterText(LENGTH_TYPE).c_str(), validMaskLengthTypes.c_str());
      throw se;
   }
   
   mSensorMaskLengthType = type;
}

void OpenFramesSensorMask::SetSensorMaskAxisLength(Real length)
{ 
   if(length > 0.0)
   {
      mSensorMaskLength = length;
   }
   else
   {
      std::string valueStr = std::to_string(length);
      SubscriberException se;
      se.SetDetails(errorMessageFormat.c_str(), valueStr.c_str(),
         GetParameterText(LENGTH).c_str(), "Real Value > 0.0");
      throw se;
   }
}

//------------------------------------------------------------------------------
//  bool Validate()
//------------------------------------------------------------------------------
/**
* Performs any pre-run validation that the object needs.
*
* @return true unless validation fails.
*/
//------------------------------------------------------------------------------
bool OpenFramesSensorMask::Validate()
{
  return GmatBase::Validate();
}


//------------------------------------------------------------------------------
// virtual bool Initialize()
/**
* Initialize an instance of the interface, or reinitailize an instance
*
* @return true for all logic paths
*/
//------------------------------------------------------------------------------
bool OpenFramesSensorMask::Initialize()
{
  return GmatBase::Initialize();
}


//------------------------------------------------------------------------------
//  GmatBase* Clone(void) const
//------------------------------------------------------------------------------
/**
* This method returns a clone of the OpenFramesSensorMask.
*
* @return clone of the OpenFramesSensorMask.
*/
//------------------------------------------------------------------------------
GmatBase* OpenFramesSensorMask::Clone() const
{
  return (new OpenFramesSensorMask(*this));
}


//---------------------------------------------------------------------------
// void Copy(const GmatBase* orig)
//---------------------------------------------------------------------------
/**
* Sets this object to match another one.
*
* @param orig The original that is being copied.
*/
//---------------------------------------------------------------------------
void OpenFramesSensorMask::Copy(const GmatBase* orig)
{
  operator=(*((OpenFramesSensorMask *)(orig)));
}


//------------------------------------------------------------------------------
// virtual bool TakeAction(const std::string &action,
//                         const std::string &actionData = "");
//------------------------------------------------------------------------------
/**
* This method performs action.
*
* @param <action> action to perform
* @param <actionData> action data associated with action
* @return true if action successfully performed
*
*/
//------------------------------------------------------------------------------
bool OpenFramesSensorMask::TakeAction(const std::string &action,
  const std::string &actionData)
{
  return GmatBase::TakeAction(action, actionData);
}


//---------------------------------------------------------------------------
//  bool RenameRefObject(const Gmat::ObjectType type,
//                       const std::string &oldName, const std::string &newName)
//---------------------------------------------------------------------------
bool OpenFramesSensorMask::RenameRefObject(const UnsignedInt type,
  const std::string &oldName,
  const std::string &newName)
{
   bool success = true;

   if (type == Gmat::SPACECRAFT || type == Gmat::GROUND_STATION ||
    type == Gmat::CALCULATED_POINT || type == Gmat::CELESTIAL_BODY || type == Gmat::SPACE_POINT)
   {
      bool renamed = false;
      TextParser parser;

      StringArray mSourceObjectParts = parser.SeparateBy(mSensorMaskSource, ".");
      if (mSourceObjectParts.size() > 0 && mSourceObjectParts[0] == oldName)
      {
         mSensorMaskSource.replace(0, mSourceObjectParts[0].size(), newName);
         renamed = true;
      }

      if (!renamed)
      success = GmatBase::RenameRefObject(type, oldName, newName);
   }
   else if (type == Gmat::FIELD_OF_VIEW)
   {
      bool renamed = false;
      TextParser parser;

      StringArray mHardwareParts = parser.SeparateBy(mSensorMaskHardware, ".");
      if (mHardwareParts.size() > 0 && mHardwareParts[0] == oldName)
      {
         mSensorMaskHardware.replace(0, mHardwareParts[0].size(), newName);
         renamed = true;
      }

      if (!renamed)
      success = GmatBase::RenameRefObject(type, oldName, newName);
   }
   else
   {
       success = GmatBase::RenameRefObject(type, oldName, newName);
   }

   return success;
}


//------------------------------------------------------------------------------
// std::string GetParameterText(const Integer id) const
//------------------------------------------------------------------------------
std::string OpenFramesSensorMask::GetParameterText(const Integer id) const
{
  if (id >= GmatBaseParamCount && id < OpenFramesSensorMaskParamCount)
    return PARAMETER_TEXT[id - GmatBaseParamCount];
  else
    return GmatBase::GetParameterText(id);

}


//------------------------------------------------------------------------------
// Integer GetParameterID(const std::string &str) const
//------------------------------------------------------------------------------
Integer OpenFramesSensorMask::GetParameterID(const std::string &str) const
{
  for (int i = GmatBaseParamCount; i < OpenFramesSensorMaskParamCount; i++)
  {
    if (str == PARAMETER_TEXT[i - GmatBaseParamCount])
      return i;
  }

  return GmatBase::GetParameterID(str);
}


//------------------------------------------------------------------------------
// Gmat::ParameterType GetParameterType(const Integer id) const
//------------------------------------------------------------------------------
Gmat::ParameterType OpenFramesSensorMask::GetParameterType(const Integer id) const
{
  if (id >= GmatBaseParamCount && id < OpenFramesSensorMaskParamCount)
    return PARAMETER_TYPE[id - GmatBaseParamCount];
  else
    return GmatBase::GetParameterType(id);
}


//------------------------------------------------------------------------------
// std::string GetParameterTypeString(const Integer id) const
//------------------------------------------------------------------------------
std::string OpenFramesSensorMask::GetParameterTypeString(const Integer id) const
{
  return GmatBase::PARAM_TYPE_STRING[GetParameterType(id)];
}


//---------------------------------------------------------------------------
//  bool IsParameterReadOnly(const Integer id) const
//---------------------------------------------------------------------------
bool OpenFramesSensorMask::IsParameterReadOnly(const Integer id) const
{
  return GmatBase::IsParameterReadOnly(id);
}


//------------------------------------------------------------------------------
// std::string GetStringParameter(const Integer id) const
//------------------------------------------------------------------------------
std::string OpenFramesSensorMask::GetStringParameter(const Integer id) const
{
   switch (id)
   {
      case SOURCE:
         return GetSensorMaskSource();
      case HARDWARE:
         return GetSensorMaskHardware();
      case LABEL:
         return GetSensorMaskLabel();
      case LENGTH_TYPE:
         return MaskLengthTypeStrings[mSensorMaskLengthType];
      default:
         return GmatBase::GetStringParameter(id);
   }
}


//------------------------------------------------------------------------------
// bool SetStringParameter(const Integer id, const std::string &value)
//------------------------------------------------------------------------------
bool OpenFramesSensorMask::SetStringParameter(const Integer id, const std::string &value)
{
   switch (id)
   {
      case SOURCE:
      {
         mSensorMaskSource = value;
         return true;
      }
      case HARDWARE:
      {
         mSensorMaskHardware = value;
         return true;
      }
      case LABEL:
      {
         mSensorMaskLabel = value;
         return true;
      }
      case LENGTH_TYPE:
      {
         SetSensorMaskAxisLengthType(value);
         return true;
      }
      default:
         return GmatBase::SetStringParameter(id, value);
   }
}


//---------------------------------------------------------------------------
//  Real GetRealParameter(const Integer id) const
//---------------------------------------------------------------------------
Real OpenFramesSensorMask::GetRealParameter(const Integer id) const
{
  switch (id)
  {
  case LENGTH:
    return mSensorMaskLength;

  default:
    return GmatBase::GetRealParameter(id);
  }
}


//---------------------------------------------------------------------------
//  Real SetRealParameter(const Integer id, const Real value)
//---------------------------------------------------------------------------
Real OpenFramesSensorMask::SetRealParameter(const Integer id, const Real value)
{
   switch(id)
   {
   case LENGTH:
   {
      SetSensorMaskAxisLength(value);
      return true;
   }

   default:
      return GmatBase::SetRealParameter(id, value);
   }
}


//---------------------------------------------------------------------------
// const StringArray& GetPropertyEnumStrings(const Integer id) const
//---------------------------------------------------------------------------
/**
 * Retrieves eumeration symbols of parameter of given id.
 *
 * @param <id> ID for the parameter.
 *
 * @return list of enumeration symbols
 */
//---------------------------------------------------------------------------
const StringArray& OpenFramesSensorMask::GetPropertyEnumStrings(const Integer id) const
{
   switch (id)
   {
      case LENGTH_TYPE:
         return MaskLengthTypeStrings;
      default:
         return GmatBase::GetPropertyEnumStrings(id);
   }
}

//------------------------------------------------------------------------------
// bool IsParameterCloaked(const Integer id) const
//------------------------------------------------------------------------------
bool OpenFramesSensorMask::IsParameterCloaked(const Integer id) const
{
  switch (id)
  {
    // Mask length cloaked if length type is Auto
    case LENGTH:
    {
      if (mSensorMaskLengthType == AUTO) return true;
      else return false;
    }

    default:
      return GmatBase::IsParameterCloaked(id);
  }
}


//---------------------------------------------------------------------------
// Gmat::ObjectType GetPropertyObjectType(const Integer id) const
//---------------------------------------------------------------------------
UnsignedInt OpenFramesSensorMask::GetPropertyObjectType(const Integer id) const
{
  return GmatBase::GetPropertyObjectType(id);
}
