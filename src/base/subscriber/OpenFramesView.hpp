//$Id$
//------------------------------------------------------------------------------
//                                  OpenFramesView
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 24, 2017
//
/**
 *  @class OpenFramesView
 *  An interface for tracking configuration parameters associated with an
 *  OpenFrames::View
 */
//------------------------------------------------------------------------------

#ifndef OPENFRAMESVIEW_hpp
#define OPENFRAMESVIEW_hpp

#include "OpenFramesInterface_defs.hpp"

#include "GmatBase.hpp"


class OpenFramesInterface_API OpenFramesView : public GmatBase
{
public:
   static const std::string ON_STRING;
   static const std::string OFF_STRING;
   static const std::string ROOT_FRAME_STRING;

   OpenFramesView(const std::string &typeName, const std::string &name);

   virtual ~OpenFramesView(void);

   OpenFramesView(const OpenFramesView &);
   OpenFramesView& operator=(const OpenFramesView&);

   // methods inhereted from GmatBase
   virtual bool         Validate();
   virtual bool         Initialize();

   virtual GmatBase*    Clone(void) const;
   virtual void         Copy(const GmatBase* orig);
   virtual bool         TakeAction(const std::string &action,
                                   const std::string &actionData);

   virtual bool         RenameRefObject(const UnsignedInt type,
                                        const std::string &oldName,
                                        const std::string &newName);

   // methods for parameters
   virtual std::string  GetParameterText(const Integer id) const;
   virtual Integer      GetParameterID(const std::string &str) const;
   virtual Gmat::ParameterType
                        GetParameterType(const Integer id) const;
   virtual std::string  GetParameterTypeString(const Integer id) const;
   virtual bool         IsParameterReadOnly(const Integer id) const;

   virtual std::string  GetStringParameter(const Integer id) const;
   virtual bool         SetStringParameter(const Integer id, const std::string &value);
   virtual std::string  GetStringParameter(const std::string &label) const;
   virtual bool         SetStringParameter(const std::string &label,
                                           const std::string &value);

   virtual std::string  GetOnOffParameter(const Integer id) const;
   virtual bool         SetOnOffParameter(const Integer id,
                                          const std::string &value);
   virtual std::string  GetOnOffParameter(const std::string &label) const;
   virtual bool         SetOnOffParameter(const std::string &label,
                                          const std::string &value);

   virtual Real         GetRealParameter(const Integer id) const;
   virtual Real         GetRealParameter(const std::string &label) const;
   virtual Real         GetRealParameter(const Integer id,
                                         const Integer index) const;
   virtual Real         GetRealParameter(const std::string &label,
                                         const Integer index) const;
   virtual Real         SetRealParameter(const Integer id,
                                         const Real value);
   virtual Real         SetRealParameter(const std::string &label,
                                         const Real value);
   virtual Real         SetRealParameter(const Integer id, const Real value,
                                         const Integer index);
   virtual Real         SetRealParameter(const std::string &label,
                                         const Real value, const Integer index);
   virtual const Rvector&
                        GetRvectorParameter(const Integer id) const;
   virtual const Rvector&
                        GetRvectorParameter(const std::string &label) const;
   virtual const Rvector&
                        SetRvectorParameter(const Integer id,
                                            const Rvector &value);
   virtual const Rvector&
                        SetRvectorParameter(const std::string &label,
                                            const Rvector &value);

   virtual bool         IsParameterCloaked(const Integer id) const;
   virtual bool         IsParameterCloaked(const std::string &label) const;

   // for GUI population
   virtual UnsignedInt  GetPropertyObjectType(const Integer id) const;

   DEFAULT_TO_NO_CLONES

protected:
   /// The name of the frame to center the view on
   std::string mViewFrame;
   /// The name of the frame to look at
   std::string mLookAtFrame;
   /// Flag to indicate an inertial view
   bool mUseAbsoluteFrame;
   /// Flag to indicate look at trajectory instead of object
   bool mUseTrajectory;
   /// Flag to indicate use of shortest angle for look at frame
   bool mUseShortestAngle;
   /// Flag to indicate that the lookup vectors shall be used instead of
   /// default trackball
   bool mUseDefault;
   /// Vector defines the default camera eye position for the view
   Rvector mDefaultEye;
   /// Vector defines the default camera center position for the view
   Rvector mDefaultCenter;
   /// Vector defines the default camera up vector for the view
   Rvector mDefaultUp;
   /// Flag to indicate that the current vectors shall be used instead of
   /// default trackball
   bool mUseCurrent;
   /// Vector defines the current camera eye position for the view
   Rvector mCurrentEye;
   /// Vector defines the current camera center position for the view
   Rvector mCurrentCenter;
   /// Vector defines the current camera up vector for the view
   Rvector mCurrentUp;
   /// Real defines the camera vertical field of view
   Real mFOVy;

   /// An emumeration of parameters for OpenFramesView, starting at the end
   /// of the @ref OrbitPlot::OrbitPlotParamCount enumeration.
   enum
   {
      VIEW_FRAME = GmatBaseParamCount, ///< The frame that this view is centered around
      VIEW_TRAJECTORY,                 ///< View focuses on trajectory instead of object
      INERTIAL_FRAME,                  ///< View does not spin with the frame
      LOOK_AT_FRAME,                   ///< The frame that this view shall look at
      SHORTEST_ANGLE,                  ///< Use the shortest angle to the look at frame
      SPECIFY_DEFAULT,                 ///< Use the default vectors instead of default trackball
      DEFAULT_EYE,                     ///< Rvector representing the default position eye
      DEFAULT_CENTER,                  ///< Rvector representing the default position center
      DEFAULT_UP,                      ///< Rvector representing the default position up
      SPECIFY_CURRENT,                 ///< Use the current vectors instead of default trackball
      CURRENT_EYE,                     ///< Rvector representing the current position eye
      CURRENT_CENTER,                  ///< Rvector representing the current position center
      CURRENT_UP,                      ///< Rvector representing the current position up
      FOV_Y,                           ///< Camera vertical field of view
      SPECIFY_CAMERA,                  ///< @deprecated Maps to SPECIFY_DEFAULT ("SetDefaultPosition")
      CAMERA_EYE,                      ///< @deprecated Maps to DEFAULT_EYE ("DefaultEye")
      CAMERA_CENTER,                   ///< @deprecated Maps to DEFAULT_CENTER ("DefaultCenter")
      CAMERA_UP,                       ///< @deprecated Maps to DEFAULT_UP ("DefaultUp")
      OpenFramesViewParamCount,        ///< Count of the parameters for this class
   };

private:
   static std::string BooleanToOnOff(bool onOff);

   static const std::string
         PARAMETER_TEXT[OpenFramesViewParamCount - GmatBaseParamCount];
   static const Gmat::ParameterType
         PARAMETER_TYPE[OpenFramesViewParamCount - GmatBaseParamCount];
};

#endif
