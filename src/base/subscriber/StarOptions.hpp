//$Id$
//------------------------------------------------------------------------------
//                                  StarOptions
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 24, 2017
/**
 *  @class StarOptions
 *  Conveniently wraps up all options associated with stars in OpenFrames.
 */
//------------------------------------------------------------------------------

#ifndef STAROPTIONS_H
#define STAROPTIONS_H

#include "OpenFramesInterface_defs.hpp"

#include <string>


class OpenFramesInterface_API StarOptions
{
public:
   static const std::string DefaultStarCatalog;
   static const StarOptions DISABLED;
   static const StarOptions PRINTER;
   static const StarOptions MONITOR;
   static const StarOptions VIRTUAL_REALITY;

   StarOptions();
   StarOptions(const std::string& starCatalog,
               bool enableStars, bool printOverride, float minStarMag,
               float maxStarMag, UnsignedInt starCount, float minStarPixels,
               float maxStarPixels, float minStarDimRatio);
   virtual ~StarOptions();

   StarOptions(const StarOptions &source);
   StarOptions& operator=(const StarOptions &rhs);

   const std::string& GetStarCatalog() const { return mStarCatalog; } ///< get @ref mStarCatalog
   void SetStarCatalog(const std::string& starCatalog) { mStarCatalog = starCatalog; } ///< set @ref mStarCatalog
   bool GetEnableStars() const { return mEnableStars; } ///< get @ref mEnableStars
   void SetEnableStars(bool value) { mEnableStars = value; } ///< set @ref mEnableStars
   bool GetPrintOverride() const { return mPrintOverride; } ///< get @ref mPrintOverride
   void SetPrintOverride(bool value) { mPrintOverride = value; } ///< set @ref mPrintOverride
   float GetMinStarMag() const { return mMinStarMag; } ///< get @ref mMinStarMag
   void SetMinStarMag(float value) { mMinStarMag = value; } ///< set @ref mMinStarMag
   float GetMaxStarMag() const { return mMaxStarMag; } ///< get @ref mMaxStarMag
   void SetMaxStarMag(float value) { mMaxStarMag = value; } ///< set @ref mMaxStarMag
   UnsignedInt GetStarCount() const { return mStarCount; } ///< get @ref mStarCount
   void SetStarCount(UnsignedInt value) { mStarCount = value; } ///< set @ref mStarCount
   float GetMinStarPixels() const { return mMinStarPixels; } ///< get @ref mMinStarPixels
   void SetMinStarPixels(float value) { mMinStarPixels = value; } ///< set @ref mMinStarPixels
   float GetMaxStarPixels() const { return mMaxStarPixels; } ///< get @ref mMaxStarPixels
   void SetMaxStarPixels(float value) { mMaxStarPixels = value; } ///< set @ref mMaxStarPixels
   float GetMinStarDimRatio() const { return mMinStarDimRatio; } ///< get @ref mMinStarDimRatio
   void SetMinStarDimRatio(float value) { mMinStarDimRatio = value; } ///< set @ref mMinStarDimRatio

   bool EqualTo(const StarOptions &options) const;

private:
   /// Star catalog filename
   std::string mStarCatalog;
   /// Enable stars
   bool mEnableStars;
   /// Enable printer-friendly background
   bool mPrintOverride;
   /// Minimum star magnitude to load from the catalog
   float mMinStarMag;
   /// Maximum star magnitude to load from the catalog
   float mMaxStarMag;
   /// Number of stars to load from the catalog
   UnsignedInt mStarCount;
   /// Minimum pixel size for displaying star
   float mMinStarPixels;
   /// Maximum pixel size for displaying star
   float mMaxStarPixels;
   /// Minimum ratio of star dimming
   float mMinStarDimRatio;
};

#endif
