//$Id: OpenFramesInterface.hpp rmathur $
//------------------------------------------------------------------------------
//                                  OpenFramesInterface
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 24, 2017
//
/**
 *  @class OpenFramesInterface
 *  The primary interface of the OpenFrames plugin, which collects subscribed
 *  and delegates it to the OpenFrames window
 */
//------------------------------------------------------------------------------

#ifndef OPENFRAMESINTERFACE_hpp
#define OPENFRAMESINTERFACE_hpp

#include "OpenFramesInterface_defs.hpp"
#include "SpacePointOptions.hpp"
#include "StarOptions.hpp"
#include "OpenGLOptions.hpp"
#include "CoordinateOptions.hpp"

#include "Subscriber.hpp"
#include "CoordinateConverter.hpp"

class BodyFixedPoint;
class Parameter;
class OFWindowProxyPanel;
class OpenFramesView;
class OpenFramesVector;
class OpenFramesSensorMask;

namespace OpenFrames
{
   class WindowProxy;
}


class OpenFramesInterface_API OpenFramesInterface : public Subscriber
{
public:
   static const Integer TRAJ_PER_SOLVER_ITER = 2;
   static const std::string ON_STRING;
   static const std::string OFF_STRING;

   OpenFramesInterface(const std::string &typeName, const std::string &name);

   virtual ~OpenFramesInterface(void);

   OpenFramesInterface(const OpenFramesInterface &);
   OpenFramesInterface& operator=(const OpenFramesInterface&);

   const OpenGLOptions& GetGLOptions() const { return mGLOptions; }
   void SetGLOptions(const OpenGLOptions &options) { mGLOptions = options; }
   const CoordinateOptions&  GetFrameOptions() const { return mFrameOptions; }
   const StarOptions&   GetStarOptions() const;
   const std::vector<SpacePointOptions>& GetSpacePointOptions() const { return mSpacePoints; }

   bool                 GetObjectBool(const std::string &property, const std::string &name);
   void                 SetObjectBool(const std::string &property, const std::string &name, bool value);
   Real                 GetObjectReal(const std::string &property, const std::string &name);
   void                 SetObjectReal(const std::string &property, const std::string &name, Real value);
   UnsignedInt          GetObjectUInt(const std::string &property, const std::string &name);
   void                 SetObjectUInt(const std::string &property, const std::string &name, UnsignedInt value);
   std::string          GetObjectString(const std::string &property, const std::string &name);
   void                 SetObjectString(const std::string &property, const std::string &name, const std::string &value);

   void                 AddTimeSyncChild(OpenFrames::WindowProxy *child);

   // methods inhereted from GmatBase
   virtual bool         Validate();

   virtual GmatBase*    Clone(void) const;
   virtual void         Copy(const GmatBase* orig);
   virtual bool         TakeAction(const std::string &action,
                                   const std::string &actionData);

   virtual bool         HasGuiPlugin();
   virtual StringArray  GetGuiPanelNames(const std::string &type = "");

   OFWindowProxyPanel*         GetWidget();
   virtual void         SetWidget(GmatWidget *widget);
   virtual void         RemoveWidget();
   virtual const char** GetIcon();
   virtual void         SetIconIndex(Integer index);
   virtual Integer      GetIconIndex();

   virtual bool         RenameRefObject(const UnsignedInt type,
                                        const std::string &oldName,
                                        const std::string &newName);

   // methods for parameters
   virtual std::string  GetParameterText(const Integer id) const;
   virtual Integer      GetParameterID(const std::string &str) const;
   virtual Gmat::ParameterType
                        GetParameterType(const Integer id) const;
   virtual std::string  GetParameterTypeString(const Integer id) const;
   virtual bool         IsParameterReadOnly(const Integer id) const;

   virtual std::string  GetStringParameter(const Integer id) const;
   virtual bool         SetStringParameter(const Integer id, const std::string &value);
   virtual std::string  GetStringParameter(const std::string &label) const;
   virtual bool         SetStringParameter(const std::string &label,
                                           const std::string &value);

   virtual bool         SetStringParameter(const Integer id, const std::string &value,
                                           const Integer index);
   virtual bool         SetStringParameter(const std::string &label,
                                           const std::string &value,
                                           const Integer index);

   virtual const StringArray&
                        GetStringArrayParameter(const Integer id) const;
   virtual const StringArray&
                        GetStringArrayParameter(const std::string &label) const;
   virtual bool         SetStringArrayParameter(const Integer id, const StringArray &stringArray);
   virtual bool         SetStringArrayParameter(const std::string &label, const StringArray &stringArray);

   virtual bool         GetBooleanParameter(const Integer id) const;
   virtual bool         GetBooleanParameter(const std::string &label) const;
   virtual bool         SetBooleanParameter(const Integer id,
                                            const bool value);
   virtual bool         SetBooleanParameter(const std::string &label,
                                            const bool value);

   virtual std::string  GetOnOffParameter(const Integer id) const;
   virtual bool         SetOnOffParameter(const Integer id,
                                          const std::string &value);
   virtual std::string  GetOnOffParameter(const std::string &label) const;
   virtual bool         SetOnOffParameter(const std::string &label,
                                          const std::string &value);

   virtual const BooleanArray&
                        GetBooleanArrayParameter(const Integer id) const;
   virtual const BooleanArray&
                        GetBooleanArrayParameter(const std::string &label) const;
   virtual bool         SetBooleanArrayParameter(const Integer id,
                                                 const BooleanArray &valueArray);
   virtual bool         SetBooleanArrayParameter(const std::string &label,
                                                 const BooleanArray &valueArray);

   virtual Integer      GetIntegerParameter(const Integer id) const;
   virtual Integer      SetIntegerParameter(const Integer id, const Integer value);
   virtual Integer      GetIntegerParameter(const std::string &label) const;
   virtual Integer      SetIntegerParameter(const std::string &label,
                                            const Integer value);

   virtual UnsignedInt  GetUnsignedIntParameter(const Integer id,
                                                const Integer index) const;
   virtual UnsignedInt  SetUnsignedIntParameter(const Integer id,
                                                const UnsignedInt value,
                                                const Integer index);
   virtual const UnsignedIntArray&
                        GetUnsignedIntArrayParameter(const Integer id) const;

   virtual Real         GetRealParameter(const Integer id) const;
   virtual Real         SetRealParameter(const Integer id, const Real value);
   virtual Real         GetRealParameter(const std::string &label) const;
   virtual Real         SetRealParameter(const std::string &label, const Real value);

   virtual Real         GetRealParameter(const Integer id,
                                         const Integer index) const;
   virtual Real         SetRealParameter(const Integer id,
                                         const Real value,
                                         const Integer index);
   virtual const Rvector& GetRvectorParameter(const Integer id) const;
   virtual const Rvector& SetRvectorParameter(const Integer id,
                                              const Rvector &value);
   virtual const Rvector& GetRvectorParameter(const std::string &label) const;
   virtual const Rvector& SetRvectorParameter(const std::string &label,
                                              const Rvector &value);

   virtual std::string  GetRefObjectName(const UnsignedInt type) const;
   virtual bool         HasRefObjectTypeArray();
   virtual const ObjectTypeArray&
                        GetRefObjectTypeArray();
   virtual const StringArray&
                        GetRefObjectNameArray(const UnsignedInt type);

   virtual GmatBase*    GetRefObject(const UnsignedInt type,
                                     const std::string &name);
   virtual bool         SetRefObject(GmatBase *obj, const UnsignedInt type,
                                     const std::string &name = "");

   virtual bool         IsParameterCloaked(const Integer id) const;
   virtual bool         IsParameterCloaked(const std::string &label) const;

   virtual void         SetOrbitColorChanged(GmatBase *originator,
                                             const std::string &newColor,
                                             const std::string &objName,
                                             const std::string &desc);
   virtual void         SetTargetColorChanged(GmatBase *originator,
                                              const std::string &newColor,
                                              const std::string &objName,
                                              const std::string &desc);
   virtual void         SetSegmentOrbitColor(GmatBase *originator,
                                             bool overrideColor,
                                             UnsignedInt orbitColor,
                                             const StringArray &objNames);
   // for GUI population
   virtual UnsignedInt  GetPropertyObjectType(const Integer id) const;

   // for GUI population
   virtual const StringArray&
                        GetPropertyEnumStrings(const Integer id) const;
   virtual const StringArray&
                        GetPropertyEnumStrings(const std::string &label) const;
   static Integer GetSolverIterOptionCount() { return OFSolverIterOptionCount; }
   static const std::string* GetSolverIterOptionList() { return OF_SOLVER_ITER_OPTION_TEXT; }

   virtual bool         Initialize();

   enum StarSettingsOption
   {
      STARS_MONITOR,
      STARS_PRINTER,
      STARS_VR,
      STARS_CUSTOM,
      StarSettingsOptionCount
   };

   enum OFSolverIterOption
   {
      OF_SI_ALL,
      OF_SI_LAST_N,
      OF_SI_CURRENT,
      OF_SI_NONE,
      OFSolverIterOptionCount
   };

   static const std::string STAR_SETTING_STRINGS[StarSettingsOptionCount];

protected:
   /// The icon instance index provided by resource tree
   static Integer ICON_INDEX;

   /// The widget created for this plugin interface instance at execution time
   OFWindowProxyPanel *mExecutionWidget;

   /// List of spacepoint names to plot (for output only)
   StringArray mAllSpNameArray;
   /// List of spacepoints and their configured options
   std::vector<SpacePointOptions> mSpacePoints;
   /// The coordinate system provided to us
   CoordinateSystem *mViewCoordSystem;
   /// List of views to configure in OpenFrames
   StringArray mViewNames;
   /// List of vectors to configure in OpenFrames
   StringArray mVectorNames;
   /// List of sensor masks to configure in OpenFrames
   StringArray mMaskNames;
   /// List of views to configure in OpenFrames
   std::vector<OpenFramesView *> mViews;
   /// List of vectors to configure in OpenFrames
   std::vector<OpenFramesVector *> mVectors;
   /// List of sensor masks to configure in OpenFrames
   std::vector<OpenFramesSensorMask *> mMasks;
   /// Enable drawing object for each added spacepoint (for output only)
   BooleanArray mDrawObjectArray;
   /// Enable drawing trajectory for each added spacepoint (for output only)
   BooleanArray mDrawTrajectoryArray;
   /// Enable drawing axes for each added spacepoint (for output only)
   BooleanArray mDrawAxesArray;
   /// Enable drawing XY plane for each added spacepoint (for output only)
   BooleanArray mDrawXYPlaneArray;
   /// Enable drawing label for each added spacepoint (for output only)
   BooleanArray mDrawLabelArray;
   /// Use propagate label instead of spacecraft for each added spacepoint (for output only)
   BooleanArray mDrawLabelPropagateArray;
   /// Enable drawing center point for each added spacepoint (for output only)
   BooleanArray mDrawCenterArray;
   /// Enable drawing end points for each added spacepoint (for output only)
   BooleanArray mDrawEndsArray;
   /// Enable drawing velocity for each added spacepoint (for output only)
   BooleanArray mDrawVelocityArray;
   /// Enable drawing latitude and longitude for each added spacepoint (for output only)
   BooleanArray mDrawGridArray;
   /// Line with of trajectories for each added spacepoint (for output only)
   Rvector mDrawLineWidthVector;
   /// Line with of trajectories for each added spacepoint (for output only)
   UnsignedIntArray mDrawMarkerSizeVector;
   /// Line with of trajectories for each added spacepoint (for output only)
   UnsignedIntArray mDrawFontSizeVector;
    /// Line with of trajectories for each added spacepoint (for output only)
   StringArray mDrawFontPositionVector;
   /// Counter for index of mDrawFontPositionVector
   UnsignedInt mDrawFontPositionCounter;
   /// Line with of trajectories for each added spacepoint (for output only)
   StringArray mDrawModelFileVector;
   /// Counter for index of mDrawModelFileVector
   UnsignedInt mDrawModelFileCounter;
   /// Reference frame and time options
   CoordinateOptions mFrameOptions;
   /// Star settings preset setting
   StarSettingsOption mStarSettings;
   /// Star settings preset string
   std::string mStarSettingsString;
   /// Options relevent to the stars
   StarOptions mCustomStarOptions;
   /// Show the toolbar
   bool mShowToolbar;
   /// Value of the solver iterations option
   OFSolverIterOption mOFSolverIterOption;
   /// The number of LastN solver iterations shown
   Integer mSolverIterLastN;
   /// The name of parent to sync time from, which may be empty
   std::string mTimeSyncParentName;
   /// The parent to sync time from
   OpenFramesInterface *mTimeSyncParentInterface;
   /// OpenGL Options
   OpenGLOptions mGLOptions;

   /// Used for coordinate conversions
   CoordinateConverter mCoordConverter;
   /// Used to compute Earth's ecliptic plane location
   CoordinateSystem *mMJ2000EcCoordSystem;
   /// Used as center of the star field
   CoordinateSystem *mMJ2000EqCoordSystem;

   /// A count of the propagator commands passed
   std::map<std::string, int> mProviderCount;
   /// The name of the currentProvider
   std::string mProviderName;
   /// Used to track how many times the Gmat::SOLVEDPASS is seen
   bool mSolverPassTracker;

   void ClearSpacePoints();
   bool AddSpacePoint(const std::string &name);
   void ClearViews();
   void ClearVectors();
   void ClearMasks();
   bool AddView(const std::string &name);
   bool AddVector(const std::string &name);
   bool AddMask(const std::string &name);

   // methods inhereted from Subscriber
   virtual bool Distribute(const Real *dat, Integer len);

   /// An emumeration of parameters for OpenFramesInterface, starting at the end
   /// of the @ref OrbitPlot::OrbitPlotParamCount enumeration.
   enum
   {
      ADD = SubscriberParamCount,    ///< Add spacepoint(s) to be plotted
      VIEW,                          ///< Add a view
      VECTOR,                        ///< Add a vector
      SENSOR_MASK,                   ///< Add a sensor mask
      COORD_SYSTEM,                  ///< Set the plotting coordinate system
      DRAW_OBJECT,                   ///< Enable drawing a spacepoint's object
      DRAW_TRAJECTORY,               ///< Enable drawing a spacepoint's trajecotry
      DRAW_AXES,                     ///< Enable drawing axes for a spacepoint
      DRAW_XY_PLANE,                 ///< Enable drawing XY plane for a spacepoint
      DRAW_LABEL,                    ///< Enable drawing name label for a spacepoint
      DRAW_LABEL_PROPAGATE,          ///< Label spacepoint with the propagator instead of spacecraft
      DRAW_CENTER_POINT,             ///< Enable drawing a spacecraft's center point
      DRAW_END_POINTS,               ///< Enable drawing a spacecraft's trajectory end points
      DRAW_VELOCITY,                 ///< Enable drawing a spacecraft's velocity
      DRAW_GRID,                     ///< Enable drawing a spacecraft's latitude and longitude
      DRAW_LINE_WIDTH,               ///< Sets the line width of a spacecraft's trajectory
      DRAW_MARKER_SIZE,              ///< Sets the marker size on a spacecraft's trajectory
      DRAW_FONT_SIZE,                ///< Sets the font size on a spacecraft's trajectory
      DRAW_MODEL_FILE,               ///< Sets the model file override for OpenFrames
      AXES,                          ///< Draw axes for the coordinate system
      AXES_LENGTH,                   ///< Scalar length of axes for the coordinate system
      AXES_LABELS,                   ///< Draw labels on each axes of the coordinate system
      FRAME_LABEL,                   ///< Draw name of the coordinate system
      XY_PLANE,                      ///< Enable drawing of the XY-Plane
      ECLIPTIC_PLANE,                ///< Enable drawing of Earth's ecliptic plane
      ENABLE_STARS,                  ///< Enable drawing of the star plane
      STAR_CATALOG,                  ///< File that contains the star catalog
      STAR_SETTINGS,                 ///< Choose preset or custom star settings
      STAR_COUNT,                    ///< Number of stars to load from catalog
      MIN_STAR_MAG,                  ///< Minimimum magnitude of stars
      MAX_STAR_MAG,                  ///< Maximimum magnitude of stars
      MIN_STAR_PIXELS,               ///< Minimimum size of stars in pixels
      MAX_STAR_PIXELS,               ///< Maximimum size of stars in pixels
      MIN_STAR_DIM_RATIO,            ///< Minimimum dim ratio of stars
      SHOW_PLOT,                     ///< Active or deactive plotting
      SHOW_TOOLBAR,                  ///< Active or deactive the toolbar
      SOLVER_ITER_LAST_N,            ///< Number of previous trajectories to draw
      SHOW_VR,                       ///< Enable VR output
      PLAYBACK_TIME_SCALE,           ///< Time scale of playback
      TIME_SYNC_PARENT,              ///< Parent interface to syncronize time with
      MULTISAMPLE_ANTI_ALIASING,     ///< Enables MSAA
      MSAA_SAMPLES,                  ///< Number of samples to use for MSAA
      DRAW_FONT_POSITION,            ///< Sets the font position on a spacecraft's trajectory
      OpenFramesInterfaceParamCount, ///< Count of the parameters for this class
   };

private:
   static const std::string
         PARAMETER_TEXT[OpenFramesInterfaceParamCount - SubscriberParamCount];
   static const Gmat::ParameterType
         PARAMETER_TYPE[OpenFramesInterfaceParamCount - SubscriberParamCount];
   static const std::string OF_SOLVER_ITER_OPTION_TEXT[OFSolverIterOptionCount];
   static StringArray gOFIterOptionStrings;

   static std::string StringArrayToString(const StringArray &array);
   static std::string BooleanToOnOff(bool onOff);

   /// Others that tried to sync time before our WindowProxy was created
   std::vector<OpenFrames::WindowProxy *> mUnregisteredTimeSyncChildren;

   bool AnyDataLabelsMatch(const std::string &name);
   bool AllDataLabelsMatch(const std::string &name, Integer &idX, Integer &idY,
      Integer &idZ, Integer &idVx, Integer &idVy, Integer &idVz);
   void AddPointToCelestialBodyTrajectories(const std::string &segmentName, Real timeInDays, bool isSolving);
   const Rvector CalculateENUAttitudeForBodyFixedPoint(Real timeInDays, BodyFixedPoint *bfp);
};

#endif
