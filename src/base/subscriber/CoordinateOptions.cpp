//$Id$
//------------------------------------------------------------------------------
//                                  CoordinateOptions
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 24, 2017
/**
 *  CoordinateOptions class
 *  Conveniently contains options related to the reference frame and time
 */
//------------------------------------------------------------------------------

#include "CoordinateOptions.hpp"

#include "GmatDefaults.hpp"

/**
 * Constructor
 *
 * @param name A name for the instance
 */
CoordinateOptions::CoordinateOptions() :
   mCoordSysName("EarthMJ2000Eq"),
   mEnableAxes(true),
   mEnableAxesLabels(true),
   mEnableFrameLabel(false),
   mEnableXYPlane(true),
   mEnableEclipticPlane(false),
   mPlaybackTimeScale(3600.0)
{
   mAxesLength = 2.0*GmatSolarSystemDefaults::PLANET_EQUATORIAL_RADIUS[GmatSolarSystemDefaults::EARTH];
}

/**
 * Destructor
 */
CoordinateOptions::~CoordinateOptions()
{
   // Nothing to do here
}


/**
 * Copy Constructor
 *
 * @param source The object to copy
 */
CoordinateOptions::CoordinateOptions(const CoordinateOptions &source) :
   mCoordSysName(source.GetCoordSysName()),
   mEnableAxes(source.GetEnableAxes()),
   mEnableAxesLabels(source.GetEnableAxesLabels()),
   mAxesLength(source.GetAxesLength()),
   mEnableFrameLabel(source.GetEnableFrameLabel()),
   mEnableXYPlane(source.GetEnableXYPlane()),
   mEnableEclipticPlane(source.GetEnableEclipticPlane()),
   mPlaybackTimeScale(source.GetPlaybackTimeScale())
{
   // Nothing else to do here
}


/**
 * Assignment operator
 *
 * @param rhs The object to copy
 */
CoordinateOptions &CoordinateOptions::operator=(const CoordinateOptions &rhs)
{
   mCoordSysName = rhs.GetCoordSysName();
   mEnableAxes = rhs.GetEnableAxes();
   mEnableAxesLabels = rhs.GetEnableAxesLabels();
   mAxesLength = rhs.GetAxesLength();
   mEnableFrameLabel = rhs.GetEnableFrameLabel();
   mEnableXYPlane = rhs.GetEnableXYPlane();
   mEnableEclipticPlane = rhs.GetEnableEclipticPlane();
   mPlaybackTimeScale = rhs.GetPlaybackTimeScale();
   return *this;
}


/**
 * Equality check
 *
 * @param options The options to check
 */
bool CoordinateOptions::EqualTo(const CoordinateOptions &options) const
{
   return ( mCoordSysName == options.GetCoordSysName()
      && mEnableAxes == options.GetEnableAxes()
      && mEnableAxesLabels == options.GetEnableAxesLabels()
      && mAxesLength == options.GetAxesLength()
      && mEnableFrameLabel == options.GetEnableFrameLabel()
      && mEnableXYPlane == options.GetEnableXYPlane()
      && mEnableEclipticPlane == options.GetEnableEclipticPlane()
      && mPlaybackTimeScale == options.GetPlaybackTimeScale() );
}
