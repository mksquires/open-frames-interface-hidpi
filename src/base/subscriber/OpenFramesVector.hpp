//$Id$
//------------------------------------------------------------------------------
//                                  OpenFramesVector
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Yasir Majeed Khan, Emergent Space Technologies, Inc.
// Created: October 4th, 2018
//
/**
*  @class OpenFramesVector
*  An interface for tracking configuration parameters associated with an
*  OpenFrames::Vector
*/
//------------------------------------------------------------------------------

#ifndef OPENFRAMESVECTOR_hpp
#define OPENFRAMESVECTOR_hpp

#include "OpenFramesInterface_defs.hpp"

#include "GmatBase.hpp"


class OpenFramesInterface_API OpenFramesVector : public GmatBase
{
public:
  static const std::string ON_STRING;
  static const std::string OFF_STRING;

  OpenFramesVector(const std::string &typeName, const std::string &name);

  virtual ~OpenFramesVector(void);

  OpenFramesVector(const OpenFramesVector &);
  OpenFramesVector& operator=(const OpenFramesVector&);

  // methods inhereted from GmatBase
  virtual bool         Validate();
  virtual bool         Initialize();

  virtual GmatBase*    Clone(void) const;
  virtual void         Copy(const GmatBase* orig);
  virtual bool         TakeAction(const std::string &action,
    const std::string &actionData);
	
  virtual bool         RenameRefObject(const UnsignedInt type,
    const std::string &oldName,
    const std::string &newName);

  // methods for parameters
  virtual std::string  GetParameterText(const Integer id) const;
  virtual Integer      GetParameterID(const std::string &str) const;
  virtual Gmat::ParameterType
    GetParameterType(const Integer id) const;
  virtual std::string  GetParameterTypeString(const Integer id) const;
  virtual bool         IsParameterReadOnly(const Integer id) const;
  virtual bool IsSquareBracketAllowedInSetting(const Integer id) const;

  virtual std::string  GetStringParameter(const Integer id) const;
  virtual bool         SetStringParameter(const Integer id, const std::string &value);
  virtual std::string  GetStringParameter(const std::string &label) const;
  virtual bool         SetStringParameter(const std::string &label,
    const std::string &value);

  virtual std::string  GetOnOffParameter(const Integer id) const;
  virtual bool         SetOnOffParameter(const Integer id,
    const std::string &value);
  virtual std::string  GetOnOffParameter(const std::string &label) const;
  virtual bool         SetOnOffParameter(const std::string &label,
    const std::string &value);

  virtual Real         GetRealParameter(const Integer id) const;
  virtual Real         GetRealParameter(const std::string &label) const;
  virtual Real         GetRealParameter(const Integer id,
    const Integer index) const;
  virtual Real         GetRealParameter(const std::string &label,
    const Integer index) const;
  virtual Real         SetRealParameter(const Integer id,
    const Real value);
  virtual Real         SetRealParameter(const std::string &label,
    const Real value);
  virtual Real         SetRealParameter(const Integer id, const Real value,
    const Integer index);
  virtual Real         SetRealParameter(const std::string &label,
    const Real value, const Integer index);

  virtual const Rvector&
    GetRvectorParameter(const Integer id) const;
  virtual const Rvector&
    GetRvectorParameter(const std::string &label) const;
  virtual const Rvector&
    SetRvectorParameter(const Integer id,
    const Rvector &value);
  virtual const Rvector&
    SetRvectorParameter(const std::string &label,
    const Rvector &value);

  virtual bool         IsParameterCloaked(const Integer id) const;
  virtual bool         IsParameterCloaked(const std::string &label) const;

  // for GUI population
  virtual UnsignedInt  GetPropertyObjectType(const Integer id) const;

  DEFAULT_TO_NO_CLONES

protected:

  /// The name of the vector
  std::string mVectorName;
  /// The color of the vector 
  std::string mVectorColor;
  /// The name of the source object
  std::string mSourceObject;
  /// The name of the vector type
  std::string mVectorType;
  /// The name of the destination object
  std::string mDestinationObject;
  /// Vector defines the start point (x,y,z) of Body-Fixed vector
  Rvector mBFStartPoint;
  /// Vector defines the direction (x,y,z) of Body-Fixed vector
  Rvector mBFDirection;
  /// The label of the vector
  std::string mVectorLabel;
  /// The vector length type
  std::string mVectorLengthType;
  /// The length of the vector
  Real mVectorLength;

  /// An emumeration of parameters for OpenFramesVector, starting at the end
  /// of the @ref OrbitPlot::OrbitPlotParamCount enumeration.
  enum
  {
    VECTOR_COLOR = GmatBaseParamCount, ///< The color of vector
    SOURCE_OBJECT,                     ///< Source of vector 
    VECTOR_TYPE,                       ///< Type of vector
    DESTINATION_OBJECT,                ///< Destination of vector 
    BF_START_POINT,                    ///< Rvector representing the Body-Fixed start point
    BF_DIRECTION,                      ///< Rvector representing the Body-Fixed direction
    VECTOR_LABEL,                      ///< The label of the vector 
    VECTOR_LENGTH_TYPE,                ///< The type of length of vector 
    VECTOR_LENGTH,                     ///< The length of vector 
    OpenFramesVectorParamCount,        ///< Count of the parameters for this class
  };

private:
  static std::string BooleanToOnOff(bool onOff);

  static const std::string
    PARAMETER_TEXT[OpenFramesVectorParamCount - GmatBaseParamCount];
  static const Gmat::ParameterType
    PARAMETER_TYPE[OpenFramesVectorParamCount - GmatBaseParamCount];
};

#endif
