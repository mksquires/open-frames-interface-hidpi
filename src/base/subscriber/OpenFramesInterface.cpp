//$Id$
//------------------------------------------------------------------------------
//                                  OpenFramesInterface
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http ://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Ravi Mathur, Emergent Space Technologies, Inc.
// Created: December 1, 2015
/**
 *  Demonstrates the ability to create an OpenFrames window within GMAT
 *  and display a scene with user interactivity.
 */
//------------------------------------------------------------------------------

#include "OFGLCanvas.hpp"
#include "OFScene.hpp"
#include "OFWindowProxyPanel.hpp"
#include "OpenFramesInterface.hpp"
#include "OpenFramesView.hpp"
#include "OpenFramesVector.hpp"
#include "OpenFramesSensorMask.hpp"
#include "TrajectoryDealer.hpp"
#include "OFStarListener.hpp"
#include "OFEclipticPlane.hpp"
#include "openframes.xpm"

#include "MessageInterface.hpp"
#include "Parameter.hpp"
#include "SubscriberException.hpp"
#include "StringUtil.hpp"
#include "CoordinateConverter.hpp"
#include "SpacePoint.hpp"
#include "TextParser.hpp"
#include "FileManager.hpp"
#include "AttitudeConversionUtility.hpp"
#include "TimeSystemConverter.hpp"
#include "Spacecraft.hpp"
#include "BodyFixedPoint.hpp"
#include "GmatCommand.hpp"
#include "RgbColor.hpp"


/// Names of parameters for OpenFramesInterface
/// IMPORTANT: These must be in the same order as the OpenFramesInterface parameter enumeration
const std::string
      OpenFramesInterface::PARAMETER_TEXT[OpenFramesInterfaceParamCount - SubscriberParamCount] =
{
   "Add",
   "View",
   "Vector",
   "SensorMask",
   "CoordinateSystem",
   "DrawObject",
   "DrawTrajectory",
   "DrawAxes",
   "DrawXYPlane",
   "DrawLabel",
   "DrawUsePropLabel",
   "DrawCenterPoint",
   "DrawEndPoints",
   "DrawVelocity",
   "DrawGrid",
   "DrawLineWidth",
   "DrawMarkerSize",
   "DrawFontSize",
   "DrawModelFile",
   "Axes",
   "AxesLength",
   "AxesLabels",
   "FrameLabel",
   "XYPlane",
   "EclipticPlane",
   "EnableStars",
   "StarCatalog",
   "StarSettings",
   "StarCount",
   "MinStarMag",
   "MaxStarMag",
   "MinStarPixels",
   "MaxStarPixels",
   "MinStarDimRatio",
   "ShowPlot",
   "ShowToolbar",
   "SolverIterLastN",
   "ShowVR",
   "PlaybackTimeScale",
   "TimeSyncParent",
   "MultisampleAntiAliasing",
   "MSAASamples",
   "DrawFontPosition"
};


/// Gmat interpreter types for the parameters in @ref OpenFramesInterface::PARAMETER_TEXT
const Gmat::ParameterType
      OpenFramesInterface::PARAMETER_TYPE[OpenFramesInterfaceParamCount - SubscriberParamCount] =
{
   Gmat::OBJECTARRAY_TYPE,       //"Add"
   Gmat::OBJECTARRAY_TYPE,       //"View"
   Gmat::OBJECTARRAY_TYPE,       //"Vector"
   Gmat::OBJECTARRAY_TYPE,       //"SensorMask"
   Gmat::OBJECT_TYPE,            //"CoordinateSystem"
   Gmat::BOOLEANARRAY_TYPE,      //"DrawObject"
   Gmat::BOOLEANARRAY_TYPE,      //"DrawTrajectory"
   Gmat::BOOLEANARRAY_TYPE,      //"DrawAxes"
   Gmat::BOOLEANARRAY_TYPE,      //"DrawXYPlane"
   Gmat::BOOLEANARRAY_TYPE,      //"DrawLabel"
   Gmat::BOOLEANARRAY_TYPE,      //"DrawUsePropLabel"
   Gmat::BOOLEANARRAY_TYPE,      //"DrawCenterPoint"
   Gmat::BOOLEANARRAY_TYPE,      //"DrawEndPoints"
   Gmat::BOOLEANARRAY_TYPE,      //"DrawVelocity"
   Gmat::BOOLEANARRAY_TYPE,      //"DrawGrid"
   Gmat::RVECTOR_TYPE,           //"DrawLineWidth"
   Gmat::UNSIGNED_INTARRAY_TYPE, //"DrawMarkerSize"
   Gmat::UNSIGNED_INTARRAY_TYPE, //"DrawFontSize"
   Gmat::STRINGARRAY_TYPE,       //"DrawModelFile"
   Gmat::ON_OFF_TYPE,            //"Axes"
   Gmat::REAL_TYPE,              //"AxesLength"
   Gmat::ON_OFF_TYPE,            //"AxesLabels"
   Gmat::ON_OFF_TYPE,            //"FrameLabel"
   Gmat::ON_OFF_TYPE,            //"XYPlane"
   Gmat::ON_OFF_TYPE,            //"EclipticPlane"
   Gmat::ON_OFF_TYPE,            //"EnableStars"
   Gmat::STRING_TYPE,            //"StarCatalog"
   Gmat::ENUMERATION_TYPE,       //"StarSettings"
   Gmat::INTEGER_TYPE,           //"StarCount"
   Gmat::REAL_TYPE,              //"MinStarMag"
   Gmat::REAL_TYPE,              //"MaxStarMag"
   Gmat::REAL_TYPE,              //"MinStarPixels"
   Gmat::REAL_TYPE,              //"MaxStarPixels"
   Gmat::REAL_TYPE,              //"MinStarDimRatio"
   Gmat::BOOLEAN_TYPE,           //"ShowPlot"
   Gmat::BOOLEAN_TYPE,           //"ShowToolbar"
   Gmat::INTEGER_TYPE,           //"SolverIterLastN"
   Gmat::BOOLEAN_TYPE,           //"ShowVR"
   Gmat::REAL_TYPE,              //"PlaybackTimeScale"
   Gmat::OBJECT_TYPE,            //"TimeSyncParent"
   Gmat::ON_OFF_TYPE,            //"PlaybackTimeScale"
   Gmat::INTEGER_TYPE,           //"TimeSyncParent"
   Gmat::STRINGARRAY_TYPE,       //"DrawFontPosition"
};

/// String that represents On
const std::string OpenFramesInterface::ON_STRING = "On";


/// String that represents OFF
const std::string OpenFramesInterface::OFF_STRING = "Off";


/// Strings for the star settings
const std::string OpenFramesInterface::STAR_SETTING_STRINGS[StarSettingsOptionCount] =
{
   "Monitor", "Printer", "Virtual Reality", "Custom"
};


/// The index of our icon if provided by resource tree
Integer OpenFramesInterface::ICON_INDEX = -1;

/// Strings that describe the OFSolverIterOption enumeration values
const std::string
OpenFramesInterface::OF_SOLVER_ITER_OPTION_TEXT[OFSolverIterOptionCount] =
{
   "All", "LastN", "Current", "None",
};


/// OF_SOLVER_ITER_OPTION_TEXT elements pushed into an std::vector
StringArray OpenFramesInterface::gOFIterOptionStrings;


//------------------------------------------------------------------------------
// OpenFramesInterface(const std::string &type, const std::string &name)
//------------------------------------------------------------------------------
/**
 * Constructor
 *
 * @param <type> Typename
 * @param <name> Name of the object
 */
//------------------------------------------------------------------------------
OpenFramesInterface::OpenFramesInterface(const std::string &type, const std::string &name) :
   Subscriber(type, name),
   mExecutionWidget(nullptr),
   mViewCoordSystem(nullptr),
   mDrawLineWidthVector(0),
   mDrawMarkerSizeVector(0),
   mDrawFontSizeVector(0),
   mDrawModelFileCounter(0U),
   mDrawFontPositionCounter(0U),
   mFrameOptions(),
   mStarSettings(STARS_MONITOR),
   mCustomStarOptions(StarOptions::MONITOR),
   mShowToolbar(true),
   mOFSolverIterOption(OF_SI_ALL),
   mSolverIterLastN(1),
   mMJ2000EcCoordSystem(nullptr),
   mMJ2000EqCoordSystem(nullptr),
   mSolverPassTracker(false),
   mTimeSyncParentInterface(nullptr),
   mGLOptions()
{
   objectTypes.push_back(GmatType::GetTypeId("OpenFramesInterface"));
   objectTypeNames.push_back("OpenFramesInterface");

   parameterCount = OpenFramesInterfaceParamCount;

   if (gOFIterOptionStrings.size() == 0)
   {
      for (int i = 0; i < OFSolverIterOptionCount; i++)
         gOFIterOptionStrings.push_back(OF_SOLVER_ITER_OPTION_TEXT[i]);
   }
}


//------------------------------------------------------------------------------
// ~OpenFramesInterface(void)
//------------------------------------------------------------------------------
/**
 * Destructor
 */
//------------------------------------------------------------------------------
OpenFramesInterface::~OpenFramesInterface(void)
{
   if (mExecutionWidget != nullptr)
   {
      mExecutionWidget->RemoveObject(this);
      // mExecutionWidget set to nullptr by OFWindowProxyPanel::RemoveObject
   }

   if (mMJ2000EcCoordSystem != nullptr)
   {
      delete mMJ2000EcCoordSystem;
      mMJ2000EcCoordSystem = nullptr;
   }

   if (mMJ2000EqCoordSystem != nullptr)
   {
      delete mMJ2000EqCoordSystem;
      mMJ2000EqCoordSystem = nullptr;
   }
}


//------------------------------------------------------------------------------
// OpenFramesInterface(const OpenFramesInterface &rf)
//------------------------------------------------------------------------------
/**
 * Copy Constructor
 */
//------------------------------------------------------------------------------
OpenFramesInterface::OpenFramesInterface(const OpenFramesInterface &dc) :
   Subscriber(dc),
   mMJ2000EcCoordSystem(nullptr),
   mMJ2000EqCoordSystem(nullptr),
   mSolverPassTracker(false)
{
   mExecutionWidget = nullptr;

   mAllSpNameArray = dc.mAllSpNameArray;
   mSpacePoints = dc.mSpacePoints;
   mViewCoordSystem = dc.mViewCoordSystem; // copy configured object pointer
   mViewNames = dc.mViewNames;
   mViews = dc.mViews;
   mVectorNames = dc.mVectorNames;
   mVectors = dc.mVectors;
   mMaskNames = dc.mMaskNames;
   mMasks = dc.mMasks;
   mDrawObjectArray = dc.mDrawObjectArray;
   mDrawTrajectoryArray = dc.mDrawTrajectoryArray;
   mDrawAxesArray = dc.mDrawAxesArray;
   mDrawXYPlaneArray = dc.mDrawXYPlaneArray;
   mDrawLabelArray = dc.mDrawLabelArray;
   mDrawLabelPropagateArray = dc.mDrawLabelPropagateArray;
   mDrawCenterArray = dc.mDrawCenterArray;
   mDrawEndsArray = dc.mDrawEndsArray;
   mDrawVelocityArray = dc.mDrawVelocityArray;
   mDrawGridArray = dc.mDrawGridArray;
   mDrawLineWidthVector = dc.mDrawLineWidthVector;
   mDrawMarkerSizeVector = dc.mDrawMarkerSizeVector;
   mDrawFontSizeVector = dc.mDrawFontSizeVector;
   mDrawModelFileVector = dc.mDrawModelFileVector;
   mDrawModelFileCounter = dc.mDrawModelFileCounter;
   mDrawFontPositionVector = dc.mDrawFontPositionVector;
   mDrawFontPositionCounter = dc.mDrawFontPositionCounter;
   mFrameOptions = dc.mFrameOptions;
   mStarSettings = dc.mStarSettings;
   mStarSettingsString = dc.mStarSettingsString;
   mCustomStarOptions = dc.mCustomStarOptions;
   mShowToolbar = dc.mShowToolbar;
   mOFSolverIterOption = dc.mOFSolverIterOption;
   mSolverIterLastN = dc.mSolverIterLastN;
   mProviderCount = dc.mProviderCount;
   mTimeSyncParentName = dc.mTimeSyncParentName;
   mTimeSyncParentInterface = dc.mTimeSyncParentInterface;
   mGLOptions = dc.mGLOptions;

   parameterCount = OpenFramesInterfaceParamCount;
}


//------------------------------------------------------------------------------
// OpenFramesInterface& OpenFramesInterface::operator=(const OpenFramesInterface& dc)
//------------------------------------------------------------------------------
/**
 * Assignment operator
 */
//------------------------------------------------------------------------------
OpenFramesInterface& OpenFramesInterface::operator=(const OpenFramesInterface& dc)
{
   if (this == &dc) {
      return *this;
   }

   Subscriber::operator=(dc);

   mExecutionWidget = nullptr;

   mAllSpNameArray = dc.mAllSpNameArray;
   mSpacePoints = dc.mSpacePoints;
   mViewCoordSystem = dc.mViewCoordSystem; // copy configured object pointer
   mViewNames = dc.mViewNames;
   mViews = dc.mViews;
   mVectorNames = dc.mVectorNames;
   mVectors = dc.mVectors;
   mMaskNames = dc.mMaskNames;
   mMasks = dc.mMasks;
   mDrawObjectArray = dc.mDrawObjectArray;
   mDrawTrajectoryArray = dc.mDrawTrajectoryArray;
   mDrawAxesArray = dc.mDrawAxesArray;
   mDrawXYPlaneArray = dc.mDrawXYPlaneArray;
   mDrawLabelArray = dc.mDrawLabelArray;
   mDrawLabelPropagateArray = dc.mDrawLabelPropagateArray;
   mDrawCenterArray = dc.mDrawCenterArray;
   mDrawEndsArray = dc.mDrawEndsArray;
   mDrawVelocityArray = dc.mDrawVelocityArray;
   mDrawGridArray = dc.mDrawGridArray;
   mDrawLineWidthVector = dc.mDrawLineWidthVector;
   mDrawMarkerSizeVector = dc.mDrawMarkerSizeVector;
   mDrawFontSizeVector = dc.mDrawFontSizeVector;
   mDrawModelFileVector = dc.mDrawModelFileVector;
   mDrawModelFileCounter = dc.mDrawModelFileCounter;
   mDrawFontPositionVector = dc.mDrawFontPositionVector;
   mDrawFontPositionCounter = dc.mDrawFontPositionCounter;
   mFrameOptions = dc.mFrameOptions;
   mStarSettings = dc.mStarSettings;
   mStarSettingsString = dc.mStarSettingsString;
   mCustomStarOptions = dc.mCustomStarOptions;
   mShowToolbar = dc.mShowToolbar;
   mOFSolverIterOption = dc.mOFSolverIterOption;
   mSolverIterLastN = dc.mSolverIterLastN;
   mProviderCount = dc.mProviderCount;
   mTimeSyncParentName = dc.mTimeSyncParentName;
   mTimeSyncParentInterface = dc.mTimeSyncParentInterface;
   mGLOptions = dc.mGLOptions;

   return *this;
}


const StarOptions& OpenFramesInterface::GetStarOptions() const
{
   const StarOptions *starOptions = &mCustomStarOptions;
   if (starOptions->GetEnableStars())
   {
      if (mStarSettings == STARS_MONITOR)
         starOptions = &StarOptions::MONITOR;
      else if (mStarSettings == STARS_VR)
         starOptions = &StarOptions::VIRTUAL_REALITY;
   }
   else
      starOptions = &StarOptions::DISABLED;
   if (mStarSettings == STARS_PRINTER)
      starOptions = &StarOptions::PRINTER;

   return *starOptions;
}


//------------------------------------------------------------------------------
// bool GetObjectBool(const std::string &property, const std::string &name)
//------------------------------------------------------------------------------
/**
 * Gets boolean parameter of a configured spacepoint object
 *
 * @param <property> The name of the parameter
 * @param <name> The name of the spacepoint
 *
 * @return The value of the parameter
 */
bool OpenFramesInterface::GetObjectBool(const std::string &property, const std::string &name)
{
   bool draw = true;
   for (std::vector<SpacePointOptions>::iterator spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
   {
      if (spo->IsNameSameAs(name))
      {
         Integer parameter = GetParameterID(property);
         if (parameter == DRAW_OBJECT)
            draw = spo->GetDrawObject();
         else if (parameter == DRAW_TRAJECTORY)
            draw = spo->GetDrawTrajectory();
         else if (parameter == DRAW_AXES)
            draw = spo->GetDrawAxes();
         else if (parameter == DRAW_XY_PLANE)
            draw = spo->GetDrawXYPlane();
         else if (parameter == DRAW_LABEL)
            draw = spo->GetDrawLabel();
         else if (parameter == DRAW_LABEL_PROPAGATE)
            draw = spo->GetUsePropagateLabel();
         else if (parameter == DRAW_CENTER_POINT)
            draw = spo->GetDrawCenter();
         else if (parameter == DRAW_END_POINTS)
            draw = spo->GetDrawEnds();
         else if (parameter == DRAW_VELOCITY)
            draw = spo->GetDrawVelocity();
         else if (parameter == DRAW_GRID)
            draw = spo->GetDrawGrid();
         else
            draw = true;
      }
   }
   return draw;
}


//------------------------------------------------------------------------------
// void SetObjectBool(const std::string &property, const std::string &name, bool value)
//------------------------------------------------------------------------------
/**
 * Sets boolean parameter of a configured spacepoint object
 *
 * @param <property> The name of the parameter
 * @param <name> The name of the spacepoint
 * @param <value> The value of the parameter
 */
void OpenFramesInterface::SetObjectBool(const std::string &property, const std::string &name, bool value)
{
   for (int ii = 0; ii < mSpacePoints.size(); ii++)
   {
      if (mSpacePoints[ii].IsNameSameAs(name))
      {
         Integer parameter = GetParameterID(property);
         if (parameter == DRAW_OBJECT)
         {
            mSpacePoints[ii].SetDrawObject(value);
            mDrawObjectArray[ii] = value;
         }
         else if (parameter == DRAW_TRAJECTORY)
         {
            mSpacePoints[ii].SetDrawTrajectory(value);
            mDrawTrajectoryArray[ii] = value;
         }
         else if (parameter == DRAW_AXES)
         {
            mSpacePoints[ii].SetDrawAxes(value);
            mDrawAxesArray[ii] = value;
         }
         else if (parameter == DRAW_XY_PLANE)
         {
            mSpacePoints[ii].SetDrawXYPlane(value);
            mDrawXYPlaneArray[ii] = value;
         }
         else if (parameter == DRAW_LABEL)
         {
            mSpacePoints[ii].SetDrawLabel(value);
            mDrawLabelArray[ii] = value;
         }
         else if (parameter == DRAW_LABEL_PROPAGATE)
         {
            mSpacePoints[ii].SetUsePropagateLabel(value);
            mDrawLabelPropagateArray[ii] = value;
         }
         else if (parameter == DRAW_CENTER_POINT)
         {
            mSpacePoints[ii].SetDrawCenter(value);
            mDrawCenterArray[ii] = value;
         }
         else if (parameter == DRAW_END_POINTS)
         {
            mSpacePoints[ii].SetDrawEnds(value);
            mDrawEndsArray[ii] = value;
         }
         else if (parameter == DRAW_VELOCITY)
         {
            mSpacePoints[ii].SetDrawVelocity(value);
            mDrawVelocityArray[ii] = value;
         }
         else if (parameter == DRAW_GRID)
         {
            mSpacePoints[ii].SetDrawGrid(value);
            mDrawGridArray[ii] = value;
         }
      }
   }
}


//------------------------------------------------------------------------------
// Real OpenFramesInterface::GetObjectReal(const std::string &property, const std::string &name)
//------------------------------------------------------------------------------
/**
 * Gets Real parameter of a configured spacepoint object
 *
 * @param <property> The name of the parameter
 * @param <name> The name of the spacepoint
 *
 * @return The value of the parameter
 */
Real OpenFramesInterface::GetObjectReal(const std::string &property, const std::string &name)
{
   Real draw = 0.0;
   for (std::vector<SpacePointOptions>::iterator spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
   {
      if (spo->IsNameSameAs(name))
      {
         if (property == "DrawLineWidth")
            draw = spo->GetDrawLineWidth();
      }
   }
   return draw;
}


//------------------------------------------------------------------------------
// void SetObjectReal(const std::string &property, const std::string &name, Real value)
//------------------------------------------------------------------------------
/**
 * Sets Real parameter of a configured spacepoint object
 *
 * @param <property> The name of the parameter
 * @param <name> The name of the spacepoint
 * @param <value> The value of the parameter
 */
void OpenFramesInterface::SetObjectReal(const std::string &property, const std::string &name, Real value)
{
   for (int ii = 0; ii < mSpacePoints.size(); ii++)
   {
      if (mSpacePoints[ii].IsNameSameAs(name))
      {
         if (property == "DrawLineWidth")
         {
            mSpacePoints[ii].SetDrawLineWidth(value);
            mDrawLineWidthVector[ii] = value;
         }
      }
   }
}


//------------------------------------------------------------------------------
// UnsignedInt OpenFramesInterface::GetObjectUInt(const std::string &property, const std::string &name)
//------------------------------------------------------------------------------
/**
 * Gets UnsignedInt parameter of a configured spacepoint object
 *
 * @param <property> The name of the parameter
 * @param <name> The name of the spacepoint
 *
 * @return The value of the parameter
 */
UnsignedInt OpenFramesInterface::GetObjectUInt(const std::string &property, const std::string &name)
{
   UnsignedInt draw = 0;
   for (std::vector<SpacePointOptions>::iterator spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
   {
      if (spo->IsNameSameAs(name))
      {
         if (property == "DrawMarkerSize")
            draw = spo->GetDrawMarkerSize();
         if (property == "DrawFontSize")
           draw = spo->GetDrawFontSize();
      }
   }
   return draw;
}


//------------------------------------------------------------------------------
// std::string OpenFramesInterface::GetObjectString(const std::string &property, const std::string &name)
//------------------------------------------------------------------------------
/**
* Gets std::string parameter of a configured spacepoint object
*
* @param <property> The name of the parameter
* @param <name> The name of the spacepoint
*
* @return The value of the parameter
*/
std::string OpenFramesInterface::GetObjectString(const std::string &property, const std::string &name)
{
  std::string draw = "";
  for (std::vector<SpacePointOptions>::iterator spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
  {
    if (spo->IsNameSameAs(name))
    {
      if (property == "DrawFontPosition")
        draw = spo->GetDrawFontPosition();   
    }
  }
  return draw;
}


//------------------------------------------------------------------------------
// void SetObjectUInt(const std::string &property, const std::string &name, UnsignedInt value)
//------------------------------------------------------------------------------
/**
 * Sets UnsignedInt parameter of a configured spacepoint object
 *
 * @param <property> The name of the parameter
 * @param <name> The name of the spacepoint
 * @param <value> The value of the parameter
 */
void OpenFramesInterface::SetObjectUInt(const std::string &property, const std::string &name, UnsignedInt value)
{
   for (int ii = 0; ii < mSpacePoints.size(); ii++)
   {
      if (mSpacePoints[ii].IsNameSameAs(name))
      {
         if (property == "DrawMarkerSize")
         {
            mSpacePoints[ii].SetDrawMarkerSize(value);
            mDrawMarkerSizeVector[ii] = value;
         }
         else if (property == "DrawFontSize")
         {
           mSpacePoints[ii].SetDrawFontSize(value);
           mDrawFontSizeVector[ii] = value;
         }
      }
   }
}

//------------------------------------------------------------------------------
// void SetObjectString(const std::string &property, const std::string &name, const std::string &value)
//------------------------------------------------------------------------------
/**
* Sets std::string parameter of a configured spacepoint object
*
* @param <property> The name of the parameter
* @param <name> The name of the spacepoint
* @param <value> The value of the parameter
*/
void OpenFramesInterface::SetObjectString(const std::string &property, const std::string &name, const std::string &value)
{
  for (int ii = 0; ii < mSpacePoints.size(); ii++)
  {
    if (mSpacePoints[ii].IsNameSameAs(name))
    {
      if (property == "DrawFontPosition")
      {
        mSpacePoints[ii].SetDrawFontPosition(value);
        mDrawFontPositionVector[ii] = value;
      }
 
    }
  }
}



//------------------------------------------------------------------------------
//  bool Validate()
//------------------------------------------------------------------------------
/**
 * Performs any pre-run validation that the object needs
 *
 * @return true unless validation fails
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::Validate()
{
   // Subscriber does not implement a Validate()
   return GmatBase::Validate();
}


//------------------------------------------------------------------------------
//  GmatBase* Clone(void) const
//------------------------------------------------------------------------------
/**
 * This method returns a clone of the OpenFramesInterface.
 *
 * @return clone of the OpenFramesInterface.
 */
//------------------------------------------------------------------------------
GmatBase* OpenFramesInterface::Clone() const
{
   return (new OpenFramesInterface(*this));
}


//---------------------------------------------------------------------------
// void Copy(const GmatBase* orig)
//---------------------------------------------------------------------------
/**
 * Sets this object to match another one
 *
 * @param orig The original that is being copied
 */
//---------------------------------------------------------------------------
void OpenFramesInterface::Copy(const GmatBase* orig)
{
   operator=(*((OpenFramesInterface *)(orig)));
}


//------------------------------------------------------------------------------
// virtual bool TakeAction(const std::string &action,
//                         const std::string &actionData = "");
//------------------------------------------------------------------------------
/**
 * This method performs action.
 *
 * @param <action> action to perform
 * @param <actionData> action data associated with action
 * @return true if action successfully performed
 *
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::TakeAction(const std::string &action,
                                     const std::string &actionData)
{
   if (action == "Clear")
   {
      ClearSpacePoints();
      return true;
   }
   else if (action == "PenUp")
   {
      active = false; // Treat PenUp the same as Toggle Off by disabling the Subscriber::active flag
      return true;
   }
   else if (action == "PenDown")
   {  
      active = true; // Treat PenDown the same as Toggle On by enabling the Subscriber::active flag
      return true;
   }

   return false;
}


//------------------------------------------------------------------------------
// bool HasGuiPlugin()
//------------------------------------------------------------------------------
/**
 * Checks an object to see if there is a matching GUI element in plugin code
 *
 * @return true if there is a GUI factory and plugin, false (the default) if not
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::HasGuiPlugin()
{
   return true;
}


//------------------------------------------------------------------------------
// StringArray GetGuiPanelNames(const std::string &type)
//------------------------------------------------------------------------------
/**
 * Gets names of GUI panels to be launched in the specified situation
 *
 * @param <type> The situation to launch GUI panels for
 *
 * @return The GUI panels to launch
 */
StringArray OpenFramesInterface::GetGuiPanelNames(const std::string &type)
{
   StringArray panels;

   if ((type == "Configuration") || (type == ""))
   {
      panels.push_back("OFConfigurationPanel");
   }
   if ((type == "Execution") || (type == ""))
   {
      panels.push_back("OFWindowProxyPanel");
   }

   return panels;
}


//------------------------------------------------------------------------------
// void GetWidget(GmatWidget* widget)
//------------------------------------------------------------------------------
/**
 * Gets the widget that will draw the OpenFrames scene
 */
OFWindowProxyPanel *OpenFramesInterface::GetWidget()
{
   return mExecutionWidget;
}


//------------------------------------------------------------------------------
// void SetWidget(GmatWidget* widget)
//------------------------------------------------------------------------------
/**
 * Sets the widget that will draw the OpenFrames scene
 *
 * @param <widget> An OFWindowProxyPanel widget for the OpenFrames scene
 */
void OpenFramesInterface::SetWidget(GmatWidget* widget)
{
   if (mExecutionWidget != nullptr && mExecutionWidget != widget->GetWidget())
   {
      mExecutionWidget->RemoveObject(this);
      // mExecutionWidget set to nullptr by OFWindowProxyPanel::RemoveObject
   }
   mExecutionWidget = static_cast<OFWindowProxyPanel *>(widget->GetWidget());
   mExecutionWidget->SetObject(this);
}


//------------------------------------------------------------------------------
// void RemoveWidget
//------------------------------------------------------------------------------
/**
 * Removes the widget that contains the OpenFrames scene
 */
void OpenFramesInterface::RemoveWidget()
{
   // Do not attempt mExecutionWidget->RemoveObject(this) here because this
   // function is called within OFWindowProxyPanel::RemoveObject().
   // If it is called, then we will end up in a recursive loop.
   mExecutionWidget = nullptr;
}


//------------------------------------------------------------------------------
// const char** GetIcon()
//------------------------------------------------------------------------------
/**
 * Retrieves the icon associated with the object (if any)
 *
 * @return The xpm based icon strign
 */
//------------------------------------------------------------------------------
const char** OpenFramesInterface::GetIcon()
{
   return openframes_xpm;
}

//------------------------------------------------------------------------------
// void SetIconIndex(Integer index)
//------------------------------------------------------------------------------
/**
 * Sets the index for icons used in GUI components
 *
 * The default method here does nothing but define the interface
 *
 * @param index The index value
 */
//------------------------------------------------------------------------------
void OpenFramesInterface::SetIconIndex(Integer index)
{
   ICON_INDEX = index;
}


//------------------------------------------------------------------------------
// Integer GetIconIndex()
//------------------------------------------------------------------------------
/**
 * Returns index of an icon, used in tree structures that handle icons by index
 *
 * Tree widgets can handle icons using an indexed list.  This method provides
 * a mechanism to access such icons rather than adding a new copy to the tree
 * each time the icon is needed.
 *
 * Classes that use this mechism should set the index as a class (static) member
 * so that all objects created from the class access the same icon instance.
 *
 * @return The icon index, or -1 if the icon does not exist or has not set an
 * index.
 */
//------------------------------------------------------------------------------
Integer OpenFramesInterface::GetIconIndex()
{
   return ICON_INDEX;
}


//---------------------------------------------------------------------------
//  bool RenameRefObject(const UnsignedInt type,
//                       const std::string &oldName, const std::string &newName)
//---------------------------------------------------------------------------
/**
 * @copydoc GmatBase::RenameRefObject()
 */
bool OpenFramesInterface::RenameRefObject(const UnsignedInt type,
                                          const std::string &oldName,
                                          const std::string &newName)
{
   bool success = true;

   if (type == Gmat::SPACECRAFT || type == Gmat::GROUND_STATION ||
      type == Gmat::CALCULATED_POINT)
   {
      // for spacecraft name
      for (int i=0; i < mAllSpNameArray.size(); i++)
      {
         if (mAllSpNameArray[i] == oldName)
         {
            mAllSpNameArray[i] = newName;
         }
      }

      // also rename in spacepoint options array
      for (std::vector<SpacePointOptions>::iterator spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
      {
         if (spo->IsNameSameAs(oldName))
         {
            spo->SetName(newName);
         }
      }
   }
   else if (type == Gmat::COORDINATE_SYSTEM)
   {
      if (mFrameOptions.GetCoordSysName() == oldName)
      {
         mFrameOptions.SetCoordSysName(newName);
      }
   }
   else if (type == GmatType::GetTypeId("OpenFramesView"))
   {
      for (StringArray::iterator viewName = mViewNames.begin(); viewName < mViewNames.end(); viewName++)
      {
         if (*viewName == oldName)
         {
            *viewName = newName;
         }
      }
   }
   else if (type == GmatType::GetTypeId("OpenFramesVector"))
   {
     for (StringArray::iterator vectorName = mVectorNames.begin(); vectorName < mVectorNames.end(); vectorName++)
     {
       if (*vectorName == oldName)
       {
         *vectorName = newName;
       }
     }
   }
   else if (type == GmatType::GetTypeId("OpenFramesSensorMask"))
   {
     for (StringArray::iterator maskName = mMaskNames.begin(); maskName < mMaskNames.end(); maskName++)
     {
       if (*maskName == oldName)
       {
         *maskName = newName;
       }
     }
   }
   else if (type == Gmat::SUBSCRIBER)
   {
      if (mTimeSyncParentName == oldName)
      {
         mTimeSyncParentName = newName;
      }
   }
   else
   {
      success = Subscriber::RenameRefObject(type, oldName, newName);
      if (!success)
      {
         success = GmatBase::RenameRefObject(type, oldName, newName);
      }
   }

   return success;
}


//------------------------------------------------------------------------------
// std::string GetParameterText(const Integer id) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetParameterText()
 */
std::string OpenFramesInterface::GetParameterText(const Integer id) const
{
   if (id >= SubscriberParamCount && id < OpenFramesInterfaceParamCount)
      return PARAMETER_TEXT[id - SubscriberParamCount];
   else
      return Subscriber::GetParameterText(id);

}


//------------------------------------------------------------------------------
// Integer GetParameterID(const std::string &str) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetParameterID()
 */
Integer OpenFramesInterface::GetParameterID(const std::string &str) const
{
   for (int i = SubscriberParamCount; i < OpenFramesInterfaceParamCount; i++)
   {
      if (str == PARAMETER_TEXT[i - SubscriberParamCount])
         return i;
   }

   return Subscriber::GetParameterID(str);
}


//------------------------------------------------------------------------------
// Gmat::ParameterType GetParameterType(const Integer id) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetParameterType()
 */
Gmat::ParameterType OpenFramesInterface::GetParameterType(const Integer id) const
{
   if (id >= SubscriberParamCount && id < OpenFramesInterfaceParamCount)
      return PARAMETER_TYPE[id - SubscriberParamCount];
   else
      return Subscriber::GetParameterType(id);
}


//------------------------------------------------------------------------------
// std::string GetParameterTypeString(const Integer id) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetParameterTypeString()
 */
std::string OpenFramesInterface::GetParameterTypeString(const Integer id) const
{
   return GmatBase::PARAM_TYPE_STRING[GetParameterType(id)];
}


//---------------------------------------------------------------------------
//  bool IsParameterReadOnly(const Integer id) const
//---------------------------------------------------------------------------
/**
 * @copydoc GmatBase::IsParameterReadOnly()
 */
bool OpenFramesInterface::IsParameterReadOnly(const Integer id) const
{
   return Subscriber::IsParameterReadOnly(id);
}


//------------------------------------------------------------------------------
// std::string GetStringParameter(const Integer id) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetStringParameter()
 */
std::string OpenFramesInterface::GetStringParameter(const Integer id) const
{
   switch (id)
   {
   case ADD:
   {
      Integer objCount = mSpacePoints.size();
      std::string objList = "{ ";
      for (Integer i = 0; i < objCount; i++)
      {
         if (i == objCount - 1)
            objList += mSpacePoints[i].GetName();
         else
            objList += mSpacePoints[i].GetName() + ", ";
      }
      objList += " }";
      return objList;
   }
   case VIEW:
      return StringArrayToString(mViewNames);
   case VECTOR:
      return StringArrayToString(mVectorNames);
   case SENSOR_MASK:
      return StringArrayToString(mMaskNames);
   case DRAW_MODEL_FILE:
      return StringArrayToString(mDrawModelFileVector);
   case DRAW_FONT_POSITION:
     return StringArrayToString(mDrawFontPositionVector);
   case COORD_SYSTEM:
      return mFrameOptions.GetCoordSysName();
   case STAR_CATALOG:
      return mCustomStarOptions.GetStarCatalog();
   case STAR_SETTINGS:
      return mStarSettingsString;
   case TIME_SYNC_PARENT:
      return mTimeSyncParentName;
   default:
      return Subscriber::GetStringParameter(id);
   }
}


//------------------------------------------------------------------------------
// std::string GetStringParameter(const std::string &label) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetStringParameter()
 */
std::string OpenFramesInterface::GetStringParameter(const std::string &label) const
{
   return GetStringParameter(GetParameterID(label));
}


//------------------------------------------------------------------------------
// bool SetStringParameter(const Integer id, const std::string &value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetStringParameter()
 */
bool OpenFramesInterface::SetStringParameter(const Integer id, const std::string &value)
{
   switch (id)
   {
   case COORD_SYSTEM:
   {
      mFrameOptions.SetCoordSysName(value);
      return true;
   }
   case ADD:
   {
      if (value[0] == '{')
      {
         try
         {
            TextParser tp;
            ClearSpacePoints();
            StringArray spList = tp.SeparateBrackets(value, "{}", ",");
            for (UnsignedInt i = 0; i < spList.size(); i++)
            {
               AddSpacePoint(spList[i]);
            }
            return true;
         }
         catch (BaseException &)
         {
            SubscriberException se;
            se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
               GetParameterText(id).c_str(), "Valid CelestialBody list");
            throw se;
         }
      }
      else
      {
         AddSpacePoint(value);
         return true;
      }
   }
   case VIEW:
   {
      if (value[0] == '{')
      {
         try
         {
            TextParser tp;
            StringArray viewList = tp.SeparateBrackets(value, "{}", ",");
            ClearViews();
            for (UnsignedInt i = 0; i < viewList.size(); i++)
            {
               AddView(viewList[i]);
            }
            return true;
         }
         catch (BaseException &)
         {
            SubscriberException se;
            se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
               GetParameterText(id).c_str(), "Valid array of strings");
            throw se;
         }
      }
      else
      {
         return AddView(value);
      }
   }
   case VECTOR:
   {
     if (value[0] == '{')
     {
       try
       {
         TextParser tp;
         StringArray vectorList = tp.SeparateBrackets(value, "{}", ",");
         ClearVectors();
         for (UnsignedInt i = 0; i < vectorList.size(); i++)
         {
           AddVector(vectorList[i]);
         }
         return true;
       }
       catch (BaseException &)
       {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
           GetParameterText(id).c_str(), "Valid array of strings");
         throw se;
       }
     }
     else
     {
       return AddVector(value);
     }
   }
   case SENSOR_MASK:
   {
     if (value[0] == '{')
     {
       try
       {
         TextParser tp;
         StringArray maskList = tp.SeparateBrackets(value, "{}", ",");
         ClearMasks();
         for (UnsignedInt i = 0; i < maskList.size(); i++)
         {
           AddMask(maskList[i]);
         }
         return true;
       }
       catch (BaseException &)
       {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
           GetParameterText(id).c_str(), "Valid array of strings");
         throw se;
       }
     }
     else
     {
       return AddMask(value);
     }
   }
   case DRAW_MODEL_FILE:
   {
      if (value[0] == '{')
      {
         try
         {
            TextParser tp;
            StringArray modelList = tp.SeparateBrackets(value, "{}", ",");
            for (UnsignedInt i = 0; i < mSpacePoints.size() && i < modelList.size(); i++)
            {
               mSpacePoints[i].SetModelFile(modelList[i]);
               mDrawModelFileVector[i] = modelList[i];
            }
            return true;
         }
         catch (BaseException &)
         {
            SubscriberException se;
            se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
               GetParameterText(id).c_str(), "Valid array of strings");
            throw se;
         }
      }
      else
      {
         if (mSpacePoints.size() > 0)
         {
            mSpacePoints[mDrawModelFileCounter].SetModelFile(value);
            mDrawModelFileVector[mDrawModelFileCounter] = value;
            mDrawModelFileCounter++;
            if (mDrawModelFileCounter >= mSpacePoints.size())
               mDrawModelFileCounter = 0U;
            return true;
         }
         else
            return false;
      }
   }
   case DRAW_FONT_POSITION:
   {
     if (value[0] == '{')
     {
       try
       {
         TextParser tp;
         StringArray modelList = tp.SeparateBrackets(value, "{}", ",");
         for (UnsignedInt i = 0; i < mSpacePoints.size() && i < modelList.size(); i++)
         {
           mSpacePoints[i].SetDrawFontPosition(modelList[i]);
           mDrawFontPositionVector[i] = modelList[i];
         }
         return true;
       }
       catch (BaseException &)
       {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
           GetParameterText(id).c_str(), "Valid array of strings");
         throw se;
       }
     }
     else
     {
       if (mSpacePoints.size() > 0)
       {
         mSpacePoints[mDrawFontPositionCounter].SetDrawFontPosition(value);
         mDrawFontPositionVector[mDrawFontPositionCounter] = value;
         mDrawFontPositionCounter++;
         if (mDrawFontPositionCounter >= mSpacePoints.size())
           mDrawFontPositionCounter = 0U;
         return true;
       }
       else
         return false;
     }
   }
   case SOLVER_ITERATIONS:
   {
      bool itemFound = false;
      int index = -1;
      for (int i=0; i < OFSolverIterOptionCount; i++)
      {
         if (value == OF_SOLVER_ITER_OPTION_TEXT[i])
         {
            itemFound = true;
            index = i;
            break;
         }
      }

      if (itemFound)
      {
         mSolverIterations = value;
         mOFSolverIterOption = static_cast<OFSolverIterOption>(index);
         if (mOFSolverIterOption == OF_SI_ALL)
            mSolverIterLastN = 0;
         else if (mOFSolverIterOption == OF_SI_CURRENT)
            mSolverIterLastN = 1;
         else if (mOFSolverIterOption == OF_SI_NONE)
            mSolverIterLastN = 0;
         // else leave mSolverIterLastN at its previous value
         return true;
      }
      else
      {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
                        GetParameterText(id).c_str(),
                        "All, Current, None");
         throw se;
      }
   }
   case STAR_CATALOG:
   {
      // Make sure input string is a valid filename
      FileManager *fm = FileManager::Instance();
      std::string starCatalogLoc = fm->FindPath(value, "STAR_PATH", true, true, false);
      if (!starCatalogLoc.empty())
      {
         // Save original value (not the found catalog filepath) to preserve relative path inputs
         mCustomStarOptions.SetStarCatalog(value);
         return true;
      }
      else
      {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
            GetParameterText(id).c_str(),
            "File must exist");
         throw se;
      }
   }
   case STAR_SETTINGS:
   {
      bool itemFound = false;
      int index = -1;
      for (int i=0; i < StarSettingsOptionCount; i++)
      {
         if (value == STAR_SETTING_STRINGS[i])
         {
            itemFound = true;
            index = i;
            break;
         }
      }

      if (itemFound)
      {
         mStarSettingsString = value;
         mStarSettings = static_cast<StarSettingsOption>(index);
         return true;
      }
      else
      {
         // Accumulate all star settings options
         std::string validSettings = STAR_SETTING_STRINGS[0];
         for (int i = 1; i < StarSettingsOptionCount; ++i) validSettings += ", " + STAR_SETTING_STRINGS[i];
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), value.c_str(),
            GetParameterText(id).c_str(), validSettings.c_str());
         throw se;
      }
   }
   case TIME_SYNC_PARENT:
   {
      mTimeSyncParentName = value;
      return true;
   }
   default:
      return Subscriber::SetStringParameter(id, value);
   }
}


//------------------------------------------------------------------------------
// bool SetStringParameter(const std::string &label, const std::string &value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetStringParameter()
 */
bool OpenFramesInterface::SetStringParameter(const std::string &label,
                                             const std::string &value)
{
   return SetStringParameter(GetParameterID(label), value);
}


//------------------------------------------------------------------------------
// virtual bool SetStringParameter(const Integer id, const std::string &value,
//                                 const Integer index)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetStringParameter()
 */
bool OpenFramesInterface::SetStringParameter(const Integer id, const std::string &value,
                                             const Integer index)
{
   switch (id)
   {
   case ADD:
      AddSpacePoint(value);
      return true;
   case VIEW:
      return AddView(value);
   case VECTOR:
     return AddVector(value);
   case SENSOR_MASK:
     return AddMask(value);
   case DRAW_MODEL_FILE:
      if (index < mSpacePoints.size())
      {
         mSpacePoints[index].SetModelFile(value);
         mDrawModelFileVector[index] = value;
      }
      else
         return false;
   case DRAW_FONT_POSITION:
     if (index < mSpacePoints.size())
     {
       mSpacePoints[index].SetDrawFontPosition(value);
       mDrawFontPositionVector[index] = value;
     }
     else
       return false;
   default:
      return Subscriber::SetStringParameter(id, value, index);
   }
}


//------------------------------------------------------------------------------
// virtual bool SetStringParameter(const std::string &label,
//                                 const std::string &value,
//                                 const Integer index)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetStringParameter()
 */
bool OpenFramesInterface::SetStringParameter(const std::string &label,
                                             const std::string &value,
                                             const Integer index)
{
   return SetStringParameter(GetParameterID(label), value, index);
}


//------------------------------------------------------------------------------
// const StringArray& GetStringArrayParameter(const Integer id) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetStringArrayParameter()
 */
const StringArray &OpenFramesInterface::GetStringArrayParameter(const Integer id) const
{
   switch (id)
   {
   case ADD:
      return mAllSpNameArray;
   case VIEW:
      return mViewNames;
   case VECTOR:
     return mVectorNames;
   case SENSOR_MASK:
     return mMaskNames;
   case DRAW_MODEL_FILE:
      return mDrawModelFileVector;
   case DRAW_FONT_POSITION:
     return mDrawFontPositionVector;
   default:
      return Subscriber::GetStringArrayParameter(id);
   }
}


//------------------------------------------------------------------------------
// const StringArray& GetStringArrayParameter(const std::string &label) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetStringArrayParameter()
 */
const StringArray &OpenFramesInterface::GetStringArrayParameter(const std::string &label) const
{
   return GetStringArrayParameter(GetParameterID(label));
}


//------------------------------------------------------------------------------
// const StringArray& SetStringArrayParameter(const Integer id, const StringArray &stringArray)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetStringArrayParameter()
 */
bool OpenFramesInterface::SetStringArrayParameter(const Integer id, const StringArray &stringArray)
{
   switch (id)
   {
   case ADD:
      ClearSpacePoints();
      for (auto str = stringArray.begin(); str < stringArray.end(); str++)
         AddSpacePoint(*str);
      return true;
   case VIEW:
      ClearViews();
      for (auto str = stringArray.begin(); str < stringArray.end(); str++)
         AddView(*str);
      return true;
   case VECTOR:
     ClearVectors();
     for (auto str = stringArray.begin(); str < stringArray.end(); str++)
       AddVector(*str);
     return true;
   case SENSOR_MASK:
     ClearMasks();
     for (auto str = stringArray.begin(); str < stringArray.end(); str++)
       AddMask(*str);
     return true;
   case DRAW_MODEL_FILE:
      for (int ii = 0; ii < mSpacePoints.size() && ii < stringArray.size(); ii++)
      {
         mSpacePoints[ii].SetModelFile(stringArray[ii]);
         mDrawModelFileVector[ii] = stringArray[ii];
      }
   case DRAW_FONT_POSITION:
     for (int ii = 0; ii < mSpacePoints.size() && ii < stringArray.size(); ii++)
     {
       mSpacePoints[ii].SetDrawFontPosition(stringArray[ii]);
       mDrawFontPositionVector[ii] = stringArray[ii];
     }
   default:
      return false;
   }
}


//------------------------------------------------------------------------------
// const StringArray& GetStringArrayParameter(const std::string &label) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetStringArrayParameter()
 */
bool OpenFramesInterface::SetStringArrayParameter(const std::string &label, const StringArray &stringArray)
{
   return SetStringArrayParameter(GetParameterID(label), stringArray);
}


//------------------------------------------------------------------------------
// void ClearSpacePoints()
//------------------------------------------------------------------------------
/**
 * Clears all objects from the list of selected space points
 */
void OpenFramesInterface::ClearSpacePoints()
{
   mSpacePoints.clear();
   mAllSpNameArray.clear();
   mDrawObjectArray.clear();
   mDrawTrajectoryArray.clear();
   mDrawAxesArray.clear();
   mDrawXYPlaneArray.clear();
   mDrawLabelArray.clear();
   mDrawLabelPropagateArray.clear();
   mDrawCenterArray.clear();
   mDrawEndsArray.clear();
   mDrawVelocityArray.clear();
   mDrawGridArray.clear();
   mDrawLineWidthVector.SetSize(0);
   mDrawMarkerSizeVector.clear();
   mDrawFontSizeVector.clear();
   mDrawModelFileVector.clear();
   mDrawFontPositionVector.clear();
}


//------------------------------------------------------------------------------
// bool AddSpacePoint(const std::string &name)
//------------------------------------------------------------------------------
/**
 * Adds an object to the list of selected space points
 *
 * @param <name> The name of the object to add
 *
 * @return True if successful
 * @return False if an error prevents adding
 */
bool OpenFramesInterface::AddSpacePoint(const std::string &name)
{
   if (find(mAllSpNameArray.begin(), mAllSpNameArray.end(), name) == mAllSpNameArray.end())
   {
      if (name != "")
      {
         mSpacePoints.push_back(SpacePointOptions(name));
         mAllSpNameArray.push_back(name);
         mDrawObjectArray.push_back(mSpacePoints.back().GetDrawObject());
         mDrawTrajectoryArray.push_back(mSpacePoints.back().GetDrawObject());
         mDrawAxesArray.push_back(mSpacePoints.back().GetDrawAxes());
         mDrawXYPlaneArray.push_back(mSpacePoints.back().GetDrawXYPlane());
         mDrawLabelArray.push_back(mSpacePoints.back().GetDrawLabel());
         mDrawLabelPropagateArray.push_back(mSpacePoints.back().GetUsePropagateLabel());
         mDrawCenterArray.push_back(mSpacePoints.back().GetDrawCenter());
         mDrawEndsArray.push_back(mSpacePoints.back().GetDrawEnds());
         mDrawVelocityArray.push_back(mSpacePoints.back().GetDrawVelocity());
         mDrawGridArray.push_back(mSpacePoints.back().GetDrawGrid());
         mDrawLineWidthVector.Resize(mDrawLineWidthVector.GetSize() + 1);
         mDrawLineWidthVector[mDrawLineWidthVector.GetSize() - 1] = mSpacePoints.back().GetDrawLineWidth();
         mDrawMarkerSizeVector.resize(mDrawMarkerSizeVector.size() + 1);
         mDrawMarkerSizeVector[mDrawMarkerSizeVector.size() - 1] = mSpacePoints.back().GetDrawMarkerSize();
         mDrawFontSizeVector.resize(mDrawFontSizeVector.size() + 1);
         mDrawFontSizeVector[mDrawFontSizeVector.size() - 1] = mSpacePoints.back().GetDrawFontSize();
         mDrawModelFileVector.push_back("");
         mDrawFontPositionVector.push_back(mSpacePoints.back().GetDrawFontPosition()); 


      }
   }
   return true;
}


//------------------------------------------------------------------------------
// bool ClearViews()
//------------------------------------------------------------------------------
/**
 * Clears all views associated with this interface
 */
void OpenFramesInterface::ClearViews()
{
   mViewNames.clear();
   mViews.clear();
   ownedObjectCount = 0;
}


//------------------------------------------------------------------------------
// bool ClearVectors()
//------------------------------------------------------------------------------
/**
* Clears all vectors associated with this interface
*/
void OpenFramesInterface::ClearVectors()
{
  mVectorNames.clear();
  mVectors.clear();
  ownedObjectCount = 0;
}


//------------------------------------------------------------------------------
// bool ClearMasks()
//------------------------------------------------------------------------------
/**
* Clears all sensor masks associated with this interface
*/
void OpenFramesInterface::ClearMasks()
{
  mMaskNames.clear();
  mMasks.clear();
  ownedObjectCount = 0;
}

//------------------------------------------------------------------------------
// bool AddView(const std::string &name)
//------------------------------------------------------------------------------
/**
 * Adds a view to the list of associated views
 *
 * @param <name> The name of the OpenFramesView to add
 *
 * @return True if successful
 * @return False if an error prevents adding
 */
bool OpenFramesInterface::AddView(const std::string &name)
{
   if (find(mViewNames.begin(), mViewNames.end(), name) == mViewNames.end())
   {
      if (name != "")
      {
         mViewNames.push_back(name);
         mViews.push_back(nullptr);
      }
   }

   return true;
}

//------------------------------------------------------------------------------
// bool AddVector(const std::string &name)
//------------------------------------------------------------------------------
/**
* Adds a vector to the list of associated vectors
*
* @param <name> The name of the OpenFramesVector to add
*
* @return True if successful
* @return False if an error prevents adding
*/
bool OpenFramesInterface::AddVector(const std::string &name)
{
  if (find(mVectorNames.begin(), mVectorNames.end(), name) == mVectorNames.end())
  {
    if (name != "")
    {
      mVectorNames.push_back(name);
      mVectors.push_back(nullptr);
    }
  }

  return true;
}

//------------------------------------------------------------------------------
// bool AddMask(const std::string &name)
//------------------------------------------------------------------------------
/**
* Adds a sensor mask to the list of associated masks
*
* @param <name> The name of the OpenFramesSensorMask to add
*
* @return True if successful
* @return False if an error prevents adding
*/
bool OpenFramesInterface::AddMask(const std::string &name)
{
  if (find(mMaskNames.begin(), mMaskNames.end(), name) == mMaskNames.end())
  {
    if (name != "")
    {
      mMaskNames.push_back(name);
      mMasks.push_back(nullptr);
    }
  }

  return true;
}

//---------------------------------------------------------------------------
//  std::string GetOnOffParameter(const Integer id) const
//---------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetOnOffParameter()
 */
std::string OpenFramesInterface::GetOnOffParameter(const Integer id) const
{
   switch (id)
   {
   case XY_PLANE:
      return BooleanToOnOff(mFrameOptions.GetEnableXYPlane());
   case ECLIPTIC_PLANE:
      return BooleanToOnOff(mFrameOptions.GetEnableEclipticPlane());
   case AXES:
      return BooleanToOnOff(mFrameOptions.GetEnableAxes());
   case AXES_LABELS:
      return BooleanToOnOff(mFrameOptions.GetEnableAxesLabels());
   case FRAME_LABEL:
      return BooleanToOnOff(mFrameOptions.GetEnableFrameLabel());
   case ENABLE_STARS:
      return BooleanToOnOff(mCustomStarOptions.GetEnableStars());
   case MULTISAMPLE_ANTI_ALIASING:
      return BooleanToOnOff(mGLOptions.GetEnableMSAA());
   default:
      return Subscriber::GetOnOffParameter(id);
   }
}


//------------------------------------------------------------------------------
// std::string OrbitView::GetOnOffParameter(const std::string &label) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetOnOffParameter()
 */
std::string OpenFramesInterface::GetOnOffParameter(const std::string &label) const
{
   return GetOnOffParameter(GetParameterID(label));
}


//---------------------------------------------------------------------------
//  bool SetOnOffParameter(const Integer id, const std::string &value)
//---------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetOnOffParameter()
 */
bool OpenFramesInterface::SetOnOffParameter(const Integer id, const std::string &value)
{
   switch (id)
   {
   case XY_PLANE:
      mFrameOptions.SetEnableXYPlane(value == ON_STRING);
      return true;
   case ECLIPTIC_PLANE:
      mFrameOptions.SetEnableEclipticPlane(value == ON_STRING);
      return true;
   case AXES:
      mFrameOptions.SetEnableAxes(value == ON_STRING);
      return true;
   case AXES_LABELS:
      mFrameOptions.SetEnableAxesLabels(value == ON_STRING);
      return true;
   case FRAME_LABEL:
      mFrameOptions.SetEnableFrameLabel(value == ON_STRING);
      return true;
   case ENABLE_STARS:
      mCustomStarOptions.SetEnableStars(value == ON_STRING);
      return true;
   case MULTISAMPLE_ANTI_ALIASING:
      mGLOptions.SetEnableMSAA(value == ON_STRING);
      return true;
   default:
      return Subscriber::SetOnOffParameter(id, value);
   }
}


//------------------------------------------------------------------------------
// bool SetOnOffParameter(const std::string &label, const std::string &value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetOnOffParameter()
 */
bool OpenFramesInterface::SetOnOffParameter(const std::string &label,
                                            const std::string &value)
{
   return SetOnOffParameter(GetParameterID(label), value);
}


//------------------------------------------------------------------------------
// bool GetBooleanParameter(const Integer id) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetBooleanParameter()
 */
bool OpenFramesInterface::GetBooleanParameter(const Integer id) const
{
   switch (id)
   {
   case SHOW_PLOT:
      return active;
   case SHOW_TOOLBAR:
      return mShowToolbar;
   case SHOW_VR:
      return mGLOptions.GetEnableVR();
   default:
      return Subscriber::GetBooleanParameter(id);
   }
}


//------------------------------------------------------------------------------
// bool GetBooleanParameter(const std::string &label) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetBooleanParameter()
 */
bool OpenFramesInterface::GetBooleanParameter(const std::string &label) const
{
   return GetBooleanParameter(GetParameterID(label));
}


//------------------------------------------------------------------------------
// bool SetBooleanParameter(const Integer id, const bool value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetBooleanParameter()
 */
bool OpenFramesInterface::SetBooleanParameter(const Integer id, const bool value)
{
   switch (id)
   {
   case SHOW_PLOT:
      active = value;
      return active;
   case SHOW_TOOLBAR:
      mShowToolbar = value;
      return value;
   case SHOW_VR:
      mGLOptions.SetEnableVR(value);
      return value;
   default:
      return Subscriber::SetBooleanParameter(id, value);
   }
}


//------------------------------------------------------------------------------
// bool SetBooleanParameter(const std::string &label, const bool value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetBooleanParameter()
 */
bool OpenFramesInterface::SetBooleanParameter(const std::string &label, const bool value)
{
   return SetBooleanParameter(GetParameterID(label), value);
}


//---------------------------------------------------------------------------
//const BooleanArray& GetBooleanArrayParameter(const Integer id) const
//---------------------------------------------------------------------------
/**
 * @see GmatBase
 */
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetBooleanArrayParameter()
 */
const BooleanArray& OpenFramesInterface::GetBooleanArrayParameter(const Integer id) const
{
   switch (id)
   {
   case DRAW_OBJECT:
   {
      return mDrawObjectArray;
   }
   case DRAW_TRAJECTORY:
   {
      return mDrawTrajectoryArray;
   }
   case DRAW_AXES:
   {
      return mDrawAxesArray;
   }
   case DRAW_XY_PLANE:
   {
      return mDrawXYPlaneArray;
   }
   case DRAW_LABEL:
   {
      return mDrawLabelArray;
   }
   case DRAW_LABEL_PROPAGATE:
   {
      return mDrawLabelPropagateArray;
   }
   case DRAW_CENTER_POINT:
   {
      return mDrawCenterArray;
   }
   case DRAW_END_POINTS:
   {
      return mDrawEndsArray;
   }
   case DRAW_VELOCITY:
   {
      return mDrawVelocityArray;
   }
   case DRAW_GRID:
   {
      return mDrawGridArray;
   }
   default:
   {
      return Subscriber::GetBooleanArrayParameter(id);
   }
   }
}


//---------------------------------------------------------------------------
//const BooleanArray& GetBooleanArrayParameter(const std::string &label) const
//---------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetBooleanArrayParameter()
 */
const BooleanArray& OpenFramesInterface::GetBooleanArrayParameter(const std::string &label) const
{
   Integer id = GetParameterID(label);
   return GetBooleanArrayParameter(id);
}


//---------------------------------------------------------------------------
//  bool SetBooleanArrayParameter(const Integer id, const BooleanArray &valueArray)
//---------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetBooleanArrayParameter()
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::SetBooleanArrayParameter(const Integer id,
                                                   const BooleanArray &valueArray)
{
   Integer i;
   switch (id)
   {
   case DRAW_OBJECT:
   {
      for (i = 0; i < mSpacePoints.size() && i < valueArray.size(); i++)
      {
         mDrawObjectArray[i] = valueArray[i];
         mSpacePoints[i].SetDrawObject(valueArray[i]);
      }
      return true;
   }
   case DRAW_TRAJECTORY:
   {
      for (i = 0; i < mSpacePoints.size() && i < valueArray.size(); i++)
      {
         mDrawTrajectoryArray[i] = valueArray[i];
         mSpacePoints[i].SetDrawTrajectory(valueArray[i]);
      }
      return true;
   }
   case DRAW_AXES:
   {
      for (i = 0; i < mSpacePoints.size() && i < valueArray.size(); i++)
      {
         mDrawAxesArray[i] = valueArray[i];
         mSpacePoints[i].SetDrawAxes(valueArray[i]);
      }
      return true;
   }
   case DRAW_XY_PLANE:
   {
      for (i = 0; i < mSpacePoints.size() && i < valueArray.size(); i++)
      {
         mDrawXYPlaneArray[i] = valueArray[i];
         mSpacePoints[i].SetDrawXYPlane(valueArray[i]);
      }
      return true;
   }
   case DRAW_LABEL:
   {
      for (i = 0; i < mSpacePoints.size() && i < valueArray.size(); i++)
      {
         mDrawLabelArray[i] = valueArray[i];
         mSpacePoints[i].SetDrawLabel(valueArray[i]);
      }
      return true;
   }
   case DRAW_LABEL_PROPAGATE:
   {
      for (i = 0; i < mSpacePoints.size() && i < valueArray.size(); i++)
      {
         mDrawLabelPropagateArray[i] = valueArray[i];
         mSpacePoints[i].SetUsePropagateLabel(valueArray[i]);
      }
      return true;
   }
   case DRAW_CENTER_POINT:
   {
      for (i = 0; i < mSpacePoints.size() && i < valueArray.size(); i++)
      {
         mDrawCenterArray[i] = valueArray[i];
         mSpacePoints[i].SetDrawCenter(valueArray[i]);
      }
      return true;
   }
   case DRAW_END_POINTS:
   {
      for (i = 0; i < mSpacePoints.size() && i < valueArray.size(); i++)
      {
         mDrawEndsArray[i] = valueArray[i];
         mSpacePoints[i].SetDrawEnds(valueArray[i]);
      }
      return true;
   }
   case DRAW_VELOCITY:
   {
      for (i = 0; i < mSpacePoints.size() && i < valueArray.size(); i++)
      {
         mDrawVelocityArray[i] = valueArray[i];
         mSpacePoints[i].SetDrawVelocity(valueArray[i]);
      }
      return true;
   }
   case DRAW_GRID:
   {
      for (i = 0; i < mSpacePoints.size() && i < valueArray.size(); i++)
      {
         mDrawGridArray[i] = valueArray[i];
         mSpacePoints[i].SetDrawGrid(valueArray[i]);
      }
      return true;
   }
   default:
   {
      return Subscriber::SetBooleanArrayParameter(id, valueArray);
   }
   }
}


//---------------------------------------------------------------------------
//  bool SetBooleanArrayParameter(const std::string &label,
//                                const BooleanArray &valueArray)
//---------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetBooleanArrayParameter()
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::SetBooleanArrayParameter(const std::string &label,
                                                   const BooleanArray &valueArray)
{
   Integer id = GetParameterID(label);
   return SetBooleanArrayParameter(id, valueArray);
}


//------------------------------------------------------------------------------
// virtual Integer GetIntegerParameter(const Integer id) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetIntegerParameter()
 */
Integer OpenFramesInterface::GetIntegerParameter(const Integer id) const
{
   switch (id)
   {
   case STAR_COUNT:
      return mCustomStarOptions.GetStarCount();
   case SOLVER_ITER_LAST_N:
      if (mOFSolverIterOption == OF_SI_ALL)
         return 0;
      else if (mOFSolverIterOption == OF_SI_CURRENT)
         return 1;
      else if (mOFSolverIterOption == OF_SI_NONE)
         return 0;
      else
         return mSolverIterLastN;
   case MSAA_SAMPLES:
      return mGLOptions.GetMSAASamples();
   default:
      return Subscriber::GetIntegerParameter(id);
   }
}


//------------------------------------------------------------------------------
// virtual Integer GetIntegerParameter(const std::string &label) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetIntegerParameter()
 */
Integer OpenFramesInterface::GetIntegerParameter(const std::string &label) const
{
   return GetIntegerParameter(GetParameterID(label));
}


//------------------------------------------------------------------------------
// virtual Integer SetIntegerParameter(const Integer id, const Integer value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetIntegerParameter()
 */
Integer OpenFramesInterface::SetIntegerParameter(const Integer id, const Integer value)
{
   switch (id)
   {
   case STAR_COUNT:
      if (value > 0)
      {
         mCustomStarOptions.SetStarCount(value);
         return value;
      }
      else
      {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(),
                       GmatStringUtil::ToString(value, 1).c_str(),
                       GetParameterText(id).c_str(), "Integer Value > 0");
         throw se;
      }
   case SOLVER_ITER_LAST_N:
      if (value >= 0)
      {
         if (mOFSolverIterOption == OF_SI_LAST_N)
            mSolverIterLastN = value;
         return mSolverIterLastN;
      }
      else
      {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(),
                       GmatStringUtil::ToString(value, 1).c_str(),
                       GetParameterText(id).c_str(), "Integer Value >= 0");
         throw se;
      }
   case MSAA_SAMPLES:
      if (value >= 0)
      {
         mGLOptions.SetMSAASamples(value);
         return value;
      }
      else
      {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(),
                       GmatStringUtil::ToString(value, 1).c_str(),
                       GetParameterText(id).c_str(), "Integer Value >= 0");
         throw se;
      }
   default:
      return Subscriber::SetIntegerParameter(id, value);
   }
}


//------------------------------------------------------------------------------
// virtual Integer SetIntegerParameter(const std::string &label,
//                                     const Integer value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetIntegerParameter()
 */
Integer OpenFramesInterface::SetIntegerParameter(const std::string &label,
                                       const Integer value)
{
   return SetIntegerParameter(GetParameterID(label), value);
}


//------------------------------------------------------------------------------
// UnsignedInt GetUnsignedIntParameter(const Integer id, onst Integer index) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetUnsignedIntParameter()
 */
UnsignedInt OpenFramesInterface::GetUnsignedIntParameter(const Integer id,
                                                         const Integer index) const
{
   switch (id)
   {
   case DRAW_MARKER_SIZE:
      if (index < mSpacePoints.size())
      {
         return mSpacePoints[index].GetDrawMarkerSize();
      }
      else
      {
         throw SubscriberException("Index is out-of-bounds for " + GetParameterText(id));
      }
   case DRAW_FONT_SIZE:
     if (index < mSpacePoints.size())
     {
       return mSpacePoints[index].GetDrawFontSize();
     }
     else
     {
       throw SubscriberException("Index is out-of-bounds for " + GetParameterText(id));
     }
   default:
      return Subscriber::GetUnsignedIntParameter(id, index);
   }
}


//------------------------------------------------------------------------------
// UnsignedInt  SetUnsignedIntParameter(const Integer id, const UnsignedInt value, const Integer index);
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetUnsignedIntParameter()
 */
UnsignedInt OpenFramesInterface::SetUnsignedIntParameter(const Integer id,
                                                         const UnsignedInt value,
                                                         const Integer index)
{
   switch (id)
   {
   case DRAW_MARKER_SIZE:
      if (index < mSpacePoints.size())
      {
         mSpacePoints[index].SetDrawMarkerSize(value);
         mDrawMarkerSizeVector[index] = value;
         return mSpacePoints[index].GetDrawMarkerSize();
      }
      else
      {
         throw SubscriberException
            ("index out of bounds for " + GetParameterText(id));
      }
   case DRAW_FONT_SIZE:
     if (index < mSpacePoints.size())
     {
       mSpacePoints[index].SetDrawFontSize(value);
       mDrawFontSizeVector[index] = value;
       return mSpacePoints[index].GetDrawFontSize();
     }
     else
     {
       throw SubscriberException
         ("index out of bounds for " + GetParameterText(id));
     }
   default:
      return Subscriber::SetUnsignedIntParameter(id, value, index);
   }
}


//------------------------------------------------------------------------------
// const Rvector& GetRvectorParameter(const Integer id)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetUnsignedIntArrayParameter()
 */
const UnsignedIntArray& OpenFramesInterface::GetUnsignedIntArrayParameter(const Integer id) const
{
   switch (id)
   {
   case DRAW_MARKER_SIZE:
      return mDrawMarkerSizeVector;
   case DRAW_FONT_SIZE:
     return mDrawFontSizeVector;
   default:
      return Subscriber::GetUnsignedIntArrayParameter(id);
   }
}


//------------------------------------------------------------------------------
// virtual Real GetRealParameter(const Integer id) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetRealParameter()
 */
Real OpenFramesInterface::GetRealParameter(const Integer id) const
{
   switch (id)
   {
   case MIN_STAR_MAG:
      return mCustomStarOptions.GetMinStarMag();
   case MAX_STAR_MAG:
      return mCustomStarOptions.GetMaxStarMag();
   case MIN_STAR_PIXELS:
      return mCustomStarOptions.GetMinStarPixels();
   case MAX_STAR_PIXELS:
      return mCustomStarOptions.GetMaxStarPixels();
   case MIN_STAR_DIM_RATIO:
      return mCustomStarOptions.GetMinStarDimRatio();
   case PLAYBACK_TIME_SCALE:
      return mFrameOptions.GetPlaybackTimeScale();
   case AXES_LENGTH:
      return mFrameOptions.GetAxesLength();
   default:
      return Subscriber::GetRealParameter(id);
   }
}


//------------------------------------------------------------------------------
// virtual Real GetRealParameter(const std::string &label) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetRealParameter()
 */
Real OpenFramesInterface::GetRealParameter(const std::string &label) const
{
   return GetRealParameter(GetParameterID(label));
}


//------------------------------------------------------------------------------
// virtual Real SetRealParameter(const Integer id, const Real value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetRealParameter()
 */
Real OpenFramesInterface::SetRealParameter(const Integer id, const Real value)
{
   switch (id)
   {
   case MIN_STAR_MAG:
      if (value <= 20.0)
      {
         mCustomStarOptions.SetMinStarMag(value);
         return value;
      }
      else
      {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(),
                       GmatStringUtil::ToString(value, 5).c_str(),
                       GetParameterText(id).c_str(), "Real Value <= 20.0");
         throw se;
      }
   case MAX_STAR_MAG:
      if (value >= mCustomStarOptions.GetMinStarMag())
      {
         mCustomStarOptions.SetMaxStarMag(value);
         return value;
      }
      else
      {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(),
                       GmatStringUtil::ToString(value, 5).c_str(),
                       GetParameterText(id).c_str(), "Real Value >= MinStarMag");
         throw se;
      }
   case MIN_STAR_PIXELS:
      if (value > 0.0f)
      {
         mCustomStarOptions.SetMinStarPixels(value);
         return value;
      }
      else
      {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(),
                       GmatStringUtil::ToString(value, 1).c_str(),
                       GetParameterText(id).c_str(), "Real Value > 0.0");
         throw se;
      }
   case MAX_STAR_PIXELS:
      if (value >= mCustomStarOptions.GetMinStarPixels())
      {
         mCustomStarOptions.SetMaxStarPixels(value);
         return value;
      }
      else
      {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(),
                       GmatStringUtil::ToString(value, 1).c_str(),
                       GetParameterText(id).c_str(), "Real Value >= MinStarPixels");
         throw se;
      }
   case MIN_STAR_DIM_RATIO:
      if (value >= 0.0f && value <= 1.0)
      {
         mCustomStarOptions.SetMinStarDimRatio(value);
         return value;
      }
      else
      {
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(),
                       GmatStringUtil::ToString(value, 1).c_str(),
                       GetParameterText(id).c_str(), "0.0 <= Real Value <= 1.0");
         throw se;
      }
   case PLAYBACK_TIME_SCALE:
      mFrameOptions.SetPlaybackTimeScale(value);
      return value;
   case AXES_LENGTH:
      if (value > 0.0)
      {
         mFrameOptions.SetAxesLength(value);
         return value;
      }
      else
      {
         std::string valueStr = std::to_string(value);
         SubscriberException se;
         se.SetDetails(errorMessageFormat.c_str(), valueStr.c_str(),
                       GetParameterText(id).c_str(), "0.0 < Real Value");
         throw se;
      }
   default:
      return Subscriber::SetRealParameter(id, value);
   }
}


//------------------------------------------------------------------------------
// virtual Real SetRealParameter(const std::string &label, const Real value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetRealParameter()
 */
Real OpenFramesInterface::SetRealParameter(const std::string &label, const Real value)
{
   return SetRealParameter(GetParameterID(label), value);
}


//------------------------------------------------------------------------------
// Real GetRealParameter(const Integer id, const Integer index)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetRealParameter()
 */
Real OpenFramesInterface::GetRealParameter(const Integer id, const Integer index) const
{
   if (index < 0)
   {
      throw SubscriberException("Index is out-of-bounds for " + GetParameterText(id));
   }
   switch (id)
   {
   case DRAW_LINE_WIDTH:
      if (index < mSpacePoints.size())
      {
         return mSpacePoints[index].GetDrawLineWidth();
      }
      else
      {
         throw SubscriberException("Index is out-of-bounds for " + GetParameterText(id));
      }
   default:
      return Subscriber::GetRealParameter(id, index);
   }
}


//------------------------------------------------------------------------------
// Real SetRealParameter(const Integer id, const Real value, const Integer index)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetRealParameter()
 */
Real OpenFramesInterface::SetRealParameter(const Integer id, const Real value, const Integer index)
{
   if (index < 0)
   {
      throw SubscriberException("index out of bounds for " + GetParameterText(id));
   }

   switch (id)
   {
   case DRAW_LINE_WIDTH:
   {
      if (index < mSpacePoints.size())
      {
         mSpacePoints[index].SetDrawLineWidth(value);
         mDrawLineWidthVector[index] = value;
         return mSpacePoints[index].GetDrawLineWidth();
      }
      else
      {
         throw SubscriberException
            ("index out of bounds for " + GetParameterText(id));
      }
   }
   default:
      return Subscriber::SetRealParameter(id, value, index);
   }
}


//------------------------------------------------------------------------------
// const Rvector& GetRvectorParameter(const Integer id)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetRvectorParameter()
 */
const Rvector& OpenFramesInterface::GetRvectorParameter(const Integer id) const
{
   switch (id)
   {
   case DRAW_LINE_WIDTH:
      return mDrawLineWidthVector;
   default:
      return Subscriber::GetRvectorParameter(id);
   }
}


//------------------------------------------------------------------------------
// const Rvector& SetRvectorParameter(const Integer id, const Rvector &value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetRvectorParameter()
 */
const Rvector& OpenFramesInterface::SetRvectorParameter(const Integer id, const Rvector &value)
{
   switch (id)
   {
   case DRAW_LINE_WIDTH:
   {
      for (int i = 0; i < mSpacePoints.size() && i < value.GetSize(); i++)
      {
         if (value[i] < 1.0)
         {
            SubscriberException se;
            se.SetDetails(errorMessageFormat.c_str(),
                          GmatStringUtil::ToString(value[i], 1).c_str(),
                          GetParameterText(id).c_str(), "Real Value >= 1.0");
            throw se;
         }
      }
      for (int i = 0; i < mSpacePoints.size() && i < value.GetSize(); i++)
      {
         mDrawLineWidthVector[i] = value[i];
         mSpacePoints[i].SetDrawLineWidth(value[i]);
      }
      return mDrawLineWidthVector;
   }
   default:
      return Subscriber::SetRvectorParameter(id, value);
   }
}


//------------------------------------------------------------------------------
// const Rvector& GetRvectorParameter(const std::string &label)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetRvectorParameter()
 */
const Rvector& OpenFramesInterface::GetRvectorParameter(const std::string &label) const
{
   return GetRvectorParameter(GetParameterID(label));
}


//------------------------------------------------------------------------------
// const Rvector& SetRvectorParameter(const std::string &label, const Rvector &value)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetRvectorParameter()
 */
const Rvector& OpenFramesInterface::SetRvectorParameter(const std::string &label, const Rvector &value)
{
   return SetRvectorParameter(GetParameterID(label), value);
}


//------------------------------------------------------------------------------
// virtual std::string GetRefObjectName(const UnsignedInt type) const
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetRefObjectName()
 */
std::string OpenFramesInterface::GetRefObjectName(const UnsignedInt type) const
{
   if (type == Gmat::COORDINATE_SYSTEM)
   {
      return mFrameOptions.GetCoordSysName();
   }
   else if (type == Gmat::SUBSCRIBER)
   {
      return mTimeSyncParentName;
   }

   return Subscriber::GetRefObjectName(type);
}


//------------------------------------------------------------------------------
// virtual bool HasRefObjectTypeArray()
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::HasRefObjectTypeArray()
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::HasRefObjectTypeArray()
{
   return true;
}


//------------------------------------------------------------------------------
// const ObjectTypeArray& GetRefObjectTypeArray()
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetRefObjectTypeArray()
 */
//------------------------------------------------------------------------------
const ObjectTypeArray& OpenFramesInterface::GetRefObjectTypeArray()
{
   refObjectTypes.push_back(Gmat::SPACE_POINT);
   refObjectTypes.push_back(GmatType::GetTypeId("OpenFramesView"));
   refObjectTypes.push_back(GmatType::GetTypeId("OpenFramesVector"));
   refObjectTypes.push_back(GmatType::GetTypeId("OpenFramesSensorMask"));
   refObjectTypes.push_back(Gmat::SUBSCRIBER);
   return refObjectTypes;
}


//------------------------------------------------------------------------------
// virtual const StringArray& GetRefObjectNameArray(const UnsignedInt type)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetRefObjectNameArray()
 */
const StringArray& OpenFramesInterface::GetRefObjectNameArray(const UnsignedInt type)
{
   refObjectNames.clear();

   if (type == Gmat::COORDINATE_SYSTEM || type == Gmat::UNKNOWN_OBJECT)
   {
      refObjectNames.push_back(mFrameOptions.GetCoordSysName());
   }

   if (type == Gmat::SPACE_POINT || type == Gmat::UNKNOWN_OBJECT)
   {
      for (std::vector<SpacePointOptions>::iterator spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
      {
         refObjectNames.push_back(spo->GetName());
      }
   }

   if (type == GmatType::GetTypeId("OpenFramesView") || type == Gmat::UNKNOWN_OBJECT)
   {
      for (StringArray::const_iterator view = mViewNames.begin(); view < mViewNames.end(); view++)
      {
         refObjectNames.push_back(*view);
      }
   }

   if (type == GmatType::GetTypeId("OpenFramesVector") || type == Gmat::UNKNOWN_OBJECT)
   {
     for (StringArray::const_iterator vector = mVectorNames.begin(); vector < mVectorNames.end(); vector++)
     {
       refObjectNames.push_back(*vector);
     }
   }
   
   if (type == GmatType::GetTypeId("OpenFramesSensorMask") || type == Gmat::UNKNOWN_OBJECT)
   {
     for (StringArray::const_iterator mask = mMaskNames.begin(); mask < mMaskNames.end(); mask++)
     {
       refObjectNames.push_back(*mask);
     }
   }

   if (type == Gmat::SUBSCRIBER || type == Gmat::UNKNOWN_OBJECT)
   {
      if (mTimeSyncParentName.size() > 0)
      {
         refObjectNames.push_back(mTimeSyncParentName);
      }
   }

  return refObjectNames;
}


//------------------------------------------------------------------------------
// virtual GmatBase* GetRefObject(const UnsignedInt type,
//                                const std::string &name)
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::GetRefObject()
 */
GmatBase* OpenFramesInterface::GetRefObject(const UnsignedInt type,
                                            const std::string &name)
{
   if (type == Gmat::COORDINATE_SYSTEM)
   {
      if (name == mFrameOptions.GetCoordSysName())
      {
         return mViewCoordSystem;
      }
   }
   else if (type == Gmat::SUBSCRIBER)
   {
      if (name == mTimeSyncParentName)
      {
         return mTimeSyncParentInterface;
      }
   }

   return Subscriber::GetRefObject(type, name);
}


//------------------------------------------------------------------------------
// virtual bool SetRefObject(GmatBase *obj, const UnsignedInt type,
//                           const std::string &name = "")
//------------------------------------------------------------------------------
/**
 * @copydoc GmatBase::SetRefObject()
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::SetRefObject(GmatBase *obj, const UnsignedInt type,
                                       const std::string &name)
{
   if (obj == NULL)
   {
      return false;
   }

   std::string realName = name;
   if (name == "")
   {
      realName = obj->GetName();
   }

   else if (obj->IsOfType(Gmat::SPACE_POINT))
   {
      for (Integer i = 0; i < mSpacePoints.size(); i++)
      {
         if (mSpacePoints[i].IsNameSameAs(realName))
         {
            mSpacePoints[i].SetSpacePoint(static_cast<SpacePoint *>(obj));
         }
      }

      return true;
   }

   else if (type == Gmat::COORDINATE_SYSTEM)
   {
      if (realName == mFrameOptions.GetCoordSysName())
      {
         mViewCoordSystem = (CoordinateSystem*)obj;
      }
      return true;
   }

   else if (type == GmatType::GetTypeId("OpenFramesView") && obj->GetTypeName() == "OpenFramesView")
   {
      for (Integer i = 0; i < mViewNames.size(); i++)
      {
         if (mViewNames[i] == realName)
         {
            mViews[i] = static_cast<OpenFramesView *>(obj);
         }
      }
   }
   
   else if (type == GmatType::GetTypeId("OpenFramesVector") && obj->GetTypeName() == "OpenFramesVector")
   {
     for (Integer i = 0; i < mVectorNames.size(); i++)
     {
       if (mVectorNames[i] == realName)
       {
         mVectors[i] = static_cast<OpenFramesVector *>(obj);
       }
     }
   }
   
   else if (type == GmatType::GetTypeId("OpenFramesSensorMask") && obj->GetTypeName() == "OpenFramesSensorMask")
   {
     for (Integer i = 0; i < mMaskNames.size(); i++)
     {
       if (mMaskNames[i] == realName)
       {
         mMasks[i] = static_cast<OpenFramesSensorMask *>(obj);
       }
     }
   }

   else if (type == Gmat::SUBSCRIBER && obj->GetTypeName() == "OpenFramesInterface")
   {
      if (realName == mTimeSyncParentName)
      {
         mTimeSyncParentInterface = static_cast<OpenFramesInterface *>(obj);
      }
      return true;
   }

   return Subscriber::SetRefObject(obj, type, realName);
}


//------------------------------------------------------------------------------
// bool IsParameterCloaked(const Integer id) const
//------------------------------------------------------------------------------
bool OpenFramesInterface::IsParameterCloaked(const Integer id) const
{
   if (id == DRAW_MODEL_FILE)
   {
      bool allEmpty = true;
      for (auto spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
         allEmpty = allEmpty && ( spo->GetModelFile().size() == 0 );
      return allEmpty;
   }
   else
      return GmatBase::IsParameterCloaked(id);
}


//------------------------------------------------------------------------------
// bool IsParameterCloaked(const std::string &label) const
//------------------------------------------------------------------------------
bool OpenFramesInterface::IsParameterCloaked(const std::string &label) const
{
   return IsParameterCloaked(GetParameterID(label));
}


//------------------------------------------------------------------------------
// virtual void SetOrbitColorChanged(GmatBase *originator, const std::string &newColor, ...)
//------------------------------------------------------------------------------
/**
 * Sets object orbit color change.
 *
 * @param originator  The assignment command pointer who is setting
 * @param newColor  New color to be applied to the object
 * @param objName  Name of the object
 * @param desc  Description of property change
 * @param isSpacecraft Set to true if object is a Spcecraft
 */
//------------------------------------------------------------------------------
void OpenFramesInterface::SetOrbitColorChanged(GmatBase *originator,
                                               const std::string &newColor,
                                               const std::string &objName,
                                               const std::string &desc)
{
   UnsignedInt intColor = RgbColor::ToIntColor(newColor);
   for (std::vector<SpacePointOptions>::iterator spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
   {
      if (spo->IsNameSameAs(objName))
      {
         spo->SetCurrentOrbitColor(intColor);
         if (mExecutionWidget != nullptr)
            mExecutionWidget->GetScene().SetOrbitColor(spo->GetName(), intColor);
      }
   }
}


//------------------------------------------------------------------------------
// virtual void SetTargetColorChanged(GmatBase *originator, const std::string &newColor, ...)
//------------------------------------------------------------------------------
/**
 * Sets object target color change.
 *
 * @param originator  The assignment command pointer who is setting
 * @param newColor  New color to be applied to the object
 * @param objName  Name of the object
 * @param desc  Description of property change
 * @param isSpacecraft Set to true if object is a Spcecraft
 */
//------------------------------------------------------------------------------
void OpenFramesInterface::SetTargetColorChanged(GmatBase *originator,
                                                const std::string &newColor,
                                                const std::string &objName,
                                                const std::string &desc)
{
   UnsignedInt intColor = RgbColor::ToIntColor(newColor);
   for (std::vector<SpacePointOptions>::iterator spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
   {
      if (spo->IsNameSameAs(objName))
      {
         spo->SetCurrentTargetColor(intColor);
         if (mExecutionWidget != nullptr)
            mExecutionWidget->GetScene().SetTargetColor(spo->GetName(), intColor);
      }
   }
}


//------------------------------------------------------------------------------
// virtual void SetSegmentOrbitColor(GmatBase *originator, bool overrideColor,
//                           UnsignedInt orbitColor)
//------------------------------------------------------------------------------
/**
 * Sets propagation segment orbit color so that subscribers can handle appropriately.
 *
 * @param originator  The Propagate command pointer who is setting
 * @param overrideColor  The flag indicating whether or not to override orbit color
 * @param orbitColor  New orbit color to be applied to the space object
 */
//------------------------------------------------------------------------------
void OpenFramesInterface::SetSegmentOrbitColor(GmatBase *originator, bool overrideColor,
                                               UnsignedInt orbitColor, const StringArray &objNames)
{
   // If overrding color, set the same color for all objects
   if (overrideColor)
   {
      for (UnsignedInt i = 0; i < objNames.size(); i++)
      {
         if (mExecutionWidget != nullptr)
            mExecutionWidget->GetScene().SetSegmentColor(objNames[i], false, orbitColor);
      }
   }
   else
   {
      for (UnsignedInt i = 0; i < objNames.size(); i++)
      {
         if (mExecutionWidget != nullptr)
            mExecutionWidget->GetScene().SetSegmentColor(objNames[i], true, orbitColor);
      }
   }
}


//---------------------------------------------------------------------------
// UnsignedInt GetPropertyObjectType(const Integer id) const
//---------------------------------------------------------------------------
/**
 * Gets the object type accepted by a parameter
 *
 * @param <id> The parameter
 *
 * @return Type accepted
 */
UnsignedInt OpenFramesInterface::GetPropertyObjectType(const Integer id) const
{
   if (id == ADD)
      return Gmat::SPACE_POINT;

   if (id == COORD_SYSTEM)
      return Gmat::COORDINATE_SYSTEM;

   if (id == VIEW)
      return GmatType::GetTypeId("OpenFramesView");

   if (id == VECTOR)
     return GmatType::GetTypeId("OpenFramesVector");
   
   if (id == SENSOR_MASK)
     return GmatType::GetTypeId("OpenFramesSensorMask");

   if (id == TIME_SYNC_PARENT)
      return Gmat::SUBSCRIBER;

   return Subscriber::GetPropertyObjectType(id);
}


//---------------------------------------------------------------------------
// const StringArray& GetPropertyEnumStrings(const Integer id) const
//---------------------------------------------------------------------------
/**
 * Gets possible values of an enumerated parameter as strings
 *
 * @param <id> The enumerated parameter
 *
 * @return Possible values
 */
const StringArray& OpenFramesInterface::GetPropertyEnumStrings(const Integer id) const
{
   if (id == SOLVER_ITERATIONS)
      return gOFIterOptionStrings;

   return GmatBase::GetPropertyEnumStrings(id);
}


//---------------------------------------------------------------------------
// const StringArray& GetPropertyEnumStrings(const std::string &label) const
//---------------------------------------------------------------------------
/**
 * Gets possible values of an enumerated parameter as strings
 *
 * @param <label> The enumerated parameter
 *
 * @return Possible values
 */
const StringArray& OpenFramesInterface::GetPropertyEnumStrings(const std::string &label) const
{
   return GetPropertyEnumStrings(GetParameterID(label));
}


//------------------------------------------------------------------------------
// std::string OpenFramesInterface::StringArrayToString(const StringArray &array)
//------------------------------------------------------------------------------
/**
 * Converts the given array of strings into the { string1, string2, ... } format
 *
 * @param array The strings to convert
 * @return The formatted output string
 */
//---------------------------------------------------------------
std::string OpenFramesInterface::StringArrayToString(const StringArray &array)
{
   Integer objCount = array.size();
   std::string objList = "{ ";
   for (Integer i = 0; i < objCount; i++)
   {
      if (i == objCount - 1)
         objList += array[i];
      else
         objList += array[i] + ", ";
   }
   objList += " }";
   return objList;
};


//------------------------------------------------------------------------------
// std::string OpenFramesInterface::StringArrayToString(const StringArray &array)
//------------------------------------------------------------------------------
/**
 * Converts a boolean to "On" or "Off" string
 *
 * @param onIfTrue The boolean to convert
 * @return "On" if input is true, "Off" otherwise
 */
//---------------------------------------------------------------
std::string OpenFramesInterface::BooleanToOnOff(bool onIfTrue)
{
   return (onIfTrue) ? ON_STRING : OFF_STRING;
};


//------------------------------------------------------------------------------
// virtual bool Initialize()
/**
 * Initialize an instance of the interface, or reinitailize an instance
 *
 * @return true for all logic paths
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::Initialize()
{
   Subscriber::Initialize();

   bool foundSc = false;
   Integer nullCounter = 0;
   mProviderCount.clear();
   mProviderName.clear();

   // Check for internal coordinate system
   if (theInternalCoordSystem == nullptr)
   {
      MessageInterface::ShowMessage("*** WARNING *** The %s named \"%s\" will be turned off. "
                                    "It has a NULL internal coordinate system pointer.\n", GetTypeName().c_str(),
                                    GetName().c_str());
   }

   // if active and not initialized already, then initialize
   if (active)
   {
      if (!isInitialized)
      {
         isInitialized = true;

         if (mExecutionWidget == nullptr)
         {
            // This should mean that Gmat is running in console
            active = false;
         }
         else
         {
            // Allow window to be displayed and start OpenFrames before resizing occurs
            mExecutionWidget->ShowToolbar(mShowToolbar);
#ifdef OFI_USE_WXWIDGETS
            wxYield(); // Required for GTK because mCanvas provides an onscreen context that must be visible to draw
#endif

            Integer numOldTrajectories;
            if (mOFSolverIterOption == OF_SI_CURRENT)
               numOldTrajectories = 1;
            else
               numOldTrajectories = mSolverIterLastN * TRAJ_PER_SOLVER_ITER;

            // Initialize the scene

            // WHAT TO DO: change the Intialize constructer to take another object?
            mExecutionWidget->GetScene().Initialize(mGLOptions, mFrameOptions, GetStarOptions(), mTimeSyncParentInterface,
                                                    mSpacePoints, GetName(), mViews, mVectors, mMasks, mViewCoordSystem,
                                                    numOldTrajectories);
            // Notify an unregistered sync children that we have been created
            for (auto child = mUnregisteredTimeSyncChildren.begin(); child < mUnregisteredTimeSyncChildren.end(); child++)
               mExecutionWidget->GetScene().AddTimeSyncChild(*child);
            mUnregisteredTimeSyncChildren.clear();
         }
      }
   }
   else
   {
      // Hide the window if not active (ShowPlot = False)
      if (mExecutionWidget != nullptr)
      {
#ifdef OFI_USE_WXWIDGETS
         static_cast<wxMDIChildFrame *>(mExecutionWidget->GetParent()->GetParent())->Close();
#endif
      }
   }

   return true;
}


//------------------------------------------------------------------------------
// bool AnyDataLabelsMatch(const std::string &name)
//------------------------------------------------------------------------------
/**
 * Check if any of the data labels (X, Y, Z, Vx, Vy, or Vz) matches the name
 *
 * @param <name> Spacecraft name to check data labels for
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::AnyDataLabelsMatch(const std::string &name)
{
   // New Publisher code doesn't assign currentProvider anymore,
   // it just copies current labels. There was an issue with
   // provider id keep incrementing if data is regisgered and
   // published inside a GmatFunction
   Integer idX, idY, idZ;
   Integer idVx, idVy, idVz;
   StringArray &dataLabel = theDataLabels[0];

   idX = FindIndexOfElement(dataLabel, name + ".X");
   idY = FindIndexOfElement(dataLabel, name + ".Y");
   idZ = FindIndexOfElement(dataLabel, name + ".Z");

   idVx = FindIndexOfElement(dataLabel, name + ".Vx");
   idVy = FindIndexOfElement(dataLabel, name + ".Vy");
   idVz = FindIndexOfElement(dataLabel, name + ".Vz");

   return (idX  != -1 || idY  != -1 || idZ  != -1 ||
           idVx != -1 || idVy != -1 || idVz != -1);
}


//------------------------------------------------------------------------------
// bool AllDataLabelsMatch(const std::string &name)
//------------------------------------------------------------------------------
/**
 * Check if all of the data labels (X, Y, Z, Vx, Vy, and Vz) matches the name
 *
 * @param <name> Spacecraft name to check data labels for
 * @param <idX> index of X label
 * @param <idY> index of X label
 * @param <idZ> index of X label
 * @param <idVx> index of Vx label
 * @param <idVy> index of Vy label
 * @param <idVz> index of Vz label
 *
 * @return true if all data labels were found and idX through idVz are false
 * @return false if some data labels were not found and their id is -1
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::AllDataLabelsMatch(const std::string &name,
   Integer &idX, Integer &idY, Integer &idZ, Integer &idVx, Integer &idVy,
   Integer &idVz)
{
   // New Publisher code doesn't assign currentProvider anymore,
   // it just copies current labels. There was an issue with
   // provider id keep incrementing if data is regisgered and
   // published inside a GmatFunction
   StringArray &dataLabel = theDataLabels[0];

   idX = FindIndexOfElement(dataLabel, name + ".X");
   idY = FindIndexOfElement(dataLabel, name + ".Y");
   idZ = FindIndexOfElement(dataLabel, name + ".Z");

   idVx = FindIndexOfElement(dataLabel, name + ".Vx");
   idVy = FindIndexOfElement(dataLabel, name + ".Vy");
   idVz = FindIndexOfElement(dataLabel, name + ".Vz");

   return (idX  != -1 && idY  != -1 && idZ  != -1 &&
           idVx != -1 && idVy != -1 && idVz != -1);
}


//------------------------------------------------------------------------------
// void AddTimeSyncChild(OpenFrames::WindowProxy *child)
//------------------------------------------------------------------------------
/**
 * Used to set this objects WindowProxy as the time synchronization parent for
 * the given child
 *
 * @param child The child to assign ourself as a parent to
 *
 * Because OpenFramesInterface's are initialized in an alternating manner, the
 * parent WindowProxy for time synchronization may not be instantiated yet. In
 * this case, the child WindowProxy is saved until a WindowProxy is
 * instantiated.
 */
void OpenFramesInterface::AddTimeSyncChild(OpenFrames::WindowProxy *child)
{
   if (mExecutionWidget == nullptr)
      mUnregisteredTimeSyncChildren.push_back(child);
   else
      mExecutionWidget->GetScene().AddTimeSyncChild(child);
}


//------------------------------------------------------------------------------
// bool Distribute(const Real * dat, Integer len)
//------------------------------------------------------------------------------
/**
 * Process trajectory data distributed by the GMAT Publisher
 *
 * @param <dat> An array of data to receive
 * @param <len> The number of data in the array
 */
//------------------------------------------------------------------------------
bool OpenFramesInterface::Distribute(const Real *dat, Integer len)
{
   if (GmatGlobal::Instance()->GetRunMode() == GmatGlobal::TESTING_NO_PLOTS)
   {
      return true;
   }

   //------------------------------------------------------------
   // Just return immediately if inactive or no objects to plot
   //------------------------------------------------------------
   if (!active || mExecutionWidget == nullptr || mSpacePoints.size() <= 0 || theInternalCoordSystem == nullptr)
   {
      return true;
   }

   //------------------------------------------------------------
   // Check for the end of the run, and finish things up appropriately
   //------------------------------------------------------------
   if (isEndOfRun)
   {
      // Bring celestial body trajectories up to date in the middle and on each end
#ifdef OFI_USE_WXWIDGETS
      mExecutionWidget->GetScene().GetCanvas().SetFocus();
#endif
      return true;
   }

   //------------------------------------------------------------
   // If this is the end of a trajectory segment
   //------------------------------------------------------------
   if (isEndOfReceive)
   {
      for (std::vector<SpacePointOptions>::iterator spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
      {
         if (AnyDataLabelsMatch(spo->GetName()))
         {
            if (runstate == Gmat::SOLVING)
            {
               // End this solver trajectory, and show/erase trajectories based on mOFSolverIterOption
               TrajectoryDealer::Instance().FinalizeSolverTrajectory(spo->GetName(), mViewCoordSystem->GetName());
            }
            else if (runstate == Gmat::SOLVEDPASS)
            {
               if (mOFSolverIterOption != OF_SI_NONE)
               {
                  // This function is called twice, remove half each time
                  Integer numToRemove;
                  mSolverPassTracker = !mSolverPassTracker;
                  if (mSolverPassTracker)
                     numToRemove = floor(TRAJ_PER_SOLVER_ITER/2.0);
                  else
                     numToRemove = ceil(TRAJ_PER_SOLVER_ITER/2.0);
                  TrajectoryDealer::Instance().RemoveLastSolverTrajectory(spo->GetName(), mViewCoordSystem->GetName(),
                                                                          numToRemove);
               }
               TrajectoryDealer::Instance().FinalizeOrbitTrajectory(spo->GetName(), mViewCoordSystem->GetName());
               mExecutionWidget->GetScene().UpdateSegmentViews(spo->GetName(), mProviderName);
            }
            else
            {
               // This segment is finished
               TrajectoryDealer::Instance().FinalizeOrbitTrajectory(spo->GetName(), mViewCoordSystem->GetName());
               mExecutionWidget->GetScene().UpdateSegmentViews(spo->GetName(), mProviderName);
            }
         }
      }

      // [OFI-17] GMAT Publisher::FlushBuffers() called at end of for/while loop, which can cause crash
      // if that loop occurs before a publisher provider is set.
      if(currentProvider != nullptr)
      {
         // Increment counters for naming anonymous providers
         if(mProviderCount.find(currentProvider->GetTypeName()) == mProviderCount.end())
            mProviderCount[currentProvider->GetTypeName()] = 1;
         else
            mProviderCount.at(currentProvider->GetTypeName()) = mProviderCount.at(currentProvider->GetTypeName()) + 1;
      }

      // Reset provider name for next segment
      mProviderName.clear();
   }

   //------------------------------------------------------------
   // If currently solving and solver iterations is none
   //------------------------------------------------------------
   if ((mOFSolverIterOption == OF_SI_NONE) && (runstate == Gmat::SOLVING))
   {
      return true;
   }

   //------------------------------------------------------------
   // publish the orbit data to the OpenFrames translator
   //------------------------------------------------------------
   if (len >= 7)
   {
      Real timeInDays = dat[0];
      bool isSolving = (runstate == Gmat::SOLVING);

      for (auto spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
      {
         if (spo->GetSpacePoint()->IsOfType(Gmat::SPACECRAFT))
         {
            Spacecraft *sc = static_cast<Spacecraft *>(spo->GetSpacePoint());
            Integer idX, idY, idZ, idVx, idVy, idVz;

            // Check for data labels that match this spacepoint
            // Check for central bodies that do not need trajectory segments
            if (AllDataLabelsMatch(spo->GetName(), idX, idY, idZ, idVx, idVy, idVz) &&
                !(mViewCoordSystem->GetOriginName() == spo->GetName() &&
                  mViewCoordSystem->AreAxesOfType("BodyFixedAxes")))
            {
               // If we convert after current epoch, it will not give correct
               // results, if origin is spacecraft,
               // ie, sat->GetAttitude(epoch) will not give correct results.
               Rvector6 inState, outState;
               inState.Set(dat[idX], dat[idY], dat[idZ],
                           dat[idVx], dat[idVy], dat[idVz]);
               Rvector quat;

               // convert position and velocity and attitude to view frame
               // OrbitView compares pointers, but the pointers will never be equal
               if (theDataCoordSystem != mViewCoordSystem)
               {
                  // Convert() checks for CoordinateSystems with the same name,
                  // do the same to avoid attitude xform
                  mCoordConverter.Convert(timeInDays, inState, theDataCoordSystem,
                                          outState, mViewCoordSystem);
                  if (sc->HasAttitude())
                  {
                     Rmatrix33 matIB = sc->GetAttitude(timeInDays);
                     Rmatrix33 matIP = mCoordConverter.GetLastRotationMatrix();
                     Rmatrix33 matBP = matIB * matIP.Transpose();
                     quat = AttitudeConversionUtility::ToQuaternion(matBP);
                  }
                  else
                      quat = Rvector(4, 0.0, 0.0, 0.0, 1.0);
               }
               else
               {
                  outState = inState;
                  if (sc->HasAttitude())
                  {
                     Rmatrix33 matIB = sc->GetAttitude(timeInDays);
                     quat = AttitudeConversionUtility::ToQuaternion(matIB);
                  }
                  else
                      quat = Rvector(4, 0.0, 0.0, 0.0, 1.0);
               }

               // Add the state to this spacecraft's trajectory segment
               if (mProviderName.size() == 0)
               {
                  // First determine name of the provider, or label anonymous provider
                  if (currentProvider->IsOfType("GmatCommand"))
                  {
                     mProviderName = static_cast<GmatCommand*>(currentProvider)->GetSummaryName();
                  }
                  else
                  {
                     mProviderName = currentProvider->GetName();
                     if (mProviderName.size() == 0)
                     {
                        if (mProviderCount.find(currentProvider->GetTypeName()) == mProviderCount.end())
                           mProviderCount[currentProvider->GetTypeName()] = 1;
                        mProviderName = currentProvider->GetTypeName() + std::to_string(mProviderCount[currentProvider->GetTypeName()]);
                     }
                  }
               }
               // Finally add the state
               TrajectoryDealer::Instance().AddState(spo->GetName(), mViewCoordSystem->GetName(), mProviderName,
                  currentProvider, timeInDays, outState.GetDataVector(), quat.GetDataVector(), isSolving, false);
               mExecutionWidget->GetScene().UpdateTimeLimits(timeInDays, isSolving);
            }
            // else Did not find all data IDs necessary to queue trajectory point
         }
      }

      // Buffer celestial body information if necessary
      AddPointToCelestialBodyTrajectories(mProviderName, timeInDays, isSolving);
   }

   return true;
}


//------------------------------------------------------------------------------
// void AddPointToCelestialBodyTrajectories(Real time)
//------------------------------------------------------------------------------
/**
 * Adds a point to the trajectories of celestial bodies
 *
 * @param time The time to add the point at (days)
 */
//---------------------------------------------------------------
void OpenFramesInterface::AddPointToCelestialBodyTrajectories(const std::string &segmentName, Real timeInDays, bool isSolving)
{
   // Update Celestial Bodies
   for (std::vector<SpacePointOptions>::iterator spo = mSpacePoints.begin(); spo < mSpacePoints.end(); spo++)
   {
      if (spo->GetSpacePoint() != nullptr &&
          !spo->GetSpacePoint()->IsOfType(Gmat::SPACECRAFT) &&
          !(mViewCoordSystem->GetOriginName() == spo->GetName() &&
            mViewCoordSystem->AreAxesOfType("BodyFixedAxes")))
      {
         CelestialBody *cb = static_cast<CelestialBody *>(spo->GetSpacePoint());
         Rvector6 objMjEqState, outState;
         Rvector quat(4);
         objMjEqState = cb->GetMJ2000State(timeInDays);

         // Convert to view coordinate system since planets are in MJ2000Eq
         mCoordConverter.Convert(timeInDays, objMjEqState, theInternalCoordSystem, outState,
                                 mViewCoordSystem);
         
         // Ground stations should be oriented in a topocentric system
         if (spo->GetSpacePoint()->IsOfType(Gmat::GROUND_STATION))
         {
            quat = CalculateENUAttitudeForBodyFixedPoint(timeInDays,
                   static_cast<BodyFixedPoint *>(spo->GetSpacePoint()));
         }
         else // Celestial body attitude can be directly retrieved
         {
            // Convert attitude
            Rmatrix33 matIB = cb->GetAttitude(timeInDays); // Internal to Body-Fixed coords
            Rmatrix33 matIP = mCoordConverter.GetLastRotationMatrix(); // View to Internal coords
            Rmatrix33 matBP = matIB.Transpose() * matIP.Transpose(); // Body-fixed to View coords
            quat = AttitudeConversionUtility::ToQuaternion(matBP);
         }
         
         TrajectoryDealer::Instance().AddState(spo->GetName(), mViewCoordSystem->GetName(),
                                               segmentName, currentProvider, timeInDays, outState.GetDataVector(),
                                               quat.GetDataVector(), isSolving, true);
      }
   }

   // Update Ecliptic Plane
   if (mFrameOptions.GetEnableEclipticPlane() && (mViewCoordSystem->GetName() != "EarthMJ2000Ec"))
   {
      if (mMJ2000EcCoordSystem == nullptr)
      {
         std::string originName = SolarSystem::EARTH_NAME;
         std::string axesType = "MJ2000Ec";
         std::string csName = originName + axesType;
         SpacePoint *origin = static_cast<SpacePoint *>(theSolarSystem->GetBody(originName.c_str()));
         SpacePoint *j2000Body = static_cast<SpacePoint *>(theSolarSystem->GetBody(SolarSystem::EARTH_NAME));

         // Create coordinate system with Earth origin and MJ2000Ec axis
         mMJ2000EcCoordSystem = CoordinateSystem::CreateLocalCoordinateSystem(
               csName, axesType, origin, nullptr, nullptr, j2000Body,
               theSolarSystem);
      }
      if (mMJ2000EcCoordSystem != nullptr)
      {
         // Convert position
         Rvector6 origin, outState; // initialized to 0.0 in constructor
         mCoordConverter.Convert(timeInDays, origin, mMJ2000EcCoordSystem, outState,
                                 mViewCoordSystem);
         // Convert attitude
         Rmatrix33 matIP = mCoordConverter.GetLastRotationMatrix();
         Rvector quat = AttitudeConversionUtility::ToQuaternion(matIP.Transpose());

         TrajectoryDealer::Instance().AddState(OFEclipticPlane::NAME, mViewCoordSystem->GetName(),
                                               segmentName, currentProvider, timeInDays, outState.GetDataVector(),
                                               quat.GetDataVector(), isSolving, true);
      }
   }

   // Update view frame location in EarthMJ2000Eq
   if (mCustomStarOptions.GetEnableStars() && (mViewCoordSystem->GetName() != "EarthMJ2000Eq"))
   {
      if (mMJ2000EqCoordSystem == nullptr)
      {
         std::string originName = SolarSystem::EARTH_NAME;
         std::string axesType = "MJ2000Eq";
         std::string csName = originName + axesType;
         SpacePoint *origin = static_cast<SpacePoint *>(theSolarSystem->GetBody(originName.c_str()));
         SpacePoint *j2000Body = static_cast<SpacePoint *>(theSolarSystem->GetBody(SolarSystem::EARTH_NAME));

         // Create coordinate system with Earth origin and MJ2000Ec axis
         mMJ2000EqCoordSystem = CoordinateSystem::CreateLocalCoordinateSystem(
               csName, axesType, origin, nullptr, nullptr, j2000Body,
               theSolarSystem);
      }
      if (mMJ2000EqCoordSystem != nullptr)
      {
         // Convert position
         Rvector6 origin, outState; // initialized to 0.0 in constructor
         mCoordConverter.Convert(timeInDays, origin, mMJ2000EqCoordSystem, outState, mViewCoordSystem);
         // Convert attitude
         Rmatrix33 matIP = mCoordConverter.GetLastRotationMatrix();
         Rvector quat = AttitudeConversionUtility::ToQuaternion(matIP.Transpose());

         TrajectoryDealer::Instance().AddState(OFStarListener::NAME, mViewCoordSystem->GetName(), segmentName,
                                               currentProvider, timeInDays, outState.GetDataVector(),
                                               quat.GetDataVector(), isSolving, true);
      }
   }
}

//------------------------------------------------------------------------------
// void CalculateENUAttitudeForBodyFixedPoint(Real time, BodyFixedPoint *bfp)
//------------------------------------------------------------------------------
/**
 * Computes an east-north-up attitude for a ground station
 *
 * @param time The time to add the point at (days)
 * @param bfp The body-fixed point (ground station) for which to compute attitude
 */
//---------------------------------------------------------------
const Rvector OpenFramesInterface::CalculateENUAttitudeForBodyFixedPoint(Real timeInDays, BodyFixedPoint *bfp)
{
   Rvector3 east, north, up;
   Rvector3 z(0.0, 0.0, 1.0); // Global z used to compute north/east
   Rvector3 location = bfp->GetBodyFixedLocation(A1Mjd()); // the parameter is ignored

   // Local Up (assumes body-fixed location not at origin)
   up = location;
   up.Normalize();
   
   // Local East
   Real dotProduct = z * up;
   if(std::abs(std::abs(dotProduct) - 1.0) < 1.0e-13) // 3m from Earth's North or South pole
   {
      east.Set(1.0, 0.0, 0.0);
   }
   else
   {
   	east = Cross(z, up);
      east.Normalize();
   }
   
   // Local North
   north = Cross(up, east);
   north.Normalize();
   
   // Assemble ENU to World transformation
   Rmatrix33 matENU_to_World(east[0], east[1], east[2],
                             north[0], north[1], north[2],
                             up[0], up[1], up[2]);

   // Get View to World transformation
   Rvector6 inState, outState;
   inState[0] = location[0];
   inState[1] = location[1];
   inState[2] = location[2];
   CoordinateSystem *bfpCoordSys = bfp->GetBodyFixedCoordinateSystem();
   mCoordConverter.Convert(timeInDays, inState, bfpCoordSys, outState, mViewCoordSystem);
   Rmatrix33 matView_to_World = mCoordConverter.GetLastRotationMatrix();
   
   // Compute ENU to View transformation
   Rmatrix33 matBP = matENU_to_World * matView_to_World.Transpose();
   return AttitudeConversionUtility::ToQuaternion(matBP);
}
