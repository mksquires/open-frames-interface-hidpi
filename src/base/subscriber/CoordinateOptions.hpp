//$Id$
//------------------------------------------------------------------------------
//                                  CoordinateOptions
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 24, 2017
/**
 *  @class CoordinateOptions
 *  Conveniently contains options related to the reference frame and time
 */
//------------------------------------------------------------------------------

#ifndef COORDINATEOPTIONS_H
#define COORDINATEOPTIONS_H

#include "OpenFramesInterface_defs.hpp"

#include <string>


class OpenFramesInterface_API CoordinateOptions
{
public:
   CoordinateOptions();
   virtual ~CoordinateOptions();

   CoordinateOptions(const CoordinateOptions &source);
   CoordinateOptions& operator=(const CoordinateOptions &rhs);

   std::string GetCoordSysName() const { return mCoordSysName; } ///< get @ref mCoordSysName
   void SetCoordSysName(std::string value) { mCoordSysName = value; } ///< set @ref mCoordSysName
   bool GetEnableAxes() const { return mEnableAxes; } ///< get @ref mEnableAxes
   void SetEnableAxes(bool value) { mEnableAxes = value; } ///< set @ref mEnableAxes
   bool GetEnableAxesLabels() const { return mEnableAxesLabels; } ///< get @ref mEnableAxesLabels
   void SetEnableAxesLabels(bool value) { mEnableAxesLabels = value; } ///< set @ref mEnableAxesLabels
   Real GetAxesLength() const { return mAxesLength; } ///< get @ref mAxesLength
   void SetAxesLength(Real value) { mAxesLength = value; } ///< set @ref mAxesLength
   bool GetEnableFrameLabel() const { return mEnableFrameLabel; } ///< get @ref mEnableFrameLabel
   void SetEnableFrameLabel(bool value) { mEnableFrameLabel = value; } ///< set @ref mEnableFrameLabel
   bool GetEnableXYPlane() const { return mEnableXYPlane; } ///< get @ref mEnableXYPlane
   void SetEnableXYPlane(bool value) { mEnableXYPlane = value; } ///< set @ref mEnableXYPlane
   bool GetEnableEclipticPlane() const { return mEnableEclipticPlane; } ///< get @ref mEnableEclipticPlane
   void SetEnableEclipticPlane(bool value) { mEnableEclipticPlane = value; } ///< set @ref mEnableEclipticPlane
   Real GetPlaybackTimeScale() const { return mPlaybackTimeScale; } ///< get @ref mPlaybackTimeScale
   void SetPlaybackTimeScale(Real value) { mPlaybackTimeScale = value; } ///< set @ref mPlaybackTimeScale

   bool EqualTo(const CoordinateOptions &options) const;

private:
   /// View coordinate system name
   std::string mCoordSysName;
   /// Enable axes
   bool mEnableAxes;
   /// Enable labes on axes
   bool mEnableAxesLabels;
   /// Length of axes
   Real mAxesLength;
   /// Enable label on reference frame
   bool mEnableFrameLabel;
   /// Enable X-Y plane
   bool mEnableXYPlane;
   /// Enable ecliptic plane
   bool mEnableEclipticPlane;
   /// Initial value of the playback time scale
   Real mPlaybackTimeScale;
};

#endif
