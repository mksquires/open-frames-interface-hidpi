//$Id$
//------------------------------------------------------------------------------
//                                  SpacePointOptions
//------------------------------------------------------------------------------
// OpenFramesInterface Plugin for GMAT (General Mission Analysis Tool)
//
// Copyright (c) 2022 Emergent Space Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Developed by Emergent Space Technologies, Inc. under contract number
// NNX16CG16C
//
// Author: Matthew Ruschmann, Emergent Space Technologies, Inc.
// Created: August 24, 2017
/**
 *  @class SpacePointOptions
 *  Conveniently wraps a Spacepoint with associated options.
 */
//------------------------------------------------------------------------------

#ifndef SPACEPOINTOPTIONS_H
#define SPACEPOINTOPTIONS_H

#include "OpenFramesInterface_defs.hpp"

#include "ColorTypes.hpp"

#include <string>

class SpacePoint;


class OpenFramesInterface_API SpacePointOptions
{
public:
   SpacePointOptions(const std::string &name);
   virtual ~SpacePointOptions();

   SpacePointOptions(const SpacePointOptions &source);
   SpacePointOptions& operator=(const SpacePointOptions &rhs);

   const std::string &GetName() const { return mName; } ///< get @ref mName
   void SetName(const std::string &name) { mName = name; } ///< get @ref mName
   SpacePoint *GetSpacePoint() { return const_cast<SpacePoint *>(GetSpacePointConst()); } ///< get @ref mSpacepoint
   const SpacePoint *GetSpacePointConst() const { return mSpacePoint; } ///< get @ref mSpacepoint
   void Hide(); ///< Configures the object to be hidden
   void SetSpacePoint(SpacePoint *value); ///< set @ref mSpacepoint
   const std::string &GetModelFile() const { return mModelFile; } ///< get @ref mModelFile
   void SetModelFile(const std::string &value) { mModelFile = value; } ///< set @ref mModelFile
   bool GetDrawObject() const { return mDrawObject; } ///< get @ref mDrawObject
   void SetDrawObject(bool value) { mDrawObject = value; } ///< set @ref mDrawObject
   bool GetDrawTrajectory() const { return mDrawTrajectory; } ///< get @ref mDrawTrajectory
   void SetDrawTrajectory(bool value) { mDrawTrajectory = value; } ///< set @ref mDrawTrajectory
   bool GetDrawAxes() const { return mDrawAxes; } ///< get @ref mDrawAxes
   void SetDrawAxes(bool value) { mDrawAxes = value; } ///< set @ref mDrawAxes
   bool GetDrawXYPlane() const { return mDrawXYPlane; } ///< get @ref mDrawXYPlane
   void SetDrawXYPlane(bool value) { mDrawXYPlane = value; } ///< set @ref mDrawXYPlane
   bool GetDrawLabel() const { return mDrawLabel; } ///< get @ref mDrawLabel
   void SetDrawLabel(bool value) { mDrawLabel = value; } ///< set @ref mDrawLabel
   bool GetUsePropagateLabel() const { return mDrawLabelPropagate; } ///< get @ref mDrawLabel
   void SetUsePropagateLabel(bool value) { mDrawLabelPropagate = value; } ///< set @ref mDrawLabel
   bool GetDrawCenter() const { return mDrawCenter; } ///< get @ref mDrawCenter
   void SetDrawCenter(bool value) { mDrawCenter = value; } ///< set @ref mDrawCenter
   bool GetDrawEnds() const { return mDrawEnds; } ///< get @ref mDrawEnds
   void SetDrawEnds(bool value) { mDrawEnds = value; } ///< set @ref mDrawEnds
   bool GetDrawVelocity() const { return mDrawVelocity; } ///< get @ref mDrawVelocity
   void SetDrawVelocity(bool value) { mDrawVelocity = value; } ///< set @ref mDrawVelocity
   bool GetDrawGrid() const { return mDrawGrid; } ///< get @ref mDrawGrid
   void SetDrawGrid(bool value) { mDrawGrid = value; } ///< set @ref mDrawGrid
   Real GetDrawLineWidth() const { return mDrawLineWidth; } ///< get @ref mDrawLineWidth
   void SetDrawLineWidth(const Real &value) { mDrawLineWidth = value; } ///< set @ref mDrawLineWidth
   UnsignedInt GetDrawMarkerSize() const { return mDrawMarkerSize; } ///< get @ref mDrawMarkerSize
   void SetDrawMarkerSize(UnsignedInt value) { mDrawMarkerSize = value; } ///< set @ref mDrawMarkerSize
   std::string GetDrawFontPosition() const { return mDrawFontPosition; } ///< get @ref mDrawFontPosition
   void SetDrawFontPosition(std::string value) { mDrawFontPosition = value; } ///< set @ref mDrawFontPosition
   UnsignedInt GetDrawFontSize() const { return mDrawFontSize; } ///< get @ref mDrawMarkerSize
   void SetDrawFontSize(UnsignedInt value) { mDrawFontSize = value; } ///< set @ref mDrawMarkerSize
   UnsignedInt GetCurrentOrbitColor() const { return mCurrentOrbitColor; } ///< get @ref mCurrentOrbitColor
   void SetCurrentOrbitColor(UnsignedInt value) { mCurrentOrbitColor = value; } ///< set @ref mCurrentOrbitColor
   UnsignedInt GetCurrentTargetColor() const { return mCurrentTargetColor; } ///< get @ref mCurrentTargetColor
   void SetCurrentTargetColor(UnsignedInt value) { mCurrentTargetColor = value; } ///< set @ref mCurrentTargetColor

   /**
    * Compare this instance's name to a value
    * @param name The name to compare
    * @return True if the name matches
    */
   bool IsNameSameAs(const std::string &name) const { return (mName == name); }
   /**
    * Compare this instance's SpacePoint to another Pointer
    * @param spacePoint The SpacePoint to compare
    * @return True if the pointers match
    */
   bool IsSpacePointSameAs(SpacePoint *spacePoint) { return (mSpacePoint == spacePoint); }

   void ColorsFromMySpacePoint();

private:
   /// The unique name of this space point
   std::string mName;
   /// The associated spacepoint
   SpacePoint *mSpacePoint;
   /// The filename of the model file to override object's setting
   std::string mModelFile;
   /// Option to draw the associated object
   bool mDrawObject;
   /// Option to draw the associated trajectory
   bool mDrawTrajectory;
   /// Option to draw the associated axes
   bool mDrawAxes;
   /// Option to draw the associated XY plane
   bool mDrawXYPlane;
   /// Option to draw the associated label
   bool mDrawLabel;
   /// Use the propagate label instead of the spacecraft name
   bool mDrawLabelPropagate;
   /// Option to draw the associated center point
   bool mDrawCenter;
   /// Option to draw the associated end points
   bool mDrawEnds;
   /// Option to draw velocity vectors along the trajectory
   bool mDrawVelocity;
   /// Option to draw velocity vectors along the trajectory
   bool mDrawGrid;
   /// Size of the trajectory to draw (pixels)
   Real mDrawLineWidth;
   /// Size of the marker to draw (pixels)
   UnsignedInt mDrawMarkerSize;
   /// Position of the marker
   std::string mDrawFontPosition;
   /// Font of the marker to draw
   UnsignedInt mDrawFontSize;
   /// Current color for the trajectory
   UnsignedInt mCurrentOrbitColor;
   /// Current color for the target
   UnsignedInt mCurrentTargetColor;
};

#endif
